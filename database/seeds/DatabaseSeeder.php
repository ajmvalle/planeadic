<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(TownsSeed::class);
        $this->call(CompetencesSeeder::class);
        $this->call(ProgramSeeder::class);
        $this->call(SubsystemSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CostSeeder::class);
        $this->call(MethodsSeeder::class);

    }
}
