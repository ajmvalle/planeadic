/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (1, 'Aguascalientes', 'Ags.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (2, 'Baja California', 'BC');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (3, 'Baja California Sur', 'BCS');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (4, 'Campeche', 'Camp.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (
    5, 'Coahuila de Zaragoza', 'Coah.'
  );
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (6, 'Colima', 'Col.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (7, 'Chiapas', 'Chis.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (8, 'Chihuahua', 'Chih.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (9, 'Ciudad de México', 'CDMX');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (10, 'Durango', 'Dgo.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (11, 'Guanajuato', 'Gto.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (12, 'Guerrero', 'Gro.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (13, 'Hidalgo', 'Hgo.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (14, 'Jalisco', 'Jal.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (15, 'México', 'Mex.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (
    16, 'Michoacán de Ocampo', 'Mich.'
  );
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (17, 'Morelos', 'Mor.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (18, 'Nayarit', 'Nay.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (19, 'Nuevo León', 'NL');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (20, 'Oaxaca', 'Oax.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (21, 'Puebla', 'Pue.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (22, 'Querétaro', 'Qro.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (23, 'Quintana Roo', 'Q. Roo');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (24, 'San Luis Potosí', 'SLP');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (25, 'Sinaloa', 'Sin.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (26, 'Sonora', 'Son.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (27, 'Tabasco', 'Tab.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (28, 'Tamaulipas', 'Tamps.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (29, 'Tlaxcala', 'Tlax.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (
    30, 'Veracruz de Ignacio de la Llave',
    'Ver.'
  );
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (31, 'Yucatán', 'Yuc.');
/* INSERT QUERY */
INSERT INTO states(id, name, shortname)
VALUES
  (32, 'Zacatecas', 'Zac.');



