INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (1,'G1','Se autodetermina y cuida de sí','Se conoce y valora a sí mismo y aborda problemas y retos teniendo en cuenta los objetivos que persigue','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (2,'G2','Se autodetermina y cuida de sí','Es sensible al arte y participa en la apreciación e interpretación de sus expresiones en distintos géneros','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (3,'G3','Se autodetermina y cuida de sí','Elige y practica estilos de vida saludables','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (4,'G4','Se expresa y comunica ','Escucha, interpreta y emite mensajes pertinentes en distintos contextos mediante la utilización de medios, códigos y herramientas apropiados','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (5,'G5','Piensa crítica y reflexivamente','Desarrolla innovaciones y propone soluciones a problemas a partir de métodos establecidos','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (6,'G6','Piensa crítica y reflexivamente','Sustenta una postura personal sobre temas de interés y relevancia general, considerando otros puntos de vista de manera crítica y reflexiva','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (7,'G7','Aprende de forma autónoma','Aprende por iniciativa e interés propio a lo largo de la vida','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (8,'G8','Trabaja en forma colaborativa','Participa y colabora de manera efectiva en equipos diversos','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (9,'G9','Participa con responsabilidad en la sociedad','Participa con una conciencia cívica y ética en la vida de su comunidad, región, México y el mundo','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (10,'G10','Participa con responsabilidad en la sociedad','Mantiene una actitud respetuosa hacia la interculturalidad y la diversidad de creencias, valores, ideas y prácticas sociales. ','Acuerdo 444','G');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (11,'G11','Participa con responsabilidad en la sociedad','Contribuye al desarrollo sustentable de manera crítica, con acciones responsables','Acuerdo 444','G');
