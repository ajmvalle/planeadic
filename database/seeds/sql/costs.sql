INSERT INTO `membershipcosts`(`id`, `name`, `months`, `cost`, `created_at`, `updated_at`)
VALUES (1,'Cortesía',1,0,'2018-04-11 02:32:39','2018-04-11 02:32:39');

INSERT INTO `membershipcosts`(`id`, `name`, `months`, `cost`, `created_at`, `updated_at`)
VALUES (2,'Mensual',1,150.00,'2018-04-11 02:32:39','2018-04-11 02:32:39');

INSERT INTO `membershipcosts`(`id`, `name`, `months`, `cost`, `created_at`, `updated_at`)
VALUES (3,'Semestral',6,825.00,'2018-04-11 02:32:39','2018-04-11 02:32:39');

INSERT INTO `membershipcosts`(`id`, `name`, `months`, `cost`, `created_at`, `updated_at`)
VALUES (4,'Anual',12 ,1500.00,'2018-04-11 02:32:39','2018-04-11 02:32:39');