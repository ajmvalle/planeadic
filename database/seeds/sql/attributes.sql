INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (1,1,'1.1','Enfrenta las dificultades que se le presentan y es consciente de sus valores, fortalezas y debilidades');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (2,1,'1.2','Identifica sus emociones, las maneja de manera constructiva y reconoce la necesidad de solicitar apoyo ante una situación que lo rebase');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (3,1,'1.3','Elige alternativas y cursos de acción con base en criterios sustentados y en el marco de un proyecto de vida');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (4,1,'1.4','Analiza críticamente los factores que influyen en su toma de decisiones');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (5,1,'1.5','Asume las consecuencias de sus comportamientos y decisiones');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (6,1,'1.6','Administra los recursos disponibles teniendo en cuenta las restricciones para el logro de sus metas');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (7,2,'2.1','Valora el arte como manifestación de la belleza y expresión de ideas, sensaciones y emociones.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (8,2,'2.2','Experimenta el arte como un hecho histórico compartido que permite la comunicación entre individuos y culturas en el tiempo y el espacio, a la vez que desarrolla un sentido de identidad');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (9,2,'2.3','Participa en prácticas relacionadas con el arte');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (10,3,'3.1','Reconoce la actividad física como un medio para su desarrollo físico, mental y social.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (11,3,'3.2','Toma decisiones a partir de la valoración de las consecuencias de distintos hábitos de consumo y conductas de riesgo');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (12,3,'3.3','Cultiva relaciones interpersonales que contribuyen a su desarrollo humano y el de quienes lo rodean');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (13,4,'4.1','Expresa ideas y conceptos mediante representaciones lingüísticas, matemáticas o gráficas.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (14,4,'4.2','Aplica distintas estrategias comunicativas según quienes sean sus interlocutores, el contexto en el que se encuentra y los objetivos que persigue');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (15,4,'4.3','Identifica las ideas clave en un texto o discurso oral e infiere conclusiones a partir de ellas.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (16,4,'4.4','Se comunica en una segunda lengua en situaciones cotidianas');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (17,4,'4.5','Maneja las tecnologías de la información y la comunicación para obtener información y expresar ideas');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (18,5,'5.1','Sigue instrucciones y procedimientos de manera reflexiva, comprendiendo como cada uno de sus pasos contribuye al alcance de un objetivo.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (19,5,'5.2','Ordena información de acuerdo a categorías, jerarquías y relaciones.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (20,5,'5.3','Identifica los sistemas y reglas o principios medulares que subyacen a una serie de fenómenos.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (21,5,'5.4','Construye hipótesis y diseña y aplica modelos para probar su validez.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (22,5,'5.5','Sintetiza evidencias obtenidas mediante la experimentación para producir conclusiones y formular nuevas preguntas.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (23,5,'5.6','Utiliza las tecnologías de la información y comunicación para procesar e interpretar información.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (24,6,'6.1','Elige las fuentes de información más relevantes para un propósito específico y discrimina entre ellas de acuerdo a su relevancia y confiabilidad');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (25,6,'6.2','Evalúa argumentos y opiniones e identifica prejuicios y falacias.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (26,6,'6.3','Reconoce los propios prejuicios, modifica sus puntos de vista al conocer nuevas evidencias, e integra nuevos conocimientos y perspectivas al acervo con el que cuenta');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (27,6,'6.4','Estructura ideas y argumentos de manera clara, coherente y sintética.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (28,7,'7.1','Define metas y da seguimiento a sus procesos de construcción de conocimiento');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (29,7,'7.2','Identifica las actividades que le resultan de menor y mayor interés y dificultad, reconociendo y controlando sus reacciones frente a retos y obstáculos.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (30,7,'7.3','Articula saberes de diversos campos y establece relaciones entre ellos y su vida cotidiana.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (31,8,'8.1','Propone maneras de solucionar un problema o desarrollar un proyecto en equipo, definiendo un curso de acción con pasos específicos');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (32,8,'8.2','Aporta puntos de vista con apertura y considera los de otras personas de manera reflexiva');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (33,8,'8.3','Asume una actitud constructiva, congruente con los conocimientos y habilidades con los que cuenta dentro de distintos equipos de trabajo.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (34,9,'9.1','Privilegia el diálogo como mecanismo para la solución de conflictos.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (35,9,'9.2','Toma decisiones a fin de contribuir a la equidad, bienestar y desarrollo democrático de la sociedad');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (36,9,'9.3','Conoce sus derechos y obligaciones como mexicano y miembro de distintas comunidades e instituciones, y reconoce el valor de la participación como herramienta para ejercerlos.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (37,9,'9.4','Contribuye a alcanzar un equilibrio entre el interés y bienestar individual y el interés general de la sociedad');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (38,9,'9.5','Actúa de manera propositiva frente a fenómenos de la sociedad y se mantiene informado.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (39,9,'9.6','Advierte que los fenómenos que se desarrollan en los ámbitos local, nacional e internacional
ocurren dentro de un contexto global interdependiente');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (40,10,'10.1','Reconoce que la diversidad tiene lugar en un espacio democrático de igualdad de dignidad y derechos de todas las personas, y rechaza toda forma de discriminación');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (41,10,'10.2','Dialoga y aprende de personas con distintos puntos de vista y tradiciones culturales mediante la ubicación de sus propias circunstancias en un contexto más amplio');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (42,10,'10.3','Asume que el respeto de las diferencias es el principio de integración y convivencia en los contextos local, nacional e internacional.');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (43,11,'11.1','Asume una actitud que favorece la solución de problemas ambientales en los ámbitos local, nacional e internacional');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (44,11,'11.2','Reconoce y comprende las implicaciones biológicas, económicas, políticas y sociales del daño ambiental en un contexto global interdependiente');

INSERT INTO `attributes`(`id`, `competence_id`, `code`, `title`)
VALUES (45,11,'11.3','Contribuye al alcance de un equilibrio entre los intereses de corto y largo plazo con relación al ambiente.');

