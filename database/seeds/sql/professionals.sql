INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (101,'TE1','Trabajo en equipo','Realizar actividades para la concreción de objetivos y metas.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (102,'TE2','Trabajo en equipo','Valorar las fortalezas de cada integrante del equipo.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (103,'TE3','Trabajo en equipo','Participar en la generación de un clima de confianza y respeto. ','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (104,'TE4','Trabajo en equipo','Compartir su experiencia, conocimiento y recursos para el desempeño armónico del equipo','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (105,'TE5','Trabajo en equipo','Cumplir compromisos de trabajo en equipo.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (106,'TE6','Trabajo en equipo','Retroalimentar con base a los resultados del trabajo en equipo.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (107,'AP1','Atención al proceso','Detectar y reportar inconsistencias o errores en el producto, en el proceso o en los insumos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (108,'AP2','Atención al proceso','Verificar el cumplimiento de los parámetros de calidad exigidos.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (109,'AP3','Atención al proceso','Registrar y revisar información para asegurar que sea correcta','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (110,'AP4','Atención al proceso','Observar permanentemente y reportar los cambios presentes en los procesos, infraestructura e insumos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (111,'AP5','Atención al proceso','Verificar que la realización de una labor no deteriore o afecte otra.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (112,'AP6','Atención al proceso','Hacer caso omiso a distracciones del medio que puedan afectar su desempeño.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (113,'CE1','Comunicación efectiva','Indagar los argumentos, evidencias y hechos que llevan a los otros a pensar o expresarse de una determinada forma','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (114,'CE2.','Comunicación efectiva','Sustentar sus ideas y puntos de vista con argumentos, basados en evidencias, hechos y datos.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (115,'CE3.','Comunicación efectiva','Expresar sus ideas de forma verbal o escrita, teniendo en cuenta las características de su (s) interlocutor (es) y la situación dada.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (116,'CE4.','Comunicación efectiva','Manifiesta sus ideas y puntos de vista de manera que los otros lo comprendan','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (117,'CE5.','Comunicación efectiva','Precisar el mensaje escrito a la vez que escribe ideas con lenguaje claro, conciso.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (118,'OL1','Orientación al logro','Orientar las acciones llevadas a cabo a lograr y superar los estándares de desempeño y los plazos establecidos.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (119,'OL2','Orientación al logro','Diseñar y utilizar indicadores para medir y comprobar los resultados obtenidos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (120,'OL3','Orientación al logro','Mostrar interés porque la empresa reconozca los resultados obtenidos, fruto del esfuerzo propio y de los colaboradores','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (121,'OL4','Orientación al logro','Trabajar hasta alcanzar las metas o retos propuestos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (122,'OL5','Orientación al logro','Mejorar la relación entre objetivos logrados y los recursos invertidos en términos de calidad, costo y oportunidad.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (123,'AD1','Adaptabilidad','Enfrentar situaciones distintas a la que se está acostumbrado/a en la rutina de trabajo de forma abierta','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (124,'AD2','Adaptabilidad','Modificar su conducta para adecuarse a nuevas estrategias.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (125,'AD3','Adaptabilidad','Se adapta para un cambio positivo','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (126,'AD4','Adaptabilidad','Utilizar los nuevos conocimientos en el trabajo diario','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (127,'AD5','Adaptabilidad','Aceptar y aplicar los cambios de los procedimientos y de las herramientas de trabajo.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (128,'PO1','Planeación y organización','Anticipar los posibles obstáculos que puedan presentarse en el desarrollo de los objetivos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (129,'PO2','Planeación y organización','Tener claras las metas y objetivos de su área y de su puesto','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (130,'PO3','Planeación y organización','Definir sistemas y esquemas de trabajo','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (131,'PO4','Planeación y organización','Establecer prioridades y tiempos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (132,'PO5','Planeación y organización','Organizar y distribuir adecuadamente el cumplimiento de los objetivos y corregir las desviaciones si fuera necesario','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (133,'PO6','Planeación y organización','Evaluar mediante seguimiento el cumplimiento de los objetivos y corregir las desviaciones si fuera necesario','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (134,'OM1','Orientación a la mejora continua','Actualizarse respecto a las mejores prácticas en su especialidad o área de trabajo','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (135,'OM2','Orientación a la mejora continua','Promover la mejora como un activo decisivo para la competitividad de la organización o empresa','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (136,'OM3','Orientación a la mejora continua','Ampliar su conocimiento más allá de su área de trabajo inmediata','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (137,'OM4','Orientación a la mejora continua','Buscar y analizar información útil para la solución de problemas de área','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (138,'OM5','Orientación a la mejora continua','Fijar nuevas metas en su área de competencia o influencia','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (139,'OM6','Orientación a la mejora continua','Revisar las acciones llevadas a cabo con el fin de realizar mejoras y adaptarlas a los procedimientos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (140,'OM7','Orientación a la mejora continua','Crear ambiente propicio para estimular la mejora continua','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (141,'AC1','Atención al cliente','Utilizar la comunicación efectiva para identificar las necesidades del cliente.','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (142,'AC2','Atención al cliente','Explorar las nuevas necesidades y carencias que surgen en el cliente al buscar
la forma de satisfacerla','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (143,'AC3','Atención al cliente','Organizar la propia actividad de forma que se pueda dar mejor servicio a los clientes','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (144,'AC4','Atención al cliente','Solucionar oportunamente los problemas que encuentran los clientes en los productos o servicios','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (145,'AC5','Atención al cliente','Escuchar, informar con veracidad y saber a dónde dirigirlo','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (146,'AC6','Atención al cliente','Realizar seguimiento de las necesidades del cliente para darle una respuesta','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (147,'AC7','Atención al cliente','Mostrar interés por atender los errores cometidos con los clientes','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (148,'RI1','Relaciones Intepersonales','Coordinar el lenguaje corporal con el lenguaje oral en las situaciones de comunicación interpersonal','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (149,'RI2','Relaciones Intepersonales','Preparar sus instrucciones antes de transmitirlas','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (150,'RI3','Relaciones Intepersonales','Realizar preguntas para asegurarse de que comprende lo que los demás están comunicando','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (151,'RI4','Relaciones Intepersonales','Exponer opiniones propias, teniendo en cuenta la de los demás interlocutores','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (152,'RI5','Relaciones Intepersonales','Mantener informados a sus colaboradores de los objetivos, responsabilidades y avances de las tareas asignadas','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (153,'EP1','Ética profesional','Identificar los comportamientos apropiados para cada situación','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (154,'EP2','Ética profesional','Orientar su actuación al logro de objetivos','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (155,'EP3','Ética profesional','Actuar por convicción personal más que por presión externa','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (156,'EP4','Ética profesional','Promover el cumplimiento de normas y disposiciones en un espacio dado','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (157,'EP5','Ética profesional','Privilegiar las acciones que atienden los intereses colectivos más que los particulares','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (158,'EP6','Ética profesional','Cuidar y manejar los recursos y bienes ajenos siguiendo normas y disposiciones definidas','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (159,'EP7','Ética profesional','Cumplir los compromisos asumidos de acuerdo con las condiciones de tiempo y forma acordados','STPS','P');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (160,'EP8','Ética profesional','Actuar responsablemente de acuerdo a las normas y disposiciones definidas en un espacio dado','STPS','P');

