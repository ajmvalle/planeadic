/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1, 'Aguascalientes', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2, 'Asientos', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (3, 'Calvillo', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (4, 'Cosío', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (5, 'Jesús María', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (6, 'Pabellón de Arteaga', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (7, 'Rincón de Romos', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (8, 'San José de Gracia', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (9, 'Tepezalá', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (10, 'El Llano', 1);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    11, 'San Francisco de los Romo',
    1
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (12, 'Ensenada', 2);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (13, 'Mexicali', 2);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (14, 'Tecate', 2);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (15, 'Tijuana', 2);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (16, 'Playas de Rosarito', 2);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (17, 'Comondú', 3);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (18, 'Mulegé', 3);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (19, 'La Paz', 3);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (20, 'Los Cabos', 3);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (21, 'Loreto', 3);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (22, 'Calkiní', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (23, 'Campeche', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (24, 'Carmen', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (25, 'Champotón', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (26, 'Hecelchakán', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (27, 'Hopelchén', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (28, 'Palizada', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (29, 'Tenabo', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (30, 'Escárcega', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (31, 'Calakmul', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (32, 'Candelaria', 4);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (33, 'Abasolo', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (34, 'Acuña', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (35, 'Allende', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (36, 'Arteaga', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (37, 'Candela', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (38, 'Castaños', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (39, 'Cuatro Ciénegas', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (40, 'Escobedo', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (41, 'Francisco I. Madero', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (42, 'Frontera', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (43, 'General Cepeda', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (44, 'Guerrero', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (45, 'Hidalgo', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (46, 'Jiménez', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (47, 'Juárez', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (48, 'Lamadrid', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (49, 'Matamoros', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (50, 'Monclova', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (51, 'Morelos', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (52, 'Múzquiz', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (53, 'Nadadores', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (54, 'Nava', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (55, 'Ocampo', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (56, 'Parras', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (57, 'Piedras Negras', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (58, 'Progreso', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (59, 'Ramos Arizpe', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (60, 'Sabinas', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (61, 'Sacramento', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (62, 'Saltillo', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (63, 'San Buenaventura', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (64, 'San Juan de Sabinas', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (65, 'San Pedro', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (66, 'Sierra Mojada', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (67, 'Torreón', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (68, 'Viesca', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (69, 'Villa Unión', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (70, 'Zaragoza', 5);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (71, 'Armería', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (72, 'Colima', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (73, 'Comala', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (74, 'Coquimatlán', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (75, 'Cuauhtémoc', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (76, 'Ixtlahuacán', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (77, 'Manzanillo', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (78, 'Minatitlán', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (79, 'Tecomán', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (80, 'Villa de Álvarez', 6);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (81, 'Acacoyagua', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (82, 'Acala', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (83, 'Acapetahua', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (84, 'Altamirano', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (85, 'Amatán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    86, 'Amatenango de la Frontera',
    7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (87, 'Amatenango del Valle', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (88, 'Angel Albino Corzo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (89, 'Arriaga', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (90, 'Bejucal de Ocampo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (91, 'Bella Vista', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (92, 'Berriozábal', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (93, 'Bochil', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (94, 'El Bosque', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (95, 'Cacahoatán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (96, 'Catazajá', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (97, 'Cintalapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (98, 'Coapilla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (99, 'Comitán de Domínguez', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (100, 'La Concordia', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (101, 'Copainalá', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (102, 'Chalchihuitán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (103, 'Chamula', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (104, 'Chanal', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (105, 'Chapultenango', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (106, 'Chenalhó', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (107, 'Chiapa de Corzo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (108, 'Chiapilla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (109, 'Chicoasén', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (110, 'Chicomuselo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (111, 'Chilón', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (112, 'Escuintla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (113, 'Francisco León', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (114, 'Frontera Comalapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (115, 'Frontera Hidalgo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (116, 'La Grandeza', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (117, 'Huehuetán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (118, 'Huixtán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (119, 'Huitiupán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (120, 'Huixtla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (121, 'La Independencia', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (122, 'Ixhuatán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (123, 'Ixtacomitán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (124, 'Ixtapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (125, 'Ixtapangajoya', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (126, 'Jiquipilas', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (127, 'Jitotol', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (128, 'Juárez', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (129, 'Larráinzar', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (130, 'La Libertad', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (131, 'Mapastepec', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (132, 'Las Margaritas', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (133, 'Mazapa de Madero', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (134, 'Mazatán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (135, 'Metapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (136, 'Mitontic', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (137, 'Motozintla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (138, 'Nicolás Ruíz', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (139, 'Ocosingo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (140, 'Ocotepec', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    141, 'Ocozocoautla de Espinosa',
    7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (142, 'Ostuacán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (143, 'Osumacinta', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (144, 'Oxchuc', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (145, 'Palenque', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (146, 'Pantelhó', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (147, 'Pantepec', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (148, 'Pichucalco', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (149, 'Pijijiapan', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (150, 'El Porvenir', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (151, 'Villa Comaltitlán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    152, 'Pueblo Nuevo Solistahuacán',
    7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (153, 'Rayón', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (154, 'Reforma', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (155, 'Las Rosas', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (156, 'Sabanilla', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (157, 'Salto de Agua', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    158, 'San Cristóbal de las Casas',
    7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (159, 'San Fernando', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (160, 'Siltepec', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (161, 'Simojovel', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (162, 'Sitalá', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (163, 'Socoltenango', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (164, 'Solosuchiapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (165, 'Soyaló', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (166, 'Suchiapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (167, 'Suchiate', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (168, 'Sunuapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (169, 'Tapachula', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (170, 'Tapalapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (171, 'Tapilula', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (172, 'Tecpatán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (173, 'Tenejapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (174, 'Teopisca', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (175, 'Tila', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (176, 'Tonalá', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (177, 'Totolapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (178, 'La Trinitaria', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (179, 'Tumbalá', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (180, 'Tuxtla Gutiérrez', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (181, 'Tuxtla Chico', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (182, 'Tuzantán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (183, 'Tzimol', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (184, 'Unión Juárez', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (185, 'Venustiano Carranza', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (186, 'Villa Corzo', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (187, 'Villaflores', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (188, 'Yajalón', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (189, 'San Lucas', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (190, 'Zinacantán', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (191, 'San Juan Cancuc', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (192, 'Aldama', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    193, 'Benemérito de las Américas',
    7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (194, 'Maravilla Tenejapa', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (195, 'Marqués de Comillas', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    196, 'Montecristo de Guerrero', 7
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (197, 'San Andrés Duraznal', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (198, 'Santiago el Pinar', 7);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (199, 'Ahumada', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (200, 'Aldama', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (201, 'Allende', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (202, 'Aquiles Serdán', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (203, 'Ascensión', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (204, 'Bachíniva', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (205, 'Balleza', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    206, 'Batopilas de Manuel Gómez Morín',
    8
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (207, 'Bocoyna', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (208, 'Buenaventura', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (209, 'Camargo', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (210, 'Carichí', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (211, 'Casas Grandes', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (212, 'Coronado', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (213, 'Coyame del Sotol', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (214, 'La Cruz', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (215, 'Cuauhtémoc', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (216, 'Cusihuiriachi', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (217, 'Chihuahua', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (218, 'Chínipas', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (219, 'Delicias', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    220, 'Dr. Belisario Domínguez',
    8
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (221, 'Galeana', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (222, 'Santa Isabel', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (223, 'Gómez Farías', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (224, 'Gran Morelos', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (225, 'Guachochi', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (226, 'Guadalupe', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (227, 'Guadalupe y Calvo', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (228, 'Guazapares', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (229, 'Guerrero', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (230, 'Hidalgo del Parral', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (231, 'Huejotitán', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (232, 'Ignacio Zaragoza', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (233, 'Janos', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (234, 'Jiménez', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (235, 'Juárez', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (236, 'Julimes', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (237, 'López', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (238, 'Madera', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (239, 'Maguarichi', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (240, 'Manuel Benavides', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (241, 'Matachí', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (242, 'Matamoros', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (243, 'Meoqui', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (244, 'Morelos', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (245, 'Moris', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (246, 'Namiquipa', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (247, 'Nonoava', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (248, 'Nuevo Casas Grandes', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (249, 'Ocampo', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (250, 'Ojinaga', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (251, 'Praxedis G. Guerrero', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (252, 'Riva Palacio', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (253, 'Rosales', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (254, 'Rosario', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (255, 'San Francisco de Borja', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    256, 'San Francisco de Conchos',
    8
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (257, 'San Francisco del Oro', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (258, 'Santa Bárbara', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (259, 'Satevó', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (260, 'Saucillo', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (261, 'Temósachic', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (262, 'El Tule', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (263, 'Urique', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (264, 'Uruachi', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (265, 'Valle de Zaragoza', 8);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (266, 'Azcapotzalco', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (267, 'Coyoacán', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (268, 'Cuajimalpa de Morelos', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (269, 'Gustavo A. Madero', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (270, 'Iztacalco', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (271, 'Iztapalapa', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (272, 'La Magdalena Contreras', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (273, 'Milpa Alta', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (274, 'Álvaro Obregón', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (275, 'Tláhuac', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (276, 'Tlalpan', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (277, 'Xochimilco', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (278, 'Benito Juárez', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (279, 'Cuauhtémoc', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (280, 'Miguel Hidalgo', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (281, 'Venustiano Carranza', 9);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (282, 'Canatlán', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (283, 'Canelas', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (284, 'Coneto de Comonfort', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (285, 'Cuencamé', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (286, 'Durango', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    287, 'General Simón Bolívar', 10
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (288, 'Gómez Palacio', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (289, 'Guadalupe Victoria', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (290, 'Guanaceví', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (291, 'Hidalgo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (292, 'Indé', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (293, 'Lerdo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (294, 'Mapimí', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (295, 'Mezquital', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (296, 'Nazas', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (297, 'Nombre de Dios', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (298, 'Ocampo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (299, 'El Oro', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (300, 'Otáez', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (301, 'Pánuco de Coronado', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (302, 'Peñón Blanco', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (303, 'Poanas', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (304, 'Pueblo Nuevo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (305, 'Rodeo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (306, 'San Bernardo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (307, 'San Dimas', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (308, 'San Juan de Guadalupe', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (309, 'San Juan del Río', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (310, 'San Luis del Cordero', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (311, 'San Pedro del Gallo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (312, 'Santa Clara', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (313, 'Santiago Papasquiaro', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (314, 'Súchil', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (315, 'Tamazula', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (316, 'Tepehuanes', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (317, 'Tlahualilo', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (318, 'Topia', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (319, 'Vicente Guerrero', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (320, 'Nuevo Ideal', 10);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (321, 'Abasolo', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (322, 'Acámbaro', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (323, 'San Miguel de Allende', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (324, 'Apaseo el Alto', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (325, 'Apaseo el Grande', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (326, 'Atarjea', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (327, 'Celaya', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (328, 'Manuel Doblado', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (329, 'Comonfort', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (330, 'Coroneo', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (331, 'Cortazar', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (332, 'Cuerámaro', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (333, 'Doctor Mora', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    334, 'Dolores Hidalgo Cuna de la Independencia Nacional',
    11
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (335, 'Guanajuato', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (336, 'Huanímaro', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (337, 'Irapuato', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (338, 'Jaral del Progreso', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (339, 'Jerécuaro', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (340, 'León', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (341, 'Moroleón', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (342, 'Ocampo', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (343, 'Pénjamo', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (344, 'Pueblo Nuevo', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (345, 'Purísima del Rincón', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (346, 'Romita', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (347, 'Salamanca', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (348, 'Salvatierra', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    349, 'San Diego de la Unión', 11
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (350, 'San Felipe', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    351, 'San Francisco del Rincón',
    11
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (352, 'San José Iturbide', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (353, 'San Luis de la Paz', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (354, 'Santa Catarina', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    355, 'Santa Cruz de Juventino Rosas',
    11
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (356, 'Santiago Maravatío', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (357, 'Silao de la Victoria', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (358, 'Tarandacuao', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (359, 'Tarimoro', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (360, 'Tierra Blanca', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (361, 'Uriangato', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (362, 'Valle de Santiago', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (363, 'Victoria', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (364, 'Villagrán', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (365, 'Xichú', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (366, 'Yuriria', 11);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (367, 'Acapulco de Juárez', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (368, 'Ahuacuotzingo', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    369, 'Ajuchitlán del Progreso',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (370, 'Alcozauca de Guerrero', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (371, 'Alpoyeca', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (372, 'Apaxtla', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (373, 'Arcelia', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (374, 'Atenango del Río', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    375, 'Atlamajalcingo del Monte',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (376, 'Atlixtac', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (377, 'Atoyac de Álvarez', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (378, 'Ayutla de los Libres', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (379, 'Azoyú', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (380, 'Benito Juárez', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    381, 'Buenavista de Cuéllar', 12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    382, 'Coahuayutla de José María Izazaga',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (383, 'Cocula', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (384, 'Copala', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (385, 'Copalillo', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (386, 'Copanatoyac', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (387, 'Coyuca de Benítez', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (388, 'Coyuca de Catalán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (389, 'Cuajinicuilapa', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (390, 'Cualác', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (391, 'Cuautepec', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (392, 'Cuetzala del Progreso', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (393, 'Cutzamala de Pinzón', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (394, 'Chilapa de Álvarez', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    395, 'Chilpancingo de los Bravo',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (396, 'Florencio Villarreal', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    397, 'General Canuto A. Neri', 12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    398, 'General Heliodoro Castillo',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (399, 'Huamuxtitlán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    400, 'Huitzuco de los Figueroa',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    401, 'Iguala de la Independencia',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (402, 'Igualapa', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    403, 'Ixcateopan de Cuauhtémoc',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (404, 'Zihuatanejo de Azueta', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (405, 'Juan R. Escudero', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (406, 'Leonardo Bravo', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (407, 'Malinaltepec', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (408, 'Mártir de Cuilapan', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (409, 'Metlatónoc', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (410, 'Mochitlán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (411, 'Olinalá', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (412, 'Ometepec', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    413, 'Pedro Ascencio Alquisiras',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (414, 'Petatlán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (415, 'Pilcaya', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (416, 'Pungarabato', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (417, 'Quechultenango', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (418, 'San Luis Acatlán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (419, 'San Marcos', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (420, 'San Miguel Totolapan', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (421, 'Taxco de Alarcón', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (422, 'Tecoanapa', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (423, 'Técpan de Galeana', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (424, 'Teloloapan', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    425, 'Tepecoacuilco de Trujano',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (426, 'Tetipac', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (427, 'Tixtla de Guerrero', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (428, 'Tlacoachistlahuaca', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (429, 'Tlacoapa', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (430, 'Tlalchapa', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    431, 'Tlalixtaquilla de Maldonado',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (432, 'Tlapa de Comonfort', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (433, 'Tlapehuala', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    434, 'La Unión de Isidoro Montes de Oca',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (435, 'Xalpatláhuac', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (436, 'Xochihuehuetlán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (437, 'Xochistlahuaca', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (438, 'Zapotitlán Tablas', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (439, 'Zirándaro', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (440, 'Zitlala', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (441, 'Eduardo Neri', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (442, 'Acatepec', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (443, 'Marquelia', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (444, 'Cochoapa el Grande', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    445, 'José Joaquín de Herrera',
    12
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (446, 'Juchitán', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (447, 'Iliatenco', 12);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (448, 'Acatlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (449, 'Acaxochitlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (450, 'Actopan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    451, 'Agua Blanca de Iturbide', 13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (452, 'Ajacuba', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (453, 'Alfajayucan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (454, 'Almoloya', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (455, 'Apan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (456, 'El Arenal', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (457, 'Atitalaquia', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (458, 'Atlapexco', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (459, 'Atotonilco el Grande', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (460, 'Atotonilco de Tula', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (461, 'Calnali', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (462, 'Cardonal', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (463, 'Cuautepec de Hinojosa', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (464, 'Chapantongo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (465, 'Chapulhuacán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (466, 'Chilcuautla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (467, 'Eloxochitlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (468, 'Emiliano Zapata', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (469, 'Epazoyucan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (470, 'Francisco I. Madero', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (471, 'Huasca de Ocampo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (472, 'Huautla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (473, 'Huazalingo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (474, 'Huehuetla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (475, 'Huejutla de Reyes', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (476, 'Huichapan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (477, 'Ixmiquilpan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (478, 'Jacala de Ledezma', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (479, 'Jaltocán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (480, 'Juárez Hidalgo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (481, 'Lolotla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (482, 'Metepec', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    483, 'San Agustín Metzquititlán',
    13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (484, 'Metztitlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (485, 'Mineral del Chico', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (486, 'Mineral del Monte', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (487, 'La Misión', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    488, 'Mixquiahuala de Juárez', 13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (489, 'Molango de Escamilla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (490, 'Nicolás Flores', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (491, 'Nopala de Villagrán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (492, 'Omitlán de Juárez', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (493, 'San Felipe Orizatlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (494, 'Pacula', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (495, 'Pachuca de Soto', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (496, 'Pisaflores', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (497, 'Progreso de Obregón', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (498, 'Mineral de la Reforma', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (499, 'San Agustín Tlaxiaca', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (500, 'San Bartolo Tutotepec', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (501, 'San Salvador', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (502, 'Santiago de Anaya', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    503, 'Santiago Tulantepec de Lugo Guerrero',
    13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (504, 'Singuilucan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (505, 'Tasquillo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (506, 'Tecozautla', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (507, 'Tenango de Doria', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (508, 'Tepeapulco', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    509, 'Tepehuacán de Guerrero', 13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    510, 'Tepeji del Río de Ocampo',
    13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (511, 'Tepetitlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (512, 'Tetepango', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (513, 'Villa de Tezontepec', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (514, 'Tezontepec de Aldama', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (515, 'Tianguistengo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (516, 'Tizayuca', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (517, 'Tlahuelilpan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (518, 'Tlahuiltepa', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (519, 'Tlanalapa', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (520, 'Tlanchinol', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (521, 'Tlaxcoapan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (522, 'Tolcayuca', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (523, 'Tula de Allende', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (524, 'Tulancingo de Bravo', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (525, 'Xochiatipan', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (526, 'Xochicoatlán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (527, 'Yahualica', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    528, 'Zacualtipán de Ángeles',
    13
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (529, 'Zapotlán de Juárez', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (530, 'Zempoala', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (531, 'Zimapán', 13);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (532, 'Acatic', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (533, 'Acatlán de Juárez', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (534, 'Ahualulco de Mercado', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (535, 'Amacueca', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (536, 'Amatitán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (537, 'Ameca', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    538, 'San Juanito de Escobedo', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (539, 'Arandas', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (540, 'El Arenal', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (541, 'Atemajac de Brizuela', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (542, 'Atengo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (543, 'Atenguillo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (544, 'Atotonilco el Alto', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (545, 'Atoyac', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (546, 'Autlán de Navarro', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (547, 'Ayotlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (548, 'Ayutla', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (549, 'La Barca', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (550, 'Bolaños', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (551, 'Cabo Corrientes', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (552, 'Casimiro Castillo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (553, 'Cihuatlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (554, 'Zapotlán el Grande', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (555, 'Cocula', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (556, 'Colotlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    557, 'Concepción de Buenos Aires',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    558, 'Cuautitlán de García Barragán',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (559, 'Cuautla', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (560, 'Cuquío', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (561, 'Chapala', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (562, 'Chimaltitán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (563, 'Chiquilistlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (564, 'Degollado', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (565, 'Ejutla', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (566, 'Encarnación de Díaz', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (567, 'Etzatlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (568, 'El Grullo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (569, 'Guachinango', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (570, 'Guadalajara', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (571, 'Hostotipaquillo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (572, 'Huejúcar', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (573, 'Huejuquilla el Alto', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (574, 'La Huerta', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    575, 'Ixtlahuacán de los Membrillos',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (576, 'Ixtlahuacán del Río', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (577, 'Jalostotitlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (578, 'Jamay', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (579, 'Jesús María', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    580, 'Jilotlán de los Dolores',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (581, 'Jocotepec', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (582, 'Juanacatlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (583, 'Juchitlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (584, 'Lagos de Moreno', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (585, 'El Limón', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (586, 'Magdalena', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (587, 'Santa María del Oro', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    588, 'La Manzanilla de la Paz', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (589, 'Mascota', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (590, 'Mazamitla', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (591, 'Mexticacán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (592, 'Mezquitic', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (593, 'Mixtlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (594, 'Ocotlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (595, 'Ojuelos de Jalisco', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (596, 'Pihuamo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (597, 'Poncitlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (598, 'Puerto Vallarta', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (599, 'Villa Purificación', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (600, 'Quitupan', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (601, 'El Salto', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    602, 'San Cristóbal de la Barranca',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    603, 'San Diego de Alejandría',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (604, 'San Juan de los Lagos', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (605, 'San Julián', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (606, 'San Marcos', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    607, 'San Martín de Bolaños', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (608, 'San Martín Hidalgo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (609, 'San Miguel el Alto', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (610, 'Gómez Farías', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    611, 'San Sebastián del Oeste',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    612, 'Santa María de los Ángeles',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (613, 'Sayula', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (614, 'Tala', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (615, 'Talpa de Allende', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (616, 'Tamazula de Gordiano', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (617, 'Tapalpa', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (618, 'Tecalitlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (619, 'Tecolotlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    620, 'Techaluta de Montenegro', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (621, 'Tenamaxtlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (622, 'Teocaltiche', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    623, 'Teocuitatlán de Corona', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    624, 'Tepatitlán de Morelos', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (625, 'Tequila', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (626, 'Teuchitlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (627, 'Tizapán el Alto', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    628, 'Tlajomulco de Zúñiga', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (629, 'San Pedro Tlaquepaque', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (630, 'Tolimán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (631, 'Tomatlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (632, 'Tonalá', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (633, 'Tonaya', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (634, 'Tonila', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (635, 'Totatiche', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (636, 'Tototlán', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (637, 'Tuxcacuesco', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (638, 'Tuxcueca', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (639, 'Tuxpan', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (640, 'Unión de San Antonio', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (641, 'Unión de Tula', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (642, 'Valle de Guadalupe', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (643, 'Valle de Juárez', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (644, 'San Gabriel', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (645, 'Villa Corona', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (646, 'Villa Guerrero', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (647, 'Villa Hidalgo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (648, 'Cañadas de Obregón', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    649, 'Yahualica de González Gallo',
    14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (650, 'Zacoalco de Torres', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (651, 'Zapopan', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (652, 'Zapotiltic', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    653, 'Zapotitlán de Vadillo', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (654, 'Zapotlán del Rey', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (655, 'Zapotlanejo', 14);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    656, 'San Ignacio Cerro Gordo', 14
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    657, 'Acambay de Ruíz Castañeda',
    15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (658, 'Acolman', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (659, 'Aculco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    660, 'Almoloya de Alquisiras', 15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (661, 'Almoloya de Juárez', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (662, 'Almoloya del Río', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (663, 'Amanalco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (664, 'Amatepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (665, 'Amecameca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (666, 'Apaxco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (667, 'Atenco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (668, 'Atizapán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (669, 'Atizapán de Zaragoza', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (670, 'Atlacomulco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (671, 'Atlautla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (672, 'Axapusco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (673, 'Ayapango', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (674, 'Calimaya', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (675, 'Capulhuac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    676, 'Coacalco de Berriozábal',
    15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (677, 'Coatepec Harinas', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (678, 'Cocotitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (679, 'Coyotepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (680, 'Cuautitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (681, 'Chalco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (682, 'Chapa de Mota', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (683, 'Chapultepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (684, 'Chiautla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (685, 'Chicoloapan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (686, 'Chiconcuac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (687, 'Chimalhuacán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (688, 'Donato Guerra', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (689, 'Ecatepec de Morelos', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (690, 'Ecatzingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (691, 'Huehuetoca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (692, 'Hueypoxtla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (693, 'Huixquilucan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (694, 'Isidro Fabela', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (695, 'Ixtapaluca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (696, 'Ixtapan de la Sal', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (697, 'Ixtapan del Oro', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (698, 'Ixtlahuaca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (699, 'Xalatlaco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (700, 'Jaltenco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (701, 'Jilotepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (702, 'Jilotzingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (703, 'Jiquipilco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (704, 'Jocotitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (705, 'Joquicingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (706, 'Juchitepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (707, 'Lerma', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (708, 'Malinalco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (709, 'Melchor Ocampo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (710, 'Metepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (711, 'Mexicaltzingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (712, 'Morelos', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (713, 'Naucalpan de Juárez', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (714, 'Nezahualcóyotl', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (715, 'Nextlalpan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (716, 'Nicolás Romero', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (717, 'Nopaltepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (718, 'Ocoyoacac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (719, 'Ocuilan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (720, 'El Oro', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (721, 'Otumba', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (722, 'Otzoloapan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (723, 'Otzolotepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (724, 'Ozumba', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (725, 'Papalotla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (726, 'La Paz', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (727, 'Polotitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (728, 'Rayón', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (729, 'San Antonio la Isla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    730, 'San Felipe del Progreso', 15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    731, 'San Martín de las Pirámides',
    15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (732, 'San Mateo Atenco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    733, 'San Simón de Guerrero', 15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (734, 'Santo Tomás', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    735, 'Soyaniquilpan de Juárez',
    15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (736, 'Sultepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (737, 'Tecámac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (738, 'Tejupilco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (739, 'Temamatla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (740, 'Temascalapa', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (741, 'Temascalcingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (742, 'Temascaltepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (743, 'Temoaya', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (744, 'Tenancingo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (745, 'Tenango del Aire', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (746, 'Tenango del Valle', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (747, 'Teoloyucan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (748, 'Teotihuacán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (749, 'Tepetlaoxtoc', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (750, 'Tepetlixpa', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (751, 'Tepotzotlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (752, 'Tequixquiac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (753, 'Texcaltitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (754, 'Texcalyacac', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (755, 'Texcoco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (756, 'Tezoyuca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (757, 'Tianguistenco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (758, 'Timilpan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (759, 'Tlalmanalco', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (760, 'Tlalnepantla de Baz', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (761, 'Tlatlaya', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (762, 'Toluca', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (763, 'Tonatico', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (764, 'Tultepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (765, 'Tultitlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (766, 'Valle de Bravo', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (767, 'Villa de Allende', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (768, 'Villa del Carbón', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (769, 'Villa Guerrero', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (770, 'Villa Victoria', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (771, 'Xonacatlán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (772, 'Zacazonapan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (773, 'Zacualpan', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (774, 'Zinacantepec', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (775, 'Zumpahuacán', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (776, 'Zumpango', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (777, 'Cuautitlán Izcalli', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    778, 'Valle de Chalco Solidaridad',
    15
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (779, 'Luvianos', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (780, 'San José del Rincón', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (781, 'Tonanitla', 15);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (782, 'Acuitzio', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (783, 'Aguililla', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (784, 'Álvaro Obregón', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (785, 'Angamacutiro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (786, 'Angangueo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (787, 'Apatzingán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (788, 'Aporo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (789, 'Aquila', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (790, 'Ario', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (791, 'Arteaga', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (792, 'Briseñas', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (793, 'Buenavista', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (794, 'Carácuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (795, 'Coahuayana', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    796, 'Coalcomán de Vázquez Pallares',
    16
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (797, 'Coeneo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (798, 'Contepec', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (799, 'Copándaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (800, 'Cotija', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (801, 'Cuitzeo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (802, 'Charapan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (803, 'Charo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (804, 'Chavinda', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (805, 'Cherán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (806, 'Chilchota', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (807, 'Chinicuila', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (808, 'Chucándiro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (809, 'Churintzio', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (810, 'Churumuco', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (811, 'Ecuandureo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (812, 'Epitacio Huerta', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (813, 'Erongarícuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (814, 'Gabriel Zamora', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (815, 'Hidalgo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (816, 'La Huacana', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (817, 'Huandacareo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (818, 'Huaniqueo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (819, 'Huetamo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (820, 'Huiramba', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (821, 'Indaparapeo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (822, 'Irimbo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (823, 'Ixtlán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (824, 'Jacona', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (825, 'Jiménez', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (826, 'Jiquilpan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (827, 'Juárez', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (828, 'Jungapeo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (829, 'Lagunillas', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (830, 'Madero', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (831, 'Maravatío', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (832, 'Marcos Castellanos', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (833, 'Lázaro Cárdenas', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (834, 'Morelia', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (835, 'Morelos', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (836, 'Múgica', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (837, 'Nahuatzen', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (838, 'Nocupétaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (839, 'Nuevo Parangaricutiro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (840, 'Nuevo Urecho', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (841, 'Numarán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (842, 'Ocampo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (843, 'Pajacuarán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (844, 'Panindícuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (845, 'Parácuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (846, 'Paracho', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (847, 'Pátzcuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (848, 'Penjamillo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (849, 'Peribán', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (850, 'La Piedad', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (851, 'Purépero', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (852, 'Puruándiro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (853, 'Queréndaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (854, 'Quiroga', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    855, 'Cojumatlán de Régules', 16
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (856, 'Los Reyes', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (857, 'Sahuayo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (858, 'San Lucas', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (859, 'Santa Ana Maya', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (860, 'Salvador Escalante', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (861, 'Senguio', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (862, 'Susupuato', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (863, 'Tacámbaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (864, 'Tancítaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (865, 'Tangamandapio', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (866, 'Tangancícuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (867, 'Tanhuato', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (868, 'Taretan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (869, 'Tarímbaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (870, 'Tepalcatepec', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (871, 'Tingambato', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (872, 'Tingüindín', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    873, 'Tiquicheo de Nicolás Romero',
    16
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (874, 'Tlalpujahua', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (875, 'Tlazazalca', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (876, 'Tocumbo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (877, 'Tumbiscatío', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (878, 'Turicato', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (879, 'Tuxpan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (880, 'Tuzantla', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (881, 'Tzintzuntzan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (882, 'Tzitzio', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (883, 'Uruapan', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (884, 'Venustiano Carranza', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (885, 'Villamar', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (886, 'Vista Hermosa', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (887, 'Yurécuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (888, 'Zacapu', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (889, 'Zamora', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (890, 'Zináparo', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (891, 'Zinapécuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (892, 'Ziracuaretiro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (893, 'Zitácuaro', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (894, 'José Sixto Verduzco', 16);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (895, 'Amacuzac', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (896, 'Atlatlahucan', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (897, 'Axochiapan', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (898, 'Ayala', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (899, 'Coatlán del Río', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (900, 'Cuautla', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (901, 'Cuernavaca', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (902, 'Emiliano Zapata', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (903, 'Huitzilac', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (904, 'Jantetelco', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (905, 'Jiutepec', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (906, 'Jojutla', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    907, 'Jonacatepec de Leandro Valle',
    17
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (908, 'Mazatepec', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (909, 'Miacatlán', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (910, 'Ocuituco', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (911, 'Puente de Ixtla', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (912, 'Temixco', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (913, 'Tepalcingo', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (914, 'Tepoztlán', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (915, 'Tetecala', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (916, 'Tetela del Volcán', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (917, 'Tlalnepantla', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    918, 'Tlaltizapán de Zapata', 17
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (919, 'Tlaquiltenango', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (920, 'Tlayacapan', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (921, 'Totolapan', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (922, 'Xochitepec', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (923, 'Yautepec', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (924, 'Yecapixtla', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (925, 'Zacatepec', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (926, 'Zacualpan de Amilpas', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (927, 'Temoac', 17);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (928, 'Acaponeta', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (929, 'Ahuacatlán', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (930, 'Amatlán de Cañas', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (931, 'Compostela', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (932, 'Huajicori', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (933, 'Ixtlán del Río', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (934, 'Jala', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (935, 'Xalisco', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (936, 'Del Nayar', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (937, 'Rosamorada', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (938, 'Ruíz', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (939, 'San Blas', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (940, 'San Pedro Lagunillas', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (941, 'Santa María del Oro', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (942, 'Santiago Ixcuintla', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (943, 'Tecuala', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (944, 'Tepic', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (945, 'Tuxpan', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (946, 'La Yesca', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (947, 'Bahía de Banderas', 18);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (948, 'Abasolo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (949, 'Agualeguas', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (950, 'Los Aldamas', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (951, 'Allende', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (952, 'Anáhuac', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (953, 'Apodaca', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (954, 'Aramberri', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (955, 'Bustamante', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (956, 'Cadereyta Jiménez', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (957, 'El Carmen', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (958, 'Cerralvo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (959, 'Ciénega de Flores', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (960, 'China', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (961, 'Doctor Arroyo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (962, 'Doctor Coss', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (963, 'Doctor González', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (964, 'Galeana', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (965, 'García', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    966, 'San Pedro Garza García', 19
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (967, 'General Bravo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (968, 'General Escobedo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (969, 'General Terán', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (970, 'General Treviño', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (971, 'General Zaragoza', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (972, 'General Zuazua', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (973, 'Guadalupe', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (974, 'Los Herreras', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (975, 'Higueras', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (976, 'Hualahuises', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (977, 'Iturbide', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (978, 'Juárez', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (979, 'Lampazos de Naranjo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (980, 'Linares', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (981, 'Marín', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (982, 'Melchor Ocampo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (983, 'Mier y Noriega', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (984, 'Mina', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (985, 'Montemorelos', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (986, 'Monterrey', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (987, 'Parás', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (988, 'Pesquería', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (989, 'Los Ramones', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (990, 'Rayones', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (991, 'Sabinas Hidalgo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (992, 'Salinas Victoria', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    993, 'San Nicolás de los Garza',
    19
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (994, 'Hidalgo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (995, 'Santa Catarina', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (996, 'Santiago', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (997, 'Vallecillo', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (998, 'Villaldama', 19);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (999, 'Abejones', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1000, 'Acatlán de Pérez Figueroa',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1001, 'Asunción Cacalotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1002, 'Asunción Cuyotepeji', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1003, 'Asunción Ixtaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1004, 'Asunción Nochixtlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1005, 'Asunción Ocotlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1006, 'Asunción Tlacolulita', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1007, 'Ayotzintepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1008, 'El Barrio de la Soledad',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1009, 'Calihualá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1010, 'Candelaria Loxicha', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1011, 'Ciénega de Zimatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1012, 'Ciudad Ixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1013, 'Coatecas Altas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1014, 'Coicoyán de las Flores',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1015, 'La Compañía', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1016, 'Concepción Buenavista', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1017, 'Concepción Pápalo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1018, 'Constancia del Rosario', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1019, 'Cosolapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1020, 'Cosoltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1021, 'Cuilápam de Guerrero', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1022, 'Cuyamecalco Villa de Zaragoza',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1023, 'Chahuites', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1024, 'Chalcatongo de Hidalgo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1025, 'Chiquihuitlán de Benito Juárez',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1026, 'Heroica Ciudad de Ejutla de Crespo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1027, 'Eloxochitlán de Flores Magón',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1028, 'El Espinal', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1029, 'Tamazulápam del Espíritu Santo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1030, 'Fresnillo de Trujano', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1031, 'Guadalupe Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1032, 'Guadalupe de Ramírez', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1033, 'Guelatao de Juárez', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1034, 'Guevea de Humboldt', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1035, 'Mesones Hidalgo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1036, 'Villa Hidalgo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1037, 'Heroica Ciudad de Huajuapan de León',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1038, 'Huautepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1039, 'Huautla de Jiménez', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1040, 'Ixtlán de Juárez', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1041, 'Heroica Ciudad de Juchitán de Zaragoza',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1042, 'Loma Bonita', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1043, 'Magdalena Apasco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1044, 'Magdalena Jaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1045, 'Santa Magdalena Jicotlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1046, 'Magdalena Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1047, 'Magdalena Ocotlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1048, 'Magdalena Peñasco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1049, 'Magdalena Teitipac', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1050, 'Magdalena Tequisistlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1051, 'Magdalena Tlacotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1052, 'Magdalena Zahuatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1053, 'Mariscala de Juárez', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1054, 'Mártires de Tacubaya', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1055, 'Matías Romero Avendaño',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1056, 'Mazatlán Villa de Flores',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1057, 'Miahuatlán de Porfirio Díaz',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1058, 'Mixistlán de la Reforma',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1059, 'Monjas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1060, 'Natividad', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1061, 'Nazareno Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1062, 'Nejapa de Madero', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1063, 'Ixpantepec Nieves', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1064, 'Santiago Niltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1065, 'Oaxaca de Juárez', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1066, 'Ocotlán de Morelos', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1067, 'La Pe', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1068, 'Pinotepa de Don Luis', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1069, 'Pluma Hidalgo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1070, 'San José del Progreso', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1071, 'Putla Villa de Guerrero',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1072, 'Santa Catarina Quioquitani',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1073, 'Reforma de Pineda', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1074, 'La Reforma', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1075, 'Reyes Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1076, 'Rojas de Cuauhtémoc', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1077, 'Salina Cruz', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1078, 'San Agustín Amatengo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1079, 'San Agustín Atenango', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1080, 'San Agustín Chayuco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1081, 'San Agustín de las Juntas',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1082, 'San Agustín Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1083, 'San Agustín Loxicha', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1084, 'San Agustín Tlacotepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1085, 'San Agustín Yatareni', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1086, 'San Andrés Cabecera Nueva',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1087, 'San Andrés Dinicuiti', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1088, 'San Andrés Huaxpaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1089, 'San Andrés Huayápam', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1090, 'San Andrés Ixtlahuaca', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1091, 'San Andrés Lagunas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1092, 'San Andrés Nuxiño', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1093, 'San Andrés Paxtlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1094, 'San Andrés Sinaxtla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1095, 'San Andrés Solaga', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1096, 'San Andrés Teotilálpam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1097, 'San Andrés Tepetlapa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1098, 'San Andrés Yaá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1099, 'San Andrés Zabache', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1100, 'San Andrés Zautla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1101, 'San Antonino Castillo Velasco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1102, 'San Antonino el Alto', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1103, 'San Antonino Monte Verde',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1104, 'San Antonio Acutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1105, 'San Antonio de la Cal', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1106, 'San Antonio Huitepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1107, 'San Antonio Nanahuatípam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1108, 'San Antonio Sinicahua', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1109, 'San Antonio Tepetlapa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1110, 'San Baltazar Chichicápam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1111, 'San Baltazar Loxicha', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1112, 'San Baltazar Yatzachi el Bajo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1113, 'San Bartolo Coyotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1114, 'San Bartolomé Ayautla', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1115, 'San Bartolomé Loxicha', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1116, 'San Bartolomé Quialana',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1117, 'San Bartolomé Yucuañe',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1118, 'San Bartolomé Zoogocho',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1119, 'San Bartolo Soyaltepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1120, 'San Bartolo Yautepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1121, 'San Bernardo Mixtepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1122, 'San Blas Atempa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1123, 'San Carlos Yautepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1124, 'San Cristóbal Amatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1125, 'San Cristóbal Amoltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1126, 'San Cristóbal Lachirioag',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1127, 'San Cristóbal Suchixtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1128, 'San Dionisio del Mar', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1129, 'San Dionisio Ocotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1130, 'San Dionisio Ocotlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1131, 'San Esteban Atatlahuca', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1132, 'San Felipe Jalapa de Díaz',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1133, 'San Felipe Tejalápam', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1134, 'San Felipe Usila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1135, 'San Francisco Cahuacuá',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1136, 'San Francisco Cajonos', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1137, 'San Francisco Chapulapa',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1138, 'San Francisco Chindúa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1139, 'San Francisco del Mar', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1140, 'San Francisco Huehuetlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1141, 'San Francisco Ixhuatán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1142, 'San Francisco Jaltepetongo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1143, 'San Francisco Lachigoló',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1144, 'San Francisco Logueche', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1145, 'San Francisco Nuxaño', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1146, 'San Francisco Ozolotepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1147, 'San Francisco Sola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1148, 'San Francisco Telixtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1149, 'San Francisco Teopan', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1150, 'San Francisco Tlapancingo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1151, 'San Gabriel Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1152, 'San Ildefonso Amatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1153, 'San Ildefonso Sola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1154, 'San Ildefonso Villa Alta',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1155, 'San Jacinto Amilpas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1156, 'San Jacinto Tlacotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1157, 'San Jerónimo Coatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1158, 'San Jerónimo Silacayoapilla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1159, 'San Jerónimo Sosola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1160, 'San Jerónimo Taviche', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1161, 'San Jerónimo Tecóatl', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1162, 'San Jorge Nuchita', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1163, 'San José Ayuquila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1164, 'San José Chiltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1165, 'San José del Peñasco', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1166, 'San José Estancia Grande',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1167, 'San José Independencia',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1168, 'San José Lachiguiri', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1169, 'San José Tenango', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1170, 'San Juan Achiutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1171, 'San Juan Atepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1172, 'Ánimas Trujano', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1173, 'San Juan Bautista Atatlahuca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1174, 'San Juan Bautista Coixtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1175, 'San Juan Bautista Cuicatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1176, 'San Juan Bautista Guelache',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1177, 'San Juan Bautista Jayacatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1178, 'San Juan Bautista Lo de Soto',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1179, 'San Juan Bautista Suchitepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1180, 'San Juan Bautista Tlacoatzintepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1181, 'San Juan Bautista Tlachichilco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1182, 'San Juan Bautista Tuxtepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1183, 'San Juan Cacahuatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1184, 'San Juan Cieneguilla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1185, 'San Juan Coatzóspam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1186, 'San Juan Colorado', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1187, 'San Juan Comaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1188, 'San Juan Cotzocón', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1189, 'San Juan Chicomezúchil',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1190, 'San Juan Chilateca', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1191, 'San Juan del Estado', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1192, 'San Juan del Río', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1193, 'San Juan Diuxi', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1194, 'San Juan Evangelista Analco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1195, 'San Juan Guelavía', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1196, 'San Juan Guichicovi', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1197, 'San Juan Ihualtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1198, 'San Juan Juquila Mixes', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1199, 'San Juan Juquila Vijanos',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1200, 'San Juan Lachao', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1201, 'San Juan Lachigalla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1202, 'San Juan Lajarcia', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1203, 'San Juan Lalana', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1204, 'San Juan de los Cués', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1205, 'San Juan Mazatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1206, 'San Juan Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1207, 'San Juan Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1208, 'San Juan Ñumí', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1209, 'San Juan Ozolotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1210, 'San Juan Petlapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1211, 'San Juan Quiahije', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1212, 'San Juan Quiotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1213, 'San Juan Sayultepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1214, 'San Juan Tabaá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1215, 'San Juan Tamazola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1216, 'San Juan Teita', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1217, 'San Juan Teitipac', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1218, 'San Juan Tepeuxila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1219, 'San Juan Teposcolula', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1220, 'San Juan Yaeé', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1221, 'San Juan Yatzona', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1222, 'San Juan Yucuita', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1223, 'San Lorenzo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1224, 'San Lorenzo Albarradas', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1225, 'San Lorenzo Cacaotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1226, 'San Lorenzo Cuaunecuiltitla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1227, 'San Lorenzo Texmelúcan',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1228, 'San Lorenzo Victoria', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1229, 'San Lucas Camotlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1230, 'San Lucas Ojitlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1231, 'San Lucas Quiaviní', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1232, 'San Lucas Zoquiápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1233, 'San Luis Amatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1234, 'San Marcial Ozolotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1235, 'San Marcos Arteaga', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1236, 'San Martín de los Cansecos',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1237, 'San Martín Huamelúlpam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1238, 'San Martín Itunyoso', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1239, 'San Martín Lachilá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1240, 'San Martín Peras', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1241, 'San Martín Tilcajete', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1242, 'San Martín Toxpalan', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1243, 'San Martín Zacatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1244, 'San Mateo Cajonos', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1245, 'Capulálpam de Méndez', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1246, 'San Mateo del Mar', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1247, 'San Mateo Yoloxochitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1248, 'San Mateo Etlatongo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1249, 'San Mateo Nejápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1250, 'San Mateo Peñasco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1251, 'San Mateo Piñas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1252, 'San Mateo Río Hondo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1253, 'San Mateo Sindihui', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1254, 'San Mateo Tlapiltepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1255, 'San Melchor Betaza', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1256, 'San Miguel Achiutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1257, 'San Miguel Ahuehuetitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1258, 'San Miguel Aloápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1259, 'San Miguel Amatitlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1260, 'San Miguel Amatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1261, 'San Miguel Coatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1262, 'San Miguel Chicahua', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1263, 'San Miguel Chimalapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1264, 'San Miguel del Puerto', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1265, 'San Miguel del Río', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1266, 'San Miguel Ejutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1267, 'San Miguel el Grande', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1268, 'San Miguel Huautla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1269, 'San Miguel Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1270, 'San Miguel Panixtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1271, 'San Miguel Peras', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1272, 'San Miguel Piedras', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1273, 'San Miguel Quetzaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1274, 'San Miguel Santa Flor', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1275, 'Villa Sola de Vega', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1276, 'San Miguel Soyaltepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1277, 'San Miguel Suchixtepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1278, 'Villa Talea de Castro', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1279, 'San Miguel Tecomatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1280, 'San Miguel Tenango', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1281, 'San Miguel Tequixtepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1282, 'San Miguel Tilquiápam', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1283, 'San Miguel Tlacamama', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1284, 'San Miguel Tlacotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1285, 'San Miguel Tulancingo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1286, 'San Miguel Yotao', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1287, 'San Nicolás', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1288, 'San Nicolás Hidalgo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1289, 'San Pablo Coatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1290, 'San Pablo Cuatro Venados',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1291, 'San Pablo Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1292, 'San Pablo Huitzo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1293, 'San Pablo Huixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1294, 'San Pablo Macuiltianguis',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1295, 'San Pablo Tijaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1296, 'San Pablo Villa de Mitla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1297, 'San Pablo Yaganiza', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1298, 'San Pedro Amuzgos', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1299, 'San Pedro Apóstol', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1300, 'San Pedro Atoyac', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1301, 'San Pedro Cajonos', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1302, 'San Pedro Coxcaltepec Cántaros',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1303, 'San Pedro Comitancillo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1304, 'San Pedro el Alto', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1305, 'San Pedro Huamelula', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1306, 'San Pedro Huilotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1307, 'San Pedro Ixcatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1308, 'San Pedro Ixtlahuaca', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1309, 'San Pedro Jaltepetongo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1310, 'San Pedro Jicayán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1311, 'San Pedro Jocotipac', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1312, 'San Pedro Juchatengo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1313, 'San Pedro Mártir', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1314, 'San Pedro Mártir Quiechapa',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1315, 'San Pedro Mártir Yucuxaco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1316, 'San Pedro Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1317, 'San Pedro Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1318, 'San Pedro Molinos', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1319, 'San Pedro Nopala', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1320, 'San Pedro Ocopetatillo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1321, 'San Pedro Ocotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1322, 'San Pedro Pochutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1323, 'San Pedro Quiatoni', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1324, 'San Pedro Sochiápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1325, 'San Pedro Tapanatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1326, 'San Pedro Taviche', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1327, 'San Pedro Teozacoalco', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1328, 'San Pedro Teutila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1329, 'San Pedro Tidaá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1330, 'San Pedro Topiltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1331, 'San Pedro Totolápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1332, 'Villa de Tututepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1333, 'San Pedro Yaneri', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1334, 'San Pedro Yólox', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1335, 'San Pedro y San Pablo Ayutla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1336, 'Villa de Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1337, 'San Pedro y San Pablo Teposcolula',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1338, 'San Pedro y San Pablo Tequixtepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1339, 'San Pedro Yucunama', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1340, 'San Raymundo Jalpan', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1341, 'San Sebastián Abasolo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1342, 'San Sebastián Coatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1343, 'San Sebastián Ixcapa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1344, 'San Sebastián Nicananduta',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1345, 'San Sebastián Río Hondo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1346, 'San Sebastián Tecomaxtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1347, 'San Sebastián Teitipac',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1348, 'San Sebastián Tutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1349, 'San Simón Almolongas', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1350, 'San Simón Zahuatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1351, 'Santa Ana', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1352, 'Santa Ana Ateixtlahuaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1353, 'Santa Ana Cuauhtémoc', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1354, 'Santa Ana del Valle', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1355, 'Santa Ana Tavela', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1356, 'Santa Ana Tlapacoyan', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1357, 'Santa Ana Yareni', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1358, 'Santa Ana Zegache', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1359, 'Santa Catalina Quierí', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1360, 'Santa Catarina Cuixtla', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1361, 'Santa Catarina Ixtepeji',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1362, 'Santa Catarina Juquila', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1363, 'Santa Catarina Lachatao',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1364, 'Santa Catarina Loxicha', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1365, 'Santa Catarina Mechoacán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1366, 'Santa Catarina Minas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1367, 'Santa Catarina Quiané', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1368, 'Santa Catarina Tayata', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1369, 'Santa Catarina Ticuá', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1370, 'Santa Catarina Yosonotú',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1371, 'Santa Catarina Zapoquila',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1372, 'Santa Cruz Acatepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1373, 'Santa Cruz Amilpas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1374, 'Santa Cruz de Bravo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1375, 'Santa Cruz Itundujia', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1376, 'Santa Cruz Mixtepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1377, 'Santa Cruz Nundaco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1378, 'Santa Cruz Papalutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1379, 'Santa Cruz Tacache de Mina',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1380, 'Santa Cruz Tacahua', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1381, 'Santa Cruz Tayata', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1382, 'Santa Cruz Xitla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1383, 'Santa Cruz Xoxocotlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1384, 'Santa Cruz Zenzontepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1385, 'Santa Gertrudis', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1386, 'Santa Inés del Monte', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1387, 'Santa Inés Yatzeche', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1388, 'Santa Lucía del Camino',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1389, 'Santa Lucía Miahuatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1390, 'Santa Lucía Monteverde',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1391, 'Santa Lucía Ocotlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1392, 'Santa María Alotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1393, 'Santa María Apazco', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1394, 'Santa María la Asunción',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1395, 'Heroica Ciudad de Tlaxiaco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1396, 'Ayoquezco de Aldama', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1397, 'Santa María Atzompa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1398, 'Santa María Camotlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1399, 'Santa María Colotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1400, 'Santa María Cortijo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1401, 'Santa María Coyotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1402, 'Santa María Chachoápam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1403, 'Villa de Chilapa de Díaz',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1404, 'Santa María Chilchotla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1405, 'Santa María Chimalapa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1406, 'Santa María del Rosario',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1407, 'Santa María del Tule', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1408, 'Santa María Ecatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1409, 'Santa María Guelacé', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1410, 'Santa María Guienagati',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1411, 'Santa María Huatulco', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1412, 'Santa María Huazolotitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1413, 'Santa María Ipalapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1414, 'Santa María Ixcatlán', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1415, 'Santa María Jacatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1416, 'Santa María Jalapa del Marqués',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1417, 'Santa María Jaltianguis',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1418, 'Santa María Lachixío', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1419, 'Santa María Mixtequilla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1420, 'Santa María Nativitas', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1421, 'Santa María Nduayaco', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1422, 'Santa María Ozolotepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1423, 'Santa María Pápalo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1424, 'Santa María Peñoles', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1425, 'Santa María Petapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1426, 'Santa María Quiegolani',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1427, 'Santa María Sola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1428, 'Santa María Tataltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1429, 'Santa María Tecomavaca',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1430, 'Santa María Temaxcalapa',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1431, 'Santa María Temaxcaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1432, 'Santa María Teopoxco', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1433, 'Santa María Tepantlali',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1434, 'Santa María Texcatitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1435, 'Santa María Tlahuitoltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1436, 'Santa María Tlalixtac', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1437, 'Santa María Tonameca', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1438, 'Santa María Totolapilla',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1439, 'Santa María Xadani', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1440, 'Santa María Yalina', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1441, 'Santa María Yavesía', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1442, 'Santa María Yolotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1443, 'Santa María Yosoyúa', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1444, 'Santa María Yucuhiti', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1445, 'Santa María Zacatepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1446, 'Santa María Zaniza', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1447, 'Santa María Zoquitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1448, 'Santiago Amoltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1449, 'Santiago Apoala', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1450, 'Santiago Apóstol', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1451, 'Santiago Astata', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1452, 'Santiago Atitlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1453, 'Santiago Ayuquililla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1454, 'Santiago Cacaloxtepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1455, 'Santiago Camotlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1456, 'Santiago Comaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1457, 'Santiago Chazumba', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1458, 'Santiago Choápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1459, 'Santiago del Río', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1460, 'Santiago Huajolotitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1461, 'Santiago Huauclilla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1462, 'Santiago Ihuitlán Plumas',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1463, 'Santiago Ixcuintepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1464, 'Santiago Ixtayutla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1465, 'Santiago Jamiltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1466, 'Santiago Jocotepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1467, 'Santiago Juxtlahuaca', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1468, 'Santiago Lachiguiri', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1469, 'Santiago Lalopa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1470, 'Santiago Laollaga', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1471, 'Santiago Laxopa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1472, 'Santiago Llano Grande', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1473, 'Santiago Matatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1474, 'Santiago Miltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1475, 'Santiago Minas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1476, 'Santiago Nacaltepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1477, 'Santiago Nejapilla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1478, 'Santiago Nundiche', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1479, 'Santiago Nuyoó', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1480, 'Santiago Pinotepa Nacional',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1481, 'Santiago Suchilquitongo',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1482, 'Santiago Tamazola', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1483, 'Santiago Tapextla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1484, 'Villa Tejúpam de la Unión',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1485, 'Santiago Tenango', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1486, 'Santiago Tepetlapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1487, 'Santiago Tetepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1488, 'Santiago Texcalcingo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1489, 'Santiago Textitlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1490, 'Santiago Tilantongo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1491, 'Santiago Tillo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1492, 'Santiago Tlazoyaltepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1493, 'Santiago Xanica', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1494, 'Santiago Xiacuí', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1495, 'Santiago Yaitepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1496, 'Santiago Yaveo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1497, 'Santiago Yolomécatl', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1498, 'Santiago Yosondúa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1499, 'Santiago Yucuyachi', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1500, 'Santiago Zacatepec', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1501, 'Santiago Zoochila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1502, 'Nuevo Zoquiápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1503, 'Santo Domingo Ingenio', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1504, 'Santo Domingo Albarradas',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1505, 'Santo Domingo Armenta', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1506, 'Santo Domingo Chihuitán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1507, 'Santo Domingo de Morelos',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1508, 'Santo Domingo Ixcatlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1509, 'Santo Domingo Nuxaá', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1510, 'Santo Domingo Ozolotepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1511, 'Santo Domingo Petapa', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1512, 'Santo Domingo Roayaga', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1513, 'Santo Domingo Tehuantepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1514, 'Santo Domingo Teojomulco',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1515, 'Santo Domingo Tepuxtepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1516, 'Santo Domingo Tlatayápam',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1517, 'Santo Domingo Tomaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1518, 'Santo Domingo Tonalá', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1519, 'Santo Domingo Tonaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1520, 'Santo Domingo Xagacía', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1521, 'Santo Domingo Yanhuitlán',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1522, 'Santo Domingo Yodohino', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1523, 'Santo Domingo Zanatepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1524, 'Santos Reyes Nopala', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1525, 'Santos Reyes Pápalo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1526, 'Santos Reyes Tepejillo', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1527, 'Santos Reyes Yucuná', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1528, 'Santo Tomás Jalieza', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1529, 'Santo Tomás Mazaltepec',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1530, 'Santo Tomás Ocotepec', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1531, 'Santo Tomás Tamazulapan',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1532, 'San Vicente Coatlán', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1533, 'San Vicente Lachixío', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1534, 'San Vicente Nuñú', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1535, 'Silacayoápam', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1536, 'Sitio de Xitlapehua', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1537, 'Soledad Etla', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1538, 'Villa de Tamazulápam del Progreso',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1539, 'Tanetze de Zaragoza', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1540, 'Taniche', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1541, 'Tataltepec de Valdés', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1542, 'Teococuilco de Marcos Pérez',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1543, 'Teotitlán de Flores Magón',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1544, 'Teotitlán del Valle', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1545, 'Teotongo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1546, 'Tepelmeme Villa de Morelos',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1547, 'Heroica Villa Tezoatlán de Segura y Luna',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1548, 'San Jerónimo Tlacochahuaya',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1549, 'Tlacolula de Matamoros', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1550, 'Tlacotepec Plumas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1551, 'Tlalixtac de Cabrera', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1552, 'Totontepec Villa de Morelos',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1553, 'Trinidad Zaachila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1554, 'La Trinidad Vista Hermosa',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1555, 'Unión Hidalgo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1556, 'Valerio Trujano', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1557, 'San Juan Bautista Valle Nacional',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1558, 'Villa Díaz Ordaz', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1559, 'Yaxe', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1560, 'Magdalena Yodocono de Porfirio Díaz',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1561, 'Yogana', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1562, 'Yutanduchi de Guerrero', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1563, 'Villa de Zaachila', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1564, 'San Mateo Yucutindoo', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1565, 'Zapotitlán Lagunas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1566, 'Zapotitlán Palmas', 20);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1567, 'Santa Inés de Zaragoza',
    20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1568, 'Zimatlán de Álvarez', 20
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1569, 'Acajete', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1570, 'Acateno', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1571, 'Acatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1572, 'Acatzingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1573, 'Acteopan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1574, 'Ahuacatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1575, 'Ahuatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1576, 'Ahuazotepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1577, 'Ahuehuetitla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1578, 'Ajalpan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1579, 'Albino Zertuche', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1580, 'Aljojuca', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1581, 'Altepexi', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1582, 'Amixtlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1583, 'Amozoc', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1584, 'Aquixtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1585, 'Atempan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1586, 'Atexcal', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1587, 'Atlixco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1588, 'Atoyatempan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1589, 'Atzala', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1590, 'Atzitzihuacán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1591, 'Atzitzintla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1592, 'Axutla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1593, 'Ayotoxco de Guerrero', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1594, 'Calpan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1595, 'Caltepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1596, 'Camocuautla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1597, 'Caxhuacan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1598, 'Coatepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1599, 'Coatzingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1600, 'Cohetzala', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1601, 'Cohuecan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1602, 'Coronango', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1603, 'Coxcatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1604, 'Coyomeapan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1605, 'Coyotepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1606, 'Cuapiaxtla de Madero', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1607, 'Cuautempan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1608, 'Cuautinchán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1609, 'Cuautlancingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1610, 'Cuayuca de Andrade', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1611, 'Cuetzalan del Progreso', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1612, 'Cuyoaco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1613, 'Chalchicomula de Sesma', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1614, 'Chapulco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1615, 'Chiautla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1616, 'Chiautzingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1617, 'Chiconcuautla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1618, 'Chichiquila', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1619, 'Chietla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1620, 'Chigmecatitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1621, 'Chignahuapan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1622, 'Chignautla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1623, 'Chila', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1624, 'Chila de la Sal', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1625, 'Honey', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1626, 'Chilchotla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1627, 'Chinantla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1628, 'Domingo Arenas', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1629, 'Eloxochitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1630, 'Epatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1631, 'Esperanza', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1632, 'Francisco Z. Mena', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1633, 'General Felipe Ángeles',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1634, 'Guadalupe', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1635, 'Guadalupe Victoria', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1636, 'Hermenegildo Galeana', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1637, 'Huaquechula', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1638, 'Huatlatlauca', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1639, 'Huauchinango', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1640, 'Huehuetla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1641, 'Huehuetlán el Chico', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1642, 'Huejotzingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1643, 'Hueyapan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1644, 'Hueytamalco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1645, 'Hueytlalpan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1646, 'Huitzilan de Serdán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1647, 'Huitziltepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1648, 'Atlequizayan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1649, 'Ixcamilpa de Guerrero', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1650, 'Ixcaquixtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1651, 'Ixtacamaxtitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1652, 'Ixtepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1653, 'Izúcar de Matamoros', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1654, 'Jalpan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1655, 'Jolalpan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1656, 'Jonotla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1657, 'Jopala', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1658, 'Juan C. Bonilla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1659, 'Juan Galindo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1660, 'Juan N. Méndez', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1661, 'Lafragua', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1662, 'Libres', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1663, 'La Magdalena Tlatlauquitepec',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1664, 'Mazapiltepec de Juárez',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1665, 'Mixtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1666, 'Molcaxac', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1667, 'Cañada Morelos', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1668, 'Naupan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1669, 'Nauzontla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1670, 'Nealtican', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1671, 'Nicolás Bravo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1672, 'Nopalucan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1673, 'Ocotepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1674, 'Ocoyucan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1675, 'Olintla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1676, 'Oriental', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1677, 'Pahuatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1678, 'Palmar de Bravo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1679, 'Pantepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1680, 'Petlalcingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1681, 'Piaxtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1682, 'Puebla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1683, 'Quecholac', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1684, 'Quimixtlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1685, 'Rafael Lara Grajales', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1686, 'Los Reyes de Juárez', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1687, 'San Andrés Cholula', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1688, 'San Antonio Cañada', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1689, 'San Diego la Mesa Tochimiltzingo',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1690, 'San Felipe Teotlalcingo',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1691, 'San Felipe Tepatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1692, 'San Gabriel Chilac', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1693, 'San Gregorio Atzompa', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1694, 'San Jerónimo Tecuanipan',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1695, 'San Jerónimo Xayacatlán',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1696, 'San José Chiapa', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1697, 'San José Miahuatlán', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1698, 'San Juan Atenco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1699, 'San Juan Atzompa', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1700, 'San Martín Texmelucan', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1701, 'San Martín Totoltepec', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1702, 'San Matías Tlalancaleca',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1703, 'San Miguel Ixitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1704, 'San Miguel Xoxtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1705, 'San Nicolás Buenos Aires',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1706, 'San Nicolás de los Ranchos',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1707, 'San Pablo Anicano', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1708, 'San Pedro Cholula', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1709, 'San Pedro Yeloixtlahuaca',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1710, 'San Salvador el Seco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1711, 'San Salvador el Verde', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1712, 'San Salvador Huixcolotla',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1713, 'San Sebastián Tlacotepec',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1714, 'Santa Catarina Tlaltempan',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1715, 'Santa Inés Ahuatempan', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1716, 'Santa Isabel Cholula', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1717, 'Santiago Miahuatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1718, 'Huehuetlán el Grande', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1719, 'Santo Tomás Hueyotlipan',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1720, 'Soltepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1721, 'Tecali de Herrera', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1722, 'Tecamachalco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1723, 'Tecomatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1724, 'Tehuacán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1725, 'Tehuitzingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1726, 'Tenampulco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1727, 'Teopantlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1728, 'Teotlalco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1729, 'Tepanco de López', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1730, 'Tepango de Rodríguez', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1731, 'Tepatlaxco de Hidalgo', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1732, 'Tepeaca', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1733, 'Tepemaxalco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1734, 'Tepeojuma', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1735, 'Tepetzintla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1736, 'Tepexco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1737, 'Tepexi de Rodríguez', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1738, 'Tepeyahualco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1739, 'Tepeyahualco de Cuauhtémoc',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1740, 'Tetela de Ocampo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1741, 'Teteles de Avila Castillo',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1742, 'Teziutlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1743, 'Tianguismanalco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1744, 'Tilapa', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1745, 'Tlacotepec de Benito Juárez',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1746, 'Tlacuilotepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1747, 'Tlachichuca', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1748, 'Tlahuapan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1749, 'Tlaltenango', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1750, 'Tlanepantla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1751, 'Tlaola', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1752, 'Tlapacoya', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1753, 'Tlapanalá', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1754, 'Tlatlauquitepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1755, 'Tlaxco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1756, 'Tochimilco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1757, 'Tochtepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1758, 'Totoltepec de Guerrero', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1759, 'Tulcingo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1760, 'Tuzamapan de Galeana', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1761, 'Tzicatlacoyan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1762, 'Venustiano Carranza', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1763, 'Vicente Guerrero', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1764, 'Xayacatlán de Bravo', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1765, 'Xicotepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1766, 'Xicotlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1767, 'Xiutetelco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1768, 'Xochiapulco', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1769, 'Xochiltepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1770, 'Xochitlán de Vicente Suárez',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1771, 'Xochitlán Todos Santos',
    21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1772, 'Yaonáhuac', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1773, 'Yehualtepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1774, 'Zacapala', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1775, 'Zacapoaxtla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1776, 'Zacatlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1777, 'Zapotitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1778, 'Zapotitlán de Méndez', 21
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1779, 'Zaragoza', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1780, 'Zautla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1781, 'Zihuateutla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1782, 'Zinacatepec', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1783, 'Zongozotla', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1784, 'Zoquiapan', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1785, 'Zoquitlán', 21);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1786, 'Amealco de Bonfil', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1787, 'Pinal de Amoles', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1788, 'Arroyo Seco', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1789, 'Cadereyta de Montes', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1790, 'Colón', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1791, 'Corregidora', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1792, 'Ezequiel Montes', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1793, 'Huimilpan', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1794, 'Jalpan de Serra', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1795, 'Landa de Matamoros', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1796, 'El Marqués', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1797, 'Pedro Escobedo', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1798, 'Peñamiller', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1799, 'Querétaro', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1800, 'San Joaquín', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1801, 'San Juan del Río', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1802, 'Tequisquiapan', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1803, 'Tolimán', 22);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1804, 'Cozumel', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1805, 'Felipe Carrillo Puerto', 23
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1806, 'Isla Mujeres', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1807, 'Othón P. Blanco', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1808, 'Benito Juárez', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1809, 'José María Morelos', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1810, 'Lázaro Cárdenas', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1811, 'Solidaridad', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1812, 'Tulum', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1813, 'Bacalar', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1814, 'Puerto Morelos', 23);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1815, 'Ahualulco', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1816, 'Alaquines', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1817, 'Aquismón', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1818, 'Armadillo de los Infante',
    24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1819, 'Cárdenas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1820, 'Catorce', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1821, 'Cedral', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1822, 'Cerritos', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1823, 'Cerro de San Pedro', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1824, 'Ciudad del Maíz', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1825, 'Ciudad Fernández', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1826, 'Tancanhuitz', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1827, 'Ciudad Valles', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1828, 'Coxcatlán', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1829, 'Charcas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1830, 'Ebano', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1831, 'Guadalcázar', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1832, 'Huehuetlán', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1833, 'Lagunillas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1834, 'Matehuala', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1835, 'Mexquitic de Carmona', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1836, 'Moctezuma', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1837, 'Rayón', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1838, 'Rioverde', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1839, 'Salinas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1840, 'San Antonio', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1841, 'San Ciro de Acosta', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1842, 'San Luis Potosí', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1843, 'San Martín Chalchicuautla',
    24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1844, 'San Nicolás Tolentino', 24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1845, 'Santa Catarina', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1846, 'Santa María del Río', 24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1847, 'Santo Domingo', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1848, 'San Vicente Tancuayalab',
    24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1849, 'Soledad de Graciano Sánchez',
    24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1850, 'Tamasopo', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1851, 'Tamazunchale', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1852, 'Tampacán', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1853, 'Tampamolón Corona', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1854, 'Tamuín', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1855, 'Tanlajás', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1856, 'Tanquián de Escobedo', 24
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1857, 'Tierra Nueva', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1858, 'Vanegas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1859, 'Venado', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1860, 'Villa de Arriaga', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1861, 'Villa de Guadalupe', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1862, 'Villa de la Paz', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1863, 'Villa de Ramos', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1864, 'Villa de Reyes', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1865, 'Villa Hidalgo', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1866, 'Villa Juárez', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1867, 'Axtla de Terrazas', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1868, 'Xilitla', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1869, 'Zaragoza', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1870, 'Villa de Arista', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1871, 'Matlapa', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1872, 'El Naranjo', 24);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1873, 'Ahome', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1874, 'Angostura', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1875, 'Badiraguato', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1876, 'Concordia', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1877, 'Cosalá', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1878, 'Culiacán', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1879, 'Choix', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1880, 'Elota', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1881, 'Escuinapa', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1882, 'El Fuerte', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1883, 'Guasave', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1884, 'Mazatlán', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1885, 'Mocorito', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1886, 'Rosario', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1887, 'Salvador Alvarado', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1888, 'San Ignacio', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1889, 'Sinaloa', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1890, 'Navolato', 25);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1891, 'Aconchi', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1892, 'Agua Prieta', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1893, 'Alamos', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1894, 'Altar', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1895, 'Arivechi', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1896, 'Arizpe', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1897, 'Atil', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1898, 'Bacadéhuachi', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1899, 'Bacanora', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1900, 'Bacerac', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1901, 'Bacoachi', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1902, 'Bácum', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1903, 'Banámichi', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1904, 'Baviácora', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1905, 'Bavispe', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1906, 'Benjamín Hill', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1907, 'Caborca', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1908, 'Cajeme', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1909, 'Cananea', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1910, 'Carbó', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1911, 'La Colorada', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1912, 'Cucurpe', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1913, 'Cumpas', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1914, 'Divisaderos', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1915, 'Empalme', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1916, 'Etchojoa', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1917, 'Fronteras', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1918, 'Granados', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1919, 'Guaymas', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1920, 'Hermosillo', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1921, 'Huachinera', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1922, 'Huásabas', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1923, 'Huatabampo', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1924, 'Huépac', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1925, 'Imuris', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1926, 'Magdalena', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1927, 'Mazatán', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1928, 'Moctezuma', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1929, 'Naco', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1930, 'Nácori Chico', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1931, 'Nacozari de García', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1932, 'Navojoa', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1933, 'Nogales', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1934, 'Onavas', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1935, 'Opodepe', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1936, 'Oquitoa', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1937, 'Pitiquito', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1938, 'Puerto Peñasco', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1939, 'Quiriego', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1940, 'Rayón', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1941, 'Rosario', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1942, 'Sahuaripa', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1943, 'San Felipe de Jesús', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1944, 'San Javier', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1945, 'San Luis Río Colorado', 26
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1946, 'San Miguel de Horcasitas',
    26
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1947, 'San Pedro de la Cueva', 26
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1948, 'Santa Ana', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1949, 'Santa Cruz', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1950, 'Sáric', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1951, 'Soyopa', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1952, 'Suaqui Grande', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1953, 'Tepache', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1954, 'Trincheras', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1955, 'Tubutama', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1956, 'Ures', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1957, 'Villa Hidalgo', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1958, 'Villa Pesqueira', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1959, 'Yécora', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1960, 'General Plutarco Elías Calles',
    26
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1961, 'Benito Juárez', 26);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    1962, 'San Ignacio Río Muerto',
    26
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1963, 'Balancán', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1964, 'Cárdenas', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1965, 'Centla', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1966, 'Centro', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1967, 'Comalcalco', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1968, 'Cunduacán', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1969, 'Emiliano Zapata', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1970, 'Huimanguillo', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1971, 'Jalapa', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1972, 'Jalpa de Méndez', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1973, 'Jonuta', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1974, 'Macuspana', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1975, 'Nacajuca', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1976, 'Paraíso', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1977, 'Tacotalpa', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1978, 'Teapa', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1979, 'Tenosique', 27);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1980, 'Abasolo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1981, 'Aldama', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1982, 'Altamira', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1983, 'Antiguo Morelos', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1984, 'Burgos', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1985, 'Bustamante', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1986, 'Camargo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1987, 'Casas', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1988, 'Ciudad Madero', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1989, 'Cruillas', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1990, 'Gómez Farías', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1991, 'González', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1992, 'Güémez', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1993, 'Guerrero', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1994, 'Gustavo Díaz Ordaz', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1995, 'Hidalgo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1996, 'Jaumave', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1997, 'Jiménez', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1998, 'Llera', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (1999, 'Mainero', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2000, 'El Mante', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2001, 'Matamoros', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2002, 'Méndez', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2003, 'Mier', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2004, 'Miguel Alemán', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2005, 'Miquihuana', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2006, 'Nuevo Laredo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2007, 'Nuevo Morelos', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2008, 'Ocampo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2009, 'Padilla', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2010, 'Palmillas', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2011, 'Reynosa', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2012, 'Río Bravo', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2013, 'San Carlos', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2014, 'San Fernando', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2015, 'San Nicolás', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2016, 'Soto la Marina', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2017, 'Tampico', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2018, 'Tula', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2019, 'Valle Hermoso', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2020, 'Victoria', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2021, 'Villagrán', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2022, 'Xicoténcatl', 28);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2023, 'Amaxac de Guerrero', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2024, 'Apetatitlán de Antonio Carvajal',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2025, 'Atlangatepec', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2026, 'Atltzayanca', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2027, 'Apizaco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2028, 'Calpulalpan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2029, 'El Carmen Tequexquitla', 29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2030, 'Cuapiaxtla', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2031, 'Cuaxomulco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2032, 'Chiautempan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2033, 'Muñoz de Domingo Arenas',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2034, 'Españita', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2035, 'Huamantla', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2036, 'Hueyotlipan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2037, 'Ixtacuixtla de Mariano Matamoros',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2038, 'Ixtenco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2039, 'Mazatecochco de José María Morelos',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2040, 'Contla de Juan Cuamatzi',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2041, 'Tepetitla de Lardizábal',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2042, 'Sanctórum de Lázaro Cárdenas',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2043, 'Nanacamilpa de Mariano Arista',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2044, 'Acuamanala de Miguel Hidalgo',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2045, 'Natívitas', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2046, 'Panotla', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2047, 'San Pablo del Monte', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2048, 'Santa Cruz Tlaxcala', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2049, 'Tenancingo', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2050, 'Teolocholco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2051, 'Tepeyanco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2052, 'Terrenate', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2053, 'Tetla de la Solidaridad',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2054, 'Tetlatlahuca', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2055, 'Tlaxcala', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2056, 'Tlaxco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2057, 'Tocatlán', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2058, 'Totolac', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2059, 'Ziltlaltépec de Trinidad Sánchez Santos',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2060, 'Tzompantepec', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2061, 'Xaloztoc', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2062, 'Xaltocan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2063, 'Papalotla de Xicohténcatl',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2064, 'Xicohtzinco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2065, 'Yauhquemehcan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2066, 'Zacatelco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2067, 'Benito Juárez', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2068, 'Emiliano Zapata', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2069, 'Lázaro Cárdenas', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2070, 'La Magdalena Tlaltelulco',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2071, 'San Damián Texóloc', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2072, 'San Francisco Tetlanohcan',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2073, 'San Jerónimo Zacualpan',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2074, 'San José Teacalco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2075, 'San Juan Huactzinco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2076, 'San Lorenzo Axocomanitla',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2077, 'San Lucas Tecopilco', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2078, 'Santa Ana Nopalucan', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2079, 'Santa Apolonia Teacalco',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2080, 'Santa Catarina Ayometla',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2081, 'Santa Cruz Quilehtla', 29);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2082, 'Santa Isabel Xiloxoxtla',
    29
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2083, 'Acajete', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2084, 'Acatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2085, 'Acayucan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2086, 'Actopan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2087, 'Acula', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2088, 'Acultzingo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2089, 'Camarón de Tejeda', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2090, 'Alpatláhuac', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2091, 'Alto Lucero de Gutiérrez Barrios',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2092, 'Altotonga', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2093, 'Alvarado', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2094, 'Amatitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2095, 'Naranjos Amatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2096, 'Amatlán de los Reyes', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2097, 'Angel R. Cabada', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2098, 'La Antigua', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2099, 'Apazapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2100, 'Aquila', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2101, 'Astacinga', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2102, 'Atlahuilco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2103, 'Atoyac', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2104, 'Atzacan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2105, 'Atzalan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2106, 'Tlaltetela', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2107, 'Ayahualulco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2108, 'Banderilla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2109, 'Benito Juárez', 30);
/* INSERT QUERY a Puerto de Veracruz*/
INSERT INTO towns(id, name, state_id)
VALUES
  (2110, 'Boca del Río', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2111, 'Calcahualco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2112, 'Camerino Z. Mendoza', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2113, 'Carrillo Puerto', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2114, 'Catemaco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2115, 'Cazones de Herrera', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2116, 'Cerro Azul', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2117, 'Citlaltépetl', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2118, 'Coacoatzintla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2119, 'Coahuitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2120, 'Coatepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2121, 'Coatzacoalcos', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2122, 'Coatzintla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2123, 'Coetzala', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2124, 'Colipa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2125, 'Comapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2126, 'Córdoba', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2127, 'Cosamaloapan de Carpio', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2128, 'Cosautlán de Carvajal', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2129, 'Coscomatepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2130, 'Cosoleacaque', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2131, 'Cotaxtla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2132, 'Coxquihui', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2133, 'Coyutla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2134, 'Cuichapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2135, 'Cuitláhuac', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2136, 'Chacaltianguis', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2137, 'Chalma', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2138, 'Chiconamel', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2139, 'Chiconquiaco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2140, 'Chicontepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2141, 'Chinameca', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2142, 'Chinampa de Gorostiza', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2143, 'Las Choapas', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2144, 'Chocamán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2145, 'Chontla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2146, 'Chumatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2147, 'Emiliano Zapata', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2148, 'Espinal', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2149, 'Filomeno Mata', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2150, 'Fortín', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2151, 'Gutiérrez Zamora', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2152, 'Hidalgotitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2153, 'Huatusco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2154, 'Huayacocotla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2155, 'Hueyapan de Ocampo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2156, 'Huiloapan de Cuauhtémoc',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2157, 'Ignacio de la Llave', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2158, 'Ilamatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2159, 'Isla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2160, 'Ixcatepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2161, 'Ixhuacán de los Reyes', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2162, 'Ixhuatlán del Café', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2163, 'Ixhuatlancillo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2164, 'Ixhuatlán del Sureste', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2165, 'Ixhuatlán de Madero', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2166, 'Ixmatlahuacan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2167, 'Ixtaczoquitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2168, 'Jalacingo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2169, 'Xalapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2170, 'Jalcomulco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2171, 'Jáltipan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2172, 'Jamapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2173, 'Jesús Carranza', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2174, 'Xico', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2175, 'Jilotepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2176, 'Juan Rodríguez Clara', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2177, 'Juchique de Ferrer', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2178, 'Landero y Coss', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2179, 'Lerdo de Tejada', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2180, 'Magdalena', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2181, 'Maltrata', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2182, 'Manlio Fabio Altamirano',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2183, 'Mariano Escobedo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2184, 'Martínez de la Torre', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2185, 'Mecatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2186, 'Mecayapan', 30);
/* INSERT QUERY a Puerto de veracruz*/
INSERT INTO towns(id, name, state_id)
VALUES
  (2187, 'Medellín de Bravo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2188, 'Miahuatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2189, 'Las Minas', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2190, 'Minatitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2191, 'Misantla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2192, 'Mixtla de Altamirano', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2193, 'Moloacán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2194, 'Naolinco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2195, 'Naranjal', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2196, 'Nautla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2197, 'Nogales', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2198, 'Oluta', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2199, 'Omealca', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2200, 'Orizaba', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2201, 'Otatitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2202, 'Oteapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2203, 'Ozuluama de Mascareñas',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2204, 'Pajapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2205, 'Pánuco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2206, 'Papantla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2207, 'Paso del Macho', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2208, 'Paso de Ovejas', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2209, 'La Perla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2210, 'Perote', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2211, 'Platón Sánchez', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2212, 'Playa Vicente', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2213, 'Poza Rica de Hidalgo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2214, 'Las Vigas de Ramírez', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2215, 'Pueblo Viejo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2216, 'Puente Nacional', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2217, 'Rafael Delgado', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2218, 'Rafael Lucio', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2219, 'Los Reyes', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2220, 'Río Blanco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2221, 'Saltabarranca', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2222, 'San Andrés Tenejapan', 30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2223, 'San Andrés Tuxtla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2224, 'San Juan Evangelista', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2225, 'Santiago Tuxtla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2226, 'Sayula de Alemán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2227, 'Soconusco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2228, 'Sochiapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2229, 'Soledad Atzompa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2230, 'Soledad de Doblado', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2231, 'Soteapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2232, 'Tamalín', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2233, 'Tamiahua', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2234, 'Tampico Alto', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2235, 'Tancoco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2236, 'Tantima', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2237, 'Tantoyuca', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2238, 'Tatatila', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2239, 'Castillo de Teayo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2240, 'Tecolutla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2241, 'Tehuipango', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2242, 'Álamo Temapache', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2243, 'Tempoal', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2244, 'Tenampa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2245, 'Tenochtitlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2246, 'Teocelo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2247, 'Tepatlaxco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2248, 'Tepetlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2249, 'Tepetzintla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2250, 'Tequila', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2251, 'José Azueta', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2252, 'Texcatepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2253, 'Texhuacán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2254, 'Texistepec', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2255, 'Tezonapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2256, 'Tierra Blanca', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2257, 'Tihuatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2258, 'Tlacojalpan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2259, 'Tlacolulan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2260, 'Tlacotalpan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2261, 'Tlacotepec de Mejía', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2262, 'Tlachichilco', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2263, 'Tlalixcoyan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2264, 'Tlalnelhuayocan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2265, 'Tlapacoyan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2266, 'Tlaquilpa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2267, 'Tlilapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2268, 'Tomatlán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2269, 'Tonayán', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2270, 'Totutla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2271, 'Tuxpan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2272, 'Tuxtilla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2273, 'Ursulo Galván', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2274, 'Vega de Alatorre', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2275, 'Veracruz', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2276, 'Villa Aldama', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2277, 'Xoxocotla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2278, 'Yanga', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2279, 'Yecuatla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2280, 'Zacualpan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2281, 'Zaragoza', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2282, 'Zentla', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2283, 'Zongolica', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2284, 'Zontecomatlán de López y Fuentes',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2285, 'Zozocolco de Hidalgo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2286, 'Agua Dulce', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2287, 'El Higo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2288, 'Nanchital de Lázaro Cárdenas del Río',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2289, 'Tres Valles', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2290, 'Carlos A. Carrillo', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2291, 'Tatahuicapan de Juárez',
    30
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2292, 'Uxpanapa', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2293, 'San Rafael', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2294, 'Santiago Sochiapan', 30);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2295, 'Abalá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2296, 'Acanceh', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2297, 'Akil', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2298, 'Baca', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2299, 'Bokobá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2300, 'Buctzotz', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2301, 'Cacalchén', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2302, 'Calotmul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2303, 'Cansahcab', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2304, 'Cantamayec', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2305, 'Celestún', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2306, 'Cenotillo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2307, 'Conkal', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2308, 'Cuncunul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2309, 'Cuzamá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2310, 'Chacsinkín', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2311, 'Chankom', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2312, 'Chapab', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2313, 'Chemax', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2314, 'Chicxulub Pueblo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2315, 'Chichimilá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2316, 'Chikindzonot', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2317, 'Chocholá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2318, 'Chumayel', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2319, 'Dzán', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2320, 'Dzemul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2321, 'Dzidzantún', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2322, 'Dzilam de Bravo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2323, 'Dzilam González', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2324, 'Dzitás', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2325, 'Dzoncauich', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2326, 'Espita', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2327, 'Halachó', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2328, 'Hocabá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2329, 'Hoctún', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2330, 'Homún', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2331, 'Huhí', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2332, 'Hunucmá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2333, 'Ixil', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2334, 'Izamal', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2335, 'Kanasín', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2336, 'Kantunil', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2337, 'Kaua', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2338, 'Kinchil', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2339, 'Kopomá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2340, 'Mama', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2341, 'Maní', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2342, 'Maxcanú', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2343, 'Mayapán', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2344, 'Mérida', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2345, 'Mocochá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2346, 'Motul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2347, 'Muna', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2348, 'Muxupip', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2349, 'Opichén', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2350, 'Oxkutzcab', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2351, 'Panabá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2352, 'Peto', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2353, 'Progreso', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2354, 'Quintana Roo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2355, 'Río Lagartos', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2356, 'Sacalum', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2357, 'Samahil', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2358, 'Sanahcat', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2359, 'San Felipe', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2360, 'Santa Elena', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2361, 'Seyé', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2362, 'Sinanché', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2363, 'Sotuta', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2364, 'Sucilá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2365, 'Sudzal', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2366, 'Suma', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2367, 'Tahdziú', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2368, 'Tahmek', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2369, 'Teabo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2370, 'Tecoh', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2371, 'Tekal de Venegas', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2372, 'Tekantó', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2373, 'Tekax', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2374, 'Tekit', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2375, 'Tekom', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2376, 'Telchac Pueblo', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2377, 'Telchac Puerto', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2378, 'Temax', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2379, 'Temozón', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2380, 'Tepakán', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2381, 'Tetiz', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2382, 'Teya', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2383, 'Ticul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2384, 'Timucuy', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2385, 'Tinum', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2386, 'Tixcacalcupul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2387, 'Tixkokob', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2388, 'Tixmehuac', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2389, 'Tixpéhual', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2390, 'Tizimín', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2391, 'Tunkás', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2392, 'Tzucacab', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2393, 'Uayma', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2394, 'Ucú', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2395, 'Umán', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2396, 'Valladolid', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2397, 'Xocchel', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2398, 'Yaxcabá', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2399, 'Yaxkukul', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2400, 'Yobaín', 31);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2401, 'Apozol', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2402, 'Apulco', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2403, 'Atolinga', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2404, 'Benito Juárez', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2405, 'Calera', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2406, 'Cañitas de Felipe Pescador',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2407, 'Concepción del Oro', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2408, 'Cuauhtémoc', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2409, 'Chalchihuites', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2410, 'Fresnillo', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2411, 'Trinidad García de la Cadena',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2412, 'Genaro Codina', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2413, 'General Enrique Estrada',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2414, 'General Francisco R. Murguía',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2415, 'El Plateado de Joaquín Amaro',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2416, 'General Pánfilo Natera',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2417, 'Guadalupe', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2418, 'Huanusco', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2419, 'Jalpa', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2420, 'Jerez', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2421, 'Jiménez del Teul', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2422, 'Juan Aldama', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2423, 'Juchipila', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2424, 'Loreto', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2425, 'Luis Moya', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2426, 'Mazapil', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2427, 'Melchor Ocampo', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2428, 'Mezquital del Oro', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2429, 'Miguel Auza', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2430, 'Momax', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2431, 'Monte Escobedo', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2432, 'Morelos', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2433, 'Moyahua de Estrada', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2434, 'Nochistlán de Mejía', 32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2435, 'Noria de Ángeles', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2436, 'Ojocaliente', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2437, 'Pánuco', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2438, 'Pinos', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2439, 'Río Grande', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2440, 'Sain Alto', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2441, 'El Salvador', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2442, 'Sombrerete', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2443, 'Susticacán', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2444, 'Tabasco', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2445, 'Tepechitlán', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2446, 'Tepetongo', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2447, 'Teúl de González Ortega',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2448, 'Tlaltenango de Sánchez Román',
    32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2449, 'Valparaíso', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2450, 'Vetagrande', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2451, 'Villa de Cos', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2452, 'Villa García', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2453, 'Villa González Ortega', 32
  );
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2454, 'Villa Hidalgo', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2455, 'Villanueva', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2456, 'Zacatecas', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (2457, 'Trancoso', 32);
/* INSERT QUERY */
INSERT INTO towns(id, name, state_id)
VALUES
  (
    2458, 'Santa María de la Paz', 32
  );
