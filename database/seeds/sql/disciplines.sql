INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (201,'M1','Matemáticas','Construye e interpreta modelos matemáticos mediante la aplicación de procedimientos aritméticos, algebraicos, geométricos y variacionales, para la comprensión y análisis de situaciones reales, hipotéticas o formales','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (202,'M2','Matemáticas','Formula y resuelve problemas matemáticos, aplicando diferentes enfoques.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (203,'M3','Matemáticas','Explica e interpreta los resultados obtenidos mediante procedimientos matemáticos y los contrasta con modelos establecidos o situaciones reales','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (204,'M4','Matemáticas','Argumenta la solución obtenida de un problema, con métodos numéricos, gráficos, analíticos o variacionales, mediante el lenguaje verbal, matemático y el uso de las tecnologías de la información y la comunicación','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (205,'M5','Matemáticas','Analiza las relaciones entre dos o más variables de un proceso social o natural para determinar o estimar su comportamiento.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (206,'M6','Matemáticas','Cuantifica, representa y contrasta experimental o matemáticamente las magnitudes del espacio y las propiedades físicas de los objetos que lo rodean','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (207,'M7','Matemáticas','Elige un enfoque determinista o uno aleatorio para el estudio de un proceso o fenómeno, y argumenta su pertinencia.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (208,'M8','Matemáticas','Interpreta tablas, gráficas, mapas, diagramas y textos con símbolos matemáticos y científicos','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (209,'CE1','Ciencias Experimentales','Establece la interrelación entre la ciencia, la tecnología, la sociedad y el ambiente en contextos históricos y sociales específicos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (210,'CE2','Ciencias Experimentales','Fundamenta opiniones sobre los impactos de la ciencia y la tecnología en su vida cotidiana, asumiendo consideraciones éticas','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (211,'CE3','Ciencias Experimentales','Identifica problemas, formula preguntas de carácter científico y plantea las hipótesis necesarias para responderlas','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (212,'CE4','Ciencias Experimentales','Obtiene, registra y sistematiza la información para responder a preguntas de carácter científico, consultando fuentes relevantes y realizando experimentos pertinentes.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (213,'CE5','Ciencias Experimentales','Contrasta los resultados obtenidos en una investigación o experimento con hipótesis previas y comunica sus conclusiones.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (214,'CE6','Ciencias Experimentales','Valora las preconcepciones personales o comunes sobre diversos fenómenos naturales a partir de evidencias científicas.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (215,'CE7','Ciencias Experimentales','Hace explícitas las nociones científicas que sustentan los procesos para la solución de problemas cotidianos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (216,'CE8','Ciencias Experimentales','Explica el funcionamiento de máquinas de uso común a partir de nociones científicas.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (217,'CE9','Ciencias Experimentales','Diseña modelos o prototipos para resolver problemas, satisfacer necesidades o demostrar principios científicos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (218,'CE10','Ciencias Experimentales','Relaciona las expresiones simbólicas de un fenómeno de la naturaleza y los rasgos observables a simple vista o mediante instrumentos o modelos científicos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (219,'CE11','Ciencias Experimentales','Analiza las leyes generales que rigen el funcionamiento del medio físico y valora las acciones humanas de impacto ambiental.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (220,'CE12','Ciencias Experimentales','Decide sobre el cuidado de su salud a partir del conocimiento de su cuerpo, sus procesos vitales y el entorno al que pertenece.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (221,'CE13','Ciencias Experimentales','Relaciona los niveles de organización química, biológica, física y ecológica de los sistemas vivos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (222,'CE14','Ciencias Experimentales','Aplica normas de seguridad en el manejo de sustancias, instrumentos y equipo en la realización de actividades de su vida cotidiana.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (223,'CS1','Ciencias Sociales','Identifica el conocimiento social y humanista como una construcción en constante transformación.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (224,'CS2','Ciencias Sociales','Sitúa hechos históricos fundamentales que han tenido lugar en distintas épocas en México y el mundo con relación al presente.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (225,'CS3','Ciencias Sociales','Interpreta su realidad social a partir de los procesos históricos locales, nacionales e internacionales que la han configurado','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (226,'CS4','Ciencias Sociales','Valora las diferencias sociales, políticas, económicas, étnicas, culturales y de género y las desigualdades que inducen.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (227,'CS5','Ciencias Sociales','Establece la relación entre las dimensiones políticas, económicas, culturales y geográficas de un acontecimiento.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (228,'CS6','Ciencias Sociales','Analiza con visión emprendedora los factores y elementos fundamentales que intervienen en la productividad y competitividad de una organización y su relación con el entorno socioeconómico','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (229,'CS7','Ciencias Sociales','Evalúa las funciones de las leyes y su transformación en el tiempo.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (230,'CS8','Ciencias Sociales','Compara las características democráticas y autoritarias de diversos sistemas sociopolíticos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (231,'CS9','Ciencias Sociales','Analiza las funciones de las instituciones del Estado Mexicano y la manera en que impactan su vida.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (232,'CS10','Ciencias Sociales','Valora distintas prácticas sociales mediante el reconocimiento de sus significados dentro de un sistema cultural, con una actitud de respeto','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (233,'CO1','Comunicación','Identifica, ordena e interpreta las ideas, datos y conceptos explícitos e implícitos en un texto, considerando el contexto en el que se generó y en el que se recibe','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (234,'CO2','Comunicación','Evalúa un texto mediante la comparación de su contenido con el de otros, en función de sus conocimientos previos y nuevos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (235,'CO3','Comunicación','Plantea supuestos sobre los fenómenos naturales y culturales de su entorno con base en la consulta de diversas fuentes.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (236,'CO4','Comunicación','Produce textos con base en el uso normativo de la lengua, considerando la intención y situación comunicativa.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (237,'CO5','Comunicación','Expresa ideas y conceptos en composiciones coherentes y creativas, con introducciones, desarrollo y conclusiones claras.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (238,'CO6','Comunicación','Argumenta un punto de vista en público de manera precisa, coherente y creativa.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (239,'CO7','Comunicación','Valora y describe el papel del arte, la literatura y los medios de comunicación en la recreación o la transformación de una cultura, teniendo en cuenta los propósitos comunicativos de distintos géneros.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (240,'CO8','Comunicación','Valora el pensamiento lógico en el proceso comunicativo en su vida cotidiana y académica.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (241,'CO9','Comunicación','Analiza y compara el origen, desarrollo y diversidad de los sistemas y medios de comunicación.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (242,'CO10','Comunicación','Identifica e interpreta la idea general y posible desarrollo de un mensaje oral o escrito en una segunda lengua, recurriendo a conocimientos previos, elementos no verbales y contexto cultural.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (243,'CO11','Comunicación','Se comunica en una lengua extranjera mediante un discurso lógico, oral o escrito, congruente con la situación comunicativa.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (244,'CO12','Comunicación','Utiliza las tecnologías de la información y comunicación para investigar, resolver problemas, producir materiales y transmitir información.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (245,'H1','Humanidades','Analiza y evalúa la importancia de la filosofía en su formación personal y colectiva.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (246,'H2','Humanidades','Caracteriza las cosmovisiones de su comunidad','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (247,'H3','Humanidades','Examina y argumenta, de manera crítica y reflexiva, diversos problemas filosóficos relacionados con la actuación humana, potenciando su dignidad, libertad y autodirección','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (248,'H4','Humanidades','Distingue la importancia de la ciencia y la tecnología y su trascendencia en el desarrollo de su comunidad con fundamentos filosóficos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (249,'H5','Humanidades','Construye, evalúa y mejora distintos tipos de argumentos, sobre su vida cotidiana de acuerdo con los principios lógicos.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (250,'H6','Humanidades','Defiende con razones coherentes sus juicios sobre aspectos de su entorno.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (251,'H7','Humanidades','Escucha y discierne los juicios de los otros de una manera respetuosa.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (252,'H8','Humanidades','Identifica los supuestos de los argumentos con los que se le trata de convencer y analiza la confiabilidad de las fuentes de una manera crítica y justificada','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (253,'H9','Humanidades','Evalúa la solidez de la evidencia para llegar a una conclusión argumentativa a través del diálogo.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (254,'H10','Humanidades','Asume una posición personal (crítica, respetuosa y digna) y objetiva, basada en la razón (lógica y epistemológica), en la ética y en los valores frente a las diversas manifestaciones del arte.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (255,'H11','Humanidades','Analiza de manera reflexiva y critica las manifestaciones artísticas a partir de consideraciones históricas y filosóficas para reconocerlas como parte del patrimonio cultural.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (256,'H12','Humanidades','Desarrolla su potencial artístico, como una manifestación de su personalidad y arraigo de la identidad, considerando elementos objetivos de apreciación estética.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (257,'H13','Humanidades','Analiza y resuelve de manera reflexiva problemas éticos relacionados con el ejercicio de su autonomía, libertad y responsabilidad en su vida cotidiana.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (258,'H14','Humanidades','Valora los fundamentos en los que se sustentan los derechos humanos y los practica de manera crítica en la vida cotidiana.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (259,'H15','Humanidades','Sustenta juicios a través de valores éticos en los distintos ámbitos de la vida.','Acuerdo 444','D');

INSERT INTO `competences`(`id`, `code`, `section`, `title`, `law`, `kind`)
VALUES (260,'H16','Humanidades','Asume responsablemente la relación que tiene consigo mismo, con los otros y con el entorno natural y sociocultural, mostrando una actitud de respeto y tolerancia.','Acuerdo 444','D');