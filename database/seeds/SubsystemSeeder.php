<?php

use Illuminate\Database\Seeder;

class SubsystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/seeds/sql/subsystems.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Subsistemas Cargados!');
    }
}
