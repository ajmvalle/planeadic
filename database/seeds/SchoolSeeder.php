<?php

use App\School;
use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/seeds/sql/schools.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Escuelas Cargadas!');
    }
}
