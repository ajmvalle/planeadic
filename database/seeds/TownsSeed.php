<?php

use Illuminate\Database\Seeder;

class TownsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loadStates();
        $this->loadTowns();
    }

    private function loadStates()
    {
        $path = 'database/seeds/sql/states.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Estados Cargados!');
    }

    private function loadTowns()
    {
        $path = 'database/seeds/sql/towns.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Municipios Cargados!');
    }
}
