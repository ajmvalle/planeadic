<?php

use Illuminate\Database\Seeder;

class CompetencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loadGenerics();
        $this->loadAttributes();
        $this->loadDisciplines();
        $this->loadProfessionals();
    }

    private function loadGenerics()
    {
        $path = 'database/seeds/sql/generics.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Competencias Genéricas Cargadas!');
    }

    private function loadAttributes()
    {
        $path = 'database/seeds/sql/attributes.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Atributos de Genéricas!');
    }

    private function loadDisciplines()
    {
        $path = 'database/seeds/sql/disciplines.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Disciplinares!');
    }

    private function loadProfessionals()
    {
        $path = 'database/seeds/sql/professionals.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Profesionales!');
    }
}
