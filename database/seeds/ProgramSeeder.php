<?php

use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loadFormats();
        $this->loadCourses();
        $this->loadPrograms();
        $this->loadCentral();
        $this->loadBlocks();
    }

    private function loadFormats()
    {
        $path = 'database/seeds/sql/formats.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Formatos Cargados!');
    }

    private function loadCourses()
    {
        $path = 'database/seeds/sql/courses.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Asignaturas Cargadas!');
    }

    private function loadPrograms()
    {
        $path = 'database/seeds/sql/programs.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Programas de Asignatura Cargadas!');
    }

    private function loadCentral()
    {
        $path = 'database/seeds/sql/centralcontents.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Contenidos Centrales!');
    }

    private function loadBlocks()
    {
        $path = 'database/seeds/sql/blocks.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Bloques de programa!');
    }

}
