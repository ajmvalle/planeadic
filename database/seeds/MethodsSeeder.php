<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/seeds/sql/methods.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Metodos Cargadas!');
    }
}
