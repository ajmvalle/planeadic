<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Alfredo',
            'lastname' => 'Morales',
            'email' => 'ajmvalle@gmail.com',
            'password' => bcrypt('dnc2007'),
            'remember_token' => str_random(10),
            'isAdmin' => true,
            'activated' => true,
            'activate_token' => Str::random(20),
        ]);
    }
}
