<?php

use Illuminate\Database\Seeder;

class CostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->loadCosts();
    }

    private function loadCosts()
    {
        $path = 'database/seeds/sql/costs.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Costos de membresías Cargados!');
    }
}
