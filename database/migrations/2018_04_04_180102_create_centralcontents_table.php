<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentralcontentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centralcontents', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('component');
            $table->string('axis',256);
            $table->text('specifics')->nullable();
            $table->text('expecteds')->nullable();
            $table->text('products')->nullable();
            $table->text('process')->nullable();
            $table->unsignedInteger('program_id');
            $table->foreign('program_id')->references('id')->on('programs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centralcontents');
    }
}
