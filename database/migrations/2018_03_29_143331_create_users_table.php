<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('isAdmin')->default(false);
            $table->string('avatar')->default('default.png');
            $table->unsignedInteger('school_id')->nullable();
            $table->foreign('school_id')->references('id')->on('schools');
            $table->string('title')->nullable();
            $table->string('profession')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('sex')->default('male');
            $table->string('context')->nullable();
            $table->string('activate_token', 20)->nullable();
            $table->boolean('activated')->default(false);
            $table->string('taxid')->nullable();
            $table->string('adviser')->nullable();
            $table->boolean('authlogo')->default(false);
            $table->boolean('principal')->default(false);
            $table->string('academychief')->nullable();
            $table->string('teacherschief')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
