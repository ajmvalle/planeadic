<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicators', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('tool_id');
            $table->foreign('tool_id')->references('id')->on('tools');

            $table->string('title','200');

            $table->text('detail1')->nullable();
            $table->text('detail2')->nullable();
            $table->text('detail3')->nullable();
            $table->text('detail4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicators');
    }
}
