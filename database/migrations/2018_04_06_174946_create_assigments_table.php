<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssigmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cycle');

            $table->unsignedInteger('school_id')->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedInteger('program_id')->unsigned()->nullable();
            $table->foreign('program_id')->references('id')->on('programs');

            $table->boolean('filed')->default('0');

            $table->string('careers')->nullable();
            $table->string('groups')->nullable();
            $table->text('context')->nullable();

            $table->unsignedInteger('format_id')->nullable();
            $table->foreign('format_id')->references('id')->on('formats');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigments');
    }
}
