<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subsystems', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name'); //Unidad de Educación Media Superior Tecnológica Industrial y de Servicios
            $table->string('shortname'); //UEMSTIS

            $table->string('office'); //Secretaría de Educación Pública
            $table->string('shortoffice'); // SEP
            $table->string('department'); // Subsecretaría de Educación Media Superior
            $table->string('shortdepartment'); // SEMS
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subsystems');
    }
}
