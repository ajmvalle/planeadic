<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');  //CETIS, CBTIS, etc
            $table->string('number'); // 15, 190
            $table->string('subname')->nullable(); //Morelos

            $table->string('address')->nullable(); //Nullable
            $table->string('officialkey')->nullable();//Clave

            $table->unsignedInteger('town_id');  //Mpio
            $table->foreign('town_id')->references('id')->on('towns');

            $table->unsignedInteger('subsystem_id');//Subsistema
            $table->foreign('subsystem_id')->references('id')->on('subsystems');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
