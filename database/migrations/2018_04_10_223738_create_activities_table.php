<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('plan_id');
            $table->foreign('plan_id')->references('id')->on('plans');

            $table->unsignedInteger('ordinal')->default(0);

            $table->boolean('extra');
            $table->string('section');

            $table->text('teacher')->nullable();
            $table->text('student')->nullable();
            $table->decimal('time')->nullable();
            $table->decimal('time_student')->nullable();
            $table->string('product')->nullable();
            $table->string('resources')->nullable();
            $table->string('eva_kind')->nullable();
            $table->string('eva_tool')->nullable();
            $table->decimal('eva_percent')->nullable();

            $table->unsignedInteger('attribute_id')->nullable();
            $table->foreign('attribute_id')->references('id')->on('attributes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
