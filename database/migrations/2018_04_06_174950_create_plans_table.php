<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('block_id')->nullable();
            $table->foreign('block_id')->references('id')->on('blocks');

            $table->unsignedInteger('assigment_id');
            $table->foreign('assigment_id')->references('id')->on('assigments');

            $table->string('title');
            $table->tinyInteger('partial')->nullable();
            $table->date('elaboration');
            $table->date('apply_from');
            $table->date('apply_to');

            $table->text('purpose');// Propósito de la planeación didáctica por asignatura
            $table->text('purpose_area')->nullable();// Propósito del campo disciplinar o carrera
            $table->text('expecteds')->nullable();
            $table->text('products')->nullable();
            $table->text('specifics')->nullable();
            $table->text('generics')->nullable();
            $table->text('disciplines')->nullable();
            $table->string('hse')->nullable();
            $table->text('references')->nullable();

            $table->string('integrator')->nullable();
            $table->string('trans_integrator')->nullable();
            $table->string('trans_course')->nullable();

            $table->boolean('space')->nullable();
            $table->boolean('energy')->nullable();
            $table->boolean('diversity')->nullable();
            $table->boolean('thetime')->nullable();
            $table->boolean('matter')->nullable();


            $table->text('factual')->nullable();
            $table->text('fundamental')->nullable();
            $table->text('subsidiary')->nullable();
            $table->text('procedimental')->nullable();
            $table->text('attitudinal')->nullable();
            $table->text('profesionalcontent')->nullable();

            //resources
            $table->text('equipment')->nullable();
            $table->text('biosecurity')->nullable();
            $table->text('reactives')->nullable();
            $table->text('material')->nullable();

            $table->text('transversality')->nullable();
            $table->text('profile_ambits')->nullable();
            $table->text('extrareferences')->nullable();
            $table->text('collaborations')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
