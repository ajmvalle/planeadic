<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('summary');

            $table->unsignedInteger('start_id')->nullable();
            $table->foreign('start_id')->references('id')->on('actions');
            $table->unsignedInteger('dev_id')->nullable();
            $table->foreign('dev_id')->references('id')->on('actions');
            $table->unsignedInteger('closure_id')->nullable();
            $table->foreign('closure_id')->references('id')->on('actions');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('reference')->nullable();
            $table->string('image')->default('default.png');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('methods');
    }
}
