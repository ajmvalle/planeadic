<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('kind');
            $table->unsignedInteger('assigment_id')->nullable();
            $table->foreign('assigment_id')->references('id')->on('assigments');
            $table->text('notes')->nullable();

            $table->string('header1')->nullable(); //Deficiente o Nunca
            $table->string('header2')->nullable(); //Regular o Menos de las veces
            $table->string('header3')->nullable(); //Bueno o Más de las veces
            $table->string('header4')->nullable(); //Excelente o Siempre


            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools');
    }
}
