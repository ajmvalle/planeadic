<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Membership extends Model
{

    protected $fillable = [
        'user_id',
        'from',
        'to',
        'payment_id'
    ];

    protected $casts = [
        'from' => 'date',
        'to' => 'date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    public function getIsActiveAttribute()
    {
        $nowStr = Carbon::now()->toDateString(); //Elimino fecha y hora p/comparar
        $now = (new Carbon)->parse($nowStr);
        return $now->lessThan($this->to);
    }

    public function setPaymentIdAttribute($value)
    {
        $this->attributes['payment_id'] = $value;
    }

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = $value;
    }

    public function setToAttribute($value)
    {
        $this->attributes['to'] = $value;
    }
}
