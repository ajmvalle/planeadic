<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $fillable = [
        'assigment_id', 'title', 'notes', 'kind', 'user_id', 'header1', 'header2', 'header3', 'header4',
    ];


    public function indicators()
    {
        return $this->hasMany(Indicator::class);
    }

    public function assigment()
    {
        return $this->belongsTo(Assigment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeByUser($query, User $user)
    {
        $query->where('user_id', $user->id);
    }

    public function getDescAttribute()
    {
        $resp = "Rúbrica";

        if ($this->kind == 'list')
            $resp = 'Lista de Cotejo';

        if ($this->kind == 'scale')
            $resp = 'Escala Estimativa';

        return $resp;
    }


}
