<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $fillable= ['program_id','centralcontent_id','description','situations','competence'];

    public function competences()
    {
        return $this->belongsToMany(Competence::class,'competence_block');
    }

    public function discompetences()
    {
        return $this->belongsToMany(Competence::class,'competence_block')->where('kind','D');
    }

    public function profcompetences()
    {
        return $this->belongsToMany(Competence::class,'competence_block')->where('kind','P');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'attribute_block');
    }


    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function centralcontent()
    {
        return $this->belongsTo(Centralcontent::class);
    }

    public function getUrlAttribute()
    {
        return route('blocks.show', [$this->id]);
    }

    public function getAxisAttribute()
    {
        $resp ="N/A";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->axis;
        }
        return $resp;
    }

    public function getComponentAttribute()
    {
        $resp ="N/A";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->component;
        }
        return $resp;
    }

    public function getTitleAttribute()
    {
        $resp ="N/A";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->title;
        }
        return $resp;
    }

    public function getExpectedsAttribute()
    {
        $resp ="";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->expecteds;
        }
        return $resp;
    }

    public function getProcessAttribute()
    {
        $resp ="";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->process;
        }
        return $resp;
    }

    public function getProductsAttribute()
    {
        $resp ="";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->products;
        }
        return $resp;
    }

    public function getSpecificsAttribute()
    {
        $resp ="";
        if(!is_null($this->centralcontent))
        {
            $resp = $this->centralcontent->specifics;
        }
        return $resp;
    }
}
