<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centralcontent extends Model
{
    public $fillable=[
        'title','axis','component','program_id','expecteds','products','process','specifics'
    ];

    public $timestamps = false;


    public function components()
    {
        return $this->belongsToMany(Component::class,'components_centralcontens');
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

}
