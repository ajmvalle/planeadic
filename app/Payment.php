<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable= [
        'order',
        'reference',
        'status',
        'way',
        'amount',
        'user_id',
        'expires',
        'membershipcost_id'
    ];

    protected $casts= [
        'amount'=>'float',
        'expires'=> 'date'
    ];

    public function membershipcost(){
        return $this->belongsTo(Membershipcost::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function setOrderAttribute($value)
    {
        $this->attributes['order']  = $value;
    }

    public function setReferenceAttribute($value)
    {
        $this->attributes['reference'] = $value;
    }


    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value;
    }

    public function setWayAttribute($value)
    {
        $this->attributes['way'] = $value;
    }

    public function setExpiresAttribute($value)
    {
        $this->attributes['expires'] = $value;
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = $value;
    }

    public function setMembershipcostIdAttribute($value)
    {
        $this->attributes['membershipcost_id'] = $value;
    }



}
