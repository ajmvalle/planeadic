<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    protected $fillable= ['plan_id', 'image','description','elaboration','kind','activity_id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'elaboration'
    ];

    protected $casts = [
        'elaboration' => 'date'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
}
