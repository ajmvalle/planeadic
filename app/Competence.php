<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    public $fillable = [
        'code', 'title', 'kind', 'law', 'section'
    ];
    public $timestamps = false;

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

    public function blocks()
    {
        return $this->belongsToMany(Block::class, 'competence_block');
    }

    public function programs()
    {
        return $this->belongsToMany(Program::class, 'competence_program');
    }

    public function addBlock(Block $block)
    {
        $this->blocks()->detach($block->id);
        $this->blocks()->attach($block->id);
    }

    public function addProgram(Program $program)
    {
        $this->programs()->detach($program->id);
        $this->programs()->attach($program->id);
    }

    public function removeBlock(Block $block)
    {
        $this->blocks()->detach($block->id);
    }

    public function removeProgram(Program $program)
    {
        $this->programs()->detach($program->id);
    }
}
