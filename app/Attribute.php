<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public $fillable= [
        'title', 'code','competence_id'
    ];

    public $timestamps = false;

    public function competence()
    {
        return $this->belongsTo(Competence::class);
    }


    public function blocks()
    {
        return $this->belongsToMany(Block::class,'attribute_block');
    }

    public function programs()
    {
        return $this->belongsToMany(Program::class,'attribute_program');
    }

}
