<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
   protected $fillable = [
       'name','module', 'area','specialty_id'
   ];

   public function programs()
   {
       return $this->hasMany(Program::class);
   }

   public function specialty()
   {
       return $this->belongsTo(Specialty::class);
   }


}
