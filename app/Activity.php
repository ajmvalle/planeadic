<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'plan_id',
        'ordinal',
        'extra',
        'section',
        'teacher',
        'student',
        'time',
        'time_student',
        'product',
        'resources',
        'eva_kind',
        'eva_tool',
        'eva_percent',
        'attribute_id',
        'order'
    ];

    protected $casts = [
        'extra' => 'boolean',
        'time' => 'float',
        'time_student' => 'float'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function getToolAttribute()
    {

        if($this->eva_tool == '1')
            return  "Rúbrica";

        if($this->eva_tool == '2')
            return "Guía de Observación";

        if($this->eva_tool == '3')
            return "Lista de Cotejo";

        if($this->eva_tool == '4')
            return "Examen";

        if($this->eva_tool == '5')
            return "Escala Estimativa";

    }

    public function getKindevaAttribute()
    {

        if($this->eva_kind == '1')
            return  "Heteroevaluación";

        if($this->eva_kind == '2')
            return "Autoevaluación";

        if($this->eva_kind == '3')
            return "Coevaluación";

        if($this->eva_kind == '4')
            return "Mixta";

    }

    public function setOrderAttribute($data)
    {
        $this->attributes['order'] = $data;
    }
}
