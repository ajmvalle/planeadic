<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $fillable=[
        'title','summary', 'reference','image','start_id', 'dev_id', 'closure_id'
    ];

    public function start()
    {
        return $this->hasOne(Action::class,'id','start_id');
    }

    public function dev()
    {
        return $this->hasOne(Action::class,'id','dev_id');
    }

    public function closure()
    {
        return $this->hasOne(Action::class,'id','closure_id');
    }

    public function getImagenAttribute()
    {
        $avatar = asset('uploads/methods/'.$this->image);

        return $avatar;
    }
}
