<?php

namespace App\Http\Controllers;

use App\Centralcontent;
use App\Component;
use App\Program;
use Illuminate\Http\Request;

class CentralcontentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $centralcontents = Centralcontent::query()
            ->paginate();
        return view('maintenance.centralcontents.index', compact('centralcontents'));
    }

    public function add()
    {
        $programs = Program::all()->pluck('name', 'id')->prepend('Seleccione el programa de asignatura', '');
        return view('maintenance.centralcontents.add', compact('programs'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'axis' => 'required',
            'component' => 'required',
            'program_id' => 'required',
            'expecteds' => 'required',
        ]);

        $centralcontent = new Centralcontent();
        $centralcontent->create($request->all());

        return redirect(route('maintenance.centralcontents.index'));
    }

    public function edit($id)
    {
        $centralcontent = Centralcontent::find($id);
        $programs = Program::all()->pluck('name', 'id')->prepend('Seleccione el plan de la asignatura', '');
        return \View::make('maintenance.centralcontents.edit', compact('centralcontent', 'programs'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'axis' => 'required',
            'component' => 'required',
            'program_id' => 'required',
            'expecteds' => 'required'
        ]);

        $centralcontent = Centralcontent::find($request->id);
        $centralcontent->update($request->all());

        return redirect(route('maintenance.centralcontents.index'));
    }

    public function destroy($id)
    {
        $centralcontent = Centralcontent::find($id);
        $centralcontent->delete();
        return redirect(route('maintenance.centralcontents.index'));
    }
    

}
