<?php

namespace App\Http\Controllers;

use App\School;
use App\Subsystem;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $schools = School::query()
            ->paginate();
        return view('maintenance.schools.index', compact('schools'));
    }

    public function add()
    {
        $subsystems = Subsystem::query()->pluck('shortname','id');
        return view('maintenance.schools.add',compact('subsystems'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $school = new School;
        $school->create($request->all());
        return redirect(route('maintenance.schools.index'));
    }

    public function edit($id)
    {
        $school = School::find($id);
        $subsystems = Subsystem::query()->pluck('shortname','id');

        return \View::make('maintenance.schools.edit', compact('school','subsystems'));

    }

    public function update(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required'
        ]);

        $school = School::find($request->id);
        $school->update($request->all());
        return redirect(route('maintenance.schools.index'));
    }

    public function destroy($id)
    {
        $school = School::find($id);
        $school->delete();
        return redirect(route('maintenance.schools.index'));
    }

}
