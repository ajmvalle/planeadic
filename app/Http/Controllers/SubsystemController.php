<?php

namespace App\Http\Controllers;

use App\Subsystem;
use Illuminate\Http\Request;

class SubsystemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $subsystems = Subsystem::query()
            ->paginate();
        return view('maintenance.subsystems.index', compact('subsystems'));
    }

    public function add()
    {
        return view('maintenance.subsystems.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $subsystem = new Subsystem;
        $subsystem->create($request->all());
        return redirect(route('maintenance.subsystems.index'));
    }

    public function edit($id)
    {
        $subsystem = Subsystem::find($id);

        return \View::make('maintenance.subsystems.edit', compact('subsystem'));

    }

    public function update(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required',
        ]);

        $subsystem = Subsystem::find($request->id);
        $subsystem->update($request->all());
        return redirect(route('maintenance.subsystems.index'));
    }

    public function destroy($id)
    {
        $subsystem = Subsystem::find($id);
        $subsystem->delete();
        return redirect(route('maintenance.subsystems.index'));
    }
}
