<?php

namespace App\Http\Controllers;

use App\Ambit;
use Illuminate\Http\Request;

class AmbitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $ambits = Ambit::query()
            ->paginate();
        return view('maintenance.ambits.index', compact('ambits'));
    }

    public function add()
    {
        return view('maintenance.ambits.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'desc' => 'required'
        ]);

        $ambit = new Ambit();
        $ambit->create($request->all());

        return redirect(route('maintenance.ambits.index'));
    }

    public function edit($id)
    {
        $ambit = ambit::find($id);

        return \View::make('maintenance.ambits.edit', compact('ambit'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'desc' => 'required'
        ]);

        $ambit = Ambit::find($request->id);
        $ambit->update($request->all());

        return redirect(route('maintenance.ambits.index'));
    }

    public function destroy($id)
    {
        $ambit = Ambit::find($id);


        $ambit->delete();
        return redirect(route('maintenance.ambits.index'));
    }

}
