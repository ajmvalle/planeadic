<?php

namespace App\Http\Controllers;

use App\Course;
use App\Specialty;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $courses = Course::query()
            ->paginate();
        return view('maintenance.courses.index', compact('courses'));
    }

    public function add()
    {
        $specialties = Specialty::query()->pluck('name','id')->prepend('Seleccione Carrera si aplica','');
        return view('maintenance.courses.add', compact('specialties'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'area' => 'required'
        ]);

        $course = new Course;
        $course->create($request->all());
        return redirect(route('maintenance.courses.index'));
    }

    public function edit($id)
    {
        $course = Course::find($id);
        $specialties = Specialty::query()->pluck('name','id')->prepend('Seleccione Carrera si aplica','');
        return \View::make('maintenance.courses.edit', compact('course','specialties'));

    }

    public function update(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'area' => 'required'
        ]);

        $course = Course::find($request->id);
        $course->update($request->all());
        return redirect(route('maintenance.courses.index'));
    }

    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect(route('maintenance.courses.index'));
    }
}
