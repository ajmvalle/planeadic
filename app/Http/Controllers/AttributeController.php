<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Competence;
use Illuminate\Http\Request;

class AttributeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function add(Competence $competence)
    {
        return view('maintenance.attributes.add', compact('competence'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'title' => 'required',
        ]);

        $attribute = new Attribute();
        $attribute->create($request->all());

        return redirect(route('maintenance.competences.edit', ['id' => $request->competence_id]));
    }

    public function edit($id)
    {
        $attribute = Attribute::find($id);

        return \View::make('maintenance.attributes.edit', compact('attribute'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'title' => 'required',
        ]);

        $attribute = Attribute::find($request->id);
        $attribute->update($request->all());

        return redirect(route('maintenance.competences.edit', ['id' => $attribute->competence->id]));
    }

    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        $aux = $attribute->competence->id;
        $attribute->delete();
        return redirect(route('maintenance.competences.edit', ['id' => $aux]));
    }
}
