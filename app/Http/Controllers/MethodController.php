<?php

namespace App\Http\Controllers;

use App\Action;
use App\Method;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class MethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $methods = Method::query()->paginate();

        return view('methods.index', compact('methods'));
    }

    public function mine()
    {
        $user = auth()->user();
        $methods = Method::query()->where('user_id','=',$user->id)->paginate();

        return view('methods.mine', compact('methods'));
    }

    public function admin()
    {
        $methods = Method::query()->whereNull('user_id')
            ->paginate();

        return view('maintenance.methods.index', compact('methods'));
    }

    public function add()
    {
        return view('maintenance.methods.add');
    }

    public function addmine()
    {
        return view('methods.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'dev.time' => 'required|numeric|between:0,99.99',
            'summary' => 'required',
        ]);

        $method = new Method();


        $dev = new Action();
        $dev = $dev->create($request->dev);
        $request->request->add(['dev_id' => $dev->id]);


        try {
            if ($request->hasFile('imagen')) {
                $image = $request->file('imagen');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(300, 300)->save(public_path('/uploads/methods/' . $filename));
                $request->request->add(['image' => $filename]);
            }
        } catch (\Intervention\Image\Exception\NotReadableException $ex) {
        }

        $method->create($request->all());

        return redirect(route('maintenance.methods.index'));
    }

    public function storemine(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'dev.time' => 'required|numeric|between:0,99.99',
            'summary' => 'required',
        ]);

        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');


        $dev = new Action();
        $dev = $dev->create($request->dev);
        $request->request->add(['dev_id' => $dev->id]);

        $user->createMethod($request->all());

        return redirect(route('methods.mine'));
    }

    public function edit($id)
    {
        $method = Method::find($id);

        return \View::make('maintenance.methods.edit', compact('method'));
    }

    public function editmine($id)
    {
        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $method = Method::find($id);

        return \View::make('methods.edit', compact('method'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'dev.time' => 'required|numeric|between:0,99.99',
            'summary' => 'required',
        ]);

        try {
            if ($request->hasFile('imagen')) {
                $image = $request->file('imagen');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(300, 300)->save(public_path('/uploads/methods/' . $filename));
                $request->request->add(['image' => $filename]);
            }
        } catch (\Intervention\Image\Exception\NotReadableException $ex) {
        }

        $method = Method::find($request->id);
        $method->update($request->all());


        $method->dev->update($request->dev);

        return redirect(route('maintenance.methods.index'));
    }

    public function updatemine(Request $request)
    {
        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');


        $this->validate($request, [
            'title' => 'required',
            'dev.time' => 'required|numeric|between:0,99.99',
            'summary' => 'required',
        ]);


        $method = Method::find($request->id);


        if($user->id!=$method->user_id)
            return back()->withErrors('Cuenta No válida');

        $method->update($request->all());
        $method->dev->update($request->dev);

        return redirect(route('methods.mine'));
    }

    public function destroy($id)
    {
        $method = Method::find($id);

        $dev = $method->dev;

        $method->dev_id = null;

        $method->save();

        $dev->delete();

        $method->delete();
        return redirect(route('maintenance.methods.index'));
    }

    public function destroymine($id)
    {
        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $method = Method::find($id);

        if($user->id!=$method->user_id)
            return back()->withErrors('Cuenta No válida');

        $dev = $method->dev;

        $method->dev_id = null;

        $method->save();

        $dev->delete();

        $method->delete();
        return redirect(route('methods.mine'));
    }
}
