<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Block;
use App\Centralcontent;
use App\Competence;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlockController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function add(Program $program)
    {
        $centralcontents = Centralcontent::query()->where('program_id', '=', $program->id)
        ->pluck('title','id')->prepend('Seleccione en caso de NME','');
        return view('maintenance.blocks.add', compact('program', 'centralcontents'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
        ]);

        $block = new Block();
        $block->create($request->all());

        return redirect(route('maintenance.programs.edit', ['id' => $request->program_id]));
    }

    public function edit($id)
    {
        $block = Block::find($id);
        $centralcontents = Centralcontent::query()->where('program_id', '=', $block->program_id)
            ->pluck('title','id')->prepend('Seleccione en caso de NME','');

        $dcompetences = Competence::select(
            DB::raw("CONCAT(code,' ',substr(title,1,100)) AS name"),'id')->where('kind','=','D')
            ->pluck('name','id')->prepend('Seleccione una competencia','');
        $pcompetences = Competence::select(
            DB::raw("CONCAT(code,' ',substr(title,1,100)) AS name"),'id')->where('kind','=','P')
            ->pluck('name','id')->prepend('Seleccione una competencia','');
        $attributes = Attribute::select(
            DB::raw("CONCAT(code,' ',title) AS name"),'id')->pluck('name','id')->prepend('Seleccione','');
        return \View::make('maintenance.blocks.edit', compact('block','centralcontents','dcompetences','pcompetences','attributes'));
    }


    public function show($id)
    {
        $block = Block::find($id);
        return \View::make('blocks.show', compact('block'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
        ]);

        $block = Block::find($request->id);
        $block->update($request->all());

        return redirect(route('maintenance.programs.edit', ['id' => $block->program->id]));
    }

    public function destroy($id)
    {
        $block = Block::find($id);
        $aux = $block->program->id;
        $block->delete();
        return redirect(route('maintenance.programs.edit', ['id' => $aux]));
    }

    public function addAttribute(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'attribute_id' => 'required',
        ]);

        $block = Block::find($request->id);
        $attribute = Attribute::find($request->attribute_id);
        $block->attributes()->detach($attribute->id);
        $block->attributes()->attach($attribute->id);

        return redirect(route('maintenance.blocks.edit', ['id' => $block->id]));
    }

    public function removeAttribute(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'attribute_id' => 'required',
        ]);

        $block = Block::find($request->id);
        $attribute = Attribute::find($request->attribute_id);
        $block->attributes()->detach($attribute->id);

        return redirect(route('maintenance.blocks.edit', ['id' => $block->id]));
    }


    public function addCompetence(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'competence_id' => 'required',
        ]);

        $block = Block::find($request->id);

        $competence = Competence::find($request->competence_id);


        $block->competences()->detach($competence->id);
        $block->competences()->attach($competence->id);

        return redirect(route('maintenance.blocks.edit', ['id' => $block->id]));
    }


    public function removeCompetence(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'competence_id' => 'required',
        ]);

        $block = Block::find($request->id);
        $competence = Competence::find($request->competence_id);
        $block->competences()->detach($competence->id);

        return redirect(route('maintenance.blocks.edit', ['id' => $block->id]));
    }


}
