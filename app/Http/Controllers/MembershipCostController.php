<?php

namespace App\Http\Controllers;

use App\Membershipcost;
use Illuminate\Http\Request;

class MembershipcostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $membershipcosts = Membershipcost::query()
            ->paginate();
        return view('maintenance.membershipcosts.index', compact('membershipcosts'));
    }


    public function add()
    {
        return view('maintenance.membershipcosts.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required'
        ]);

        $membershipcost = new Membershipcost();
        $membershipcost->create($request->all());
        return redirect(route('maintenance.membershipcosts.index'));
    }

    public function edit($id)
    {
        $membershipcost = Membershipcost::find($id);

        return \View::make('maintenance.membershipcosts.edit', compact('membershipcost'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required'
        ]);

        $membershipcost = Membershipcost::find($request->id);
        $membershipcost->update($request->all());

        return redirect(route('maintenance.membershipcosts.index'));
    }

    public function destroy($id)
    {
        $Mmembershipcost = Membershipcost::find($id);
        $Mmembershipcost->delete();
        return redirect(route('maintenance.membershipcosts.index'));
    }
}
