<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

class ListGroupController extends Controller
{
    public function __invoke(Request $request)
    {

        $text = $request->text;


        if ($text == "") {
            $groups = Group::query()->paginate();
            return view('groups.search', compact('groups', 'text'));
        }

        $groups = Group::query()
            ->Where('title', 'like', '%' . $text . '%')
            ->orderBy('created_at', 'desc')
            ->with('users')
            ->paginate();

        return view('groups.search', compact('groups', 'text'));
    }


}
