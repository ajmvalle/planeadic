<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = auth()->user();
        $groups = $user->mygroups()->paginate();
        return view('groups.index', compact('groups'));
    }

    public function add()
    {
        return view('groups.add');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $group = Group::find($request->id);
        $user = Auth::user();
        if ($user->can('update', $group))
            $group->update($request->all());

        return redirect(route('groups.index'));

    }

    public function edit($id)
    {
        $group = Group::find($id);
        return view('groups.edit', compact('group'));
    }

    public function search()
    {
        $groups = Group::query()->with('users')->paginate();
        $text = "";
        return view('groups.search', compact('groups', 'text'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);


        \auth()->user()->createGroup($request->all());


        return redirect(route('groups.index'));
    }

    public function subscribe(Request $request)
    {

        $group = Group::find($request->id);

        $user = Auth::user();

        $group->users()->detach($user);
        $group->users()->attach($user);

        return redirect(route('groups.index'));
    }

    public function quit(Request $request)
    {

        $group = Group::find($request->id);

        $user = Auth::user();

        $group->users()->detach($user);

        return redirect(route('groups.index'));
    }


    public function show(Group $group, $slug)
    {
        $user = \auth()->user();
        if (!$user->can('view', $group))
            abort('403', "Usuario No Autorizado");

        $members = $group->users()->paginate();

        $assigments = $group->assigments()->paginate();
        $comments = $group->comments;

        return view('groups.show', compact('group', 'members', 'comments', 'assigments'));
    }

    public function destroy($id)
    {
        $group = Group::find($id);
        $user = Auth::user();
        if ($user->can('delete', $group)) {


            foreach ($group->users as $associate) {
                $associate->mygroups()->detach($group->id);
            }

            foreach ($group->comments as $comment) {
                $comment->delete();
            }

            foreach ($group->assigments as $assigment) {

                foreach ($assigment->tools as $tool) {
                    $tool->assigment()->dissociate();
                    $tool->save();
                }

                foreach ($assigment->plans as $plan)
                    $plan->eliminate();

                $assigment->delete();
            }

            $group->delete();
        }
        return redirect(route('groups.index'));
    }


    public function chat(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required',
        ]);

        \auth()->user()->comment($request->all());

        $group = Group::find($request->group_id);

        return redirect($group->url);
    }

    public function exchange(Request $request)
    {
        $group = Group::find($request->group_id);
        $user = Auth::user();
        $member = User::find($request->member_id);

        if ($user->can('update', $group)) {
            $edit = !$request->edit;
            $group->users()->detach($member);
            $group->users()->attach($member, ['edit' => $edit]);
        }

        return redirect($group->url);
    }


}
