<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Attribute;
use App\Method;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use function Sodium\compare;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Plan $plan, $section)
    {
        $user = Auth::user();
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $attributes = Attribute::select(
            DB::raw("CONCAT(code,' ', substr(title,1,130) ,'...') AS name"), 'id')->pluck('name', 'id')->prepend('Seleccione(Opcional en NME)', '');

        $methods = Method::all()->pluck('title', 'id')->prepend('Seleccione un método', '');

        return view('activities.add', compact('plan', 'attributes', 'methods', 'section'));
    }

    public function edit($id)
    {
        $activity = Activity::find($id);

        $user = Auth::user();
        if (!$user->can('update', $activity->plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$activity->plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $attributes = Attribute::select(
            DB::raw("CONCAT(code,' ',title) AS name"), 'id')->pluck('name', 'id')->prepend('Seleccione(Opcional en NME)', '');

        return view('activities.edit', compact('activity', 'attributes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'time_student' => 'required|numeric'
        ]);

        $this->validate($request, [
            'time' => 'required|numeric|gte:time_student',
            'section' => 'required',
            'product' => 'required',
            'eva_percent' => 'required|numeric',
            'order' => 'required|numeric',
        ]);

        $plan = Plan::find($request->plan_id);
        $user = Auth::user();

        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        //dd($request->all());
        $request['extra'] = $request->has('bextra');
        $request['ordinal'] = 0;

        $activity = new Activity();
        $activity->create($request->all());

        return redirect(route('plans.activities', $plan));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'time_student' => 'required|numeric'
        ]);

        $this->validate($request, [
            'time' => 'required|numeric|gte:time_student',
            'section' => 'required',
            'product' => 'required',
            'eva_percent' => 'numeric',
            'order' => 'required|numeric',
        ]);
        $request['extra'] = $request->has('bextra');
        $request['ordinal'] = 0;


        $user = Auth::user();
        $activity = Activity::find($request->id);

        if (!$user->can('update', $activity->plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$activity->plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');


        $activity->update($request->all());

        return redirect(route('plans.activities', $activity->plan));
    }


    public function destroy($id)
    {
        $activity = Activity::find($id);
        $user = Auth::user();
        $plan = $activity->plan;

        if (!$user->can('delete', $activity->plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$activity->plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $activity->delete();
        return redirect(route('plans.activities', $plan));
    }


}
