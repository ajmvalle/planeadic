<?php

namespace App\Http\Controllers;

use App\Ambit;
use App\Assigment;
use App\Course;
use App\Method;
use App\Payment;
use App\Plan;
use App\Point;
use App\Program;
use App\Specialty;
use App\State;
use App\Town;
use App\User;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getStates()
    {
        $states = State::all();

        return response()->json($states);
    }



    public function getTowns($id = null)
    {
        $towns = [];

        if ($id) {
            $towns = Town::query()->where([
                ['state_id', '=', $id]])->get(['id', 'name']);
        }

        return response()->json($towns);
    }


    public function getSpecialties()
    {
        $specialties = Specialty::orderBy('name')->get();

        return response()->json($specialties);
    }

    public function getMethods($id = null)
    {

        $methods = Method::with("dev")->whereNull('user_id')->orWhere('user_id', '=', $id)->orderBy('user_id', 'desc')->get();

        return response()->json($methods);
    }

    public function getCourses($id = null)
    {
        $courses = [];
        if ($id > 0) {
            $courses = Course::query()->orderBy('name')->where([
                ['specialty_id', '=', $id]])->get(['id', 'name']);
        } else {
            $courses = Course::query()->orderBy('name')->whereNull('specialty_id')->get(['id', 'name']);
        }

        return response()->json($courses);
    }

    public function getPrograms($id = null)
    {
        $programs = [];

        if ($id) {
            $programs = Program::query()->orderBy('name')->where([
                ['course_id', '=', $id]])->get(['id', 'name']);
        }

        return response()->json($programs);
    }


    public function OxxoConfirm()
    {
        $body = @file_get_contents('php://input');
        $data = json_decode($body);
        http_response_code(200); // Return 200 OK

        if ($data->type == 'charge.paid') {


            $payments = Payment::where([
                ['order', '=', $data->data->object->order_id]
            ])->get();

            foreach ($payments as $payment) {

                if ($payment->status == "pending") {

                    $payment->setStatusAttribute("paid");
                    $payment->save();

                    $user = User::find($payment->user_id);

                    $user->checkForMemberships();

                }
            }

        }
    }

    public function functions()
    {
        return view('maintenance.functions.index');
    }

    public function orderplans()
    {
        $assigments = Assigment::all();


        foreach ($assigments as $assigment) {

            $c = 1;
            foreach ($assigment->plans as $plan) {
                $plan->setOrderAttribute($c);
                $plan->update();
                $c++;
            }
        }

        return redirect(route('maintenance.index'));
    }

    public function orderactivities()
    {
        $plans = Plan::all();

        foreach ($plans as $plan) {
            $c = 1;
            foreach ($plan->activities as $act) {
                $act->setOrderAttribute($c);
                $act->update();
                $c++;
            }
        }

        return redirect(route('maintenance.index'));
    }

    public function getAmbits()
    {
        $ambits = Ambit::all();

        return response()->json($ambits);
    }

}
