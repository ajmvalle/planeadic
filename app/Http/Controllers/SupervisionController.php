<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupervisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $user = Auth::user();
        if (!$user->principal || !$user->hasmembership)
            abort(403, 'Unauthorized action.');

        $school = School::find($user->school_id);

        $teachers = $school->users;

        return view('supervise.index', compact('teachers'));
    }
}
