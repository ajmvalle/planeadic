<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $users = User::query()
            ->with('assigments')
            ->paginate();


        return view('maintenance.users.index', compact('users'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $schools = School::select(
            DB::raw("CONCAT(name,' ',number) AS name"), 'id')->orderBy('name')->pluck('name', 'id')->prepend('Puede seleccionar una Escuela por defecto', '');
        return \View::make('maintenance.users.edit', compact('user','schools'));

    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required'
        ]);

        $request['activated'] = $request->has('bactivated');
        $request['principal'] = $request->has('bprincipal');

        $user = User::find($request->id);
        $user->update($request->all());
        return redirect(route('maintenance.users.edit',['id'=>$user->id]));
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect(route('maintenance.users.index'));
    }
}
