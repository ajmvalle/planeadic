<?php

namespace App\Http\Controllers;

use App\Assigment;
use App\Attribute;
use App\Block;
use App\Competence;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class PlanController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Assigment $assigment, Block $block)
    {
        $user = Auth::user();
        if (!$user->can('update', $assigment))
            abort(403, 'Unauthorized action.');

        return view('plans.add', compact('block', 'user', 'assigment'));
    }

    public function edit(Plan $plan)
    {
        $user = Auth::user();

        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');

        $blocks = Block::select(
            'description', 'id')->where('program_id', '=', $plan->assigment->program_id)->orderBy('description')->pluck('description', 'id');

        $dcompetences = Competence::select(
            DB::raw("CONCAT(code,' ',substr(title,1,100)) AS name"), 'id')->where('kind', '=', 'D')
            ->pluck('name', 'id')->prepend('Seleccione una competencia', '');
        $pcompetences = Competence::select(
            DB::raw("CONCAT(code,' ',substr(title,1,100)) AS name"), 'id')->where('kind', '=', 'P')
            ->pluck('name', 'id')->prepend('Seleccione una competencia', '');
        $attributes = Attribute::select(
            DB::raw("CONCAT(code,' ',substr(title,1,100)) AS name"), 'id')->pluck('name', 'id')->prepend('Seleccione', '');

        return view('plans.edit', compact('plan', 'dcompetences', 'pcompetences', 'attributes', 'blocks'));
    }

    public function send(Plan $plan)
    {
        $user = Auth::user();

        if (!$user->can('view', $plan->assigment))
            abort(403, 'Unauthorized action.');

        $assigments = Assigment::query()->where('program_id', '=', $plan->assigment->program_id)->where('user_id', '=', $user->id)->get();

        $groups = $user->myEditableGroups;

        foreach ($groups as $group) {

            //dd($group);
            foreach ($group->assigments as $assigment) {
                if ($assigment->program_id == $plan->assigment->program_id) {
                    $assigments->add($assigment);
                }
            }
        }
        $assigments = $assigments->pluck('title', 'id')->prepend('Seleccione un Portafolio', 0);

        return view('plans.send', compact('plan', 'assigments'));
    }

    public function confirmsend(Request $request)
    {
        $user = Auth::user();
        $plan = Plan::find($request->id);
        if ($request->assigment_id == 0)
            return redirect(route('assigments.dashboard', $plan->assigment->id));

        $assigment = Assigment::find($request->assigment_id);

        if ($plan->assigment->program_id != $assigment->program_id)
            return back()->withErrors('Portafolio destino incompatible. (Programa)');

        if (!$user->can('view', $plan->assigment))
            abort(403, 'Unauthorized action.');

        $plan->send($assigment);

        return redirect(route('assigments.dashboard', $assigment->id));
    }

    public function activities(Plan $plan)
    {
        $user = Auth::user();

        if (!$user->can('view', $plan->assigment))
            abort(403, 'Unauthorized action.');

        return view('plans.activities', compact('plan'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'order' => 'numeric',
            'elaboration'=> 'required',
            'apply_from'=> 'required',
            'apply_to'=> 'required',
        ]);

        $request['space'] = $request->has('isspace');
        $request['energy'] = $request->has('isenergy');
        $request['diversity'] = $request->has('isdiversity');
        $request['thetime'] = $request->has('isthetime');
        $request['matter'] = $request->has('ismatter');

        $user = Auth::user();
        $assigment = Assigment::find($request->assigment_id);

        if (!$user->can('update', $assigment))
            abort(403, 'Unauthorized action.');

        if (!$assigment->isGrupal) {
            if (!Auth::user()->IsActive)
                return back()->withErrors('Cuenta No válida');
        }

        $plan = new Plan();
        $plan = $plan->create($request->all());

        if ($plan->block) {

            if ($plan->block->competences) {
                foreach ($plan->block->competences as $competence) {
                    $plan->competences()->attach($competence->id);
                }
            }

            if ($plan->block->attributes) {
                foreach ($plan->block->attributes as $att) {
                    $plan->attributes()->attach($att->id);
                }
            }
        }

        return redirect($assigment->url);
    }


    public function update(Request $request)
    {
        $request['space'] = $request->has('isspace');
        $request['energy'] = $request->has('isenergy');
        $request['diversity'] = $request->has('isdiversity');
        $request['thetime'] = $request->has('isthetime');
        $request['matter'] = $request->has('ismatter');
        $user = Auth::user();
        $plan = Plan::find($request->id);


        $this->validate($request, [
            'title' => 'required',
            'order' => 'numeric',
            'elaboration'=> 'required',
            'apply_from'=> 'required',
            'apply_to'=> 'required',
        ]);

        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');

        if (!$plan->assigment->isGrupal) {
            if (!Auth::user()->IsActive)
                return back()->withErrors('Cuenta No válida');
        }


        $plan->update($request->all());
        return redirect($plan->assigment->url);
    }

    public function destroy($id)
    {
        $plan = Plan::find($id);
        $user = Auth::user();

        if (!$user->can('delete', $plan->assigment))
            abort(403, 'Unauthorized action.');

        if (!$plan->assigment->isGrupal) {
            if (!$user->IsActive)
                return back()->withErrors('Cuenta No válida');
        }

        $assigment = $plan->assigment;

        foreach ($plan->activities as $activity)
            $activity->delete();

        foreach ($plan->evidences as $evi)
            $evi->delete();

        $plan->delete();

        return redirect($assigment->url);
    }

    public function print(Plan $plan)
    {
        $user = Auth::user();
        if (!$user->can('update', $plan->assigment) || $plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');

        if ($plan->assigment->format) {
            return view($plan->assigment->format->view, compact('user', 'plan'));
        } else {
            return view('reports.default', compact('user', 'plan'));
        }
    }

    public function pdf(Plan $plan)
    {
        $user = Auth::user();
        if (!$user->can('update', $plan->assigment) || $plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');

        $html = '<h1>Contacte al Administrador</h1>';
        if ($plan->assigment->format) {
            $html = view($plan->assigment->format->view, compact('user', 'plan'));

        } else {
            $html = view('reports.default', compact('user', 'plan'));
        }


        $pdf = \App::make('dompdf.wrapper')->setPaper('a4', 'landscape');
        $pdf->loadHTML($html);
        return $pdf->stream();

    }


    public function addCompetence(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'competence_id' => 'required',
        ]);

        $plan = Plan::find($request->id);

        $user = Auth::user();
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');

        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $competence = Competence::find($request->competence_id);

        $plan->competences()->detach($competence->id);
        $plan->competences()->attach($competence->id);


        return redirect(route('plans.edit', ['id' => $plan->id]));
    }


    public function removeCompetence(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'competence_id' => 'required',
        ]);

        $plan = Plan::find($request->id);

        $user = Auth::user();
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $competence = Competence::find($request->competence_id);
        $plan->competences()->detach($competence->id);

        return redirect(route('plans.edit', ['id' => $plan->id]));
    }

    public function addAttribute(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'attribute_id' => 'required',
        ]);

        $plan = Plan::find($request->id);

        $user = Auth::user();
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $attribute = Attribute::find($request->attribute_id);

        $plan->attributes()->detach($attribute->id);
        $plan->attributes()->attach($attribute->id);

        return redirect(route('plans.edit', ['id' => $plan->id]));
    }


    public function removeAttribute(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'attribute_id' => 'required',
        ]);

        $plan = Plan::find($request->id);

        $user = Auth::user();
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');
        if (!$plan->assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        $attribute = Attribute::find($request->attribute_id);
        $plan->attributes()->detach($attribute->id);

        return redirect(route('plans.edit', ['id' => $plan->id]));
    }

}
