<?php

namespace App\Http\Controllers;

use App\Specialty;
use Illuminate\Http\Request;

class SpecialtyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $specialties = Specialty::query()
            ->paginate();
        return view('maintenance.specialties.index', compact('specialties'));
    }


    public function add()
    {
        return view('maintenance.specialties.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $specialty = new Specialty();
        $specialty->create($request->all());
        return redirect(route('maintenance.specialties.index'));
    }

    public function edit($id)
    {
        $specialty = Specialty::find($id);

        return \View::make('maintenance.specialties.edit', compact('specialty'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $specialty = Specialty::find($request->id);
        $specialty->update($request->all());

        return redirect(route('maintenance.specialties.index'));
    }

    public function destroy($id)
    {
        $specialty = Specialty::find($id);
        $specialty->delete();
        return redirect(route('maintenance.specialties.index'));
    }
}
