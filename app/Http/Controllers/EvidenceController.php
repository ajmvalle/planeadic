<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Evidence;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class EvidenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Plan $plan)
    {
        return view('evidences.index', compact('plan'));
    }

    public function add(Plan $plan)
    {
        if ($plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');

        $activities = Activity::select(
            DB::raw("product AS name"), 'id')->where('plan_id', '=', $plan->id)->pluck('name', 'id')->prepend('Seleccione Producto (Opcional)', '');
        return view('evidences.add', compact('plan', 'activities'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'plan_id' => 'required',
            'description' => 'required',
            'kind' => 'required',
            'imagen' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);

        $plan = Plan::find($request->plan_id);
        $user = Auth::user();

        if ($plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');

        try {
            if ($request->hasFile('imagen')) {
                $image = $request->file('imagen');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(600, 600)->save(public_path('/uploads/evidence/' . $filename));
                $request->request->add(['image' => $filename]);
            }
        } catch (\Intervention\Image\Exception\NotReadableException $ex) {
        }

        $newevi = new Evidence();
        $newevi->create($request->all());

        return redirect(route('evidences.index', $request->plan_id));
    }

    public function edit(Evidence $evidence)
    {
        if ($evidence->plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');
        if (!\auth()->user()->can('update', $evidence->plan->assigment))
            abort(403, 'Unauthorized action.');

        $activities = Activity::select(
            DB::raw("product AS name"), 'id')->pluck('name', 'id')->prepend('Seleccione Producto (Opcional)', '');
        return view('evidences.edit', compact('evidence', 'activities'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'plan_id' => 'required',
            'description' => 'required',
            'kind' => 'required',

        ]);

        $plan = Plan::find($request->plan_id);
        $user = Auth::user();
        if ($plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');
        if (!$user->can('update', $plan->assigment))
            abort(403, 'Unauthorized action.');

        try {
            if ($request->hasFile('imagen')) {
                $image = $request->file('imagen');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(600, 600)->save(public_path('/uploads/evidence/' . $filename));
                $request->request->add(['image' => $filename]);
            }
        } catch (\Intervention\Image\Exception\NotReadableException $ex) {
        }

        $newevi = Evidence::find($request->id);
        $newevi->update($request->all());

        return redirect(route('evidences.index', $request->plan_id));
    }


    public function destroy($id)
    {
        $evidence = Evidence::find($id);

        $plan_id = $evidence->plan_id;
        $user = Auth::user();
        if ($evidence->plan->assigment->isGrupal)
            abort(403, 'Unauthorized action.');
        if (!$user->can('update', $evidence->plan->assigment))
            abort(403, 'Unauthorized action.');
        $evidence->delete();
        return redirect(route('evidences.index', $plan_id));
    }
}
