<?php

namespace App\Http\Controllers;

use App\Course;
use App\Format;
use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $programs = Program::query()
            ->paginate();
        return view('maintenance.programs.index', compact('programs'));
    }

    public function add()
    {
        $courses = Course::query()->pluck('name', 'id')->prepend('Seleccione la Asignatura', '');

        return view('maintenance.programs.add', compact('courses'));
    }

    public function store(Request $request)
    {
        $request['nme'] = $request->has('isnme');
        $this->validate($request, [
            'name' => 'required',
            'course_id' => 'required',
            'purpose' => 'required'
        ]);

        $program = new Program;
        $program->create($request->all());
        return redirect(route('maintenance.programs.index'));
    }

    public function edit($id)
    {
        $program = Program::find($id);
        $courses = Course::query()->pluck('name', 'id')->prepend('Seleccione la Asignatura', '');
        return \View::make('maintenance.programs.edit', compact('program', 'courses'));
    }

    public function update(Request $request)
    {
        $request['nme'] = $request->has('isnme');
        $this->validate($request, [
            'name' => 'required',
            'course_id' => 'required',
            'purpose' => 'required'
        ]);

        $program = Program::find($request->id);
        $program->update($request->all());
        return redirect(route('maintenance.programs.index'));
    }

    public function destroy($id)
    {
        $program = Program::find($id);
        $program->delete();
        return redirect(route('maintenance.programs.index'));
    }
}
