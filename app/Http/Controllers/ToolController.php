<?php

namespace App\Http\Controllers;

use App\Assigment;
use App\Tool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{
    public function __invoke(Request $request)
    {
        $tools = Tool::query()
            ->scopes($this->getListScopes($request))
            ->paginate();
        return view('tools.index', compact('tools'));
    }

    public function addrubric()
    {
        $kind = 'rubric';
        $tipo = 'Rúbrica';
        $assigments = Assigment::select(
            'name', 'id')->where('user_id', '=', auth()->user()->id)->orderBy('name')->pluck('name', 'id')->prepend('Seleccione un Portafolio', '');
        return view('tools.add', compact('kind', 'tipo', 'assigments'));
    }

    public function addlist()
    {
        $kind = 'list';
        $tipo = 'Lista de Cotejo';
        $assigments = Assigment::select(
            'name', 'id')->where('user_id', '=', auth()->user()->id)->orderBy('name')->pluck('name', 'id')->prepend('Seleccione un Portafolio', '');
        return view('tools.add', compact('kind', 'tipo', 'assigments'));
    }

    public function addscale()
    {
        $kind = 'scale';
        $tipo = 'Escala Estimativa';
        $assigments = Assigment::select(
            'name', 'id')->where('user_id', '=', auth()->user()->id)->orderBy('name')->pluck('name', 'id')->prepend('Seleccione un Portafolio', '');
        return view('tools.add', compact('kind', 'tipo', 'assigments'));
    }

    public function edit($id)
    {
        $tool = Tool::find($id);
        $user = \auth()->user();

        if ($tool->user_id != $user->id || !$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $assigments = Assigment::select(
            'name', 'id')->where('user_id', '=', $user->id)->orderBy('name')->pluck('name', 'id')->prepend('Seleccione un Portafolio', '');
        return view('tools.edit', compact('tool', 'assigments'));
    }

    private function getListScopes(Request $request)
    {
        $scopes['byUser'] = [$request->user()];

        return $scopes;
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $user = Auth::user();

        if (!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $user->createTool($request->all());

        return redirect(route('tools.index'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $user = Auth::user();
        $tool = Tool::find($request->id);


        if (!$user->IsActive||$tool->user_id != $user->id)
            return back()->withErrors('Cuenta No válida');


        $tool->update($request->all());


        return redirect(route('tools.edit',$tool->id));
    }


    public function destroy($id)
    {
        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $tool = Tool::find($id);

        if($user->id!=$tool->user_id)
            return back()->withErrors('Cuenta No válida');


        foreach ($tool->indicators as $indicator)
        {
            $indicator->delete();
        }

        $tool->delete();

        return redirect(route('tools.index'));
    }

    public function print($id)
    {
        $user = Auth::user();

        if(!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $tool = Tool::find($id);

        if($user->id!=$tool->user_id)
            return back()->withErrors('Cuenta No válida');

        return view('reports.tools',compact('tool'));
    }




}
