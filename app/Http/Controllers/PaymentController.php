<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function oxxo(Payment $payment)
    {
        $payment = Payment::find($payment->id);

        return view('payments.oxxo', compact('payment'));
    }


    public function spei(Payment $payment)
    {
        $payment = Payment::find($payment->id);

        return view('payments.spei', compact('payment'));
    }


    public function __invoke(Request $request)
    {
        $payments = Payment::query()->orderBy('created_at', 'desc')
            ->paginate();
        return view('maintenance.payments.index', compact('payments'));
    }

    public function apply(Payment $payment)
    {
        if (!auth()->user()->isAdmin)
            abort(403, 'Unauthorized action.');

        if ($payment->status == "paid")
            abort(403, 'Unauthorized action.');

        $payment->setStatusAttribute("paid");
        $payment->save();
        $user = User::find($payment->user_id);
        $user->checkForMemberships();

        return redirect(route('maintenance.payments.index'));
    }

}
