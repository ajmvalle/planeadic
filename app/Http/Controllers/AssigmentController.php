<?php

namespace App\Http\Controllers;

use App\Assigment;
use App\Format;
use App\Group;
use App\Program;
use App\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AssigmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke(Request $request)
    {
        $assigments = Assigment::query()
            ->with('plans')
            ->scopes($this->getListScopes($request))
            ->orderBy('created_at', 'desc')
            ->paginate();
        return view('assigments.index', compact('assigments'));
    }

    private function getListScopes(Request $request)
    {
        $scopes['byUser'] = [$request->user()];

        return $scopes;
    }

    public function add($group_id = null)
    {
        $group = null;

        if (!is_null($group_id))
            $group = Group::find($group_id);

        $schools = School::select(
            DB::raw("CONCAT(name,' ',number) AS name"), 'id')->orderBy('name')->pluck('name', 'id')->prepend('Seleccione una Escuela', '');
        $programs = Program::query()->orderBy('name')->pluck('name', 'id')->prepend('Seleccione una Asignatura', '');
        $formats = Format::query()->orderBy('name')->pluck('name', 'id')->prepend('Seleccione el formato', '');
        $user = Auth::user();
        return view('assigments.add', compact('schools', 'programs', 'user', 'formats', 'group'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cycle' => 'required',
            'program_id' => 'required',
            'school_id' => 'required',
        ]);

        $user = \auth()->user();

        if (is_null($request->group_id)) { //Portafolio de Usuario

            if (!Auth::user()->IsActive)
                return back()->withErrors('Cuenta No válida');
            $user->createAssigment($request->all());
            return redirect(route('assigments.index'));

        } else {  //Portafolio de Grupo

            $group = Group::find($request->group_id);
            if (is_null($group))
                abort(403, 'Unauthorized action.');

            if ($user->can('create', $group))
                $group->createAssigment($request->all());

            return redirect($group->url);
        }

    }

    public function edit($id)
    {
        $user = \auth()->user();
        $assigment = Assigment::find($id);

        if (!$user->can('update', $assigment))
            abort(403, 'Unauthorized action.');

        $schools = School::select(
            DB::raw("CONCAT(name,' ',number) AS name"), 'id')->orderBy('name')->pluck('name', 'id')->prepend('Seleccione una Escuela', '');
        $programs = Program::query()->orderBy('name')->pluck('name', 'id')->prepend('Seleccione una Asignatura', '');
        $formats = Format::query()->orderBy('name')->pluck('name', 'id')->prepend('Seleccione el formato', '');
        return \View::make('assigments.edit', compact('assigment', 'schools', 'programs', 'formats'));
    }

    public function dashboard($id)
    {
        $user = \auth()->user();
        $assigment = Assigment::find($id);

        if (is_null($assigment)) {
            abort(404);
        } else {
            if (!$user->can('view', $assigment)) {
                abort(403);
            }
        }
        if (is_null($assigment->school_id))
            return Redirect::back()->withErrors(['Portafolio sin plantel, favor de editarlo']);
        else
            return \View::make('plans.dashboard', compact('assigment'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cycle' => 'required',
            'school_id' => 'required',
        ]);

        $assigment = Assigment::find($request->id);
        $user = Auth::user();

        if (!$user->can('update', $assigment))
            abort(403, 'Unauthorized action.');

        if ($assigment->isGrupal) {
            $assigment->update($request->all());
            return redirect($assigment->group->url);
        } else {
            if (!Auth::user()->IsActive)
                return back()->withErrors('Cuenta No válida');
            $assigment->update($request->all());
            return redirect(route('assigments.index'));
        }

    }

    public function copy(Request $request)
    {
        $id = $request->id;
        $group_id = $request['copygroup' . $id];

        if (!Auth::user()->IsActive) {
            return back()->withErrors('Cuenta No válida');
        }

        $assigment = Assigment::find($id);


        if (is_null($group_id))
            $assigment->copy();
        else
            $assigment->copyToGroup($group_id);


        return redirect(route('assigments.index'));
    }

    public function destroy($id)
    {
        $assigment = Assigment::find($id);
        $user = Auth::user();

        if (!$user->can('delete', $assigment))
            abort(403, 'Unauthorized action.');

        if (!$assigment->isGrupal && !Auth::user()->IsActive)
            return back()->withErrors('Cuenta No válida');

        foreach ($assigment->tools as $tool) {
            $tool->assigment()->dissociate();
            $tool->save();
        }

        foreach ($assigment->plans as $plan)
            $plan->eliminate();

        $group = $assigment->group;
        $assigment->delete();

        if (is_null($group))
            return redirect(route('assigments.index'));
        else
            return redirect($group->url);
    }
}
