<?php

namespace App\Http\Controllers;

use App\Membershipcost;
use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $membershipcosts = Membershipcost::all();

        if ($user->hadMemberships) {
            $membershipcosts->forget(0);
        }

        if ($user->principal) {
            $school = School::find($user->school_id);

            $docentes = $school->users;
            return view('account.principal', compact('membershipcosts', 'docentes'));
        }

        return view('account.individual', compact('membershipcosts'));
    }

    public function activate($token)
    {
        $user = Auth::user();

        if ($user->activate_token == $token) {
            $user->Activate(true);
        }
        return redirect('home');
    }

    public function askToken()
    {
        return view('account.ask');
    }

    public function sendToken()
    {
        $user = Auth::user();
        $user->sendAccountActivationNotification(
            $user->activate_token
        );
        return back()->with('status', "Hemos enviado nuevamente tu código de activación a tu e-mail , favor de seguir las instrucciones del mismo.");
    }

    public function preview(Request $request)
    {
        $n = count($request->detail);
        $membership = Membershipcost::query()->where('months','=','6')->first();

        $individual = $membership->cost;
        $total = $individual * $n;

        if ($n >= 30)
            $total = ($total * .5);
        else {
            if ($n >= 10)
                $total = ($total * .75);
        }

        $users = User::query()->whereIn('id',$request->detail)->get();


       return view('account.preview',compact('total','users'));

    }


}
