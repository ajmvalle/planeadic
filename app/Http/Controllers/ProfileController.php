<?php

namespace App\Http\Controllers;

use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $user = auth()->user();
        $schools = School::select(
            DB::raw("CONCAT(name,' ',number) AS name"), 'id')->orderBy('name')->pluck('name', 'id')->prepend('Puede seleccionar una Escuela por defecto', '');


        return view('profile.show', compact('user', 'schools'));
    }


    public function delavatar()
    {
        $user = auth()->user();

        $user->avatar = 'default.png';

        $user->save();

        $schools = School::select(
            DB::raw("CONCAT(name,' ',number) AS name"), 'id')->orderBy('name')->pluck('name', 'id')->prepend('Puede seleccionar una Escuela por defecto', '');


        return view('profile.show', compact('user', 'schools'));
    }

    public function avatar(Request $request)
    {

        // Handle the user upload of avatar
        try {
            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $filename));

                $user = Auth::user();
                $user->avatar = $filename;
                $user->save();
            }
        } catch (\Intervention\Image\Exception\NotReadableException $ex) {
        }

        return redirect(route('profile.show'));

    }

    public function update(Request $request)
    {
        if (Auth::user()->id == $request->id) {


            $request['authlogo'] = $request->has('bauthlogo');
            $request['principal'] = $request->has('bprincipal');

            $user = User::find($request->id);

            $user->update($request->except(['name', 'lastname']));
        }
        return redirect(route('profile.show'));

    }


    public function tax(Request $request)
    {
        if (Auth::user()->id == $request->id) {

            $user = User::find($request->id);
            $user->setTaxiIdAttribute($request->taxid);
            $user->update();
        }
        return redirect(route('profile.show'));


    }
}