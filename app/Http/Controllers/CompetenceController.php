<?php

namespace App\Http\Controllers;

use App\Competence;
use Illuminate\Http\Request;

class CompetenceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function __invoke()
    {
        $competences = Competence::query()
            ->paginate();
        return view('maintenance.competences.index', compact('competences'));
    }

    public function add()
    {
        return view('maintenance.competences.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'title' => 'required',
            'kind' => 'required',
        ]);

        $competence = new Competence();
        $competence->create($request->all());

        return redirect(route('maintenance.competences.index'));
    }

    public function edit($id)
    {
        $competence = Competence::find($id);

        return \View::make('maintenance.competences.edit', compact('competence'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'title' => 'required',
            'kind' => 'required',
        ]);

        $competence = Competence::find($request->id);
        $competence->update($request->all());

        return redirect(route('maintenance.competences.index'));
    }

    public function destroy($id)
    {
        $competence = Competence::find($id);

        foreach ($competence->attributes as $attribute)
        {
            $attribute->delete();
        }

        $competence->delete();
        return redirect(route('maintenance.competences.index'));
    }
}
