<?php

namespace App\Http\Controllers;

use App\Membershipcost;
use App\User;
use Carbon\Carbon;
use Conekta\Conekta;
use Conekta\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MembershipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get(Membershipcost $membershipcost, $kind)
    {
        return back()->withErrors(['payment', 'Servicio No Disponible']);

        $payment = $this->getIndividual($membershipcost, $kind);

        if ($kind != "free") {

            if ($kind == "oxxo_cash")
                return redirect(route('payment.oxxo', $payment));
            else
                return redirect(route('payment.spei', $payment));
        }
    }

    public function getPrincipal(Request $request)
    {
        $kind = $request->optionpay;
        $detail = $request->detail;
        $payment = $this->getGrupal($kind, $detail);
        if ($kind == "oxxo_cash")
            return redirect(route('payment.oxxo', $payment));
        else
            return redirect(route('payment.spei', $payment));
    }


    public function getFree(Membershipcost $membershipcost)
    {
        $this->get($membershipcost, "free");
        return redirect(route('account.index'));
    }


    public function getIndividual(Membershipcost $membershipcost, $way)
    {
        $payment = null;
        $membershipcost = Membershipcost::find($membershipcost->id);
        $user = Auth::user();

        if ($membershipcost->cost == 0) {
            if (!$user->hadmemberships) {
                $user->createFreePayment($membershipcost);
            }
        } else {


            try {

                Conekta::setApiKey(env('CONEKTA_SECRET'));
                Conekta::setApiVersion("2.0.0");
                $order = Order::create(
                    array(
                        "line_items" => array(
                            array(
                                "name" => 'Membresía: ' . $membershipcost->name,
                                "unit_price" => intval($membershipcost->cost) . '00',
                                "quantity" => 1
                            )//first line_item
                        ), //line_items
                        "currency" => "MXN",
                        "customer_info" => array(
                            "name" => $user->fullname,
                            "email" => $user->email,
                            "phone" => $user->phone
                        ), //customer_info
                        "charges" => array(
                            array(
                                "payment_method" => array(
                                    "type" => $way,
                                    "expires_at" => Carbon::now()->addDays(3)->timestamp
                                )//payment_method
                            ) //first charge
                        ) //charges
                    )//order
                );


            } catch (\Conekta\ParameterValidationError $error) {
                return back()->withErrors($error->getMessage());

            } catch (\Conekta\Handler $error) {
                return back()->withErrors($error->getMessage());
            }

            $payment = $user->createPayment($membershipcost, $order);


        }

        return $payment;


    }


    public function getGrupal($way, $detail)
    {

        $users = User::query()->whereIn('id', $detail)->get();

        $n = $users->count();
        $membershipcost = Membershipcost::query()->where('months', '=', '6')->first();

        $individual = $membershipcost->cost;
        $total = $individual * $n;

        if ($n >= 30)
            $total = ($total * .5);
        else {
            if ($n >= 10)
                $total = ($total * .75);
        }

        $payment = null;
        $user = Auth::user();

        try {

            Conekta::setApiKey(env('CONEKTA_SECRET'));
            Conekta::setApiVersion("2.0.0");
            $order = Order::create(
                array(
                    "line_items" => array(
                        array(
                            "name" => 'Membresía: Grupal Semestral',
                            "unit_price" => intval($total) . '00',
                            "quantity" => 1
                        )//first line_item
                    ), //line_items
                    "currency" => "MXN",
                    "customer_info" => array(
                        "name" => $user->fullname,
                        "email" => $user->email,
                        "phone" => $user->phone
                    ), //customer_info
                    "charges" => array(
                        array(
                            "payment_method" => array(
                                "type" => $way,
                                "expires_at" => Carbon::now()->addDays(3)->timestamp
                            )//payment_method
                        ) //first charge
                    ) //charges
                )//order
            );


        } catch (\Conekta\ParameterValidationError $error) {
            return back()->withErrors($error->getMessage());

        } catch (\Conekta\Handler $error) {
            return back()->withErrors($error->getMessage());
        }

        foreach ($users as $user) {
            $payment = $user->createPayment($membershipcost, $order);
        }

        return $payment;


    }


}
