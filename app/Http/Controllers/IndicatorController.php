<?php

namespace App\Http\Controllers;

use App\Indicator;
use App\Program;
use App\Tool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndicatorController extends Controller
{
    public function add(Tool $tool)
    {
        $tool = Tool::find($tool->id);
        $user = auth()->user();

        if ($tool->user_id != $user->id || !$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        return view('indicators.add', compact('tool'));
    }

    public function edit($id)
    {
        $indicator = Indicator::find($id);
        $user = auth()->user();

        if ($indicator->tool->user_id != $user->id || !$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        return view('indicators.edit', compact('indicator'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'tool_id' => 'required'
        ]);

        $user = Auth::user();
        $tool = Tool::find($request->tool_id);

        if (!$user->IsActive || $user->id != $tool->user_id)
            return back()->withErrors('Cuenta No válida');
        $indicator = new Indicator($request->all());
        $tool->indicators()->save($indicator);

        return redirect(route('tools.edit', $tool->id));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'tool_id' => 'required'
        ]);

        $user = Auth::user();
        $indicator = Indicator::find($request->id);

        if (!$user->IsActive || $user->id != $indicator->tool->user_id)
            return back()->withErrors('Cuenta No válida');

        $indicator->update($request->all());

        return redirect(route('tools.edit', $indicator->tool->id));
    }

    public function destroy($id)
    {
        $user = Auth::user();

        if (!$user->IsActive)
            return back()->withErrors('Cuenta No válida');

        $indicator = Indicator::find($id);

        if ($user->id != $indicator->tool->user_id)
            return back()->withErrors('Cuenta No válida');

        $toolid = $indicator->tool->id;

        $indicator->delete();

        return redirect(route('tools.edit', $toolid));
    }
}
