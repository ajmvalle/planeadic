<?php

namespace App\Http\Controllers;

use App\Format;
use Illuminate\Http\Request;

class FormatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $formats = Format::query()
            ->paginate();
        return view('maintenance.formats.index', compact('formats'));
    }

    public function add()
    {
        return view('maintenance.formats.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'view' => 'required'
        ]);

        $format = new Format;
        $format->create($request->all());
        return redirect(route('maintenance.formats.index'));
    }

    public function edit($id)
    {
        $format = Format::find($id);
        return \View::make('maintenance.formats.edit', compact('format'));

    }

    public function update(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'view' => 'required'
        ]);

        $format = Format::find($request->id);
        $format->update($request->all());
        return redirect(route('maintenance.formats.index'));
    }

    public function destroy($id)
    {
        $format = Format::find($id);
        $format->delete();
        return redirect(route('maintenance.formats.index'));
    }
}
