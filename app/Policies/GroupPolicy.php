<?php

namespace App\Policies;

use App\User;
use App\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function update(User $user, Group $group)
    {
        return $user->owns($group);
    }

    /**
     * Determine whether the user can create somtehing at the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function create(User $user, Group $group)
    {
        return $user->owns($group) || $group->editableBy($user);
    }

    /**
     * Determine whether the user can delete the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function delete(User $user, Group $group)
    {
        return $user->owns($group);
    }


    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        return $user->owns($group) || $group->availableTo($user);
    }
}
