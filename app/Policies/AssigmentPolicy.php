<?php

namespace App\Policies;

use App\Assigment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssigmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function create(User $user, Assigment $assigment)
    {
        return $user->owns($assigment) || $user->owns($assigment->group);
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function view(User $user, Assigment $assigment)
    {
        if (is_null($assigment->group))
            return $user->owns($assigment);
        else
            return $user->owns($assigment->group) || $assigment->group->availableTo($user);
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function update(User $user, Assigment $assigment)
    {
        if (is_null($assigment->group))
            return $user->owns($assigment);
        else
            return $user->owns($assigment->group) || $assigment->group->editableBy($user);
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\User $user
     * @param  \App\Group $group
     * @return mixed
     */
    public function delete(User $user, Assigment $assigment)
    {
        if ($assigment->isGrupal)
            return $user->owns($assigment->group);
        else
            return $user->owns($assigment);

    }
}