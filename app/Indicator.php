<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $fillable=[
        'tool_id','title','detail1','detail2','detail3','detail4'
    ];

    public $timestamps = false;


    public  function tool()
    {
        return $this->belongsTo(Tool::class);
    }
}
