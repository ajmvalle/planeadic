<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Assigment extends Model
{
    protected $fillable =
        [
            'cycle', 'name', 'program_id', 'school_id', 'user_id', 'filed', 'careers', 'format_id', 'groups', 'context'
        ];

    public $casts = [
        'filed' => 'boolean'
    ];

    public function getTitleAttribute()
    {
        return $this->name . ' ' . (!is_null($this->group_id) ? 'Grupo: ' . $this->group->title : '');
    }


    public function tools()
    {
        return $this->hasMany(Tool::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function plans()
    {
        return $this->hasMany(Plan::class)->orderBy('order');
    }

    public function no_plans()
    {
        return $this->plans()->count();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeByUser($query, User $user)
    {
        $query->where('user_id', $user->id);
    }

    public function format()
    {
        return $this->belongsTo(Format::class);
    }

    public function getHoursAttribute()
    {
        $resp = 0;

        foreach ($this->plans as $plan)
            $resp += $plan->hours;

        return $resp;
    }


    public function getAdvanceAttribute()
    {
        $resp = 0;

        if ($this->program->hours > 0)
            $resp = $this->hours / $this->program->hours * 100;

        return round($resp, 2);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
    }

    public function getIsGrupalAttribute()
    {
        return is_null($this->user_id);
    }


    public function copy()
    {
        $clone = $this->replicate();
        $clone->setNameAttribute($clone->name . ' (Copia)');

        $group = null;
        if ($clone->isGrupal) {
            $group= Group::find($clone->group_id);
            $user = auth()->user();
            $clone->user_id = $user->id;
            $clone->school_id = $user->school_id;
            $clone->group_id = null;
        }
        $clone->save();


        //Copying ECAS' relationships
        foreach ($this->plans as $plan) {
            $planclone = $plan->copy($clone->id);

            if ($plan->competences) {
                foreach ($plan->competences as $competence) {
                    $planclone->competences()->attach($competence->id);
                }
            }

            if ($plan->attribs) {
                foreach ($plan->attribs as $attribute) {
                    $planclone->attribs()->attach($attribute->id);
                }
            }

            if (!is_null($group)) {
                $planclone->collaborations = $group->credits;//Pase de créditos
                $planclone->save();
            }

        }

    }


    public function copyToGroup($group_id)
    {
        $clone = $this->replicate();
        $clone->setNameAttribute($clone->name . ' (Copia)');


        $clone->user_id = null;

        $group = Group::find($group_id);
        if (is_null($group))
            return null;

        $clone->group_id = $group->id;

        $clone->save();

        //Copying ECAS' relationships
        foreach ($this->plans as $plan) {
            $planclone = $plan->copy($clone->id);

            if ($plan->competences) {
                foreach ($plan->competences as $competence) {
                    $planclone->competences()->attach($competence->id);
                }
            }

            if ($plan->attribs) {
                foreach ($plan->attribs as $attribute) {
                    $planclone->attribs()->attach($attribute->id);
                }
            }
        }

    }


    public function getSlugAttribute()
    {
        return Str::slug($this->name);
    }

    public function getUrlAttribute()
    {
        if ($this->getIsGrupalAttribute())
            return route('groups.assigments.dashboard', [$this->id, $this->slug]);
        else
            return route('assigments.dashboard', [$this->id, $this->slug]);
    }


    public function getUserFullNameAttribute()
    {
        if ($this->isGrupal) {
            return $this->group->title;
        } else
            return $this->user->FullName;
    }


}
