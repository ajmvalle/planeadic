<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable=[
        'teacher','student','time','product'
    ];

    public $timestamps = false;

}
