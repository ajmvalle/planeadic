<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{
    protected $fillable = [
        'title',
        'block_id',
        'assigment_id',
        'elaboration',
        'apply_from',
        'apply_to',
        'partial',
        'purpose',
        'purpose_area',
        'expecteds',
        'products',
        'process',
        'specifics',
        'generics',
        'disciplines',
        'hse',
        'references',
        'resources',
        'integrator',
        'trans_integrator',
        'trans_course',
        'space',
        'energy',
        'diversity',
        'thetime',
        'matter',
        'factual',
        'fundamental',
        'subsidiary',
        'procedimental',
        'attitudinal',
        'profesionalcontent',
        'equipment',
        'biosecurity',
        'reactives',
        'material',
        'order',
        'transversality',
        'profile_ambits',
        'extrareferences',
        'collaborations',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'elaboration'
    ];

    protected $casts = [
        'elaboration' => 'date',
        'apply_from' => 'date',
        'apply_to' => 'date',
        'space' => 'boolean',
        'energy' => 'boolean',
        'diversity' => 'boolean',
        'thetime' => 'boolean',
        'matter' => 'boolean',
    ];


    public function block()
    {
        return $this->belongsTo(Block::class);
    }


    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
    }

    public function eliminate()
    {
        $user = Auth::user();

        if (!$user->can('delete', $this->assigment))
            abort(403, 'Unauthorized action.');

        foreach ($this->evidences as $evi)
            $evi->delete();

        foreach ($this->activities as $activity)
            $activity->delete();


        $this->delete();
    }

    public function evidences()
    {
        return $this->hasMany(Evidence::class);
    }

    public function assigment()
    {
        return $this->belongsTo(Assigment::class);
    }

    public function activities()
    {
        return $this->hasMany(Activity::class)->orderBy('order');
    }

    public function startActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'start'],
            ['extra', '=', '0']
        ])->orderBy('order');
    }


    public function getStartTimeAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'start']
        ])->get();


        foreach ($acts as $act) {
            $resp += $act->time;
        }

        return $resp;
    }

    public function getDevTimeAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'dev']
        ])->get();

        foreach ($acts as $act) {
            $resp += $act->time;
        }

        return $resp;
    }

    public function getCloseTimeAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'close']
        ])->get();

        foreach ($acts as $act) {
            $resp += $act->time;
        }

        return $resp;
    }

    public function getStartEvaAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'start']
        ])->get();

        foreach ($acts as $act) {
            $resp += $act->eva_percent;
        }

        return $resp;
    }

    public function getDevEvaAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'dev']
        ])->get();

        foreach ($acts as $act) {
            $resp += $act->eva_percent;
        }

        return $resp;
    }

    public function getCloseEvaAttribute()
    {
        $resp = 0;
        $acts = $this->hasMany(Activity::class)->where([
            ['section', '=', 'close']
        ])->get();

        foreach ($acts as $act) {
            $resp += $act->eva_percent;
        }

        return $resp;
    }


    public function devActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'dev'],
            ['extra', '=', '0']
        ])->orderBy('order');
    }

    public function closeActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'close'],
            ['extra', '=', '0']
        ])->orderBy('order');
    }


    public function extraStartActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'start'],
            ['extra', '=', '1']
        ])->orderBy('order');
    }

    public function extraDevActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'dev'],
            ['extra', '=', '1']
        ])->orderBy('order');
    }

    public function extraCloseActivities()
    {
        return $this->hasMany(Activity::class)->where([
            ['section', '=', 'close'],
            ['extra', '=', '1']
        ])->orderBy('order');
    }


    public function getHoursAttribute()
    {
        $resp = 0;

        foreach ($this->activities as $activity)
            $resp += $activity->time;

        return $resp;
    }

    public function getStartCountAttribute()
    {
        return count($this->startActivities) + count($this->extraStartActivities);
    }

    public function getDevCountAttribute()
    {
        return count($this->devActivities) + count($this->extraDevActivities);
    }

    public function getCloseCountAttribute()
    {
        return count($this->closeActivities) + count($this->extraCloseActivities);
    }

    public function getTotalPercentAttribute()
    {
        $resp = 0;

        foreach ($this->activities as $activity) {
            $resp += $activity->eva_percent;
        }

        return $resp;
    }

    public function getSocialAttribute()
    {
        if ($this->hse == 1) return "Conoce T - Autoconocimiento";
        if ($this->hse == 2) return "Conoce T - Autoregulación";
        if ($this->hse == 3) return "Relaciona T - Conciencia Social";
        if ($this->hse == 4) return "Relaciona T - Colaboración";
        if ($this->hse == 5) return "Elige T - Toma responsable de Decisiones";
        if ($this->hse == 6) return "Elige T - Perseverancia";

        else return "";
    }


    public function getAuthorAttribute()
    {
        if ($this->assigment->isGrupal)
            return $this->assigment->group->title;
        return $this->assigment->user->fullname;
    }

    public function getChiefAcademyAttribute()
    {
        if (!$this->assigment->isGrupal)
            return $this->assigment->user->academychief;
    }

    public function getChiefTeachersAttribute()
    {
        if (!$this->assigment->isGrupal)
            return $this->assigment->user->teacherschief;
    }

    public function competences()
    {
        return $this->belongsToMany(Competence::class, 'plan_competence');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'plan_attribute');
    }

    public function attribs()
    {
        return $this->belongsToMany(Attribute::class, 'plan_attribute');
    }

    public function getGenericsAttribute()
    {
        $competences = Competence::select(DB::raw('competences.*'))
            ->join('attributes', 'competences.id', '=', 'attributes.competence_id')
            ->join('plan_attribute', 'attributes.id', '=', 'plan_attribute.attribute_id')
            ->where('plan_attribute.plan_id', "=", $this->id)->distinct()->get();

        return $competences;
    }

    public function discompetences()
    {
        return $this->belongsToMany(Competence::class, 'plan_competence')->where('kind', 'D');
    }

    public function profcompetences()
    {
        return $this->belongsToMany(Competence::class, 'plan_competence')->where('kind', 'P');
    }

    public function copy($assigment)
    {
        $planclone = $this->replicate();
        $planclone->assigment_id = $assigment;
        $portfolio = Assigment::find($assigment);

        $planclone->setTitleAttribute($planclone->title . ' ' . now()->format('d/m/y'));

        if ($planclone->apply_to->format('Y')=='-0001')
            $planclone->apply_to = $planclone->elaboration;
        if ($planclone->apply_from->format('Y')=='-0001')
            $planclone->apply_from = $planclone->elaboration;

        $planclone->save();

        //Copying activities
        foreach ($this->activities as $activity) {
            $actclone = $activity->replicate();
            $actclone->plan_id = $planclone->id;
            $actclone->save();
        }

        return $planclone;
    }

    public function setOrderAttribute($data)
    {
        $this->attributes['order'] = $data;
    }

    public function send(Assigment $assigment)
    {

        $planclone = $this->copy($assigment->id);

        if ($this->competences) {
            foreach ($this->competences as $competence) {
                $planclone->competences()->attach($competence->id);
            }
        }

        if ($this->attribs) {
            foreach ($this->attribs as $attribute) {
                $planclone->attribs()->attach($attribute->id);
            }
        }


    }

    public function getApplicationAttribute()
    {
        if (($this->apply_from->format('d/m/Y') == "30/11/-0001") || ($this->apply_to->format('d/m/Y') == "30/11/-0001"))
            return "";

        return $this->apply_from->format('d/m/Y') . ' a ' . $this->apply_to->format('d/m/Y');
    }


}
