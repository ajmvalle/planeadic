<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Group extends Model
{
    protected $fillable = [
        'title', 'slug', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;

        $this->attributes['slug'] = Str::slug($value);
    }

    public function getUrlAttribute()
    {
        return route('groups.show', [$this->id, $this->slug]);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_group')->withPivot('edit')->orderBy('edit', 'desc');
    }

    public function editors()
    {
        return $this->belongsToMany(User::class, 'user_group')->withPivot('edit')->where('edit', "=", 1);
    }

    public function getCreditsAttribute()
    {
        $resp = "";
        foreach ($this->editors as $editor) {
            $resp = $resp . $editor->fullname;
            if($editor->school!=null)
               $resp= $resp.' ('.$editor->school->shortname.') ';
            $resp = $resp."\n";
        }
        return $resp;
    }

    public function editableBy(User $user)
    {
        $editors = $this->editors;
        foreach ($editors as $editor) {
            if ($user->id == $editor->id)
                return true;
        }
        return false;
    }

    public function availableTo(User $user)
    {
        $members = $this->users;
        foreach ($members as $member) {
            if ($user->id == $member->id)
                return true;
        }
        return false;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'desc');
    }


    public function assigments()
    {
        return $this->hasMany(Assigment::class)->orderBy('created_at', 'desc');
    }

    public function createAssigment(array $data)
    {
        $assigment = new Assigment($data);
        $this->assigments()->save($assigment);
        return $assigment;
    }


}
