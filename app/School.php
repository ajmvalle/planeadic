<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'number', 'subname', 'address', 'officialkey', 'town_id', 'subsystem_id'
    ];

    public function town()
    {
        return $this->belongsTo(Town::class);
    }

    public function subsystem()
    {
        return $this->belongsTo(Subsystem::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getFullNameAttribute()
    {
        return $this->getTranslatedAttribute().' No. '.$this->number;
    }

    public function getShortNameAttribute()
    {
        return $this->name.' '.$this->number;
    }

    public function getTranslatedAttribute(){

        $resp = "";
        if($this->name == "CBTIS")
        {
            $resp= "Centro de Bachillerato Tecnológico industrial y de Servicios";
        }

        if($this->name == "CETIS")
        {
            $resp= "Centro de Estudios Tecnológico industrial y de Servicios";
        }

        return $resp;
    }


}
