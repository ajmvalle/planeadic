<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambit extends Model
{
    protected $fillable=['title','desc'];
    public $timestamps = false;
}
