<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsystem extends Model
{
    protected $fillable= [
        'name',
        'shortname',
        'office',
        'shortoffice',
        'department',
        'shortdepartment'
    ];

    public $timestamps= false;
}
