<?php

namespace App;

use App\Notifications\AccountActivation;
use App\Notifications\MembershipCreatedNotification;
use App\Notifications\ResetPasswordNotification;
use Conekta\Order;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'profession', 'sex', 'school_id', 'title', 'address', 'context', 'phone', 'activate_token', 'taxid', 'authlogo', 'principal', 'activated', 'academychief', 'teacherschief'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $casts = [
        'isAdmin' => 'boolean',
        'activated' => 'boolean',
        'principal' => 'boolean',
        'authlogo' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function Activate()
    {
        $this->activated = true;
        $this->save();
    }


    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function memberships()
    {
        return $this->hasMany(Membership::class)->orderBy('created_at', 'desc');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }


    public function getLastMembershipAttribute()
    {
        return $this->memberships->sortByDesc('to')->first();
    }

    public function getIsActiveAttribute()
    {
        return $this->activated && (!is_null($this->lastMembership) && $this->lastMembership->isActive);
    }

    public function getHasMembershipAttribute()
    {
        return !is_null($this->LastMembership) && $this->LastMembership->IsActive;
    }

    public function createFreePayment(Membershipcost $membershipcost)
    {
        if ($membershipcost->cost == 0) {

            //Creación del Pago
            $payment = new Payment();
            $payment->setMembershipcostIdAttribute($membershipcost->id);
            $payment->setAmountAttribute($membershipcost->cost);
            $payment->setStatusAttribute('paid');
            $payment->setWayAttribute('Cortesía');
            //dd($payment);
            $pay = $this->payments()->save($payment);


            //Creación de la Membresía
            $membership = new Membership();
            $membership->setPaymentIdAttribute($pay->id);
            $membership->setFromAttribute(Carbon::now());
            $membership->setToAttribute(Carbon::now()->addDays(7));
            $this->memberships()->save($membership);

            try {
                $this->sendMembershipCreatedNotification(
                    $membership->to->format('d/m/y')
                );
            } catch (Exception $e) {
                // catch code
            }

        }
    }

    public function createPayment(Membershipcost $membershipcost, Order $order)
    {

        if ($membershipcost->cost > 0) {

            //Creación del Pago
            $payment = new Payment();
            $payment->setMembershipcostIdAttribute($membershipcost->id);

            $payment->setOrderAttribute($order->id);

            //dd($order);

            $payment->setWayAttribute($order->charges[0]->payment_method->type);
            if ($order->charges[0]->payment_method->type == "oxxo")
                $payment->setReferenceAttribute($order->charges[0]->payment_method->reference);
            else
                $payment->setReferenceAttribute($order->charges[0]->payment_method->receiving_account_number);


            $payment->setExpiresAttribute(Carbon::createFromTimestamp($order->charges[0]->payment_method->expires_at));

            $payment->setAmountAttribute($order->amount / 100);
            $payment->setStatusAttribute('pending');//Importante cambiar a pending cuando interactúes con Oxxo
            return $this->payments()->save($payment);
        }
    }


    public function checkForMemberships()
    {
        $count = 0;
        foreach ($this->payments as $payment) {
            if ($payment->status == 'paid') {
                $auxmem = Membership::query()->where('payment_id', '=', $payment->id)->get();


                if (count($auxmem) == 0) {

                    $membership = new Membership();
                    $membership->setPaymentIdAttribute($payment->id);

                    $start = Carbon::now();
                    if (!is_null($this->LastMembership) && $this->LastMembership->IsActive) {
                        $membership->setFromAttribute($this->LastMembership->to);
                        $start = $this->LastMembership->to;
                    } else {
                        $membership->setFromAttribute(Carbon::now());
                    }

                    $membership->setToAttribute($start->addMonths($payment->membershipcost->months));
                    $this->memberships()->save($membership);

                    try {
                        $this->sendMembershipCreatedNotification(
                            $membership->to->format('d/m/y')
                        );
                    } catch (Exception $e) {
                        // catch code
                    }
                }

            }
        }
        //dd($count);

    }


    public function getHadMembershipsAttribute()
    {
        $resp = false;
        if ($this->memberships()->count() > 0) {
            $resp = true;
        }

        return $resp;
    }

    public function sendAccountActivationNotification($token)
    {
        $this->notify(new AccountActivation($token));
    }

    public function getImagenAttribute()
    {
        $avatar = asset('uploads/avatars/' . $this->avatar);

        if ($this->sex == "female" && $this->avatar == "default.png") {
            $avatar = asset('uploads/avatars/default2.png');
        }

        return $avatar;
    }


    public function getFullNameAttribute()
    {
        return $this->title . ' ' . $this->name . ' ' . $this->lastname;
    }

    public function assigments()
    {
        return $this->hasMany(Assigment::class)->orderBy('created_at', 'desc');
    }

    public function methods()
    {
        return $this->hasMany(Method::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tools()
    {
        return $this->hasMany(Tool::class);
    }

    public function getTotalAssigmentsAttribute()
    {
        return $this->hasMany(Assigment::class)->whereUserId($this->id)->count();
    }

    public function createAssigment(array $data)
    {
        $assigment = new Assigment($data);
        $this->assigments()->save($assigment);
        return $assigment;
    }

    public function createMethod(array $data)
    {
        $method = new Method($data);
        $this->methods()->save($method);
        return $method;
    }

    public function comment(array $data)
    {
        $comment = new Comment($data);
        $this->comments()->save($comment);
        return $comment;
    }

    public function createTool(array $data)
    {
        $tool = new Tool($data);
        $this->tools()->save($tool);
        return $tool;
    }

    public function createGroup(array $data)
    {
        $group = new Group($data);
        $group = $this->groups()->save($group);

        $group->users()->attach($this->id, ['edit' => true]);

        return $group;
    }


    public function getTotalEcasAttribute()
    {
        $resp = 0;

        foreach ($this->assigments as $assigment) {
            foreach ($assigment->plans as $plan) {
                $resp++;
            }

        }

        return $resp;
    }


    public function setTaxiIdAttribute($taxid)
    {
        $this->taxid = $taxid;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendMembershipCreatedNotification($to)
    {
        $this->notify(new MembershipCreatedNotification($to));
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function mygroups()
    {
        return $this->belongsToMany(Group::class, 'user_group');
    }

    public function myAuthorizeGroups()
    {
        $groups = $this->belongsToMany(Group::class, 'user_group')->where('edit', '=', 1)
            ->get(['id', 'title'])->pluck('title', 'id');

        return $groups;
    }

    public function myEditableGroups()
    {
        return $this->belongsToMany(Group::class, 'user_group')->where('edit', '=', 1);
    }


    public function owns(Model $model)
    {
        return $this->id == $model->user_id;
    }

}
