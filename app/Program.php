<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = [
        'name', 'hours', 'course_id', 'key', 'purpose', 'grade', 'plan', 'nme','purpose_area'
    ];

    public $casts = [
        'nme' => 'boolean'
    ];

    public function competences()
    {
        return $this->belongsToMany(Competence::class, 'competence_program');
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function blocks()
    {
        return $this->hasMany(Block::class);
    }

    public function getAreaAttribute()
    {
        $resp = "";

        if (!is_null($this->course))
            $resp = $this->course->area;
        return $resp;
    }


}
