<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MembershipCreatedNotification extends Notification
{
    use Queueable;

    protected $to;

    public function __construct($to)
    {
        $this->to = $to;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Creación de Membresía en PLANEADIC')
            ->greeting('Creación de membresía.')
            ->line('Le notificamos que se ha creado, una membresía para su cuenta en PLANEADIC, puede verificar el detalle de sus membresías en el Sistema')
            ->line('Fecha de Finalización: '.$this->to)
            ->action('Visitar PLANEADIC', url(config('app.url')))
            ->salutation('El equipo de Planeadic');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
