<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccountActivation extends Notification
{
    use Queueable;


    protected $token;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Activar Cuenta')
            ->greeting('¡Bienvenido!')
            ->line('Muchas gracias por usar PLANEADIC. Para completar tu registro y aprovechar las ventajas de esta herramienta necesitamos activar tu cuenta, sólo da clic en el siguiente link:')
            ->action('Activar cuenta', route('account.activate', $this->token))
            ->line('Si no realizaste esta solicitud, no es necesario ninguna otra acción.')
            ->line('Atentamente')
            ->salutation('El equipo de planeadic.com');


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
