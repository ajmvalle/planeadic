<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membershipcost extends Model
{
    protected $fillable = ['name','months','cost'];

    protected $casts = [
        'cost'=>'float'
    ];
}
