<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('autor', function () {
    return view('autor');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('profile')->group(function () {
    Route::get('show', 'ProfileController')->name('profile.show');
});

Route::post('contact', [
    'uses' => 'ContactController',
    'as' => 'contact.store'
]);

Route::prefix('methods')->group(function () {
    Route::get('index', 'MethodController@index')->name('methods.index');
    Route::get('mine', 'MethodController@mine')->name('methods.mine');

    Route::get('add', 'MethodController@addmine')->name('methods.addmine');
    Route::post('add', [
        'uses' => 'MethodController@storemine',
        'as' => 'methods.storemine'
    ]);
    Route::get('edit/{id}', 'MethodController@editmine')->name('methods.editmine');
    Route::post('edit', [
        'uses' => 'MethodController@updatemine',
        'as' => 'methods.updatemine'
    ]);
    Route::delete('delete/{id}', [
        'uses' => 'MethodController@destroymine',
        'as' => 'methods.deletemine'
    ]);

});

Route::prefix('groups')->group(function () {

    Route::get('index', 'GroupController')->name('groups.index');
    Route::get('add', 'GroupController@add')->name('groups.add');
    Route::post('add', [
        'uses' => 'GroupController@store',
        'as' => 'groups.store'
    ]);

    Route::post('subscribe', 'GroupController@subscribe')->name('groups.subscribe');
    Route::post('exchange', 'GroupController@exchange')->name('groups.exchange');

    Route::post('chat', 'GroupController@chat')->name('groups.chat');
    Route::post('quit', 'GroupController@quit')->name('groups.quit');

    Route::get('edit/{id}', 'GroupController@edit')->name('groups.edit');
    Route::post('update', [
        'uses' => 'GroupController@update',
        'as' => 'groups.update'
    ]);

    Route::get('search', 'GroupController@search')->name('groups.search');
    Route::post('search', 'ListGroupController')->name('groups.locate');
    Route::get('{group}-{slug}', [
        'as' => 'groups.show',
        'uses' => 'GroupController@show'
    ])->where('group', '\d+');


    Route::delete('delete/{id}', [
        'uses' => 'GroupController@destroy',
        'as' => 'groups.delete'
    ]);

    Route::get('dashboard/{id}', 'AssigmentController@dashboard')->name('groups.assigments.dashboard');
    Route::get('addassigment/{group_id?}', 'AssigmentController@add')->name('groups.addassigment');
    Route::get('editassigment/{id}', 'AssigmentController@edit')->name('groups.editassigments');
});

Route::prefix('indicators')->group(function () {

    Route::get('add/{tool}', 'IndicatorController@add')->name('indicators.add');
    Route::post('add', [
        'uses' => 'IndicatorController@store',
        'as' => 'indicators.store'
    ]);
    Route::get('edit/{id}', 'IndicatorController@edit')->name('indicators.edit');
    Route::post('edit', [
        'uses' => 'IndicatorController@update',
        'as' => 'indicators.update'
    ]);
    Route::delete('delete/{id}', [
        'uses' => 'IndicatorController@destroy',
        'as' => 'indicators.delete'
    ]);

});

Route::prefix('tools')->group(function () {
    Route::get('index', 'ToolController')->name('tools.index');
    Route::get('print/{id}', 'ToolController@print')->name('tools.print');

    Route::get('addlist', 'ToolController@addlist')->name('tools.addlist');
    Route::get('addrubric', 'ToolController@addrubric')->name('tools.addrubric');
    Route::get('addscale', 'ToolController@addscale')->name('tools.addscale');
    Route::post('add', [
        'uses' => 'ToolController@store',
        'as' => 'tools.store'
    ]);
    Route::get('edit/{id}', 'ToolController@edit')->name('tools.edit');
    Route::post('edit', [
        'uses' => 'ToolController@update',
        'as' => 'tools.update'
    ]);
    Route::delete('delete/{id}', [
        'uses' => 'ToolController@destroy',
        'as' => 'tools.delete'
    ]);

});

Route::prefix('tutorials')->group(function () {
    Route::get('index', 'TutorialController')->name('tutorials.index');
});

Route::prefix('assigments')->group(function () {
    Route::get('index', 'AssigmentController')->name('assigments.index');
    Route::get('add/{group_id?}', 'AssigmentController@add')->name('assigments.add');
    Route::post('add', [
        'uses' => 'AssigmentController@store',
        'as' => 'assigments.store'
    ]);
    Route::get('edit/{id}', 'AssigmentController@edit')->name('assigments.edit');

    Route::post('edit', [
        'uses' => 'AssigmentController@update',
        'as' => 'assigments.update'
    ]);
    Route::delete('delete/{id}', [
        'uses' => 'AssigmentController@destroy',
        'as' => 'assigments.delete'
    ]);
    Route::post('copy', [
        'uses' => 'AssigmentController@copy',
        'as' => 'assigments.copy'
    ]);

    Route::get('dashboard/{id}', 'AssigmentController@dashboard')->name('assigments.dashboard');

});

Route::get('plan/add/{assigment}/{block}', 'PlanController@add')->name('plans.add');
Route::post('plan/add', [
    'uses' => 'PlanController@store',
    'as' => 'plans.store'
]);
Route::get('plan/edit/{plan}', 'PlanController@edit')->name('plans.edit');
Route::post('plan/edit', [
    'uses' => 'PlanController@update',
    'as' => 'plans.update'
]);
Route::delete('plan/delete/{id}', [
    'uses' => 'PlanController@destroy',
    'as' => 'plans.delete'
]);
Route::get('plan/activities/{plan}', 'PlanController@activities')->name('plans.activities');
Route::post('plan/RemoveCompetence', [
    'uses' => 'PlanController@RemoveCompetence',
    'as' => 'plans.removecompetence'
]);
Route::post('plan/AddCompetence', [
    'uses' => 'PlanController@AddCompetence',
    'as' => 'plans.addcompetence'
]);
Route::post('plan/RemoveAttribute', [
    'uses' => 'PlanController@RemoveAttribute',
    'as' => 'plans.removeattribute'
]);
Route::post('plan/AddAttribute', [
    'uses' => 'PlanController@AddAttribute',
    'as' => 'plans.addattribute'
]);
Route::get('plan/send/{plan}', 'PlanController@send')->name('plans.send');
Route::post('plan/send', 'PlanController@confirmsend')->name('plans.confirmsend');

Route::get('activity/add/{plan}/{section}', 'ActivityController@add')->name('activities.add');
Route::post('activity/add', 'ActivityController@store')->name('activities.store');
Route::get('activity/edit/{plan}', 'ActivityController@edit')->name('activities.edit');
Route::post('activity/edit', 'ActivityController@update')->name('activities.update');
Route::delete('activity/delete/{id}', [
    'uses' => 'ActivityController@destroy',
    'as' => 'activities.delete'
]);


Route::get('evidence/index/{plan}', 'EvidenceController')->name('evidences.index');
Route::get('evidence/add/{plan}', 'EvidenceController@add')->name('evidences.add');
Route::post('evidence/add', 'EvidenceController@store')->name('evidences.store');
Route::get('evidence/edit/{evidence}', 'EvidenceController@edit')->name('evidences.edit');
Route::post('evidence/edit', 'EvidenceController@update')->name('evidences.update');
Route::delete('evidece/delete/{id}', [
    'uses' => 'EvidenceController@destroy',
    'as' => 'evidences.delete'
]);


Route::get('block/{id}', 'BlockController@show')->name('blocks.show');

Route::get('plan/print/{plan}', 'PlanController@print')->name('plans.print');
Route::get('plan/pdf/{plan}', 'PlanController@pdf')->name('plans.pdf');



Route::prefix('searchplans')->group(function () {
    Route::get('index', 'SearchPlansController@index')->name('searchplans.index');
});


Route::prefix('account')->group(function () {
    Route::get('index', 'AccountController@index')->name('account.index');

    Route::post('preview', 'AccountController@preview')->name('account.preview');
    Route::get('preview', 'AccountController@index')->name('account.getpreview');
});

Route::prefix('profile')->group(function () {
    Route::post('avatar', 'ProfileController@avatar')->name('profile.avatar');
});

Route::prefix('profile')->group(function () {
    Route::get('delavatar', 'ProfileController@delavatar')->name('profile.delavatar');
});

Route::prefix('profile')->group(function () {
    Route::post('tax', 'ProfileController@tax')->name('profile.tax');
});


Route::prefix('profile')->group(function () {
    Route::post('update', 'ProfileController@update')->name('profile.update');
});

Route::get('activar-cuenta/{token}', [
    'uses' => 'AccountController@activate',
    'as' => 'account.activate',
]);

Route::get('solicitar-token', [
    'uses' => 'AccountController@askToken',
    'as' => 'activation.ask',
]);

Route::post('solicitar-token', [
    'uses' => 'AccountController@sendToken',
    'as' => 'account.sendtoken',
]);


Route::get('obtener-membresiaFree/{membershipcost}', [
    'uses' => 'MembershipController@getFree',
    'as' => 'membership.getFree',
]);

Route::get('supervision', [
    'uses' => 'SupervisionController',
    'as' => 'supervision.index',
]);

Route::get('obtener-membresia/{membershipcost}/{kind}', [
    'uses' => 'MembershipController@get',
    'as' => 'membership.get',
]);

Route::post('obtener-membresia-grupal', [
    'uses' => 'MembershipController@getPrincipal',
    'as' => 'membership.getPrincipal',
]);

Route::get('obtener-membresia-grupal', [
    'uses' => 'AccountController@index',
    'as' => 'membership.getPrincipal',
]);


Route::get('oxxo/{payment}', [
    'uses' => 'PaymentController@oxxo',
    'as' => 'payment.oxxo',
]);

Route::get('spei/{payment}', [
    'uses' => 'PaymentController@spei',
    'as' => 'payment.spei',
]);

Route::get('terminos-condiciones', function () {
    return view('terms');
})->name('terms');

Route::get('aviso-privacidad', function () {
    return view('privacy');
})->name('privacy');