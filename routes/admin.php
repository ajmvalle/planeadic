<?php
/**
 * Created by PhpStorm.
 * User: Alfredo
 * Date: 29/03/2018
 * Time: 02:17 PM
 */


Route::prefix('maintenance')->group(function () {
    Route::get('index', 'MaintenanceController@index')->name('maintenance.index');


    Route::get('subsystems/index', 'SubsystemController')->name('maintenance.subsystems.index');
    Route::get('subsystems/add', 'SubsystemController@add')->name('maintenance.subsystems.add');
    Route::post('subsystems/add', [
        'uses' => 'SubsystemController@store',
        'as'   => 'maintenance.subsystems.store'
    ]);
    Route::get('subsystems/edit/{id}', 'SubsystemController@edit')->name('maintenance.subsystems.edit');
    Route::post('subsystems/edit', [
        'uses' => 'SubsystemController@update',
        'as'   => 'maintenance.subsystems.update'
    ]);
    Route::delete('subsystems/delete/{id}', [
        'uses' => 'SubsystemController@destroy',
        'as'   => 'maintenance.subsystems.delete'
    ]);

    Route::get('schools/index', 'SchoolController')->name('maintenance.schools.index');
    Route::get('schools/add', 'SchoolController@add')->name('maintenance.schools.add');
    Route::post('schools/add', [
        'uses' => 'SchoolController@store',
        'as'   => 'maintenance.schools.store'
    ]);
    Route::get('schools/edit/{id}', 'SchoolController@edit')->name('maintenance.schools.edit');
    Route::post('schools/edit', [
        'uses' => 'SchoolController@update',
        'as'   => 'maintenance.schools.update'
    ]);
    Route::delete('schools/delete/{id}', [
        'uses' => 'SchoolController@destroy',
        'as'   => 'maintenance.schools.delete'
    ]);


    Route::get('users/index', 'UserController')->name('maintenance.users.index');
    Route::get('users/edit/{id}', 'UserController@edit')->name('maintenance.users.edit');
    Route::post('users/edit', [
        'uses' => 'UserController@update',
        'as'   => 'maintenance.users.update'
    ]);
    Route::delete('users/delete/{id}', [
        'uses' => 'UserController@destroy',
        'as'   => 'maintenance.users.delete'
    ]);

    Route::get('courses/index', 'CourseController')->name('maintenance.courses.index');
    Route::get('courses/add', 'CourseController@add')->name('maintenance.courses.add');
    Route::post('courses/add', [
        'uses' => 'CourseController@store',
        'as'   => 'maintenance.courses.store'
    ]);
    Route::get('courses/edit/{id}', 'CourseController@edit')->name('maintenance.courses.edit');
    Route::post('courses/edit', [
        'uses' => 'CourseController@update',
        'as'   => 'maintenance.courses.update'
    ]);
    Route::delete('courses/delete/{id}', [
        'uses' => 'CourseController@destroy',
        'as'   => 'maintenance.courses.delete'
    ]);

    Route::get('methods/index', 'MethodController@admin')->name('maintenance.methods.index');
    Route::get('methods/add', 'MethodController@add')->name('maintenance.methods.add');
    Route::post('methods/add', [
        'uses' => 'MethodController@store',
        'as'   => 'maintenance.methods.store'
    ]);
    Route::get('methods/edit/{id}', 'MethodController@edit')->name('maintenance.methods.edit');
    Route::post('methods/edit', [
        'uses' => 'MethodController@update',
        'as'   => 'maintenance.methods.update'
    ]);
    Route::delete('methods/delete/{id}', [
        'uses' => 'MethodController@destroy',
        'as'   => 'maintenance.methods.delete'
    ]);

    Route::get('competences/index', 'CompetenceController')->name('maintenance.competences.index');
    Route::get('competences/add', 'CompetenceController@add')->name('maintenance.competences.add');
    Route::post('competences/add', [
        'uses' => 'CompetenceController@store',
        'as'   => 'maintenance.competences.store'
    ]);
    Route::get('competences/edit/{id}', 'CompetenceController@edit')->name('maintenance.competences.edit');
    Route::post('competences/edit', [
        'uses' => 'CompetenceController@update',
        'as'   => 'maintenance.competences.update'
    ]);
    Route::delete('competences/delete/{id}', [
        'uses' => 'CompetenceController@destroy',
        'as'   => 'maintenance.competences.delete'
    ]);

    Route::get('attributes/add/{competence}', 'AttributeController@add')->name('maintenance.attributes.add');
    Route::post('attributes/add', [
        'uses' => 'AttributeController@store',
        'as'   => 'maintenance.attributes.store'
    ]);
    Route::get('attributes/edit/{id}', 'AttributeController@edit')->name('maintenance.attributes.edit');
    Route::post('attributes/edit', [
        'uses' => 'AttributeController@update',
        'as'   => 'maintenance.attributes.update'
    ]);
    Route::delete('attributes/delete/{id}', [
        'uses' => 'AttributeController@destroy',
        'as'   => 'maintenance.attributes.delete'
    ]);

    Route::get('ambits/index', 'AmbitController')->name('maintenance.ambits.index');
    Route::get('ambits/add', 'AmbitController@add')->name('maintenance.ambits.add');
    Route::post('ambits/add', [
        'uses' => 'AmbitController@store',
        'as'   => 'maintenance.ambits.store'
    ]);
    Route::get('ambits/edit/{id}', 'AmbitController@edit')->name('maintenance.ambits.edit');
    Route::post('ambits/edit', [
        'uses' => 'AmbitController@update',
        'as'   => 'maintenance.ambits.update'
    ]);
    Route::delete('ambits/delete/{id}', [
        'uses' => 'AmbitController@destroy',
        'as'   => 'maintenance.ambits.delete'
    ]);

    Route::get('centralcontents/index', 'CentralcontentController')->name('maintenance.centralcontents.index');
    Route::get('centralcontents/add', 'CentralcontentController@add')->name('maintenance.centralcontents.add');
    Route::post('centralcontents/add', [
        'uses' => 'CentralcontentController@store',
        'as'   => 'maintenance.centralcontents.store'
    ]);
    Route::get('centralcontents/edit/{id}', 'CentralcontentController@edit')->name('maintenance.centralcontents.edit');
    Route::post('centralcontents/edit', [
        'uses' => 'CentralcontentController@update',
        'as'   => 'maintenance.centralcontents.update'
    ]);
    Route::delete('centralcontents/delete/{id}', [
        'uses' => 'CentralcontentController@destroy',
        'as'   => 'maintenance.centralcontents.delete'
    ]);


    Route::get('programs/index', 'ProgramController')->name('maintenance.programs.index');
    Route::get('programs/add', 'ProgramController@add')->name('maintenance.programs.add');
    Route::post('programs/add', [
        'uses' => 'ProgramController@store',
        'as'   => 'maintenance.programs.store'
    ]);
    Route::get('programs/edit/{id}', 'ProgramController@edit')->name('maintenance.programs.edit');
    Route::post('programs/edit', [
        'uses' => 'ProgramController@update',
        'as'   => 'maintenance.programs.update'
    ]);
    Route::delete('programs/delete/{id}', [
        'uses' => 'ProgramController@destroy',
        'as'   => 'maintenance.programs.delete'
    ]);


    Route::get('blocks/add/{program}', 'BlockController@add')->name('maintenance.blocks.add');
    Route::post('blocks/add', [
        'uses' => 'BlockController@store',
        'as'   => 'maintenance.blocks.store'
    ]);
    Route::get('blocks/edit/{id}', 'BlockController@edit')->name('maintenance.blocks.edit');
    Route::post('blocks/edit', [
        'uses' => 'BlockController@update',
        'as'   => 'maintenance.blocks.update'
    ]);
    Route::delete('blocks/delete/{id}', [
        'uses' => 'BlockController@destroy',
        'as'   => 'maintenance.blocks.delete'
    ]);


    Route::post('blocks/addCompetence', [
        'uses' => 'BlockController@addCompetence',
        'as'   => 'maintenance.blocks.addcompetence'
    ]);

    Route::post('blocks/removeCompetence', [
        'uses' => 'BlockController@removeCompetence',
        'as'   => 'maintenance.blocks.removecompetence'
    ]);


    Route::post('blocks/addAttribute', [
        'uses' => 'BlockController@addAttribute',
        'as'   => 'maintenance.blocks.addattribute'
    ]);

    Route::post('blocks/removeAttribute', [
        'uses' => 'BlockController@removeAttribute',
        'as'   => 'maintenance.blocks.removeattribute'
    ]);

    Route::get('specialties/index', 'SpecialtyController')->name('maintenance.specialties.index');
    Route::get('specialties/add', 'SpecialtyController@add')->name('maintenance.specialties.add');
    Route::post('specialties/add', [
        'uses' => 'SpecialtyController@store',
        'as'   => 'maintenance.specialties.store'
    ]);
    Route::get('specialties/edit/{id}', 'SpecialtyController@edit')->name('maintenance.specialties.edit');
    Route::post('specialties/edit', [
        'uses' => 'SpecialtyController@update',
        'as'   => 'maintenance.specialties.update'
    ]);
    Route::delete('specialties/delete/{id}', [
        'uses' => 'SpecialtyController@destroy',
        'as'   => 'maintenance.specialties.delete'
    ]);


    Route::get('formats/index', 'FormatController')->name('maintenance.formats.index');
    Route::get('formats/add', 'FormatController@add')->name('maintenance.formats.add');
    Route::post('formats/add', [
        'uses' => 'FormatController@store',
        'as'   => 'maintenance.formats.store'
    ]);
    Route::get('formats/edit/{id}', 'FormatController@edit')->name('maintenance.formats.edit');
    Route::post('formats/edit', [
        'uses' => 'FormatController@update',
        'as'   => 'maintenance.formats.update'
    ]);
    Route::delete('formats/delete/{id}', [
        'uses' => 'FormatController@destroy',
        'as'   => 'maintenance.formats.delete'
    ]);


    Route::get('membershipcosts/index', 'MembershipCostController')->name('maintenance.membershipcosts.index');
    Route::get('membershipcosts/add', 'MembershipCostController@add')->name('maintenance.membershipcosts.add');
    Route::post('membershipcosts/add', [
        'uses' => 'MembershipCostController@store',
        'as'   => 'maintenance.membershipcosts.store'
    ]);
    Route::get('membershipcosts/edit/{id}', 'MembershipCostController@edit')->name('maintenance.membershipcosts.edit');
    Route::post('membershipcosts/edit', [
        'uses' => 'MembershipCostController@update',
        'as'   => 'maintenance.membershipcosts.update'
    ]);
    Route::delete('membershipcosts/delete/{id}', [
        'uses' => 'MembershipCostController@destroy',
        'as'   => 'maintenance.membershipcosts.delete'
    ]);

    Route::get('payments/index', 'PaymentController')->name('maintenance.payments.index');
    Route::post('payments/apply/{payment}', [
        'uses' => 'PaymentController@apply',
        'as'   => 'maintenance.payments.apply'
    ]);

    Route::get('functions/index', 'APIController@functions')->name('maintenance.functions.index');
    Route::post('functions/plans/order', [
        'uses' => 'APIController@orderplans',
        'as'   => 'maintenance.plans.order'
    ]);
    Route::post('functions/activities/order', [
        'uses' => 'APIController@orderactivities',
        'as'   => 'maintenance.activities.order'
    ]);


});
