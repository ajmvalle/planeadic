<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('GetStates','APIController@getStates');
Route::get('GetTowns/{id}','APIController@getTowns');

Route::get('GetSpecialties','APIController@getSpecialties');
Route::get('GetCourses/{id}','APIController@getCourses');
Route::get('GetPrograms/{id}','APIController@getPrograms');

Route::get('GetMethods/{id}','APIController@getMethods');
Route::get('GetAmbits','APIController@getAmbits');

Route::post('webhook','APIController@OxxoConfirm');
