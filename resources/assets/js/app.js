
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.jQuery = window.$ = require('jquery');
require('./bootstrap');
require('bootstrap-switch');


window.Vue = require('vue');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('edit-location', require('./components/EditLocation.vue'));
Vue.component('program',require('./components/Program.vue'));
Vue.component('method',require('./components/Method.vue'));
Vue.component('ambit',require('./components/Ambit.vue'));




const app = new Vue({
    el: '#app'
});


$('#file-upload').change(function() {
    var i = $(this).prev('label').clone();
    var file = $('#file-upload')[0].files[0].name;
    $(this).prev('label').text(file);
});
