@extends('layouts.primary')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="texto01" align="center">TÉRMINOS Y CONDICIONES DE USO</h2>
                    <p class="texto01" data-original-title="" title=""><strong data-original-title=""
                                                                               title="">Planeadic.com </strong>,
                        con ubicación en el Municipio de Medellín de Bravo, Veracruz México, pone a su disposición los
                        términos
                        y
                        condiciones de Uso del Sistema PLANEADIC</p>
                    <p class="about-pb">LEA DETALLADAMENTE ESTAS CONDICIONES DETENIDAMENTE ANTES DE SEGUIR USANDO LA
                        PÁGINA O
                        USAR LOS
                        SERVICIOS.</p>
                    <h4 class="titulo">1. Descripción del Servicio</h4>
                    <p class="texto01" data-original-title="" title="">Los servicios del Sitio constituyen una
                        plataforma de
                        tecnología
                        que permite a usuarios docentes elaborar planeaciones didácticas.
                        USTED RECONOCE QUE PLANEADIC ES UNA HERRAMIENTA DE APOYO PARA REALIZAR SUS PLANEACIONES
                        DIDÁCTICAS.</p>

                    <br>
                    <h4 class="titulo">2. Límites geográficos del servicio</h4>
                    <p class="texto01" data-original-title="" title="">PLANEADIC está dirigido actualmente a docentes
                        del Nivel
                        Medio
                        Superior de los Estados Unidos Mexicanos.</p>

                    <br>
                    <h4 class="titulo">3. Membresías</h4>
                    <p class="texto01" data-original-title="" title="">A través de una membresía de suscripción el
                        usuario
                        puede crear
                        y modificar portafolios y planeaciones didácticas dentro del sistema. Al crear la cuenta el
                        usuario
                        tiene la
                        posibilidad de activar una membresía gratuita de 7 días naturales que le permiten probar el
                        entorno.
                        Posterior
                        a ese tiempo el usuario deberá adquirir una membresía a través de uno de los planes mostrados
                        dentro de
                        la
                        plataforma para poder seguir usando los
                        servicios.</p>

                    <br>
                    <h4 class="titulo">4. Cancelaciones y Devoluciones</h4>
                    <p class="texto01" data-original-title="" title="">
                        Únicamente se permiten cancelaciones sobre membresías con tiempo mayor a 6 meses de vigencia con
                        respecto a la fecha de solicitud de cancelación, para lo cual se pacta el 50% de pena
                        convencional por terminación anticipada.
                        <br>
                        Para notificar una cancelación y recibir información del proceso de devolución, se pone a
                        disposición el formulario de contacto:<a
                                href="http://www.planeadic.com">
                            http://www.planedic.com/</a> o el correo electrónico buzonadmin@planeadic.com
                    </p>

                    <br>

                    <h4 class="titulo">5. Contracargos</h4>
                    <p class="texto01" data-original-title="" title="">
                        Por el momento, PLANEADIC NO maneja pago por medio de tarjetas de crédito o débito , por lo
                        tanto la
                        figura del
                        contracargo no aplica y se debe estar al tanto del párrafo anterior para cualquier cancelación.
                    </p>

                    <br>
                    <h4 class="titulo">6. Grupos Colegiados</h4>
                    <p class="texto01" data-original-title="" title="">Este sistema contiene una sección de grupos
                        colegiados donde los usuarios construyen de forma colaborativa planeaciones didácticas, al usar
                        esta funcionalidad usted acepta el uso de sus datos básicos (Nombre, Imagen o Avatar y escuela
                        de procedencia), tambien reconoce y manifiesta estar enterado de que las planeaciones realizadas en ese
                        esquema son <strong>públicas</strong>, y cualquier usuario con membresía activa puede
                        verlas o importarlas a sus portafolios individuales.
                    </p>

                    <h4 class="titulo">7. Jurisdicción</h4>
                    <p class="texto01" data-original-title="" title="">Este acuerdo estará regido en todos sus puntos
                        por las
                        leyes
                        vigentes en los Estados Unidos Mexicanos, y se regirá por lo dispuesto por la legislación
                        federal y
                        local
                        respectiva.
                        Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación,
                        alcance o
                        cumplimiento, será sometida a las leyes aplicables y a los Tribunales competentes.
                        Para la interpretación, cumplimiento y ejecución del presente Contrato, las partes expresamente
                        se
                        someten a la
                        jurisdicción de los tribunales competentes de la Ciudad de Veracruz, México, renunciando en
                        consecuencia
                        a
                        cualquier fuero que en razón de su domicilio presente o futuro pudiera corresponderles.
                    </p>

                    <br>
                    <h4 class="titulo">8. Modificación a los términos y condiciones del servicio</h4>
                    <p class="texto01" data-original-title="" title="">
                        Planeadic podrá modificar éstos Términos y Condiciones en cualquier momento haciendo
                        públicos en
                        el
                        Sitio los términos modificados. Todos los términos modificados entrarán en vigor a los 10 (diez)
                        días de
                        su
                        publicación. Dichas modificaciones serán comunicadas por Planeadic.com a los usuarios a
                        través de
                        correo electrónico.
                        Su acceso o uso continuo de PLANEADIC después de dicha publicación constituye su aceptación a
                        las
                        Condiciones, Términos y sus modificaciones.
                        Todo usuario que no esté de acuerdo con las modificaciones podrá solicitar la baja de la cuenta.
                    </p>


                    <p data-original-title="" title="" align="right"><span
                                class="texto01">Última   actualización <strong
                                    data-original-title="" title="">[<em>18/Noviembre/2018</em>]</strong></span><strong
                                data-original-title=""
                                title="">.</strong>
                    </p></td>
                    </tr>
                    </tbody></table>
                    <p class="texto01" data-original-title="" title="">&nbsp;</p>

                </div>

            </div>
        </div>

    </div>

@endsection
