@extends('layouts.app')

@section('title')

    <a class="navbar-brand">
        ¡Bienvenido {{auth()->user()->name}}! {{(auth()->user()->principal?"(Director)":"")}}
    </a>



@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        <h4 class="card-title">Navegación del Sistema</h4>
                        <p class="category">Por favor inicia visitando los lugares sugeridos</p>
                    </div>


                    <div class="card-content">

                        @if(auth()->user()->TotalAssigments<1)
                            <p>Bienvenido a Planeadic, tu aliado en la creación de planeaciones didácticas, recuerda
                                mantener una membresía vigente y disfruta de los beneficios de este sistema.</p>
                        @else
                            <p>Bienvenido a Planeadic, tu aliado en la creación de planeaciones didácticas, mantener tus
                                planeaciones actualizadas y con evidencias cargadas permite disponer de mayor tiempo
                                para tí y tus alumnos.</p>

                        @endif

                        <h4>Menús del Sistema</h4>

                        <ul>
                            <li class="list-group"><strong>Perfil de Usuario: </strong>Aquí puedes actualizar tu
                                información y establecer
                                preferencias, recuerda llenar ahí tu teléfono para poder comprar suscripciones.
                            </li>
                            <li class="list-group"><strong>Portafolio: </strong>En esta parte se encuentra el sistema,
                                iniciamos creando un portafolio que da cuenta de la asignación de grupo que tenemos. A
                                partir de ese momento inicia el proceso de creación de planeaciones
                            </li>

                            <li class="list-group"><strong>Marco Institucional: </strong>Recomendamos ampliamente
                                visitar el sitio del <strong>Nuevo Currículo de la EMS</strong>, principalmente la
                                sección <a
                                        href="http://www.sems.gob.mx/curriculoems/videos-planeacion-estrategica"
                                        target="_blank">Videos de Apoyo</a>
                            </li>
                        </ul>


                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
