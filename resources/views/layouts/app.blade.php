<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon.ico')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{asset('css/paper-dashboard.css')}}" rel="stylesheet"/>

    <!--  Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/themify-icons.css')}}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>


<div class="wrapper">
    <div class="sidebar" data-background-color="brown" data-active-color="danger">


        <!--
            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
        -->


        <div class="logo">

            <a href="{{route('home')}}" class="simple-text logo-mini">
                PL
            </a>

            <a href="{{route('home')}}" class="simple-text logo-normal">
                PLANEADIC
            </a>
        </div>
        <div class="sidebar-wrapper">

            <div class="user">
                <div class="photo ">
                    <img src="{{auth()->user()->imagen}}"/>
                </div>

                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed ">
	                       <span>
							   	{{auth()->user()->name}}
                               <b class="caret"></b>
						   </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{route('profile.show')}}">
                                    <span class="sidebar-mini">Mp</span>
                                    <span class="sidebar-normal ">Mi Perfil</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('account.index')}}">
                                    <span class="sidebar-mini">MC</span>
                                    <span class="sidebar-normal">Mi Cuenta @if(!is_null(Auth::user()->LastMembership))
                                            Vig. {{Auth::user()->LastMembership->to->format('d/m/y')}}
                                        @endif</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <ul class="nav">

                <li class="{{ Route::currentRouteName() == "home" ? 'active' : '' }}">
                    <a href="{{route('home')}}">
                        <i class="ti-home"></i>
                        <p>Inicio</p>
                    </a>
                </li>
                <li class="{{ getRoot(Route::currentRouteName()) == "assigments" ? 'active' : '' }}">
                    <a href="{{route('assigments.index')}}">
                        <i class="ti-briefcase"></i>
                        <p>Mis Portafolios</p>
                    </a>
                </li>

                <li class="{{ getRoot(Route::currentRouteName()) == "methods" ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#dashboardOverview">
                        <i class="ti-book"></i>
                        <p>Estrategias
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="dashboardOverview">
                        <ul class="nav">

                            <li>
                                <a href="{{route('methods.index')}}">
                                    <span class="sidebar-mini">EG</span>
                                    <span class="sidebar-normal">Generales</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('methods.mine')}}">
                                    <span class="sidebar-mini">EP</span>
                                    <span class="sidebar-normal">Propias</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="{{ getRoot(Route::currentRouteName()) == "tools" ? 'active' : '' }}">
                    <a href="{{route('tools.index')}}">
                        <i class="ti-marker-alt"></i>
                        <p>Instrumentos</p>
                    </a>
                </li>

                <li class="{{ getRoot(Route::currentRouteName()) == "groups" ? 'active' : '' }}">
                    <a href="{{route('groups.index')}}">
                        <i class="ti-world"></i>
                        <p>Grupos Colegiados</p>
                    </a>

                </li>

                <li class="{{ getRoot(Route::currentRouteName()) == "tutorials" ? 'active' : '' }}">
                    <a href="{{route('tutorials.index')}}">
                        <i class="ti-control-play"></i>
                        <p>Tutoriales</p>
                    </a>
                </li>

                @if(auth()->user()->isAdmin)
                    <li class="{{ getRoot(Route::currentRouteName()) == "maintenance" ? 'active' : '' }}">
                        <a href="{{route('maintenance.index')}}">
                            <i class="ti-settings"></i>
                            <p>Mantenimiento</p>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>


                    @yield('title')

                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn-rotate" data-toggle="dropdown">
                                <i class="ti-settings"></i>
                                <p>Opciones</p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>


        <div class="card-content" id="app">

            @if(!auth()->user()->activated)
                <div class="alert alert-warning">
                    <span><b> Cuenta no activa - </b> No será posible realizar altas o cambios hasta que realice la activación. Siga estas <a
                                href="{{route('activation.ask')}}">instrucciones</a>  </span>
                </div>
            @endif

            @if(!Auth::user()->hasMembership)
                <div class="alert alert-warning">
                    <span><b>Sin membresía- </b> No será posible hacer cambios sobre tus portafolios e instrumentos. Adquiere una renovación <a href="{{route('account.index') }}">aquí</a></span>
                </div>
                <div class="alert alert-info">
                    <span><b><a href="{{route('account.index') }}">Colabora</a>- </b> Puedes seguir trabajando en los grupos colegiados.</span>
                </div>
            @endif

            @yield('content')
        </div>


        <footer class="footer">

        </footer>

    </div>
</div>


</body>


<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
<script src="{{asset('js/jquery-3.1.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/perfect-scrollbar.min.js')}}" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="{{asset('js/jquery.validate.min.js')}}"></script>

<!-- Promise Library for SweetAlert2 working on IE -->
<script src="{{asset('js/es6-promise-auto.min.js')}}"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{asset('js/moment.min.js')}}"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="{{asset('js/bootstrap-datetimepicker.js')}}"></script>

<!--  Select Picker Plugin -->
<script src="{{asset('js/bootstrap-selectpicker.js')}}"></script>

<!--  Switch and Tags Input Plugins -->
<script src="{{asset('js/bootstrap-switch-tags.js')}}"></script>


<!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>

<!-- Sweet Alert 2 plugin -->
<script src="{{asset('js/sweetalert2.js')}}"></script>

<!-- Vector Map plugin -->
<script src="{{asset('js/jquery-jvectormap.js')}}"></script>


<!--  Full Calendar Plugin    -->
<script src="{{asset('js/fullcalendar.min.js')}}"></script>


<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{asset('js/paper-dashboard.js')}}"></script>
<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{asset('js/tools.js')}}"></script>

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</html>
