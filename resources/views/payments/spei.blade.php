<html>
<head>
    <link href="{{asset('css/oxxostyles.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
</head>
<body>
<div class="opps">
    <div class="opps-header">
        <div class="opps-reminder">Ficha digital. Imprima si lo considera necesario.</div>
        <div class="opps-info">
            <div class="opps-brand"><img src="{{asset('img/spei_brand.png')}}" alt="SPEI"></div>
            <div class="opps-ammount">
                <h3>Monto a pagar</h3>
                <h2>$ {{$payment->amount}} <sup>MXN</sup></h2>
            </div>
        </div>
        <div class="opps-reference">
            <h3>MEDIANTE BANCO <strong>STP</strong> REALICE PAGO A LA CLABE: </h3>
            <h1>{{$payment->reference}}</h1>
            <h4>Expira el {{$payment->expires->format('d/m/y')}}</h4>
        </div>
    </div>
    <div class="opps-instructions">
        <h3>Instrucciones</h3>
        <ol>
            <li>Ingresa al portal de tu Banco.</li>
            <li>Registra la CLABE arriba mencionada. <strong>El banco es STP</strong>.</li>
            <li>Realiza la transferencia correspondiente por la cantidad exacta en esta ficha, <strong>de lo contrario
                    se rechazará el cargo</strong>.
            </li>
            <li>Al confirmar tu pago, el portal de tu banco generará un comprobante digital. <strong>En el podrás
                    verificar que se haya realizado correctamente.</strong> Conserva el comprobante de pago.
            </li>
        </ol>
        <div class="opps-footnote">Al completar estos pasos recibirás un correo de <strong>Nombre del negocio</strong>
            confirmando tu pago.
        </div>


    </div>

</div>
</body>
</html>
