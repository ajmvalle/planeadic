@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('tools.index')}}">Mis Instrumentos de Evaluación</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$tool->desc}}</h4>
                        <p class="category">Ingrese los datos del instrumento, posteriormente defina sus indicadores</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'tools.update']) !!}
                        {{Form::hidden('kind',$tool->kind)}}
                        {{Form::hidden('id',$tool->id)}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input "
                                           value="{{ $tool->title }}"
                                           placeholder="¿Qué evalúa este instrumento?">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Portafolio:') !!}
                                    {!! Form::select('assigment_id', $assigments, $tool->assigment_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('tipo') ? ' has-error' : '' }}">
                                    <label>Tipo</label>
                                    <input type="text" name="tipo" disabled class="form-control border-input"
                                           value="{{ $tool->desc }}">
                                </div>
                            </div>
                        </div>

                        @if($tool->kind=='list')

                            <div class="row">
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header3') ? ' has-error' : '' }}">
                                        <label>Nivel 1</label>
                                        <input type="text" name="header3" class="form-control border-input "
                                               value="{{$tool->header3}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header4') ? ' has-error' : '' }}">
                                        <label>Nivel 2</label>
                                        <input type="text" name="header4" class="form-control border-input "
                                               value="{{$tool->header4}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">

                                </div>
                            </div>

                        @else

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header1') ? ' has-error' : '' }}">
                                        <label>Nivel 1</label>
                                        <input type="text" name="header1" class="form-control border-input "
                                               value="{{$tool->header1}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header2') ? ' has-error' : '' }}">
                                        <label>Nivel 2</label>
                                        <input type="text" name="header2" class="form-control border-input "
                                               value="{{$tool->header2}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header3') ? ' has-error' : '' }}">
                                        <label>Nivel 3</label>
                                        <input type="text" name="header3" class="form-control border-input "
                                               value="{{$tool->header3}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header4') ? ' has-error' : '' }}">
                                        <label>Nivel 4</label>
                                        <input type="text" name="header4" class="form-control border-input "
                                               value="{{$tool->header4}}"
                                               placeholder="Grado">
                                    </div>
                                </div>
                            </div>

                        @endif


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                    <label>Notas</label>
                                    <textarea rows="3" name="notes" class="form-control border-input "
                                              placeholder="Consideraciones adicionales">{{ $tool->notes }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                            <button type="button" onclick="location.href='{{ route('tools.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                            <a target="_blank"
                               href="{{route('tools.print',$tool->id)}}"
                               title="Imprimir Instrumento" type="button"
                               class="btn btn-warning btn-fill btn-wd">Imprimir</a>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="card-header">
                                <h4 class="card-title">Indicadores</h4>
                                <p class="category">Detalle de criterios principales en evaluación</p>
                            </div>

                        </div>
                        <div class="col-md-8 ">
                            <br>
                            <button onclick="location.href='{{ route('indicators.add',['tool'=>$tool]) }}'"
                                    type="button"
                                    title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                        class="fa fa-plus"></i>Agregar Indicador
                            </button>
                        </div>
                    </div>
                    <div class="card-content">

                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Indicador</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($tool->indicators as $indicator)
                                    <tr>
                                        <td>{{$indicator->title}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a onclick="location.href='{{ route('indicators.edit',['id'=>$indicator->id]) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                                <div class="col-md-4">
                                                    @include('buttons.delete',['route'=>'indicators.delete','id'=>$indicator->id])
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection
