@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('tools.index')}}">Mis Instrumentos de Evaluación</a>

@endsection

@section('content')

    <div class="card-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4 class="card-title">Instrumentos</h4>
                                <p class="category">Administre aquí los instrumentos y criterios que utiliza para
                                    evaluar</p>
                            </div>

                            <div class="col-md-6 ">
                                <button onclick="location.href='{{ route('tools.addrubric') }}'" type="button"
                                        title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>Rúbrica
                                </button>
                                <button onclick="location.href='{{ route('tools.addlist') }}'" type="button"
                                        title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>Lista de Cotejo
                                </button>
                                <button onclick="location.href='{{ route('tools.addscale') }}'" type="button"
                                        title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>Escala Estimativa
                                </button>
                            </div>
                        </div>

                        <div class="card-content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Tipo</th>
                                <th>Título</th>
                                <th>Portafolio</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($tools as $tool)
                                    <tr>
                                        <td>{{$tool->desc}}</td>
                                        <td>{{$tool->title}}</td>
                                        <td>{{empty($tool->assigment_id) ? "" : $tool->assigment->name}}</td>
                                        <td>
                                            <div class="row">

                                                <div class="col-md-4">
                                                    <a onclick="location.href='{{ route('tools.edit',['id'=>$tool->id]) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-success btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                                <div class="col-md-4">
                                                    <a target="_blank"
                                                       href="{{route('tools.print',$tool->id)}}"
                                                       title="Imprimir Instrumento" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-print"></i></a>
                                                </div>
                                                <div class="col-md-4">
                                                    @include('buttons.delete',['route'=>'tools.delete','id'=>$tool->id])
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$tools->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
