@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('tools.index')}}">Mis Instrumentos de Evaluación</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$tipo}}</h4>
                        <p class="category">Ingrese los datos del instrumento, posteriormente defina sus indicadores</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'tools.store']) !!}
                        {{Form::hidden('kind',$kind)}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input "
                                           value="{{ old('title') }}"
                                           placeholder="¿Qué evalúa este instrumento?">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Portafolio:') !!}
                                    {!! Form::select('assigment_id', $assigments, 0, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('tipo') ? ' has-error' : '' }}">
                                    <label>Tipo</label>
                                    <input type="text" name="tipo" disabled class="form-control border-input"
                                           value="{{ $tipo }}">
                                </div>
                            </div>
                        </div>

                        @if($kind=='list')

                            <div class="row">
                                <div class="col-md-3">

                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header3') ? ' has-error' : '' }}">
                                        <label>Nivel 1</label>
                                        <input type="text" name="header3" class="form-control border-input "
                                               value="No"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header4') ? ' has-error' : '' }}">
                                        <label>Nivel 2</label>
                                        <input type="text" name="header4" class="form-control border-input "
                                               value="Sí"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">

                                </div>
                            </div>

                        @else

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header1') ? ' has-error' : '' }}">
                                        <label>Nivel 1</label>
                                        <input type="text" name="header1" class="form-control border-input "
                                               value="{{$kind=='rubric'?'Deficiente':'Nunca'}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header2') ? ' has-error' : '' }}">
                                        <label>Nivel 2</label>
                                        <input type="text" name="header2" class="form-control border-input "
                                               value="{{$kind=='rubric'?'Regular':'Menos de lo esperado'}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header3') ? ' has-error' : '' }}">
                                        <label>Nivel 3</label>
                                        <input type="text" name="header3" class="form-control border-input "
                                               value="{{$kind=='rubric'?'Bueno':'Más de lo esperado'}}"
                                               placeholder="Grado">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('header4') ? ' has-error' : '' }}">
                                        <label>Nivel 4</label>
                                        <input type="text" name="header4" class="form-control border-input "
                                               value="{{$kind=='rubric'?'Excelente':'Siempre'}}"
                                               placeholder="Grado">
                                    </div>
                                </div>
                            </div>

                        @endif


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                    <label>Notas</label>
                                    <textarea rows="3" name="notes" class="form-control border-input "
                                              placeholder="Consideraciones adicionales">{{ old('notes') }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                            <button type="button" onclick="location.href='{{ route('tools.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
