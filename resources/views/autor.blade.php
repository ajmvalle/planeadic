<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Alfredo Morales</title>

    <!-- favicon -->
    <link href="favicon.ico" rel=icon>

    <!-- web-fonts -->
    <link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">

    <!-- font-awesome -->
    <link href="{{asset('cv/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="{{asset('cv/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Style CSS -->
    <link href="{{asset('cv/css/style.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar">
<div id="main-wrapper">
    <!-- Page Preloader -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>

    <div class="columns-block container">
        <div class="left-col-block blocks">
            <header class="header theiaStickySidebar">
                <div class="profile-img">
                    <img src="{{asset('cv/img/avatar.jpg')}}" class="img-responsive" alt=""/>
                </div>
                <div class="content">
                    <h1 class="text-center">Ing. Alfredo Jesús Morales del Valle</h1>
                    <h2 class="text-center"><span class="lead"><strong>Docente - Programador</strong></span></h2>

                    <div class="about-text">
                        <p>
                            Me considero un profesional visionario comprometido con la calidad de los trabajos en los
                            que participo. A través del tiempo he tenido la fortuna de trabajar en equipos diversos
                            siempre con la actitud de aprender algo nuevo todos los días.
                        </p>

                        <p>Me motivan dos cosas en mi vida profesional, conectar a mis alumnos al fascinante mundo de la
                            tecnología y desarrollar software práctico que deje una buena impresión en los usuarios
                            que lo utilizan.</p>

                    </div>


                    <ul class="social-icon">
                        <li><a href="https://www.facebook.com/AlfredoJ.Morales"><i class="fa fa-facebook"
                                                                                   aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/planeadic"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/alfredo-morales-3ab92312/"><i class="fa fa-linkedin"
                                                                                               aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>

            </header>
            <!-- .header-->
        </div>
        <!-- .left-col-block -->


        <div class="right-col-block blocks">
            <div class="theiaStickySidebar">
                <section class="expertise-wrapper section-wrapper gray-bg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title">
                                    <h2>Áreas profesionales</h2>
                                </div>
                            </div>
                        </div>
                        <!-- .row -->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="expertise-item">
                                    <h3>Docencia</h3>

                                    <p>
                                        Más de 300 horas de capacitación docente de COSDAC. Maestría en Ciencias de la
                                        Educación concluída. Clases de Programación y Matemáticas.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="expertise-item">
                                    <h3>Análisis de Sistemas </h3>

                                    <p>
                                        Más de 5 años de experiencia realizando análisis funcional de sistemas de
                                        trabajo, coordinación de grupos de trabajo.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="expertise-item">
                                    <h3>Programación</h3>

                                    <p>
                                        Desarrollador de software desde 2006, diversas plataformas dominadas desde
                                        entonces.
                                    </p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="expertise-item">
                                    <h3>Valores</h3>

                                    <p> Considero a la responsabilidad, la humildad, la asertividad y la ética como las
                                        bases donde se desarrollan lo grandes trabajos que permiten crecer en lo
                                        profesional y lo humano.</p>
                                </div>
                            </div>

                        </div>


                    </div>
                </section>
                <!-- .expertise-wrapper -->

                <section class="section-wrapper skills-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title">
                                    <h2>Habilidades</h2>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="progress-wrapper">

                                    <div class="progress-item">
                                        <span class="progress-title">Inglés (Berlitz Level 4)</span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="62"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 75%"><span class="progress-percent"> 75%</span>
                                            </div>
                                        </div>
                                        <!-- .progress -->
                                    </div>
                                    <!-- .skill-progress -->


                                    <div class="progress-item">
                                        <span class="progress-title">Desarrollador Laravel Back-End</span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="90"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 90%"><span class="progress-percent"> 90%</span>
                                            </div>
                                        </div>
                                        <!-- /.progress -->
                                    </div>
                                    <!-- /.skill-progress -->


                                    <div class="progress-item">
                                        <span class="progress-title">Análisis de Sistemas (RUP)</span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 86%;"><span class="progress-percent"> 86%</span>
                                            </div>
                                        </div>
                                        <!-- /.progress -->
                                    </div>
                                    <!-- /.skill-progress -->

                                    <div class="progress-item">
                                        <span class="progress-title">Bases de datos SQL</span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="55"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 97%;"><span class="progress-percent"> 97%</span>
                                            </div>
                                        </div>
                                        <!-- /.progress -->
                                    </div>
                                    <!-- /.skill-progress -->
                                    <div class="progress-item">
                                        <span class="progress-title">Web Developer Front-End</span>

                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="55"
                                                 aria-valuemin="0"
                                                 aria-valuemax="100" style="width: 76%;"><span class="progress-percent"> 76%</span>
                                            </div>
                                        </div>
                                        <!-- .progress -->
                                    </div>
                                    <!-- .skill-progress -->

                                </div>
                                <!-- /.progress-wrapper -->
                            </div>
                        </div>
                        <!--.row -->
                    </div>
                    <!-- .container-fluid -->
                </section>
                <!-- .skills-wrapper -->

                <section class="section-wrapper section-experience gray-bg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title"><h2>Experiencia Profesional</h2></div>
                            </div>
                        </div>
                        <!--.row-->
                        <div class="row">
                            <div class="col-md-12">

                                <div class="content-item">
                                    <small>2015 - Presente</small>
                                    <h3>Docente de Programación y Matemáticas</h3>
                                    <h4>CBTIS No. 190</h4>

                                    <p>Boca del Río, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                                <div class="content-item">
                                    <small>2012 - 2015</small>
                                    <h3>Analista Funcional de Sistemas (Carga al Mil)</h3>
                                    <h4>Manpower (Asignado a Tenaris TAMSA)</h4>

                                    <p>Veracruz, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                                <div class="content-item">
                                    <small>2009 - 2012</small>
                                    <h3>Encargado de Sistemas</h3>
                                    <h4>Corporativo Enciso</h4>

                                    <p>Veracruz, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!-- .container-fluid -->

                </section>
                <!-- .section-experience -->

                <section class="section-wrapper section-education">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title"><h2>Formación Académica</h2></div>
                            </div>
                        </div>
                        <!--.row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="content-item">
                                    <small>2017 - 2018</small>
                                    <h3>Maestro en Ciencias de la Educación(Certificado en Trámite)</h3>
                                    <h4>Instituto de Estudios Universitarios de Puebla</h4>

                                    <p>Campus Veracruz, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                                <div class="content-item">
                                    <small>2001 - 2006</small>
                                    <h3>Ingeniero en Sistemas Computacionales</h3>
                                    <h4>Instituto Tecnológico de Veracruz</h4>

                                    <p>Veracruz, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                                <div class="content-item">
                                    <small>1998 - 2001</small>
                                    <h3>Técnico en computación</h3>
                                    <h4>CBTIS No. 79</h4>

                                    <p>Boca del Río, Ver.</p>
                                </div>
                                <!-- .experience-item -->
                            </div>
                        </div>
                        <!--.row-->
                    </div>
                    <!-- .container-fluid -->

                </section>
                <!-- .section-education -->

                <section class="section-wrapper section-interest gray-bg">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title">
                                    <h2>Hobbies</h2>
                                </div>
                            </div>
                        </div>
                        <!-- .row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="content-item">
                                    <h3>Cine</h3>

                                    <p>Me interesan películas de diferentes géneros, acudo al cine cada fin de semana
                                        con mi familia, es un arte que disfrutamos mucho.</p>
                                </div>

                                <div class="content-item">
                                    <h3>Música</h3>

                                    <p>Me gusta mucho la música en Inglés de los 70's, 80's y 90's cada una de esas
                                        décadas dejó piezas únicas que mantienen referencia en estos tiempos.</p>
                                </div>
                            </div>
                        </div>
                        <!-- .row -->

                    </div>
                </section>
                <!-- .section-publications -->


                <footer class="footer">
                    <div class="copyright-section">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="copytext">&copy; Resumex. All rights reserved | Design By: <a
                                                href="https://themehippo.com">themehippo</a></div>
                                </div>
                            </div>
                            <!--.row-->
                        </div>
                        <!-- .container-fluid -->
                    </div>
                    <!-- .copyright-section -->
                </footer>
                <!-- .footer -->
            </div>
            <!-- Sticky -->
        </div>
        <!-- .right-col-block -->
    </div>
    <!-- .columns-block -->
</div>
<!-- #main-wrapper -->

<!-- jquery -->
<script src="{{asset('cv/js/jquery-2.1.4.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('cv/js/bootstrap.min.js')}}"></script>
<script src="{{asset('cv/js/theia-sticky-sidebar.js')}}"></script>
<script src="{{asset('cv/js/scripts.js')}}"></script>
</body>
</html>