@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Asignaturas</h4>
                        <p class="category">Editar un método o técnica</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.methods.update','enctype'=>'multipart/form-data']) !!}
                        {{Form::hidden('id',$method->id)}}
                       
                        {{Form::hidden('dev_id',$method->dev->id)}}
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Método o Técnica</label>
                                    <input type="text" name="title" class="form-control border-input"
                                           value="{{$method->title}}"
                                           placeholder="Titule el método o técnica">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Resumen</label>
                                    <input type="text" name="summary" class="form-control border-input"
                                           value="{{$method->summary}}"
                                           placeholder="Describa brevemente el objetivo del método">
                                </div>
                            </div>
                        </div>

                        <h4>Actividades de Desarrollo</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Docente</label>
                                    <textarea  name="dev[teacher]" class="form-control border-input" rows="4"
                                               placeholder="Actividades del docente">{{$method->dev->teacher}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Alumno</label>
                                    <textarea type="text" name="dev[student]" class="form-control border-input" rows="4"
                                              placeholder="Actividades del alumno">{{$method->dev->student}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <br>
                                    <label>Tiempo</label>
                                    <input type="text" name="dev[time]" class="form-control border-input"
                                           value="{{$method->dev->time}}"
                                           placeholder="Tiempo estimado en Horas">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <br>
                                    <label>Producto</label>
                                    <input type="text" name="dev[product]" class="form-control border-input"
                                           value="{{$method->dev->product}}"
                                           placeholder="Productos esperados">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Referencias Pedagógicas</label>
                                    <textarea name="reference" class="form-control border-input" size="3"
                                              placeholder="Fundamentos pedagógicos aplicados">{{$method->reference}}                                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group h-100 d-inline-blocks text-center">
                                    <a href="{{$method->imagen}}"> <img class="border-white text-center"
                                                                        height="300" width="300"
                                                                        src="{{$method->imagen}}"/></a>
                                    <label class="fileContainer center-block text-center">
                                        Toca <strong>aquí</strong> para cambiar Imagen
                                        <input name="imagen" type="file"/>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.methods.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
