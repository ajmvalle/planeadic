@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Competencias</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>

                        <div class="col-md-8 ">
                            <Button onclick="location.href='{{ route('maintenance.competences.add') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </Button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Clave</th>
                            <th>Título</th>
                            <th>Sección</th>
                            <th>Acuerdo</th>
                            <th>Tipo</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($competences as $competence)
                                <tr>
                                    <td>{{$competence->id}}</td>
                                    <td>{{$competence->code}}</td>
                                    <td>{{$competence->title}}</td>
                                    <td>{{$competence->section}}</td>
                                    <td>{{$competence->law}}</td>
                                    <td>{{$competence->kind}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.competences.edit',['id'=>$competence->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.competences.delete','id'=>$competence->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$competences->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
