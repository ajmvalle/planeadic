@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias</h4>
                        <p class="category">Dar de alta competencia</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.competences.store']) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="code" class="form-control border-input"
                                           placeholder="Clave o código">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input"
                                           placeholder="Texto de la competencia">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Acuerdo o Ley</label>
                                    <input type="text" name="law" class="form-control border-input"
                                           placeholder="Acuerdo secretarial">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Sección</label>
                                    <input type="text" name="section" class="form-control border-input"
                                           placeholder="Sección o grupo de la competencia">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tipo</label>
                                    <select  name="kind" class="form-control border-input">
                                        <option value="G" selected="selected">Genérica</option>
                                        <option value="D">Disciplinar</option>
                                        <option value="P" >Profesional</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.competences.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
