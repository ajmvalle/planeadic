@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias</h4>
                        <p class="category">Modificar Competencia</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.competences.update']) !!}
                        {{Form::hidden('id',$competence->id)}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="code" class="form-control border-input"
                                           value="{{$competence->code}}"
                                           placeholder="Clave o código">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input"
                                           value="{{$competence->title}}"
                                           placeholder="Texto de la competencia">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Acuerdo o Ley</label>
                                    <input type="text" name="law" class="form-control border-input"
                                           value="{{$competence->law}}"
                                           placeholder="Acuerdo secretarial">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Sección</label>
                                    <input type="text" name="section" class="form-control border-input"
                                           value="{{$competence->section}}"
                                           placeholder="Sección o grupo de la competencia">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tipo</label>
                                    <select name="kind" class="form-control border-input">
                                        <option value="G" {{$competence->kind == 'G'? ("selected='selected'"): ("") }}>
                                            Genérica
                                        </option>
                                        <option value="D" {{$competence->kind == 'D'? ("selected='selected'"): ("") }}>
                                            Disciplinar
                                        </option>
                                        <option value="P" {{$competence->kind == 'P'? ("selected='selected'"): ("") }}>
                                            Profesional
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.competences.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="card">
                    <hr>
                    <div class="col-md-4">
                        <h4 class="card-title">Atributos (Si aplican)</h4>
                        <p class="category">Detalle de la Competencia</p>
                    </div>

                    <div class="col-md-8 ">
                        <button onclick="location.href='{{ route('maintenance.attributes.add',['competence'=>$competence]) }}'" type="button"
                             title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <div class="card-content">
                        <div class="card-content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>ID</th>
                                <th>Código</th>
                                <th>Título</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($competence->attributes as $attribute)
                                    <tr>
                                        <td>{{$attribute->id}}</td>
                                        <td>{{$attribute->code}}</td>
                                        <td>{{$attribute->title}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a onclick="location.href='{{ route('maintenance.attributes.edit',['id'=>$attribute->id]) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                                <div class="col-md-4">
                                                    @include('buttons.delete',['route'=>'maintenance.attributes.delete','id'=>$attribute->id])
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
