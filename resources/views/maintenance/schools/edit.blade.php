@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Escuelas</h4>
                        <p class="category">Editar datos</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición
                                .<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.schools.update']) !!}
                        {!! Form::hidden('id',$school->id) !!}


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Subsistema:') !!}
                                    {!! Form::select('subsystem_id', $subsystems, $school->subsystem_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <select name="name" class="form-control border-input"
                                            placeholder="Nombre completo de la escuela">
                                        <option {{$school->name=="CBTIS"?"selected":""}} >CBTIS</option>
                                        <option {{$school->name=="CETIS"?"selected":""}}>CETIS</option>
                                        <option {{$school->name=="CBTA"?"selected":""}}>CBTA</option>
                                        <option {{$school->name=="CETMAR"?"selected":""}}>CETMAR</option>
                                        <option {{$school->name=="CETAC"?"selected":""}}>CETAC</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Plantel</label>
                                    <input type="text" name="number" class="form-control border-input"
                                           value="{{$school->number}}"
                                           placeholder="Plantel"/>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nombre alternativo</label>
                                    <input type="text" name="subname" class="form-control border-input"
                                           value="{{$school->subname}}"
                                           placeholder="Nombre por bicentenario o aniversario">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>CCT</label>
                                    <input type="text" name="officialkey" class="form-control border-input"
                                           value="{{$school->officialkey}}"
                                           placeholder="Centro de trabajo">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" name="address" class="form-control border-input"
                                           value="{{$school->address}}"
                                           placeholder="Ubicación de plantel, procure agregar ciudad, estado, etc.">
                                </div>
                            </div>
                        </div>


                        <edit-location :town="{{json_encode($school->town)}}"
                                       :state="{{json_encode($school->town->state)}}"></edit-location>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.schools.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection

