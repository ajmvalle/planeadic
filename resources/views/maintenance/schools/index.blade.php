@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Escuelas</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>

                        <div class="col-md-8 ">
                            <button onclick="location.href='{{ route('maintenance.schools.add') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Número</th>
                            <th>Subsistema</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($schools as $school)
                                <tr>
                                    <td>{{$school->id}}</td>
                                    <td>{{$school->name}}</td>
                                    <td>{{$school->number}}</td>
                                    <td>{{$school->subsystem->shortname}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.schools.edit',['id'=>$school->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.schools.delete','id'=>$school->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$schools->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
