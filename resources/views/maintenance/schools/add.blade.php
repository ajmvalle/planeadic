@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Escuelas</h4>
                        <p class="category">Dar de alta escuelas</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.schools.store']) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::Label('item', 'Subsistema:') !!}
                                        {!! Form::select('subsystem_id', $subsystems, 0, ['class' => 'form-control border-input']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <select  name="name" class="form-control border-input"  placeholder="Nombre completo de la escuela">
                                            <option>CBTIS</option>
                                            <option>CETIS</option>
                                            <option>CBTA</option>
                                            <option>CETAC</option>
                                            <option>CETMAR</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Plantel</label>
                                        <input  type="text" name="number" class="form-control border-input"  placeholder="Plantel"/>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nombre alternativo</label>
                                        <input type="text" name="subname" class="form-control border-input" placeholder="Nombre por bicentenario o aniversario" >
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>CCT</label>
                                        <input type="text" name="officialkey" class="form-control border-input" placeholder="Centro de trabajo" >
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" name="address" class="form-control border-input" placeholder="Ubicación de plantel, procure agregar ciudad, estado, etc." >
                                    </div>
                                </div>
                            </div>

                            <edit-location :town="{{json_encode(new \App\Town())}}"
                                           :state="{{json_encode(new \App\State())}}"></edit-location>

                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                                <button type="button" onclick="location.href='{{ route('maintenance.schools.index') }}'" class="btn btn-success btn-fill btn-wd">Cancelar</button>
                            </div>
                            <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
