@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Vistas</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>

                        <div class="col-md-8 ">
                            <button onclick="location.href='{{ route('maintenance.formats.add') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Vista</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($formats as $format)
                                <tr>
                                    <td>{{$format->id}}</td>
                                    <td>{{$format->name}}</td>
                                    <td>{{$format->view}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.formats.edit',['id'=>$format->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.formats.delete','id'=>$format->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$formats->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
