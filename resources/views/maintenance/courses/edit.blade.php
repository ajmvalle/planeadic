@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Asignaturas</h4>
                        <p class="category">Dar de alta curso o asignatura</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.courses.update']) !!}

                        {!! Form::hidden('id',$course->id) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Asignatura/Submódulo</label>
                                    <input type="text" name="name" class="form-control border-input"
                                           value="{{$course->name}}"
                                           placeholder="Nombre completo de la asignatura o Submódulo">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Módulo</label>
                                    <input type="text" name="module" class="form-control border-input"
                                           value="{{$course->module}}"
                                           placeholder="Ingresar nombre de módulo si Aplica">
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                {!! Form::Label('item', 'Carrera:') !!}
                                {!! Form::select('specialty_id', $specialties, $course->specialty_id, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Campo disciplnar</label>
                                    <input type="text" name="area" class="form-control border-input"
                                           value="{{$course->area}}"
                                           placeholder="Campo disciplinar o tipo de formación">
                                </div>
                            </div>
                        </div>
                            
                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.courses.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
