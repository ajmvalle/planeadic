@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Costos de Membresías</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>

                        <div class="col-md-8 ">
                            <button onclick="location.href='{{ route('maintenance.membershipcosts.add') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Meses</th>
                            <th>Costo</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($membershipcosts as $membershipcost)
                                <tr>
                                    <td>{{$membershipcost->id}}</td>
                                    <td>{{$membershipcost->name}}</td>
                                    <td>{{$membershipcost->months}}</td>
                                    <td>{{$membershipcost->cost}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.membershipcosts.edit',['id'=>$membershipcost->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.membershipcosts.delete','id'=>$membershipcost->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$membershipcosts->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
