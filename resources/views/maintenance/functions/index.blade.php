@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="card-header">
                <div class="col-md-4">

                    <h4 class="card-title">Funciones Especiales</h4>
                    <p class="category">Mantenimiento</p>
                </div>

                <div class="col-md-8 ">

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="card">
                    <div class="card-content">
                        <div class="row">

                            {!! Form::open(['method'=> 'POST','route'=>'maintenance.plans.order']) !!}

                            <button onclick="return confirm('Confirme Corrida de Orden, le sugiero hacer esto muy tarde')"
                                    title="Eliminar" type="submit"
                                    class="btn btn-sm btn-danger btn-icon"><i
                                        class="fa fa-play"></i> Ordenar Planeaciones
                            </button>
                            {!! Form::close() !!}

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-3">
                <div class="card">
                    <div class="card-content">
                        <div class="row">

                            {!! Form::open(['method'=> 'POST','route'=>'maintenance.activities.order']) !!}

                            <button onclick="return confirm('Confirme Corrida de Orden, le sugiero hacer esto muy tarde')"
                                    title="Eliminar" type="submit"
                                    class="btn btn-sm btn-danger btn-icon"><i
                                        class="fa fa-play"></i> Ordenar Actividades
                            </button>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>


@endsection
