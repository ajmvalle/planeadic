@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Bloques de Programa</h4>
                        <p class="category">Dar de alta bloque de programa</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.store']) !!}
                        {{{Form::hidden('program_id',$program->id)}}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Programa</label>
                                    <input type="text" name="competencecode" class="form-control border-input"
                                           value="{{$program->name}}"
                                           disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" name="description" class="form-control border-input"
                                           placeholder="Descripción general del bloque">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Contenido Central:') !!}
                                    {!! Form::select('centralcontent_id', $centralcontents, 0, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Competencia a desarrollar</label>
                                    <input type="text" name="competence" class="form-control border-input"
                                           placeholder="Competencia Profesional">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Situaciones</label>
                                    <textarea type="text" name="situations" class="form-control border-input"
                                              placeholder="Situaciones (Componente profesional)"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('maintenance.programs.edit',['id'=>$program->id]) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
