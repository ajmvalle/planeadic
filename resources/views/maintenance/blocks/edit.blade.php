@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Bloques de Programa</h4>
                        <p class="category">Edición de bloque de programa</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.update']) !!}
                        {{Form::hidden('program_id',$block->program_id)}}
                        {{Form::hidden('id',$block->id)}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Programa</label>
                                    <input type="text" name="competencecode" class="form-control border-input"
                                           value="{{$block->program->name}}"
                                           disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" name="description" class="form-control border-input"
                                           value="{{$block->description}}"
                                           placeholder="Descripción general del bloque">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Contenido Central:') !!}
                                    {!! Form::select('centralcontent_id', $centralcontents, $block->centralcontent_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Competencia a desarrollar</label>
                                    <input type="text" name="competence" class="form-control border-input"
                                           value="{{$block->competence}}"
                                           placeholder="Competencia Profesional">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Situaciones</label>
                                    <textarea type="text" name="situations" class="form-control border-input"
                                              placeholder="Situaciones (Componente profesional)">{{$block->situations}}</textarea>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('maintenance.programs.edit',['id'=>$block->program_id]) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Atributos de Competencias Genéricas</h4>
                    </div>
                    <div class="card-content">
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.addattribute']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">

                                {{Form::hidden('id',$block->id)}}

                                {!! Form::select('attribute_id', $attributes, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}
                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Número</th>
                                <th>Atributo</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($block->attributes as $attribute)
                                    <tr>
                                        <td>{{$attribute->competence->code}}</td>
                                        <td>{{$attribute->code}}</td>
                                        <td>{{$attribute->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.removeattribute']) !!}
                                            {{Form::hidden('attribute_id',$attribute->id)}}
                                            {{Form::hidden('id',$block->id)}}
                                            <button onclick="return confirm('Confirme eliminación del atributo')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>  <!-- end card -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias Disciplinares</h4>
                    </div>
                    <div class="card-content">

                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.addcompetence']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">


                                {{Form::hidden('id',$block->id)}}

                                {!! Form::select('competence_id', $dcompetences, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}

                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Competencia</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($block->discompetences as $competence)
                                    <tr>
                                        <td>{{$competence->code}}</td>
                                        <td>{{$competence->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.removecompetence']) !!}
                                            {{Form::hidden('competence_id',$competence->id)}}
                                            {{Form::hidden('id',$block->id)}}
                                            <button onclick="return confirm('Confirme eliminación de la competencia')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>  <!-- end card -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias Profesionales</h4>
                    </div>
                    <div class="card-content">

                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.addcompetence']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">


                                {{Form::hidden('id',$block->id)}}

                                {!! Form::select('competence_id', $pcompetences, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}

                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Competencia</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($block->profcompetences as $competence)
                                    <tr>
                                        <td>{{$competence->code}}</td>
                                        <td>{{$competence->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'maintenance.blocks.removecompetence']) !!}
                                            {{Form::hidden('competence_id',$competence->id)}}
                                            {{Form::hidden('id',$block->id)}}
                                            <button onclick="return confirm('Confirme eliminación de la competencia')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>  <!-- end card -->
        </div>


    </div>


@endsection
