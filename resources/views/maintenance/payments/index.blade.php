@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Pagos</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Monto</th>
                            <th>Status</th>
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{$payment->id}}</td>
                                    <td>{{$payment->user->fullname}}</td>
                                    <td>{{$payment->amount}}</td>
                                    <td>{{$payment->status}}</td>
                                    <td>{{$payment->membershipcost->name}}</td>
                                    <td>{{$payment->created_at}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <form method="POST"
                                                      action="{{route('maintenance.payments.apply',['$payment'=>$payment])}}">
                                                    {!! Form::token() !!} {{ csrf_field() }}
                                                    <button onclick="return confirm('¿Marcar este pago como aplicado?')"
                                                            title="Aplicar" type="submit"
                                                            class="btn btn-sm btn-danger btn-icon"><i
                                                                class="fa fa-money"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$payments->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
