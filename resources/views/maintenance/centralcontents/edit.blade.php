@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Contenidos centrales</h4>
                        <p class="category">Modificar contenido central</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.centralcontents.update']) !!}
                        {{Form::hidden('id',$centralcontent->id)}}

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Programa:') !!}
                                    {!! Form::select('program_id', $programs, $centralcontent->program_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Eje de aprendizaje</label>
                                    <input type="text" name="axis" class="form-control border-input"
                                           value="{{$centralcontent->axis}}"
                                           placeholder="Eje de aprendizaje">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Componente</label>
                                    <input type="text" name="component" class="form-control border-input"
                                           value="{{$centralcontent->component}}"
                                           placeholder="Componente">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contenido Central</label>
                                    <textarea type="text" name="title" class="form-control border-input"
                                              placeholder="Texto del Contenido Central">{{$centralcontent->title}}</textarea>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contenidos Específicos</label>
                                    <textarea size="3" type="text" name="specifics" class="form-control border-input"
                                              placeholder="Contenidos Específicos">{{$centralcontent->specifics}}
                                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Aprendizajes Esperados</label>
                                    <textarea size="3" type="text" name="expecteds" class="form-control border-input"
                                              placeholder="Aprendizajes esperados">{{$centralcontent->expecteds}}
                                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Procesos de Aprendizaje </label>
                                    <textarea size="3" type="text" name="process" class="form-control border-input"
                                              placeholder="Procesos de Aprendizaje">{{$centralcontent->process}}
                                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Productos Esperados</label>
                                    <textarea size="3" type="text" name="products" class="form-control border-input"
                                              placeholder="Productos Esperados">{{$centralcontent->products}}
                                        </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('maintenance.centralcontents.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
