@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Atributos</h4>
                        <p class="category">Modificar Atributos de Competencia</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.attributes.update']) !!}
                        {{Form::hidden('id',$attribute->id)}}
                        {{Form::hidden('competence_id',$attribute->competence->id)}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="pcode" class="form-control border-input"
                                           value="{{$attribute->competence->code}}"
                                           disabled
                                           placeholder="Clave o código">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="ptitle" class="form-control border-input"
                                           value="{{$attribute->competence->title}}"
                                           disabled
                                           placeholder="Texto de la competencia">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="code" class="form-control border-input"
                                           value="{{$attribute->code}}"
                                           placeholder="Clave o código">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input"
                                           value="{{$attribute->title}}"
                                           placeholder="Texto del atributo">
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('maintenance.competences.edit',['id'=>$attribute->competence->id]) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
