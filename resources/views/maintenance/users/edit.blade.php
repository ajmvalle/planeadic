@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Usuarios</h4>
                        <p class="category">Editar datos</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición
                                .<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                            {!! Form::open(['url' => route('maintenance.users.update'), 'method' => 'post']) !!}
                            {!! Form::hidden('id',$user->id) !!}
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <label>Título</label>
                                        <input type="text" class="form-control border-input"
                                               name="title" value="{{$user->title}}"
                                               placeholder="Lic., Ing., etc...">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control border-input"
                                               name="name" value="{{$user->name}}"
                                               placeholder="Lucía, Juan Carlos, etc.">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Apellidos</label>
                                        <input type="text" class="form-control border-input"
                                               name="lastname" value="{{$user->lastname}}"
                                               placeholder="Pérez, Hernández, etc.">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Carrera o Profesión</label>
                                        <input type="text" class="form-control border-input"
                                               name="profession"
                                               placeholder="Licenciado en Ciencias de la Educación"
                                               value="{{$user->profession}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" class="form-control border-input"
                                               name="address"
                                               placeholder="Av. de las Américas, Fracc. Vista Hermosa"
                                               value="{{$user->address}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class="form-control border-input" placeholder="2299123456"
                                               name="phone"
                                               value="{{$user->phone}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <div class="radio">
                                            <input type="radio" name="sex" id="radio1" value="male" {{($user->sex!="female") ?"checked":""}}>
                                            <label for="radio1">
                                                Hombre
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <div class="radio">
                                            <input type="radio" name="sex" id="radio2" value="female" {{($user->sex=="female") ?"checked":""}} >
                                            <label for="radio2">
                                                Mujer
                                            </label>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::Label('item', 'Escuela:') !!}
                                        {!! Form::select('school_id', $schools, $user->school_id, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Acerca de mí</label>
                                        <textarea rows="3" class="form-control border-input" name="about"
                                                  placeholder="Puedes describir algo acerca de tí">{{$user->about}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class=" checkbox">
                                <input id="checkbox2" name="bprincipal"
                                       type="checkbox" {{$user->principal? "checked":""}} >
                                <label for="checkbox2">
                                    <strong>Director de plantel</strong>
                                </label>
                            </div>

                            <div class=" checkbox">
                                <input id="checkbox3" name="bactivated"
                                       type="checkbox" {{$user->activated? "checked":""}} >
                                <label for="checkbox3">
                                    <strong>Cuenta Activada</strong>
                                </label>
                            </div>


                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Actualizar datos</button>
                            </div>
                            <div class="clearfix"></div>
                            {!! Form::close() !!}

                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
