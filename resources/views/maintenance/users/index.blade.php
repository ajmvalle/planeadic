@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Usuarios</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Mail</th>
                            <th>Teléfono</th>
                            <th>Activada</th>
                            <th>Planeaciones</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->lastname}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->activated}}</td>
                                    <td>{{$user->TotalEcas}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.users.edit',['id'=>$user->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.users.delete','id'=>$user->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$users->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
