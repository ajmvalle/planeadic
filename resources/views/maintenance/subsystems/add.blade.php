@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Subsistemas</h4>
                        <p class="category">Dar de alta subsistema</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.subsystems.store']) !!}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Siglas</label>
                                        <input type="text" name="shortname" class="form-control border-input"  placeholder="Siglas de la Institución" >
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="name" class="form-control border-input" placeholder="Nombre complelto del subsistema">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Siglas</label>
                                        <input type="text" name="shortoffice" class="form-control border-input"  placeholder="SEP"
                                        value="SEP">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Nombre Secretaría</label>
                                        <input type="text" name="office" class="form-control border-input" placeholder="Secretaría de Educación Pública"
                                        value="Secretaría de Educación Pública">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Siglas</label>
                                        <input type="text" name="shortdepartment" class="form-control border-input"  placeholder="SEMS"
                                               value="SEMS">
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Nombre Subsecretaría</label>
                                        <input type="text" name="department" class="form-control border-input" placeholder="Subsecretaría de Educación Media Superior"
                                               value="Subsecretaría de Educación Media Superior">
                                    </div>
                                </div>
                            </div>


                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                                <button type="button" onclick="location.href='{{ route('maintenance.subsystems.index') }}'" class="btn btn-success btn-fill btn-wd">Cancelar</button>
                            </div>
                            <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
