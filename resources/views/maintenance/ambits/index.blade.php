@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Ámbitos del Perfil de Egreso</h4>
                            <p class="category">Tabla de mantenimiento</p>
                        </div>

                        <div class="col-md-8 ">
                            <Button onclick="location.href='{{ route('maintenance.ambits.add') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </Button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Título</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($ambits as $ambit)
                                <tr>
                                    <td>{{$ambit->id}}</td>
                                    <td>{{$ambit->title}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('maintenance.ambits.edit',['id'=>$ambit->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'maintenance.ambits.delete','id'=>$ambit->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$ambits->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
