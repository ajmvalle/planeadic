@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Programas de asignaturas</h4>
                        <p class="category">Dar de alta curso o asignatura</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.programs.update']) !!}

                        {!! Form::hidden('id',$program->id) !!}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control border-input"
                                           value="{{$program->name}}"
                                           placeholder="Nombre para identificar al programa">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <div class="checkbox">
                                    <input id="checkbox1" name="isnme" type="checkbox" {{$program->nme? "checked": ""}}>
                                    <label for="checkbox1">
                                        Nuevo Modelo Educativo
                                    </label>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="key" class="form-control border-input"
                                           value="{{$program->key}}"
                                           placeholder="Clave o código Oficial">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Asignatura:') !!}
                                    {!! Form::select('course_id', $courses, $program->course_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Plan</label>
                                    <input type="text" name="plan" class="form-control border-input"
                                           value="{{$program->plan}}"
                                           placeholder="Plan o ley en el que deriva en la asignatura o submódulo">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Grado o Semestre</label>
                                    <input type="text" name="grade" class="form-control border-input"
                                           value="{{$program->grade}}"
                                           placeholder="Semestre en el que se cursa la asignatura">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Horas</label>
                                    <input type="text" name="hours" class="form-control border-input"
                                           value="{{$program->hours}}"
                                           placeholder="Horas destinadas para el ciclo">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Propósito de la asignatura</label>
                                    <textarea rows="3" class="form-control border-input" name="purpose_area"
                                              placeholder="Propósito del Campo o Carrera">{{$program->purpose_area}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Propósito de la asignatura</label>
                                    <textarea rows="3" class="form-control border-input" name="purpose"
                                              placeholder="Propósito formativo de la asignatura">{{$program->purpose}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.programs.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>

                <div class="card">

                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="card-title">Bloques</h4>
                            </div>

                            <div class="col-md-8 ">
                                <button onclick="location.href='{{ route('maintenance.blocks.add',['program'=>$program]) }}'"
                                        type="button"
                                        title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Descripción</th>
                                <th>Contenido</th>
                                </thead>
                                <tbody>
                                @foreach($program->blocks as $block)
                                    <tr>
                                        <td>{{$block->description}}</td>
                                        <td>{{$block->centralcontent?$block->centralcontent->title :""}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a onclick="location.href='{{ route('maintenance.blocks.edit',['id'=>$block->id]) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                                <div class="col-md-4">
                                                    @include('buttons.delete',['route'=>'maintenance.blocks.delete','id'=>$block->id])
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
