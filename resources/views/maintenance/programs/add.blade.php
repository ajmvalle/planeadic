@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Programas</h4>
                        <p class="category">Dar de alta programas de asignaturas</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'maintenance.programs.store']) !!}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control border-input"
                                           placeholder="Nombre Distitivo del Programa de estudios">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <div class="checkbox">
                                    <input id="checkbox1" name="isnme" type="checkbox" checked >
                                    <label for="checkbox1">
                                        Nuevo Modelo Educativo
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="text" name="key" class="form-control border-input"
                                           placeholder="Clave o código Oficial">
                                </div>
                            </div>
                            <div class="col-md-8">
                                {!! Form::Label('item', 'Asignatura:') !!}
                                {!! Form::select('course_id', $courses, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Plan</label>
                                    <input type="text" name="plan" class="form-control border-input"
                                           placeholder="Plan o ley en el que deriva en la asignatura o submódulo">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Grado o Semestre</label>
                                    <input type="text" name="grade" class="form-control border-input"
                                           placeholder="Semestre en el que se cursa la asignatura">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Horas</label>
                                    <input type="text" name="hours" class="form-control border-input"
                                           placeholder="Horas destinadas para el ciclo">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Propósito del campo disciplinar o de Carrera </label>
                                    <textarea rows="3" class="form-control border-input" name="purpose_area"
                                              placeholder="Propósito formativo de la asignatura"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Propósito de la asignatura</label>
                                    <textarea rows="3" class="form-control border-input" name="purpose"
                                              placeholder="Propósito formativo de la asignatura"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button" onclick="location.href='{{ route('maintenance.programs.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
