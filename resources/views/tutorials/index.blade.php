@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('tutorials.index')}}">Videos de Introducción</a>

@endsection

@section('content')

    <div class="card-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-12">
                                <h4 class="card-title">Tutoriales</h4>
                                <p class="category">Revise estos videos para una mejor experiencia del sistema</p>
                            </div>


                        </div>

                        <div class="card-content text-center">
                            La siguiente es una lista de reproducción que contiene los videos de inicio del sistema:
                            <iframe width="672" height="378"
                                    src="https://www.youtube.com/embed/videoseries?list=PLbfTKxa2jfR3x4FfXopgw15sJvhlxSF02"
                                    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                            <br>
                            No dejes de visitar y suscribirte al <a href="https://www.youtube.com/channel/UCC8oKV1m_r9dxv9BKA22Iqg"> canal de Youtube!</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
