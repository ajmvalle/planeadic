<!DOCTYPE html>
<html lang="en">

<head>

<meta name="google-site-verification" content="fBxZMw_-xLikv4Qpn_aWXQIxgIcdK3rCAcL3yEdsNp0" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PLANEADIC</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('vendor/font-awesome/fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="{{asset('fonts/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/vendor/creative.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">PLANEADIC</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#concept">Concepto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Ventajas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contacto</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<header class="masthead text-center text-white d-flex" style="background-image: url('{{ asset('img/header.jpg') }}')">
    @if(session()->has('success'))
        <script>
            window.alert('¡Gracias por contactarnos!. Hemos enviado tu mensaje al administrador');
        </script>
    @endif
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <h1 class="text-uppercase" style="color: #f05f40;">
                    <strong>Planeaciones didácticas</strong>
                </h1>
                <hr>
            </div>
            <div class="col-lg-8 mx-auto">
                <p class="text-faded mb-5"  style="color: #000000;">Bienvenido al lugar donde tus planeaciones didácticas potencian su
                    efectividad. ¡Gracias por visitarnos!</p>
                <a class="btn btn-primary btn-xl js-scroll-trigger" href="login">Ingresa</a>
            </div>
        </div>
    </div>
</header>

<section class="bg-primary" id="concept">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading text-white">Encuentra aquí soluciones de organización y seguimiento</h2>
                <hr class="light my-4">
                <p class="text-faded mb-4">Planeadic es un sistema diseñado por docentes que encuentran la necesidad de realizar
                    la práctica del aula con mayor efectividad con los estudiantes, empleando de manera efectiva las
                    TICS para potenciar su función en esta apasionante carrera</p>
                <a class="btn btn-light btn-xl js-scroll-trigger" href="login">¡Inicia Ahora!</a>
            </div>
        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Ventajas</h2>
                <hr class="my-4">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Comparte Experiencias</h3>
                    <p class="text-muted mb-0">Puedes compartir este sitio y recibir beneficios</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Actualiza tus planeaciones</h3>
                    <p class="text-muted mb-0">Enriquece tus planeaciones didácticas mediante la posibilidad de
                        actualizarlas fácilmente y en línea</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-calendar text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Administra tu tiempo</h3>
                    <p class="text-muted mb-0">Tendrás la posibilidad de organizar de mejor manera las horas del ciclo
                        escolar</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Servicios activos</h3>
                    <p class="text-muted mb-0">Planeadic se actualiza constantemente, ¡Adquiere tu membresía!</p>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading">Contáctanos</h2>
                <hr class="my-4">
                <p class="mb-5">Planeadic requiere de las opiniones y sugerencias de los docentes que día a día realizan
                    una labor formidable con sus estudiantes</p>
            </div>
        </div>


        {{Form::open(['url' => route('contact.store'), 'method' => 'post','class' => 'form-control border-input'] )}}

        <div class="form-group">
            <div class="row">
                <div class="col-md-8">
                    <label>Nombre</label>
                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' has-error' : '' }}"
                           value=""
                           placeholder="Por favor, proporcione su nombre.">
                    @if ($errors->has('name'))
                        <span class="help-block">
                                                <strong>El nombre es requerido</strong>
                                </span>
                    @endif
                </div>

                <div class="col-md-4">
                    <label>e-Mail</label>
                    <input type="email" name="email"
                           class="form-control  {{ $errors->has('name') ? ' has-error' : '' }}"
                           value=""
                           placeholder="ejemplo@tumail.com">
                    @if ($errors->has('name'))
                        <span class="help-block">
                                                <strong>e-Mail es requerido</strong>
                                </span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label>Mensaje</label>
                    <textarea name="message" class="form-control {{ $errors->has('message') ? ' has-error' : '' }}"
                              placeholder="Detalle a continuación el mensaje para PLANEADIC"></textarea>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                                <strong>El mensaje es requerido</strong>
                                </span>
                    @endif

                </div>
            </div>

        </div>

        <div class="form-group">

            <div class="row">
                <div class="col-md-6 col-md-offset-4 text-center">
                    {!! app('captcha')->display() !!}
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                    @endif
                </div>

                <div class="col-md-6 col-md-offset-4">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </div>

            </div>

        </div>

        {{Form::close()}}


    </div>



</section>

<div class="container">
    <a href="{{asset('terminos-condiciones')}}">Términos y Condiciones</a>
<span>     </span>
    <a class="pull-right" href="{{asset('aviso-privacidad')}}">Aviso de Privacidad</a>
</div>


{!! NoCaptcha::renderJs() !!}
<!-- Bootstrap core JavaScript -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('vendor/scrollreveal/scrollreveal.min.js')}}"></script>
<script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/vendor/creative.min.js')}}"></script>

</body>

</html>
