@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Director: {{auth()->user()->school->shortname}}</a>

@endsection

@section('content')

    {!! Form::open(['method'=> 'POST','route'=>'account.preview']) !!}
    <div class="container-fluid">
        <div class="row">


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            @if($error == 'Could not connect to https://api.conekta.io.')
                                <li>Compruebe su conexión a Internet</li>
                            @endif
                            @if($error == 'Formato inválido para name"')
                                <li>Favor de verificar nombre y apellidos del perfil (Sin números)</li>
                            @endif
                            @if($error == 'phone" tiene un tipo inválido.')
                                <li>Favor de capturar su teléfono en el menú Mi Perfil(a 10 dígitos)</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b> Membresías en Planeadic</b></h4>
                        <small>Detalle de tus movimientos</small>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Pago</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            </thead>
                            <tbody>
                            @foreach(auth()->user()->memberships as $membership)
                                <tr>
                                    <td><strong>{{$membership->payment->membershipcost->name}}</strong></td>
                                    <td>{{$membership->created_at->format('d/m/y')}}</td>
                                    <td>${{$membership->payment->amount}}.00</td>
                                    <td>{{$membership->from->format('d/m/y')}}</td>
                                    <td>{{$membership->to->format('d/m/y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>


            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b> Adquiere una membresía</b></h4>
                        <small>Nuestros paquetes (IVA incluido)</small>
                        <br>
                        <small><a href="{{route('terms')}}">Términos y condiciones del servicio</a></small>
                    </div>
                </div>


                @foreach($membershipcosts as $mbc)


                    @if($mbc->months==6)
                        <div class="card text-center">
                            <div class="card-header">
                                <h4 class="card-title"><b> {{$mbc->name}}</b></h4>
                            </div>


                            <div class="card-content">
                                <p>Meses: 6 Precio: ${{$mbc->cost}} MXN <strong>X Docente</strong></p>
                                <p><strong>A partir de 10 docentes 25% de descuento</strong></p>
                                <p><strong>A partir de 30 docentes 50% de descuento</strong></p>
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Calcular</button>
                                <button type="button" class="btn btn-info btn-fill btn-wd" onclick="location.href='{{ route('supervision.index') }}'">Supervisar</button>
                            </div>
                        </div>



                    @endif

                @endforeach
            </div>

        </div>


    </div>

    <div class="container-fluid">
        <div class="row">

            <div class="alert alert-info">

                <span><b> Info - </b> Es importante que seleccione aquellos maestros a los que desee crearles una membresía</span>
            </div>


            <div class="col-md-12">
                <div class="card">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Nombre</th>
                            <th>Fin membresía</th>
                            <th>Planeaciones</th>
                            <th class="text-right">Incluir</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($docentes as $indexKey => $docente)

                            <tr>
                                <td>{{$indexKey+1}}</td>
                                <td>
                                    <div class="img-container">
                                        <img src="{{$docente->imagen}}" alt="Stylus" height="36" width="36"/>
                                    </div>
                                </td>
                                <td>{{$docente->fullname}}</td>
                                <td  class="{{$docente->hasMembership ? "" : "danger"}}">
                                    {{date('d-m-Y', strtotime($docente->lastmembership->to))}}
                                </td>
                                <td>{{$docente->TotalEcas}}</td>
                                <td class="text-right">
                                    <input type="checkbox" class="switch-plain" name="detail[]"
                                           value="{{ $docente->id }}" {{$docente->hasMembership?"":"checked"}} >
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

    {!! Form::close() !!}
@endsection
