@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mi cuenta</a>

@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">

                        <p>
                            Al crear su cuenta en PLANEADIC, se envió un correo electrónico con el botón de activación,
                            si
                            no encuentra ese correo, puede enviarlo nuevamente.
                        </p>

                        {!! Form::open(['route' => ['account.sendtoken'], 'method' => 'POST']) !!}
                        <button type="submit" class="btn btn-success">Reenviar Código de Activación</button>
                        {!! Form::close() !!}

                        @if (session('status'))
                            <br>
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
