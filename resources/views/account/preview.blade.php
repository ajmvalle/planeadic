@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Director: {{auth()->user()->school->shortname}}</a>

@endsection

@section('content')

    {!! Form::open(['method'=> 'POST','route'=>'membership.getPrincipal']) !!}

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-content table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th></th>
                                <th>Nombre</th>
                                <th>Fin membresía</th>
                                <th>Planeaciones</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($users as $indexKey => $docente)

                                <tr>
                                    <td>{{$indexKey+1}}</td>
                                    <td>
                                        <div class="img-container">
                                            <img src="{{$docente->imagen}}" alt="Stylus" height="36" width="36"/>
                                        </div>
                                    </td>
                                    <td>{{$docente->fullname}}</td>
                                    <td>{{date('d-m-Y', strtotime($docente->lastmembership->to))}}</td>
                                    <td>{{$docente->TotalEcas}}</td>
                                    {{Form::hidden('detail[]',$docente->id)}}
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b> Adquiere estas membresías</b></h4>
                        <small>IVA incluido</small>
                        <br>
                        <small><a href="{{route('terms')}}">Términos y condiciones del servicio</a></small>
                    </div>
                    <div class="card text-center">
                        <div class="card-header">
                            <h4 class="card-title"><b> Grupal ({{count($users)}})</b></h4>
                        </div>

                        <div class="card-content">

                            <p>Meses: 6 Precio: ${{$total}} MXN</p>
                            <button type="submit" name="optionpay" value="oxxo_cash"
                                    title="Agregar"
                                    class="btn btn-sm btn-success btn-icon">OXXO
                            </button>
                            <button type="submit" name="optionpay" value="spei"
                                    title="Agregar"
                                    class="btn btn-sm btn-success btn-icon">SPEI
                            </button>

                        </div>
                    </div>
                </div>
                <div class="card">
                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
@endsection
