@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mi cuenta</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Lo sentimos!</strong> Existen inconvenientes en su solicitud.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            @if($error == 'Could not connect to https://api.conekta.io.')
                                <li>Compruebe su conexión a Internet</li>
                            @endif
                            @if($error == 'Formato inválido para name"')
                                <li>Favor de verificar nombre y apellidos del perfil (Sin números)</li>
                            @endif
                            @if($error == 'phone" tiene un tipo inválido.')
                                <li>Favor de capturar su teléfono en el menú Mi Perfil(a 10 dígitos)</li>
                            @endif
                            @if($error == 'Servicio No Disponible')
                                <li>Servicio no disponible</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b> Membresías en Planeadic</b></h4>
                        <small>Detalle de tus movimientos</small>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>Tipo</th>
                            <th>Fecha</th>
                            <th>Pago</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            </thead>
                            <tbody>
                            @foreach(auth()->user()->memberships as $membership)
                                <tr>
                                    <td><strong>{{$membership->payment->membershipcost->name}}</strong></td>
                                    <td>{{$membership->created_at->format('d/m/y')}}</td>
                                    <td>${{$membership->payment->amount}}.00</td>
                                    <td>{{$membership->from->format('d/m/y')}}</td>
                                    <td>{{$membership->to->format('d/m/y')}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>


            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><b> Adquiere una membresía</b></h4>
                        <small>Nuestros paquetes (IVA incluido)</small>
                        <br>
                        <small><a href="{{route('terms')}}">Términos y condiciones del servicio</a></small>
                    </div>
                </div>


                @foreach($membershipcosts as $mbc)
                    <div class="card text-center">
                        <div class="card-header">
                            <h4 class="card-title"><b> {{$mbc->name}}</b></h4>
                        </div>


                        <div class="card-content">

                            @if($mbc->cost==0)
                                <p>Días: 7 Precio: ${{$mbc->cost}} MXN</p>
                                <button type="button" name="optionpay" value="free"
                                        title="Agregar" onclick="location.href='{{route('membership.getFree', $mbc )}}'"
                                        class="btn btn-sm btn-success btn-icon">Adquirir
                                </button>
                            @else
                                <p>Meses: {{$mbc->months}} Precio: ${{$mbc->cost}} MXN</p>
                                <button type="button" name="optionpay" value="oxxo"
                                        title="Agregar"
                                        onclick="location.href='{{route('membership.get',['membershipcost'=>$mbc,'kind'=>'oxxo_cash'])}}'"
                                        class="btn btn-sm btn-success btn-icon">OXXO
                                </button>
                                <button type="button" name="optionpay" value="spei"
                                        title="Agregar"
                                        onclick="location.href='{{route('membership.get',['membershipcost'=>$mbc,'kind'=>'spei'])}}'"
                                        class="btn btn-sm btn-success btn-icon">SPEI
                                </button>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>



@endsection
