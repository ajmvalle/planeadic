<form method="POST" id="copyassigment{{$id}}"
      action="{{route($route)}}">
    {!! Form::token() !!} {{ csrf_field() }}
    {!! Form::hidden('id',$id) !!}
    {!! Form::hidden('copygroup'.$id,null,['id'=>'copygroup'.$id]) !!}
    <button onclick="tools.showSwal('copy-assigment',{{ $id }},{{auth()->user()->myAuthorizeGroups()}})"
            title="Copiar" type="button"
            class="btn btn-sm btn-warning btn-icon"><i
                class="fa fa-copy"></i>
    </button>
</form>