<form method="POST" id="formdelete{{$id}}"
      action="{{route($route,['id'=>$id])}}">
    {!! Form::token() !!} {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button onclick="tools.showSwal('delete-record',{{$id}})"
            title="Eliminar" type="button"
            class="btn btn-sm btn-danger btn-icon"><i
                class="fa fa-eraser"></i>
    </button>
</form>