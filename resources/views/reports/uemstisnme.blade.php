<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: "Segoe UI";
            panose-1: 2 11 5 2 4 2 4 2 2 3;
        }

        @font-face {
            font-family: Verdana;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        @font-face {
            font-family: "Soberana Sans";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Adobe Caslon Pro Bold";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Adobe Caslon Pro";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        h1 {
            mso-style-link: "Título 1 Car";
            margin-top: 12.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            page-break-after: avoid;
            font-size: 16.0pt;
            font-family: "Calibri Light", sans-serif;
            color: #365F91;
            font-weight: normal;
        }

        h2 {
            mso-style-link: "Título 2 Car";
            margin-top: 2.0pt;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 0cm;
            margin-bottom: .0001pt;
            line-height: 107%;
            page-break-after: avoid;
            font-size: 13.0pt;
            font-family: "Calibri Light", sans-serif;
            color: #365F91;
            font-weight: normal;
        }

        p.MsoFootnoteText, li.MsoFootnoteText, div.MsoFootnoteText {
            mso-style-link: "Texto nota pie Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-link: "Encabezado Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-link: "Pie de página Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.MsoFootnoteReference {
            vertical-align: super;
        }

        a:link, span.MsoHyperlink {
            color: blue;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: purple;
            text-decoration: underline;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Texto de globo Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 9.0pt;
            font-family: "Segoe UI", sans-serif;
        }

        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing {
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.Ttulo1Car {
            mso-style-name: "Título 1 Car";
            mso-style-link: "Título 1";
            font-family: "Calibri Light", sans-serif;
            color: #365F91;
        }

        span.Ttulo2Car {
            mso-style-name: "Título 2 Car";
            mso-style-link: "Título 2";
            font-family: "Calibri Light", sans-serif;
            color: #365F91;
        }

        span.Mencinsinresolver1 {
            mso-style-name: "Mención sin resolver1";
            color: gray;
            background: #E6E6E6;
        }

        span.TextodegloboCar {
            mso-style-name: "Texto de globo Car";
            mso-style-link: "Texto de globo";
            font-family: "Segoe UI", sans-serif;
        }

        p.Default, li.Default, div.Default {
            mso-style-name: Default;
            margin: 0cm;
            margin-bottom: .0001pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Calibri", sans-serif;
            color: black;
        }

        span.TextonotapieCar {
            mso-style-name: "Texto nota pie Car";
            mso-style-link: "Texto nota pie";
        }

        span.EncabezadoCar {
            mso-style-name: "Encabezado Car";
            mso-style-link: Encabezado;
        }

        span.PiedepginaCar {
            mso-style-name: "Pie de página Car";
            mso-style-link: "Pie de página";
        }

        .MsoPapDefault {
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 792.0pt 612.0pt;
            margin: 3.0cm 70.85pt 70.9pt 70.85pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body lang=ES-MX link=blue vlink=purple>

<div class=WordSection1>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=0
           style='width:678.75pt;border-collapse:collapse;border:none'>
        <tr style='height:77.7pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:77.7pt'>
                <p class=MsoNormal align=right style='margin-top:0.1cm;margin-right:-14.15pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:right;
  line-height:normal'>
                    @if(auth()->user()->authlogo)
                        <img width=288 height=88
                             src="{{asset('img/uemstis001.jpg')}}" align=left hspace=12>
                    @endif
                </p>
                <p><br></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Adobe Caslon Pro Bold",serif'>{{$plan->assigment->school->subsystem->department}}
                            Superior</span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Adobe Caslon Pro Bold",serif'>{{$plan->assigment->school->subsystem->name}}</span></b></p>
                <p class=MsoHeader align=right style='text-align:right'><b><span
                                style='font-size:8.0pt;font-family:"Adobe Caslon Pro",serif'>{{$plan->assigment->school->subsystem->shortname}}
                            de {{$plan->assigment->school->town->state->name}}</span></b></p>
                <p class=MsoHeader align=right style='text-align:right'><b><span
                                style='font-size:8.0pt;font-family:"Adobe Caslon Pro",serif'>{{$plan->assigment->school->fullname}}</span></b>
                </p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:12.0pt'>&nbsp;</span></p>
            </td>
        </tr>
        <tr style='height:14.15pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid windowtext 1.0pt;
  border-top:none;background:black;padding:0cm 5.4pt 0cm 5.4pt;height:14.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:12.0pt'>INSTRUMENTO
  DE REGISTRO DE ESTRATEGIAS DIDÁCTICAS</span></p>
            </td>
        </tr>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:12.0pt'>IDENTIFICACIÓN</span></p>
            </td>
        </tr>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span style='font-size:10.0pt'>Institución:</span><span
                            style='font-size:11.0pt;color:black'> <strong>{{$plan->assigment->school->subsystem->name}}</strong></span><span
                            style='font-size:14.0pt;
  font-family:"Verdana",sans-serif;color:black'> </span></p>
            </td>
        </tr>
        <tr>
            <td width=206 valign=top style='width:154.25pt;border:solid windowtext 1.0pt;
  border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span style='font-size:10.0pt'>Plantel: {{$plan->assigment->school->name.' '.$plan->assigment->school->number}} ({{$plan->assigment->school->officialkey}})</span></p>
            </td>
            <td width=699 colspan=5 valign=top style='width:524.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span style='font-size:10.0pt'>Profesor: {{$user->fullname}}</span></b></p>
            </td>
        </tr>
        <tr style='height:5.1pt'>
            <td width=206 rowspan=2 valign=top style='width:154.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span
                            style='font-size:10.0pt'>Asignatura:  <strong>{{$plan->assigment->program->course->name}}</strong> </span>
                </p>
            </td>
            <td width=113 rowspan=2 valign=top style='width:3.0cm;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span
                                style='font-size:10.0pt'>Semestre: {{$plan->assigment->program->grade}}</span></b></p>
            </td>
            <td width=189 rowspan=2 valign=top style='width:5.0cm;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span style='font-size:10.0pt'>Carrera:  </span></b>{{$plan->assigment->careers}}</p>
            </td>
            <td width=142 valign=top style='width:106.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span style='font-size:10.0pt'>Periodo de
  aplicación:</span></b></p>
            </td>
            <td width=113 valign=top style='width:3.0cm;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span style='font-size:10.0pt'>&nbsp;{{$plan->assigment->cycle}}</span></p>
            </td>
            <td width=142 rowspan=2 valign=top style='width:106.35pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span
                                style='font-size:10.0pt'>Fecha: {{$plan->elaboration->format('d/m/Y')}}</span></b></p>
            </td>
        </tr>
        <tr style='height:16.7pt'>
            <td width=142 valign=top style='width:106.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span style='font-size:10.0pt'>Duración en
  horas: </span></b></p>
            </td>
            <td width=113 valign=top style='width:3.0cm;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><span style='font-size:10.0pt'>&nbsp;{{$plan->hours}}</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormalCxSpMiddle style='margin-left:60.55pt;text-align:justify;
text-indent:-18.0pt;line-height:150%'><span style='font-size:1.0pt;line-height:
150%;font-family:"Soberana Sans"'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:1.0pt;line-height:150%'>&nbsp;</span></p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0 width=0
           style='width:678.75pt;border-collapse:collapse;border:none'>
        <tr style='height:8.35pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt;height:8.35pt'>
                <p class=MsoNormalCxSpMiddle align=center style='margin-bottom:0cm;
  margin-bottom:.0001pt;text-align:center;line-height:normal'><b><span
                                style='font-size:12.0pt;color:white'>INTENCIONES FORMATIVAS</span></b></p>
            </td>
        </tr>
        <tr style='height:16.5pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><b><span style='font-size:10.0pt;
  line-height:150%'>Propósito de la estrategia didáctica por asignatura:  </span></b></p>
            </td>
        </tr>

        <tr style='height:16.5pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;{{$plan->purpose}}</span></b></p>
            </td>
        </tr>


        @if($plan->integrator)

            <tr style='height:16.5pt'>
                <td width=236 colspan=3 valign=top style='width:176.95pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt'>Tema
  integrador:</span><b> </b></p>
                </td>
                <td width=367 colspan=2 valign=top style='width:275.55pt;border-top:none;
  border-left:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt'>Otras asignaturas, módulos o Sub-módulos que trabajan el tema
  integrador: </span></b></p>
                </td>
                <td width=302 valign=top style='width:226.25pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
  line-height:150%'>{{$plan->trans_integrator}}</span></p>
                </td>
            </tr>
            <tr style='height:16.5pt'>
                <td width=236 colspan=3 valign=top style='width:176.95pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
  line-height:150%'><strong> {{$plan->integrator}}</strong></span></p>
                </td>
                <td width=367 colspan=2 valign=top style='width:275.55pt;border-top:none;
  border-left:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt'>Asignaturas, módulos y/o sub-módulos con los que se relaciona:</span></b></p>
                </td>
                <td width=302 valign=top style='width:226.25pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><span style='font-size:10.0pt;
  line-height:150%'>{{$plan->trans_course}}</span></p>
                </td>
            </tr>
            <tr style='height:16.5pt'>
                <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%;text-autospace:none'><b><span style='font-size:10.0pt;
  line-height:150%'>Categorías</span></b></p>
                </td>
            </tr>
            <tr style='height:13.65pt'>
                <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:13.65pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>         
  Espacio  (   {{$plan->space? "X":" "}}
                                   )                                        Energía (  {{$plan->energy? "X":" "}}   )                                  Diversidad
   (   {{$plan->diversity? "X":" "}}  )                          Tiempo (  {{$plan->thetime? "X":" "}}   )                         Materia 
  (   {{$plan->matter? "X":" "}}   )</span></b></p>
                </td>
            </tr>

        @endif

        <tr style='height:13.65pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:13.65pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Ejes
  disciplinarios: </span></b> {{$plan->block->axis}}</p>
            </td>
        </tr>

        <tr style='height:18.45pt'>
            <td width=158 valign=top style='width:118.8pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Componente:
  </span></b>{{$plan->block->component}}</p>
            </td>
            <td width=191 colspan=3 valign=top style='width:143.2pt;border-top:none;
  border-left:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:10.0pt;color:black'>Contenido
  central</span></b><span style='font-size:10.0pt;color:black'>: </span>{{$plan->block->title}}</p>
            </td>
            <td width=556 colspan=2 valign=top style='width:416.75pt;border-top:none;
  border-left:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:10.0pt'>Aprendizaje
  esperado:</span></b><br>{{$plan->expecteds}}</p>
            </td>
        </tr>
        <tr style='height:20.2pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:20.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:10.0pt;color:black'>Producto
  esperado:</span></b><br>{{$plan->products}}</p>
            </td>
        </tr>
        <tr style='height:6.95pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:6.95pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Contenidos
  específicos:</span></b></p>
            </td>
        </tr>
        <tr style='height:13.8pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:13.8pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span
                                style='font-size:10.0pt;color:black'></span></b>{{$plan->specifics}}
                </p>
            </td>
        </tr>
        <tr style='height:16.75pt'>
            <td width=206 colspan=2 valign=top style='width:154.25pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Habilidad
  Socioemocional (HSE): </span></b></p>
            </td>
            <td width=699 colspan=4 valign=top style='width:524.5pt;border-top:none;
  border-left:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt'>&nbsp;{{$plan->social}}</span></p>
            </td>
        </tr>
        <tr style='height:12.2pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:12.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Competencias
  genéricas y atributos:</span></b></p>
            </td>
        </tr>
        <tr style='height:14.6pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:14.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:12.0pt;color:black'></span></b>
                    @if($plan->generics)
                        @foreach($plan->generics as $gen)
                            <strong>{{$gen->code}} </strong>{{$gen->title}} <br>
                            @foreach($plan->attributes as $attribute)
                                @if($gen->id==$attribute->competence_id)
                                    {{$attribute->code.' '.$attribute->title}}<br>
                                @endif
                            @endforeach
                        @endforeach
                    @endif
                </p>
            </td>
        </tr>
        <tr style='height:14.05pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:14.05pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Competencias
  disciplinares:</span></b></p>
            </td>
        </tr>
        <tr style='height:9.45pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:9.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'><b><span style='font-size:10.0pt;color:black'></span></b>
                    @foreach($plan->discompetences as $competence)
                        <strong>{{$competence->code}} </strong>{{$competence->title}}
                        <br>
                    @endforeach
                </p>
            </td>
        </tr>
        <tr height=0>
            <td width=158 style='border:none'></td>
            <td width=47 style='border:none'></td>
            <td width=30 style='border:none'></td>
            <td width=113 style='border:none'></td>
            <td width=254 style='border:none'></td>
            <td width=302 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.55pt;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span
                style='font-size:10.0pt'>&nbsp;</span></p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0 width=0
           style='width:678.75pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
  color:white'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Apertura</span></b></p>
            </td>
        </tr>
        <tr style='height:36.75pt'>
            <td width=251 valign=top style='width:188.05pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Actividades
  docente</span></b></p>
            </td>
            <td width=212 valign=top style='width:159.0pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Actividades
  estudiantes</span></b></p>
            </td>
            <td width=135 valign=top style='width:100.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Recursos
  utilizados</span></b></p>
            </td>
            <td width=111 valign=top style='width:83.35pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Producto(s)
  de aprendizaje</span></b></p>
            </td>
            <td width=132 valign=top style='width:99.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Evaluación
  Tipo / instrumento / ponderación)</span></b></p>
            </td>
            <td width=64 valign=top style='width:48.2pt;border-top:none;border-left:none;
  border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:36.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Duración</span></b></p>
            </td>
        </tr>
        @foreach($plan->startActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
        <tr style='height:13.25pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Reforzamiento</span></b></p>
            </td>
        </tr>
        @foreach($plan->extraStartActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
    </table>

    <p class=MsoNormalCxSpMiddle style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:justify;line-height:normal'><span style='font-size:8.0pt'>&nbsp;</span></p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0 width=0
           style='width:678.75pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
  color:white'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Desarrollo</span></b></p>
            </td>
        </tr>
        <tr style='height:16.85pt'>
            <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>Actividades docente</span></b></p>
            </td>
            <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Actividades
  estudiante</span></b></p>
            </td>
            <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Recursos
  utilizados</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b></p>
            </td>
            <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Producto(s)
  de</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Aprendizaje</span></b></p>
            </td>
            <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Evaluación
  Tipo / instrumento / ponderación</span></b></p>
            </td>
            <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Duración</span></b></p>
            </td>
        </tr>
        @foreach($plan->devActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
        <tr style='height:16.85pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Reforzamiento</span></b></p>
            </td>
        </tr>
        @foreach($plan->extraDevActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
    </table>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.55pt;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span
                style='font-size:8.0pt'>&nbsp;</span></p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0 width=0
           style='width:678.75pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
  color:white'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Cierre</span></b></p>
            </td>
        </tr>
        <tr style='height:16.85pt'>
            <td width=236 valign=top style='width:176.95pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>Actividades docente</span></b></p>
            </td>
            <td width=198 valign=top style='width:148.85pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Actividades
  estudiante</span></b></p>
            </td>
            <td width=123 valign=top style='width:92.15pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Recursos
  utilizados</span></b></p>
            </td>
            <td width=104 valign=top style='width:77.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Producto(s)
  de</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Aprendizaje</span></b></p>
            </td>
            <td width=169 valign=top style='width:127.0pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Evaluación
  Tipo / instrumento / ponderación)</span></b></p>
            </td>
            <td width=74 valign=top style='width:55.85pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Duración</span></b></p>
            </td>
        </tr>
        @foreach($plan->closeActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
        <tr style='height:23.45pt'>
            <td width=905 colspan=6 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:23.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Reforzamiento</span></b></p>
            </td>
        </tr>
        @foreach($plan->extraCloseActivities as $act)
            <tr style='height:16.85pt'>
                <td width=236 valign=top style='width:188.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%'>&nbsp;</span></b>{{$act->teacher}}</p>
                </td>
                <td width=202 valign=top style='width:160.95pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->student}}</p>
                </td>
                <td width=123 valign=top style='width:96.45pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->resources}}</p>
                </td>
                <td width=111 valign=top style='width:85.2pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->product}}</p>
                </td>
                <td width=171 valign=top style='width:134.65pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>&nbsp;</span></b>{{$act->kindeva.' / '.$act->tool.' / '.$act->eva_percent.'%'  }}
                    </p>
                </td>
                <td width=62 valign=top style='width:12.75pt;border-top:none;border-left:
  none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;
  background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>&nbsp;</span></b>{{$act->time}}</p>
                </td>
            </tr>
        @endforeach
    </table>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0
           align=left width=0 style='width:678.75pt;border-collapse:collapse;border:none;
 margin-left:4.8pt;margin-right:4.8pt'>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%;color:white'>Referencias </span></b></p>
            </td>
        </tr>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Fuentes
  de información:</span></b></p>
            </td>
        </tr>
        <tr style='height:23.15pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:23.15pt'>
                <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:10.0pt;margin-bottom:.0001pt;text-align:justify;
  line-height:normal'>{{$plan->references}}<b><span style='font-size:10.0pt'>&nbsp;</span></b></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.4pt;margin-bottom:.0001pt;text-align:justify;line-height:normal'><span
                style='font-size:8.0pt'>&nbsp;</span></p>

    <p class=MsoNormal>&nbsp;</p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0
           align=left width=0 style='width:678.75pt;border-collapse:collapse;border:none;
 margin-left:4.8pt;margin-right:4.8pt'>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%;color:white'>Evidencia Fotográfica</span></b></p>
            </td>
        </tr>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Relatoría
  fotográfica:</span></b></p>
            </td>
        </tr>
        <tr style='height:23.15pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:23.15pt'>
                @foreach($plan->evidences as $evi)

                    @if($evi->kind==1)

                        <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:16.0pt;margin-bottom:.0001pt;text-align:justify;
  line-height:normal'>


                            <strong> {{$evi->elaboration->format('d/m/y')}}.</strong>{{$evi->description}}
                            <br>
                            @if($evi->activity)
                                <strong> Estudiante:</strong>{{$evi->activity->student}}
                                <br>
                    @endif
                    <div align=center>
                        <img src="{{asset('uploads/evidence/'.$evi->image)}}" height="300" width="300">
                    </div>

                    <b><span style='font-size:10.0pt'>&nbsp;</span></b></p>

                    @endif

                @endforeach

            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <table class=Tabladecuadrcula41 border=1 cellspacing=0 cellpadding=0
           align=left width=0 style='width:678.75pt;border-collapse:collapse;border:none;
 margin-left:4.8pt;margin-right:4.8pt'>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid black 1.0pt;
  background:black;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:10.0pt;
  line-height:150%;color:white'>Evidencia de Evaluación</span></b></p>
            </td>
        </tr>
        <tr style='height:12.7pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;background:#CCCCCC;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><b><span style='font-size:10.0pt;line-height:150%'>Evidencia
  e Instrumentos de Evaluación:</span></b></p>
            </td>
        </tr>
        <tr style='height:23.15pt'>
            <td width=905 valign=top style='width:678.75pt;border:solid #666666 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:23.15pt'>
                @foreach($plan->evidences as $evi)

                    @if($evi->kind==2)

                        <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:16.0pt;margin-bottom:.0001pt;text-align:justify;
  line-height:normal'>


                            <strong> {{$evi->elaboration->format('d/m/y')}}.</strong>{{$evi->description}}
                            <br>
                            @if($evi->activity)
                                <strong> Estudiante:</strong>{{$evi->activity->student}}
                                <br>
                    @endif
                    <div align=center>
                        <img src="{{asset('uploads/evidence/'.$evi->image)}}" height="300" width="300">
                    </div>

                    <b><span style='font-size:10.0pt'>&nbsp;</span></b></p>

                    @endif

                @endforeach

            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>
