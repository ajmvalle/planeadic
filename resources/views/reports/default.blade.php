<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: "Soberana Sans";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        @font-face {
            font-family: Verdana;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-link: "Encabezado Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-link: "Pie de página Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        a:link, span.MsoHyperlink {
            color: #0563C1;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p {
            margin-right: 0cm;
            margin-left: 0cm;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Texto de globo Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 8.0pt;
            font-family: "Tahoma", sans-serif;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.PiedepginaCar {
            mso-style-name: "Pie de página Car";
            mso-style-link: "Pie de página";
            font-family: "Calibri", sans-serif;
        }

        span.EncabezadoCar {
            mso-style-name: "Encabezado Car";
            mso-style-link: Encabezado;
            font-family: "Calibri", sans-serif;
        }

        p.Default, li.Default, div.Default {
            mso-style-name: Default;
            margin: 0cm;
            margin-bottom: .0001pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Arial", sans-serif;
            color: black;
        }

        span.TextodegloboCar {
            mso-style-name: "Texto de globo Car";
            mso-style-link: "Texto de globo";
            font-family: "Tahoma", sans-serif;
        }

        .MsoChpDefault {
            font-family: "Soberana Sans";
        }

        .MsoPapDefault {
            margin-bottom: 10.0pt;
            line-height: 115%;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 792.0pt 612.0pt;
            margin: 70.9pt 70.85pt 21.3pt 70.85pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        @page WordSection2 {
            size: 792.0pt 612.0pt;
            margin: 3.0cm 70.85pt 3.0cm 70.85pt;
        }

        div.WordSection2 {
            page: WordSection2;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body lang=ES-MX link="#0563C1" vlink="#954F72">

<div class=WordSection1>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:664.55pt;border-collapse:collapse;border:none'>
        <tr style='height:14.15pt'>
            <td width=886 colspan=6 valign=top style='width:664.55pt;border:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:14.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:12.0pt;
  font-family:"Soberana Sans"'>INSTRUMENTO DE REGISTRO DE ESTRATEGIAS
  DIDÁCTICAS</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=886 colspan=6 valign=top style='width:664.55pt;border:solid black 1.0pt;
  border-top:none;background:#BBFDFD;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='position:absolute;z-index:-1895825408;margin-left:239px;
  margin-top:8px;width:387px;height:388px'></span><b><span lang=ES style='font-size:
  12.0pt;font-family:"Soberana Sans"'>IDENTIFICACIÓN</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=886 colspan=6 valign=top style='width:664.55pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES
                   style='font-size:10.0pt;font-family:"Soberana Sans"'>Institución:</span></b>{{$plan->assigment->school->office}}
                    <b><span
                                lang=ES style='font-family:"Soberana Sans";color:black'> </span></b><span
                            lang=ES style='font-family:"Verdana",sans-serif;color:black'> </span></p>
            </td>
        </tr>
        <tr>
            <td width=358 valign=top style='width:274.75pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES
                   style='font-size:10.0pt;font-family:"Soberana Sans"'>Plantel:  </span>{{$plan->assigment->school->acronym}}
                    </b><span
                            lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'> </span></p>
            </td>
            <td width=528 colspan=5 valign=top style='width:389.8pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Profesor:
  </span>{{$plan->author}}</b></p>
            </td>
        </tr>
        <tr style='height:5.1pt'>
            <td width=358 rowspan=2 valign=top style='width:274.75pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Asignatura:
  </span><br>{{$plan->assigment->program->course->name}}</b></p>
            </td>
            <td width=160 rowspan=2 valign=top style='width:121.65pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Semestre:
  </span><br>{{$plan->assigment->program->grade}}</b></p>
            </td>
            <td width=92 rowspan=2 valign=top style='width:69.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Carrera:
  </span><br>{{$plan->assigment->careers}}</b></p>
            </td>
            <td width=102 valign=top style='width:66.35pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Periodo
  de aplicación:</span></b></p>
            </td>
            <td width=85 valign=top style='width:65.8pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->assigment->cycle}}</span></p>
            </td>
            <td width=88 rowspan=2 valign=top style='width:66.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:5.1pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Fecha:</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES
                style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->elaboration->format('d/m/Y')}}</span></p>
            </td>
        </tr>
        <tr style='height:28.8pt'>
            <td width=102 valign=top style='width:66.35pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:28.8pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Duración
  en horas:</span></b></p>
            </td>
            <td width=85 valign=top style='width:65.8pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:28.8pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->hours}} </span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal><span lang=ES style='font-size:1.0pt;line-height:115%;
font-family:"Soberana Sans"'>&nbsp;</span></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:666.0pt;border-collapse:collapse;border:none'>
        <tr style='height:8.35pt'>
            <td width=888 colspan=7 valign=top style='width:666.0pt;border:solid black 1.0pt;
  background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:8.35pt'>
                <p class=MsoNormalCxSpMiddle style='margin-bottom:0cm;margin-bottom:.0001pt;
  line-height:normal'><b><span lang=ES style='font-size:12.0pt;font-family:
  "Soberana Sans"'>INTENCIONES FORMATIVAS</span></b></p>
            </td>
        </tr>
        <tr style='height:40.3pt'>
            <td width=888 colspan=7 valign=top style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:40.3pt'>
                <p class=MsoNormal style='text-align:justify;text-autospace:none'><b><span
                                lang=ES style='font-size:10.0pt;line-height:115%;font-family:"Soberana Sans"'>Propósito
  de la estrategia didáctica por asignatura: </span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Soberana Sans"'>{{$plan->purpose}}</span></b></p>
            </td>
        </tr>
        <tr style='height:25.9pt'>
            <td width=111 rowspan=2 style='width:83.4pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.9pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Tema Integrador y situación de aprendizaje: </span></b></p>
                <p class=MsoNormal><span lang=ES style='font-size:6.0pt;line-height:115%;
  font-family:"Soberana Sans"'>&nbsp;</span></p>
            </td>
            <td width=198 colspan=2 rowspan=2 style='width:148.8pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:25.9pt'>
                <p class=MsoNormal><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>{{$plan->integrator}}</span></p>
            </td>
            <td width=298 colspan=3 style='width:223.25pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:25.9pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Otras asignaturas, módulos o Submódulos que
  trabajan el tema integrador:</span></b></p>
            </td>
            <td width=281 style='width:210.55pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:25.9pt'>
                <p class=Default><span style='font-family:"Soberana Sans"'>&nbsp;</span></p>
                <p class=MsoNormal><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>{{$plan->trans_integrator}}</span></p>
            </td>
        </tr>
        <tr style='height:25.9pt'>
            <td width=298 colspan=3 style='width:223.25pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:25.9pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Asignaturas, módulos y/o submódulos con los que
  se relaciona:</span></b></p>
            </td>
            <td width=281 style='width:210.55pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:25.9pt'>
                <p class=MsoNormal><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>{{$plan->trans_course}}</span></p>
            </td>
        </tr>
        <tr style='height:13.65pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:13.65pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Contenidos fácticos:</span></b>
                    {{$plan->factual}}
                </p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=158 colspan=2 style='width:118.8pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Conceptos fundamentales:</span></b></p>
            </td>
            <td width=285 colspan=2 style='width:213.45pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=Default><span
                            style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->fundamental}}</span></p>

            </td>
            <td width=150 style='width:112.6pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Conceptos subsidiarios:</span></b></p>
            </td>
            <td width=295 colspan=2 style='width:221.15pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->subsidiary}}</span>
                </p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Contenidos procedimentales:</span></b></p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
  line-height:normal;text-autospace:none'><span style='font-size:9.0pt;
  font-family:"Soberana Sans"'>{{$plan->procedimental}}</span></p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Contenidos actitudinales:</span></b></p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->attitudinal}}</p>

            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Competencias genéricas y atributos:</span></b></p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                @if($plan->block)
                    @foreach($plan->block->attributes as $att)
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            <b>{{$att->competence->code}}</b>{{' '.$att->competence->title}}
                        </p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            {{'   '.$att->code.' '.$att->title}}
                        </p>
                        <br>
                    @endforeach
                @endif

            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:#D8E5BE;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                <p class=MsoNormal><b><span lang=ES style='font-size:10.0pt;line-height:115%;
  font-family:"Soberana Sans"'>Competencias disciplinares:</span></b></p>
            </td>
        </tr>
        <tr style='height:25.05pt'>
            <td width=888 colspan=7 style='width:666.0pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt;height:25.05pt'>
                @if($plan->block)
                    @foreach($plan->discompetences as $comp)
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            {{$comp->code.' '.$comp->title}}
                        </p>
                        <br>
                    @endforeach
                @endif
            </td>
        </tr>
        <tr height=0>
            <td width=111 style='border:none'></td>
            <td width=47 style='border:none'></td>
            <td width=151 style='border:none'></td>
            <td width=133 style='border:none'></td>
            <td width=150 style='border:none'></td>
            <td width=14 style='border:none'></td>
            <td width=281 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:666.25pt;margin-left:-1.7pt;border-collapse:collapse;border:none'>
        <tr style='height:10.55pt'>
            <td width="100%" colspan=5 valign=top style='width:100.0%;border:solid black 1.0pt;
  background:#EBD9E2;padding:0cm 5.4pt 0cm 5.4pt;height:10.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:12.0pt;
  font-family:"Soberana Sans"'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr style='height:8.8pt'>
            <td width="100%" colspan=5 valign=top style='width:100.0%;border:solid black 1.0pt;
  border-top:none;background:#D0CECE;padding:0cm 5.4pt 0cm 5.4pt;height:8.8pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Apertura</span></b></p>
            </td>
        </tr>
        <tr style='height:39.7pt'>
            <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:39.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Actividades docente</span></b></p>
            </td>
            <td width="25%" style='width:25.1%;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:39.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Actividades estudiante</span></b></p>
            </td>
            <td width="13%" style='width:13.6%;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:39.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Recursos utilizados</span></b></p>
            </td>
            <td width="17%" style='width:17.94%;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:39.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Producto(s) de aprendizaje</span></b></p>
            </td>
            <td width="15%" style='width:15.36%;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:39.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Evaluación</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Tipo / instrumento / ponderación)</span></b></p>
            </td>
        </tr>

        @if($plan->startActivities)

            @foreach($plan->startActivities as $acti)

                <tr style='height:48.9pt'>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->teacher}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->student}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->resources}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->product}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->kindeva.' / '.$acti->tool.' / '.$acti->eva_percent.'%'  }}</p>
                    </td>
                </tr>

            @endforeach
        @endif

    </table>

</div>

<span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'><br
            clear=all style='page-break-before:always'>
</span>

<div class=WordSection2>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:666.25pt;margin-left:-1.7pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=888 colspan=5 valign=top style='width:666.25pt;border:solid black 1.0pt;
  background:#ECF2DE;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:12.0pt;
  font-family:"Soberana Sans"'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=888 colspan=5 valign=top style='width:666.25pt;border:solid black 1.0pt;
  border-top:none;background:#D0CECE;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Desarrollo</span></b></p>
            </td>
        </tr>
        <tr style='height:16.85pt'>
            <td width=255 style='width:191.4pt;border-top:none;border-left:solid black 1.0pt;
  border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
                                                                                   style='font-size:10.0pt;line-height:115%;font-family:"Soberana Sans"'>Actividades
  docente</span></b></p>
            </td>
            <td width=217 style='width:163.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Actividades estudiante</span></b></p>
            </td>
            <td width=142 style='width:106.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Recursos utilizados</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>&nbsp;</span></b></p>
            </td>
            <td width=142 style='width:106.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Producto(s) de</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Aprendizaje</span></b></p>
            </td>
            <td width=132 style='width:99.2pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Evaluación</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Tipo / instrumento / ponderación)</span></b></p>
            </td>
        </tr>
        @if($plan->devActivities)

            @foreach($plan->devActivities as $acti)

                <tr style='height:48.9pt'>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->teacher}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->student}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->resources}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->product}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->kindeva.' / '.$acti->tool.' / '.$acti->eva_percent.'%'  }}</p>
                    </td>
                </tr>

            @endforeach
        @endif
    </table>




    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:667.7pt;margin-left:-1.7pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=890 colspan=5 valign=top style='width:667.7pt;border:solid black 1.0pt;
  background:#ECF2DE;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:12.0pt;
  font-family:"Soberana Sans"'>ACTIVIDADES DE APRENDIZAJE</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=890 colspan=5 valign=top style='width:667.7pt;border:solid black 1.0pt;
  border-top:none;background:#DDDDDD;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Cierre</span></b></p>
            </td>
        </tr>
        <tr style='height:16.85pt'>
            <td width=265 style='width:198.5pt;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.85pt'>
                <p class=MsoNormal align=center style='text-align:center'><b><span lang=ES
                                                                                   style='font-size:10.0pt;line-height:115%;font-family:"Soberana Sans"'>Actividades
  docente</span></b></p>
            </td>
            <td width=198 style='width:148.8pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Actividades estudiante</span></b></p>
            </td>
            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Recursos utilizados</span></b></p>
            </td>
            <td width=142 style='width:106.35pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Producto(s) de</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Aprendizaje</span></b></p>
            </td>
            <td width=134 style='width:100.65pt;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:16.85pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Evaluación</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Tipo / instrumento / ponderación)</span></b></p>
            </td>
        </tr>
        @if($plan->closeActivities)

            @foreach($plan->closeActivities as $acti)

                <tr style='height:48.9pt'>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->teacher}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->student}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->resources}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->product}}</p>
                    </td>
                    <td width="28%" style='width:28.0%;border:solid black 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:48.9pt'>
                        <p class=MsoNormal>{{$acti->kindeva.' / '.$acti->tool.' / '.$acti->eva_percent.'%'  }}</p>
                    </td>
                </tr>

            @endforeach
        @endif    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:35.7pt;margin-bottom:.0001pt;line-height:normal'><span lang=ES
                                                                   style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span>
    </p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 align=left
           width=0 style='width:669.4pt;border-collapse:collapse;border:none;margin-left:
 4.8pt;margin-right:4.8pt'>
        <tr style='height:12.7pt'>
            <td width=893 valign=top style='width:669.4pt;border:solid windowtext 1.0pt;
  background:#DAEFF6;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:10.0pt;
  font-family:"Soberana Sans"'>Referencias </span></b></p>
            </td>
        </tr>
        <tr style='height:12.7pt'>
            <td width=893 valign=top style='width:669.4pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:12.7pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Fuentes
  de información:</span></b></p>
            </td>
        </tr>
        <tr style='height:109.25pt'>
            <td width=893 valign=top style='width:669.4pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:109.25pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Soberana Sans"'>{{$plan->references}}</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span lang=ES style='font-size:8.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 align=left
           width=0 style='width:666.25pt;border-collapse:collapse;border:none;margin-left:
 4.8pt;margin-right:4.8pt'>
        <tr>
            <td width=888 colspan=3 valign=top style='width:666.25pt;border:solid black 1.0pt;
  background:#A0BF5E;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span lang=ES style='font-size:12.0pt;
  font-family:"Soberana Sans"'>VALIDACIÓN</span></b></p>
            </td>
        </tr>
        <tr style='height:31.45pt'>
            <td width=321 valign=top style='width:240.95pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:31.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Elabora: {{$plan->author}}</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>_______________________     
  </span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
            </td>
            <td width=302 valign=top style='width:226.85pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:31.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Recibe:</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>____________________________________</span>
                </p>
                <p class=Default><span style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
            </td>
            <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:31.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>Avala:</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:10.0pt;font-family:"Soberana Sans"'>______________________________</span></p>
                <p class=Default><span style='font-size:10.0pt;font-family:"Soberana Sans"'>&nbsp;</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='text-align:justify'><span lang=ES style='font-size:
14.0pt;line-height:115%;font-family:"Soberana Sans"'>&nbsp;</span></p>

</div>

</body>

</html>
