<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: "Arial Narrow";
            panose-1: 2 11 6 6 2 2 2 3 2 4;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-link: "Encabezado Car";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-link: "Pie de página Car";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.MsoFootnoteReference {
            vertical-align: super;
        }

        p.MsoBodyText3, li.MsoBodyText3, div.MsoBodyText3 {
            mso-style-link: "Texto independiente 3 Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            text-align: justify;
            line-height: 150%;
            font-size: 12.0pt;
            font-family: "Arial", sans-serif;
        }

        a:link, span.MsoHyperlink {
            color: blue;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Texto de globo Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 8.0pt;
            font-family: "Tahoma", sans-serif;
        }

        p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing {
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.Textoindependiente3Car {
            mso-style-name: "Texto independiente 3 Car";
            mso-style-link: "Texto independiente 3";
            font-family: "Arial", sans-serif;
        }

        span.PiedepginaCar {
            mso-style-name: "Pie de página Car";
            mso-style-link: "Pie de página";
            font-family: "Calibri", sans-serif;
        }

        span.TextodegloboCar {
            mso-style-name: "Texto de globo Car";
            mso-style-link: "Texto de globo";
            font-family: "Tahoma", sans-serif;
        }

        span.EncabezadoCar {
            mso-style-name: "Encabezado Car";
            mso-style-link: Encabezado;
        }

        p.Default, li.Default, div.Default {
            mso-style-name: Default;
            margin: 0cm;
            margin-bottom: .0001pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Calibri", sans-serif;
            color: black;
        }

        .MsoChpDefault {
            font-family: "Calibri", sans-serif;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 792.0pt 612.0pt;
            margin: 1.0cm 1.0cm 42.55pt 1.0cm;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body lang=ES-MX link=blue vlink="#954F72">

<div class=WordSection1>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:749.6pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=138 colspan=3 valign=top style='width:97.8pt;border:solid black 1.0pt;
  border-right:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='position:absolute;z-index:-1895825408;margin-left:1px;
  margin-top:8px;width:304px;height:88px'>
                        @if(auth()->user()->authlogo)
                            <img width=304 height=88
                                 src="{{  asset('img/dgeti001.jpg')}}">
                        @endif
                    </span></p>
            </td>
            <td width=862 colspan=9 valign=top style='width:651.8pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>
                    <span style='position:absolute;
  z-index:-1895826432;left:0px;margin-left:890px;margin-top:4px;width:100px;
  height:124px'>

                    </span>
                    @if(auth()->user()->authlogo)
                        <img width=154 height=46 id="1 Imagen" src="{{asset('img/dgeti003.jpg')}}"
                             alt=sems.JPG>
                    @endif
                </p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:12.0pt;line-height:115%;
  font-family:"Arial",sans-serif;color:black'>SUBSECRETARÍA  DE  EDUCACIÓN 
  MEDIA  SUPERIOR</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
                                style='font-family:"Arial",sans-serif;color:black'>                                                           INSTRUMENTO 
  DE   REGISTRO  DE  ESTRATEGIAS                                                          </span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
                                style='font-family:"Arial",sans-serif;color:black'>                                                                           
  CENTRADAS  EN  EL  APRENDIZAJE.</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><u><span style='text-decoration:
   none'>&nbsp;</span></u></b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=12 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>A)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>IDENTIFICACIÓN
                    </b></p>
            </td>
        </tr>
        <tr>
            <td width=110 colspan=2 valign=top style='width:76.3pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Institución:</p>
            </td>
            <td width=890 colspan=10 valign=top style='width:673.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->assigment->school->subsystem->name}}</p>
            </td>
        </tr>
        <tr style='height:20.6pt'>
            <td width=110 colspan=2 valign=top style='width:76.3pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:20.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Plantel:</p>
            </td>
            <td width=441 colspan=6 valign=top style='width:333.1pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'> {{$plan->assigment->school->name.' '.$plan->assigment->school->number}}
                    &nbsp;({{$plan->assigment->school->officialkey}})</p>
            </td>
            <td width=113 valign=top style='width:3.0cm;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:20.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Profesor:</p>
            </td>
            <td width=336 colspan=3 valign=top style='width:9.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:20.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->author}} </p>
            </td>
        </tr>
        <tr style='height:16.5pt'>
            <td width=110 colspan=2 valign=top style='width:76.3pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:none;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>ASIGNATURA </p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>O MODULO</p>
            </td>
            <td width=186 colspan=2 rowspan=2 valign=top style='width:5.0cm;border-top:
  none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->assigment->program->course->module=="" ? $plan->assigment->program->course->name: $plan->assigment->program->course->module }}
                    &nbsp;</p>
            </td>
            <td width=77 rowspan=3 valign=top style='width:2.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Semestre:</p>
            </td>
            <td width=29 rowspan=3 valign=top style='width:22.05pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->assigment->program->grade}}</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
            </td>
            <td width=65 rowspan=3 valign=top style='width:48.8pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Carrera:</p>
            </td>
            <td width=84 rowspan=3 valign=top style='width:63.8pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                    &nbsp;{{$plan->assigment->careers}}  </p>
            </td>
            <td width=113 rowspan=2 valign=top style='width:3.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt'>Periodo de Aplicación:</span></p>
            </td>
            <td width=186 rowspan=2 valign=top style='width:5.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->assigment->cycle}}</p>
            </td>
            <td width=57 rowspan=3 valign=top style='width:42.55pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Fecha:</p>
            </td>
            <td width=93 rowspan=3 valign=top style='width:70.85pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.5pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->elaboration->format('d/m/Y')}}</p>
            </td>
        </tr>
        <tr style='height:13.45pt'>
            <td width=81 valign=top style='width:55.05pt;border:none;border-left:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:13.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt'>&nbsp;</span></p>
            </td>
            <td width=28 valign=top style='width:21.25pt;border:none;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:13.45pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td width=81 valign=top style='width:55.05pt;border-top:none;border-left:
  solid black 1.0pt;border-bottom:solid black 1.0pt;border-right:none;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt'>&nbsp;</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt'>SUBMODULO</span></p>
            </td>
            <td width=28 valign=top style='width:21.25pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
            </td>
            <td width=186 colspan=2 valign=top style='width:5.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->assigment->program->course->module=="" ? "": $plan->assigment->program->course->name }}</p>
            </td>
            <td width=113 valign=top style='width:3.0cm;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:
  #BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:9.0pt'>Duración en horas:</span></p>
            </td>
            <td width=186 valign=top style='width:5.0cm;border-top:none;border-left:none;
  border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->hours}}</p>
            </td>
        </tr>
        <tr height=0>
            <td width=81 style='border:none'></td>
            <td width=28 style='border:none'></td>
            <td width=28 style='border:none'></td>
            <td width=158 style='border:none'></td>
            <td width=77 style='border:none'></td>
            <td width=29 style='border:none'></td>
            <td width=65 style='border:none'></td>
            <td width=84 style='border:none'></td>
            <td width=113 style='border:none'></td>
            <td width=186 style='border:none'></td>
            <td width=57 style='border:none'></td>
            <td width=93 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNoSpacing>&nbsp;</p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=0
           style='width:749.6pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>B)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>INTENCIONES
                        FORMATIVAS </b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal><span style='font-size:10.0pt;line-height:115%'>&nbsp;{{$plan->purpose}}</span></p>
            </td>
        </tr>
        <tr style='height:10.0pt'>
            <td width=209 colspan=3 valign=top style='width:152.3pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Tema integrador: </p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;</p>
            </td>
            <td width=374 colspan=9 valign=top style='width:246.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:10.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Otras asignaturas, módulos o Sub-módulos que trabajan el tema
                    integrador:</p>
            </td>
            <td width=417 colspan=14 valign=top style='width:351.0pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:10.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$plan->trans_integrator}}</p>
            </td>
        </tr>
        <tr>
            <td width=209 colspan=3 valign=top style='width:152.3pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b>{{$plan->integrator}}</b></p>
            </td>
            <td width=374 colspan=9 valign=top style='width:246.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Asignaturas, módulos y/o sub-módulos con los que se relaciona:</p>
            </td>
            <td width=417 colspan=14 valign=top style='width:351.0pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->trans_course}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Categorías: (2)</p>
            </td>
        </tr>
        <tr>
            <td width=110 valign=top style='width:75.15pt;border:none;border-left:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='color:black'>Espacio  (</span></p>
            </td>
            <td width=37 valign=top style='width:28.55pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'> </span>&nbsp; {{$plan->space? "X": ""}}</p>
            </td>
            <td width=62 valign=top style='width:48.6pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>)</span></p>
            </td>
            <td width=86 valign=top style='width:49.4pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>Energía </span></p>
            </td>
            <td width=39 colspan=2 valign=top style='width:28.1pt;border:none;padding:
  0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>(   {{$plan->energy? "X": ""}}</span></p>
            </td>
            <td width=62 colspan=3 valign=top style='width:48.7pt;border:none;padding:
  0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>)</span></p>
            </td>
            <td width=126 valign=top style='width:77.9pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='color:black'>Diversidad  (</span></p>
            </td>
            <td width=37 valign=top style='width:0.1pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>{{$plan->diversity? "X": ""}}</p>
            </td>
            <td width=32 colspan=2 valign=top style='width:0.7pt;border:none;padding:
  0cm 2.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>)</span></p>
            </td>
            <td width=120 valign=top style='width:84.6pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='color:black'>Tiempo (</span></p>
            </td>
            <td width=29 colspan=2 valign=top style='width:21.2pt;border:none;padding:
  0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>{{$plan->thetime? "X": ""}}</p>
            </td>
            <td width=88 colspan=5 valign=top style='width:89.2pt;border:none;padding:
  0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>)</span></p>
            </td>
            <td width=84 colspan=2 valign=top style='width:65.25pt;border:none;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><span style='color:black'>Materia  (</span></p>
            </td>
            <td width=32 valign=top style='width:28.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>{{$plan->matter? "X": ""}}</p>
            </td>
            <td width=56 colspan=2 valign=top style='width:55.95pt;border:none;
  border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>)</span></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>C)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>CONTENIDOS
                        FACTICOS</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraphCxSpFirst style='margin-bottom:0cm;margin-bottom:
  .0001pt;line-height:normal'><b> </b></p>
                <p class=MsoListParagraphCxSpLast style='margin-bottom:0cm;margin-bottom:
  .0001pt;line-height:normal'>{{$plan->factual}}</p>
            </td>
        </tr>
        <tr>
            <td width=383 colspan=8 valign=top style='width:267.65pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b>CONCEPTO FUNDAMENTAL: </b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->fundamental}}</p>
                <p class=MsoNoSpacing>&nbsp;</p>
            </td>
            <td width=616 colspan=18 valign=top style='width:17.0cm;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b>CONCEPTOS
                        SUBSIDIARIOS: </b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'>{{$plan->subsidiary}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>D)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>CONTENIDOS
                        PROCEDIMENTALES</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->procedimental}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-top:0cm;margin-right:
  0cm;margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:
  center;line-height:normal'><b>E)CONTENIDOS ACTITUDINALES</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->attitudinal}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>E)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>CONTENIDOS
                        EN COMPETENCIAS PROFESIONALES</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->profesionalcontent}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>F)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>COMPETENCIAS
                        GENERICAS Y ATRIBUTOS RELACIONADAS CON EL MCC</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                @if($plan->generics)
                    @foreach($plan->generics as $gen)
                        <strong>{{$gen->code}} </strong>{{$gen->title}} <br>
                        @foreach($plan->attributes as $attribute)
                            @if($gen->id==$attribute->competence_id)
                                {{$attribute->code.' '.$attribute->title}}<br>
                            @endif
                        @endforeach
                    @endforeach
                @endif

            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:0cm;
  margin-bottom:0cm;margin-left:36.0pt;margin-bottom:.0001pt;text-align:center;
  text-indent:-18.0pt;line-height:normal'><b>G)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
  </span></b><b>COMPETENCIAS DISCIPLINARES</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                @if($plan->block)
                    @foreach($plan->discompetences as $comp)
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            {{$comp->code.' '.$comp->title}}
                        </p>

                    @endforeach
                @endif
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;text-indent:-18.0pt;line-height:normal'><b>H)<span
                                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></b><b>COMPETENCIAS
                        DE PRODUCTIVIDAD Y EMPLEABILIDAD DE LA SECRETARIA DE PREVISION SOCIAL</b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                @if($plan->block)
                    @foreach($plan->profcompetences as $comp)
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            {{$comp->code.' '.$comp->title}}
                        </p>

                    @endforeach
                @endif
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b>I) ACTIVIDADES DE APRENDIZAJE </b></p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#7F7F7F;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>FASE DE APERTURA</p>
            </td>
        </tr>
        <tr style='height:27.35pt'>
            <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>ATRIBUTOS</p>
            </td>
            <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>ACTIVIDADES</p>
            </td>
            <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Producto(s) de Aprendizaje</p>
            </td>
            <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Instrumento de Evaluación</p>
            </td>
        </tr>

        @if($plan->startActivities)

            @foreach($plan->startActivities as $acti)
                <tr>

                    <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            @if($acti->attribute)
                                {{$acti->attribute->code.' '.$acti->attribute->title}}
                            @endif
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            <b>Docente:</b>{{$acti->teacher}}
                            <br>
                            <b>Estudiante:</b> {{$acti->student}}
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->product}}</p>
                    </td>
                    <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->tool}}</p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</p>
                    </td>

                </tr>
            @endforeach

        @endif


        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:#7F7F7F;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>FASE DE DESARROLLO</p>
            </td>
        </tr>
        <tr style='height:27.35pt'>
            <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>ATRIBUTOS</p>
            </td>
            <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='text-align:center'>ACTIVIDADES</p>
            </td>
            <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Producto(s) de Aprendizaje</p>
            </td>
            <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Instrumento de Evaluación</p>
            </td>
        </tr>
        @if($plan->devActivities)

            @foreach($plan->devActivities as $acti)
                <tr>

                    <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            @if($acti->attribute)
                                {{$acti->attribute->code.' '.$acti->attribute->title}}
                            @endif
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            <b>Docente:</b>{{$acti->teacher}}
                            <br>
                            <b>Estudiante:</b> {{$acti->student}}
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->product}}</p>
                    </td>
                    <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->tool}}</p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</p>
                    </td>

                </tr>
            @endforeach

        @endif

        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:gray;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>FASE DE CIERRE</p>
            </td>
        </tr>
        <tr style='height:27.35pt'>
            <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>ATRIBUTOS</p>
            </td>
            <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>ACTIVIDADES</p>
            </td>
            <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Producto(s) de Aprendizaje</p>
            </td>
            <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt;height:27.35pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Evaluación</p>
            </td>
        </tr>
        @if($plan->closeActivities)

            @foreach($plan->closeActivities as $acti)
                <tr>

                    <td width=308 colspan=5 valign=top style='width:209.7pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            @if($acti->attribute)
                                {{$acti->attribute->code.' '.$acti->attribute->title}}
                            @endif
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=441 colspan=14 valign=top style='width:308.9pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            <b>Docente:</b>{{$acti->teacher}}
                            <br>
                            <b>Estudiante:</b> {{$acti->student}}
                            <span lang=ES style='font-family:"Arial Narrow",sans-serif'>&nbsp;</span></p>
                    </td>
                    <td width=128 colspan=3 valign=top style='width:119.3pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->product}}</p>
                    </td>
                    <td width=123 colspan=4 valign=top style='width:111.7pt;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->tool}}</p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</p>
                    </td>

                </tr>
            @endforeach

        @endif
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border:solid black 1.0pt;
  border-top:none;background:gray;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-top:0cm;margin-right:
  0cm;margin-bottom:0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:
  center;text-indent:-18.0pt;line-height:normal'>K)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span>RECURSOS</p>
            </td>
        </tr>
        <tr>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Equipo</p>
            </td>
            <td width=644 colspan=19 rowspan=8 valign=top style='width:504.0pt;
  border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:
  solid black 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Fuentes de información</p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US>{{$plan->references}}</span></p>

            </td>
        </tr>
        <tr style='height:37.35pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:37.35pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->equipment}}</p>
            </td>
        </tr>
        <tr style='height:3.5pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt;height:3.5pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Reactivos</p>
            </td>
        </tr>
        <tr style='height:13.85pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:13.85pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal'>{{$plan->reactives}}</p>
            </td>
        </tr>
        <tr style='height:16.15pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt;height:16.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Material</p>
            </td>
        </tr>
        <tr style='height:34.65pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:34.65pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>{{$plan->material}}</p>
            </td>
        </tr>
        <tr style='height:14.0pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;background:#A6A6A6;padding:0cm 5.4pt 0cm 5.4pt;height:14.0pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>Equipo de bioseguridad</p>
            </td>
        </tr>
        <tr style='height:13.45pt'>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:13.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'>{{$plan->biosecurity}}</p>
            </td>
        </tr>
        <tr>
            <td width=999 colspan=26 valign=top style='width:749.6pt;border-top:none;
  border-left:solid black 1.0pt;border-bottom:solid windowtext 1.0pt;
  border-right:solid black 1.0pt;background:gray;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoListParagraph align=center style='margin-top:0cm;margin-right:
  0cm;margin-bottom:0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:
  center;text-indent:-18.0pt;line-height:normal'>L)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span> VALIDACIÓN</p>
            </td>
        </tr>
        <tr>
            <td width=355 colspan=7 valign=top style='width:245.6pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Elabora: {{$plan->author}}</p>
                <br>
                <br>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>_______________________________________________</p>
            </td>
            <td width=300 colspan=11 valign=top style='width:269.35pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Avala: PRESIDENTE DE ACADEMIA LOCAL o ESTATAL</p>
                <br>
                <br>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>___________________________________________________</p>
                <p class=MsoNormal style='margin-left:5em;margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->chiefacademy}}</p>
            </td>
            <td width=300 colspan=11 valign=top style='width:269.35pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>Recibe: Jefe de Serv. Docentes</p>
                <br>
                <br>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>______________________________</p>
                <p class=MsoNormal style='margin-left:1em;margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>&nbsp;{{$plan->chiefteachers}}</p>

            </td>
        </tr>


        <tr height=0>
            <td width=110 style='border:none'></td>
            <td width=37 style='border:none'></td>
            <td width=62 style='border:none'></td>
            <td width=86 style='border:none'></td>
            <td width=13 style='border:none'></td>
            <td width=27 style='border:none'></td>
            <td width=21 style='border:none'></td>
            <td width=28 style='border:none'></td>
            <td width=14 style='border:none'></td>
            <td width=126 style='border:none'></td>
            <td width=37 style='border:none'></td>
            <td width=23 style='border:none'></td>
            <td width=9 style='border:none'></td>
            <td width=120 style='border:none'></td>
            <td width=14 style='border:none'></td>
            <td width=14 style='border:none'></td>
            <td width=4 style='border:none'></td>
            <td width=1 style='border:none'></td>
            <td width=5 style='border:none'></td>
            <td width=10 style='border:none'></td>
            <td width=69 style='border:none'></td>
            <td width=49 style='border:none'></td>
            <td width=35 style='border:none'></td>
            <td width=32 style='border:none'></td>
            <td width=30 style='border:none'></td>
            <td width=26 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify'><b><span style='font-size:12.0pt;line-height:115%'>&nbsp;</span></b></p>

</div>

</body>

</html>
