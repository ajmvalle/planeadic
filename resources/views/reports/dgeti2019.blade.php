<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: "Segoe UI";
            panose-1: 2 11 5 2 4 2 4 2 2 3;
        }

        @font-face {
            font-family: "Soberana Texto";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoCommentText, li.MsoCommentText, div.MsoCommentText {
            mso-style-link: "Texto comentario Car";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-link: "Encabezado Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-link: "Pie de página Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        a:link, span.MsoHyperlink {
            color: blue;
            text-decoration: underline;
        }

        a:visited, span.MsoHyperlinkFollowed {
            color: #954F72;
            text-decoration: underline;
        }

        p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject {
            mso-style-link: "Asunto del comentario Car";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
            font-weight: bold;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Texto de globo Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 9.0pt;
            font-family: "Segoe UI", sans-serif;
        }

        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 36.0pt;
            line-height: 107%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.EncabezadoCar {
            mso-style-name: "Encabezado Car";
            mso-style-link: Encabezado;
        }

        span.TextocomentarioCar {
            mso-style-name: "Texto comentario Car";
            mso-style-link: "Texto comentario";
        }

        span.AsuntodelcomentarioCar {
            mso-style-name: "Asunto del comentario Car";
            mso-style-link: "Asunto del comentario";
            font-weight: bold;
        }

        span.TextodegloboCar {
            mso-style-name: "Texto de globo Car";
            mso-style-link: "Texto de globo";
            font-family: "Segoe UI", sans-serif;
        }

        span.PiedepginaCar {
            mso-style-name: "Pie de página Car";
            mso-style-link: "Pie de página";
        }

        p.Pa6, li.Pa6, div.Pa6 {
            mso-style-name: Pa6;
            margin: 0cm;
            margin-bottom: .0001pt;
            line-height: 12.05pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Soberana Texto", serif;
        }

        span.A2 {
            mso-style-name: A2;
            font-family: "Soberana Texto", serif;
            color: black;
        }

        p.Default, li.Default, div.Default {
            mso-style-name: Default;
            margin: 0cm;
            margin-bottom: .0001pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Calibri", sans-serif;
            color: black;
        }

        .MsoChpDefault {
            font-family: "Calibri", sans-serif;
        }

        .MsoPapDefault {
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 792.0pt 612.0pt;
            margin: 72.0pt 54.0pt 72.0pt 54.0pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body lang=ES-MX link=blue vlink="#954F72">

<div class=WordSection1>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=927
           style='width:695.05pt;border-collapse:collapse;border:none'>
        <tr style='height:87.9pt'>
            <td width=927 colspan=12 style='width:695.05pt;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:87.9pt'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><img width=249 height=83
                                            src="{{  asset('img/dgeti001.jpg')}}" align=left hspace=12><a
                            name="LogoSEP"></a><a name="_Hlk489278748"></a><b><span
                                style='font-size:14.0pt;font-family:"Arial",sans-serif;position:relative;
  top:-3.0pt;letter-spacing:1.7pt'>SECRETARÍA DE EDUCACIÓN PÚBLICA</span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-family:"Arial",sans-serif;
  letter-spacing:1.1pt'>SUBSECRETARÍA DE EDUCACIÓN MEDIA SUPERIOR</span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:8.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->school->subsystem->name}}</span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->school->fullname}}</span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-size:9.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->school->subname}}</span></b></p>
            </td>
            <td style='height:87.9pt;border:none' width=0 height=117></td>
        </tr>
        <tr style='height:16.25pt'>
            <td width=927 colspan=12 style='width:695.05pt;border:none;border-bottom:
  solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td style='height:16.25pt;border:none' width=0 height=22></td>
        </tr>
        <tr style='height:16.25pt'>
            <td width=927 colspan=12 style='width:695.05pt;border:solid windowtext 1.0pt;
  border-top:none;background:#99FF33;padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Instrumento de registro de la Planeación
  Didáctica</span></b></p>
            </td>
            <td style='height:16.25pt;border:none' width=0 height=22></td>
        </tr>
        <tr style='height:16.25pt'>
            <td width=45 rowspan=10 style='width:33.7pt;border:solid windowtext 1.0pt;
  border-top:none;background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'>
                    <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;writing-mode: tb-rl;transform: rotate(-180deg)'>Identificación</span></b>
                </p>
            </td>
            <td width=99 style='width:74.3pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#FFFF99;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Institución:
  </span></b></p>
            </td>
            <td width=82 style='width:61.25pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->school->subsystem->shortname}}</span></b></p>
            </td>
            <td width=66 style='width:49.55pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>Plantel:</span></b></p>
            </td>
            <td width=317 colspan=3 style='width:237.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                    <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$plan->assigment->school->shortname}}</span></b>
                </p>
            </td>
            <td width=112 style='width:83.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>C.C.T</span></b></p>
            </td>
            <td width=206 colspan=4 style='width:154.6pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.25pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->school->officialkey}}</span></b></p>
            </td>
            <td style='height:16.25pt;border:none' width=0 height=22></td>
        </tr>
        <tr style='height:9.75pt'>
            <td width=99 rowspan=2 style='width:74.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Docente (s) que elaboró el instrumento:</span></b></p>
            </td>
            <td width=465 colspan=5 rowspan=2 style='width:348.5pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->author}}</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width=112 rowspan=2 style='width:83.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Fecha de elaboración:</span></b></p>
            </td>
            <td width=58 colspan=2 style='width:43.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->elaboration->format('d')}}</span></b></p>
            </td>
            <td width=66 style='width:49.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->elaboration->format('m')}}</span></b></p>
            </td>
            <td width=82 style='width:61.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->elaboration->format('Y')}}</span></b></p>
            </td>
            <td style='height:9.75pt;border:none' width=0 height=13></td>
        </tr>
        <tr style='height:9.75pt'>
            <td width=58 colspan=2 style='width:43.6pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Día</span></b></p>
            </td>
            <td width=66 style='width:49.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Mes</span></b></p>
            </td>
            <td width=82 style='width:61.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:9.75pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Año</span></b></p>
            </td>
            <td style='height:9.75pt;border:none' width=0 height=13></td>
        </tr>
        <tr style='page-break-inside:avoid;height:26.65pt'>
            <td width=334 colspan=4 style='width:250.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Asignatura o submódulo:</span></b></p>
            </td>
            <td width=99 style='width:74.45pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Semestre:</span></b></p>
            </td>
            <td width=131 style='width:98.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Carrera:</span></b></p>
            </td>
            <td width=152 colspan=2 style='width:113.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Periodo de la aplicación:</span></b></p>
            </td>
            <td width=166 colspan=3 style='width:124.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->application}}</span></b></p>
            </td>
            <td style='height:26.65pt;border:none' width=0 height=36></td>
        </tr>
        <tr style='page-break-inside:avoid;height:26.55pt'>
            <td width=334 colspan=4 style='width:250.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->program->course->name}}</span></b></p>
            </td>
            <td width=99 style='width:74.45pt;border-top:none;border-left:none;
  border-bottom:double windowtext 1.5pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->program->grade}}</span></b></p>
            </td>
            <td width=131 style='width:98.0pt;border-top:none;border-left:none;
  border-bottom:double windowtext 1.5pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->careers}}</span></b></p>
            </td>
            <td width=152 colspan=2 style='width:113.7pt;border-top:none;border-left:
  none;border-bottom:double windowtext 1.5pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración en Horas</span></b></p>
            </td>
            <td width=166 colspan=3 style='width:124.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->hours}}</span></b></p>
            </td>
            <td style='height:26.55pt;border:none' width=0 height=35></td>
        </tr>
        <tr style='page-break-inside:avoid;height:26.55pt'>
            <td width=334 colspan=4 style='width:250.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:double windowtext 1.5pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Campo disciplinar de la asignatura</span></b></p>
            </td>
            <td width=548 colspan=7 rowspan=2 style='width:411.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:double windowtext 1.5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:26.55pt'>
                <p class=Default align=center style='text-align:center'><b><span
                                style='font-size:11.0pt;color:windowtext'>{{$plan->purpose_area}}</span></b></p>
            </td>
            <td style='height:26.55pt;border:none' width=0 height=35></td>
        </tr>
        <tr style='page-break-inside:avoid;height:53.65pt'>
            <td width=334 colspan=4 rowspan=2 style='width:250.35pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:double windowtext 1.5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:53.65pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:12.0pt;
  font-family:"Arial",sans-serif'>{{$plan->assigment->program->course->area}}</span></b></p>
            </td>
            <td style='height:53.65pt;border:none' width=0 height=72></td>
        </tr>
        <tr style='page-break-inside:avoid;height:48.95pt'>
            <td width=548 colspan=7 style='width:411.0pt;border-top:none;border-left:
  none;border-bottom:double windowtext 1.5pt;border-right:double windowtext 1.5pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:48.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Propósito formativo del campo disciplinar</span></b></p>
            </td>
            <td style='height:48.95pt;border:none' width=0 height=65></td>
        </tr>
        <tr style='page-break-inside:avoid;height:47.95pt'>
            <td width=334 colspan=4 style='width:250.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:47.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Transversalidad con otras asignaturas</span></b></p>
            </td>
            <td width=548 colspan=7 style='width:411.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:47.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->transversality}}</span></b></p>

            </td>
            <td style='height:47.95pt;border:none' width=0 height=64></td>
        </tr>
        <tr style='page-break-inside:avoid;height:45.7pt'>
            <td width=334 colspan=4 style='width:250.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFFF99;padding:0cm 5.4pt 0cm 5.4pt;height:45.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Ámbitos del perfil de egreso en el que
  contribuye la asignatura</span></b></p>
            </td>
            <td width=548 colspan=7 style='width:411.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:45.7pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->profile_ambits}}</span></b></p>
            </td>
            <td style='height:45.7pt;border:none' width=0 height=61></td>
        </tr>
        <tr height=0>
            <td width=45 style='border:none'></td>
            <td width=99 style='border:none'></td>
            <td width=82 style='border:none'></td>
            <td width=66 style='border:none'></td>
            <td width=87 style='border:none'></td>
            <td width=99 style='border:none'></td>
            <td width=131 style='border:none'></td>
            <td width=112 style='border:none'></td>
            <td width=40 style='border:none'></td>
            <td width=18 style='border:none'></td>
            <td width=66 style='border:none'></td>
            <td width=82 style='border:none'></td>
            <td style='border:none' width=0><p class='MsoNormal'>&nbsp;</td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal>&nbsp;</p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=924
           style='width:693.2pt;border-collapse:collapse;border:none'>
        <tr style='page-break-inside:avoid;height:44.45pt'>
            <td width=45 rowspan=11 style='width:33.7pt;border:solid windowtext 1.0pt;
  border-bottom:none;background:#66CCFF;padding:0cm 5.4pt 0cm 5.4pt;height:
  44.45pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif;writing-mode: tb-rl;transform: rotate(-180deg)'><b>Intenciones
  Formativas</b></span></p>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>
            </td>
            <td width=180 colspan=2 style='width:135.2pt;border:solid windowtext 1.0pt;
  border-left:none;background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:44.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%;text-autospace:none'><b><span
                                style='font-size:10.0pt;line-height:150%;font-family:"Arial",sans-serif'>Propósito
  formativo de la asignatura</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt;height:44.45pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->purpose}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=49 rowspan=3 style='width:36.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:150%;text-autospace:none'><b><span style='font-size:10.0pt;
  line-height:150%;font-family:"Arial",sans-serif'>Aprendizajes clave de la
  asignatura</span></b></p>
            </td>
            <td width=131 style='width:98.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%;text-autospace:none'><b><span
                                style='font-size:10.0pt;line-height:150%;font-family:"Arial",sans-serif'>&nbsp;</span></b>
                </p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%;text-autospace:none'><b><span
                                style='font-size:10.0pt;line-height:150%;font-family:"Arial",sans-serif'>Ejes
  disciplinarios</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:150%;text-autospace:none'><b><span
                                style='font-size:10.0pt;line-height:150%;font-family:"Arial",sans-serif'>&nbsp;</span></b>
                </p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->block->axis}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:89.45pt'>
            <td width=131 style='width:98.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:89.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Componente</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:89.45pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->block->component}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=131 style='width:98.4pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Contenido central</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->block->title}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Aprendizaje esperado</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->expecteds}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Proceso de aprendizaje</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->process}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Contenidos específicos</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=Default><span style='font-size:10.0pt;font-family:"Arial",sans-serif;
  color:windowtext'>{{$plan->specifics}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Habilidades socioemocionales (HSE) a
  desarrollar</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$plan->social}}</span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Competencias Genéricas y atributos</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>
                        @if($plan->generics)
                            @foreach($plan->generics as $gen)
                                <strong>{{$gen->code}} </strong>{{$gen->title}} <br>
                                @foreach($plan->attributes as $attribute)
                                    @if($gen->id==$attribute->competence_id)
                                        {{$attribute->code.' '.$attribute->title}}<br>
                                    @endif
                                @endforeach
                            @endforeach
                        @endif
                    </span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Competencias Disciplinares</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>
                        @foreach($plan->discompetences as $competence)
                            <strong>{{$competence->code}} </strong>{{$competence->title}}
                            <br>
                        @endforeach
                    </span></p>
            </td>
        </tr>
        <tr style='page-break-inside:avoid;height:43.95pt'>
            <td width=180 colspan=2 style='width:135.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#66FFFF;padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Competencias profesionales</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width=699 style='width:524.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.95pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>
                @if($plan->block)
                    @foreach($plan->profcompetences as $comp)
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                            {{$comp->code.' '.$comp->title}}
                        </p>

                        @endforeach
                        @endif
                        </span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=919
           style='width:688.95pt;border-collapse:collapse;border:none'>


        <tr style='height:15.45pt'>
            <td width=919 colspan=6 style='width:688.95pt;border:solid windowtext 1.0pt;
  background:#99FF33;padding:0cm 5.4pt 0cm 5.4pt;height:15.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividades de aprendizaje</span></b></p>
            </td>
        </tr>

        @if($plan->startActivities)

            @foreach($plan->startActivities as $acti)

                <tr style='height:35.1pt'>
                    <td width=35 rowspan=4 style='width:26.3pt;border:solid windowtext 1.0pt;
  border-top:none;background:lime;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'>
                            <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;writing-mode: tb-rl;transform: rotate(-180deg)'>Apertura</span></b>
                        </p>
                    </td>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del Docente</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Recursos utilizados</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.2pt'>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->teacher}}</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>                                                                                 </span>
                        </p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->resources}}</span></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time}}</span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del estudiante</span></b></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Producto de aprendizaje esperado</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Tipo de evaluación</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Ponderación  </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->student}}</span></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time_student}}</span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->product}}</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->kindeva.' / '.$acti->tool}}</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</span></b></p>
                    </td>
                </tr>

            @endforeach

        @endif

        <tr style='height:14.4pt'>
            <td width=919 colspan=6 style='width:688.95pt;border:solid windowtext 1.0pt;
  border-top:none;background:#99FF33;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividades de aprendizaje</span></b></p>
            </td>
        </tr>

        @if($plan->devActivities)

            @foreach($plan->devActivities as $acti)

                <tr style='height:35.1pt'>
                    <td width=35 rowspan=4 style='width:26.3pt;border:solid windowtext 1.0pt;
  border-top:none;background:lime;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'>
                            <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;writing-mode: tb-rl;transform: rotate(-180deg)'>Desarrollo</span></b>
                        </p>
                    </td>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del Docente</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Recursos utilizados</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.2pt'>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->teacher}}</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>                                                                                 </span>
                        </p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->resources}}</span></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time}}</span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del estudiante</span></b></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Producto de aprendizaje esperado</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Tipo de evaluación</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Ponderación  </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->student}}</span></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time_student}}</span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->product}}</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->kindeva.' / '.$acti->tool}}</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</span></b></p>
                    </td>
                </tr>

            @endforeach

        @endif

        <tr style='height:15.45pt'>
            <td width=919 colspan=6 style='width:688.95pt;border:solid windowtext 1.0pt;
  border-top:none;background:#99FF33;padding:0cm 5.4pt 0cm 5.4pt;height:15.45pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividades de aprendizaje</span></b></p>
            </td>
        </tr>

        @if($plan->closeActivities)

            @foreach($plan->closeActivities as $acti)

                <tr style='height:35.1pt'>
                    <td width=35 rowspan=4 style='width:26.3pt;border:solid windowtext 1.0pt;
  border-top:none;background:lime;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'>
                            <b><span style='font-size:10.0pt;font-family:"Arial",sans-serif;writing-mode: tb-rl;transform: rotate(-180deg)'>Cierre</span></b>
                        </p>
                    </td>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del Docente</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Recursos utilizados</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:35.1pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.2pt'>
                    <td width=541 colspan=3 style='width:405.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->teacher}}</span></p>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>                                                                                 </span>
                        </p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->resources}}</span></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.2pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time}}</span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Actividad del estudiante</span></b></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Duración </span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Producto de aprendizaje esperado</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Tipo de evaluación</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:yellow;padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Ponderación  </span></b></p>
                    </td>
                </tr>
                <tr style='height:34.85pt'>
                    <td width=328 style='width:245.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$acti->student}}</span></p>
                    </td>
                    <td width=77 style='width:57.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->time_student}}</span></b></p>
                    </td>
                    <td width=136 style='width:102.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->product}}</span></b></p>
                    </td>
                    <td width=239 style='width:178.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->kindeva.' / '.$acti->tool}}</span></b></p>
                    </td>
                    <td width=104 style='width:77.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:34.85pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$acti->eva_percent!=''?($acti->eva_percent.'%'):''}}</span></b></p>
                    </td>
                </tr>

            @endforeach

        @endif
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal>&nbsp;</p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=926
           style='width:694.35pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=926 colspan=4 style='width:694.35pt;border:solid windowtext 1.0pt;
  background:#00B0F0;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Recursos por utilizar</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=453 colspan=2 style='width:339.95pt;border:solid windowtext 1.0pt;
  border-top:none;background:#B4C6E7;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Materiales</span></p>
            </td>
            <td width=473 colspan=2 style='width:354.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#B4C6E7;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Equipo</span></p>
            </td>
        </tr>
        <tr>
            <td width=453 colspan=2 style='width:339.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->material}}</span></b></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>&nbsp;</span></p>
            </td>
            <td width=473 colspan=2 style='width:354.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>{{$plan->equipment}}</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=926 colspan=4 style='width:694.35pt;border:solid windowtext 1.0pt;
  border-top:none;background:red;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Referencias </span></b></p>
            </td>
        </tr>
        <tr>
            <td width=453 colspan=2 style='width:339.95pt;border:solid windowtext 1.0pt;
  border-top:none;background:#FFA3A3;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Bibliográficas</span></p>
            </td>
            <td width=473 colspan=2 style='width:354.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#FFA3A3;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Internet; otras fuentes</span></p>
            </td>
        </tr>
        <tr>
            <td width=453 colspan=2 style='width:339.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$plan->references}}</span></b></p>
            </td>
            <td width=473 colspan=2 style='width:354.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial",sans-serif'>{{$plan->extrareferences}}</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=926 colspan=4 valign=top style='width:694.35pt;border:solid windowtext 1.0pt;
  border-top:none;background:lime;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Validación </span></b></p>
            </td>
        </tr>
        <tr>
            <td width=304 valign=top style='width:227.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>Elaborado
  por: </span></p>
            </td>
            <td width=304 colspan=2 valign=top style='width:227.85pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>Recibido
  por:</span></p>
            </td>
            <td width=318 valign=top style='width:238.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>Avalado
  por:</span></p>
            </td>
        </tr>
        <tr style='height:63.2pt'>
            <td width=304 style='width:227.8pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt;height:63.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-family:"Arial",sans-serif;color:red'>&nbsp;</span></b></p>
            </td>
            <td width=304 colspan=2 style='width:227.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:63.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-family:"Arial",sans-serif;color:red'>&nbsp;</span></b></p>
            </td>
            <td width=318 style='width:238.7pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:63.2pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-family:"Arial",sans-serif;color:red'>&nbsp;</span></b></p>
            </td>
        </tr>
        <tr style='height:25.8pt'>
            <td width=304 valign=top style='width:227.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.8pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>{{$plan->author}}</span></p>
            </td>
            <td width=304 colspan=2 valign=top style='width:227.85pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:25.8pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>{{$plan->chiefteachers}}</span></p>
            </td>
            <td width=318 valign=top style='width:238.7pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:25.8pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Arial",sans-serif'>{{$plan->chiefacademy}}</span></p>
            </td>
        </tr>
        <tr height=0>
            <td width=304 style='border:none'></td>
            <td width=150 style='border:none'></td>
            <td width=154 style='border:none'></td>
            <td width=318 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    @if($plan->collaborations!="")
        <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=926
               style='width:694.35pt;border-collapse:collapse;border:none'>
            <tr style='height:12.15pt'>
                <td width=926 valign=top style='width:694.35pt;border:solid windowtext 1.0pt;
  background:lime;padding:0cm 5.4pt 0cm 5.4pt;height:12.15pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif'>Contribuciones y/o colaboraciones</span></b></p>
                </td>
            </tr>
            <tr style='height:75.3pt'>
                <td width=926 valign=top style='width:694.35pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:75.3pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-family:"Arial",sans-serif'>{{$plan->collaborations}}&nbsp;</span></b></p>
                </td>
            </tr>
        </table>
    @endif

    <p class=MsoNormal align=center style='text-align:center'><span
                style='font-family:"Arial",sans-serif'>&nbsp;</span></p>

</div>

</body>

</html>
