<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face {
            font-family: Wingdings;
            panose-1: 5 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        @font-face {
            font-family: "Lucida Sans Unicode";
            panose-1: 2 11 6 2 3 5 4 2 2 4;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoHeader, li.MsoHeader, div.MsoHeader {
            mso-style-link: "Encabezado Car";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoFooter, li.MsoFooter, div.MsoFooter {
            mso-style-link: "Pie de página Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
            mso-style-link: "Texto de globo Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 8.0pt;
            font-family: "Tahoma", sans-serif;
        }

        span.EncabezadoCar {
            mso-style-name: "Encabezado Car";
            mso-style-link: Encabezado;
            font-family: "Calibri", sans-serif;
        }

        p.Listavistosa-nfasis11, li.Listavistosa-nfasis11, div.Listavistosa-nfasis11 {
            mso-style-name: "Lista vistosa - Énfasis 11";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.Listavistosa-nfasis11CxSpFirst, li.Listavistosa-nfasis11CxSpFirst, div.Listavistosa-nfasis11CxSpFirst {
            mso-style-name: "Lista vistosa - Énfasis 11CxSpFirst";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.Listavistosa-nfasis11CxSpMiddle, li.Listavistosa-nfasis11CxSpMiddle, div.Listavistosa-nfasis11CxSpMiddle {
            mso-style-name: "Lista vistosa - Énfasis 11CxSpMiddle";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        p.Listavistosa-nfasis11CxSpLast, li.Listavistosa-nfasis11CxSpLast, div.Listavistosa-nfasis11CxSpLast {
            mso-style-name: "Lista vistosa - Énfasis 11CxSpLast";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 36.0pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
        }

        span.TextodegloboCar {
            mso-style-name: "Texto de globo Car";
            mso-style-link: "Texto de globo";
            font-family: "Tahoma", sans-serif;
        }

        span.PiedepginaCar {
            mso-style-name: "Pie de página Car";
            mso-style-link: "Pie de página";
            font-family: "Calibri", sans-serif;
        }

        p.Ttulo, li.Ttulo, div.Ttulo {
            mso-style-name: Título;
            mso-style-link: "Título Car";
            margin: 0cm;
            margin-bottom: .0001pt;
            text-align: center;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        span.TtuloCar {
            mso-style-name: "Título Car";
            mso-style-link: Título;
            font-family: "Times New Roman", serif;
            font-weight: bold;
        }

        p.Default, li.Default, div.Default {
            mso-style-name: Default;
            margin: 0cm;
            margin-bottom: .0001pt;
            text-autospace: none;
            font-size: 12.0pt;
            font-family: "Lucida Sans Unicode", sans-serif;
            color: black;
        }

        .MsoChpDefault {
            font-size: 10.0pt;
            font-family: "Calibri", sans-serif;
        }

        /* Page Definitions */
        @page WordSection1 {
            size: 792.1pt 612.1pt;
            margin: 70.6pt 70.9pt 1.0cm 70.9pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        /* List Definitions */
        ol {
            margin-bottom: 0cm;
        }

        ul {
            margin-bottom: 0cm;
        }

        -->
    </style>

</head>

<body lang=ES-MX>

<div class=WordSection1>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="100%"
           style='width:100.0%;border-collapse:collapse;border:none'>
        <tr style='height:63.6pt'>
            <td width="2%" style='width:2.82%;border:solid black 1.0pt;border-right:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:63.6pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'>
                    @if(auth()->user()->authlogo)
                        <img width=304 height=88
                             src="{{  asset('img/dgeti001.jpg')}}">
                    @endif
                </p>
            </td>
            <td width="97%" style='width:97.18%;border:solid black 1.0pt;border-left:
  none;padding:0cm 5.4pt 0cm 5.4pt;height:63.6pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span lang=ES>&nbsp;</span></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'>
                    @if(auth()->user()->authlogo)
                        <img width=154 height=46 id="1 Imagen" src="{{asset('img/dgeti003.jpg')}}"
                             alt=sems.JPG>
                    @endif

                </p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span lang=ES style='font-size:12.0pt'>
                            @if($tool->assigment)
                                @if($tool->assigment->school)
                                    {{mb_strtoupper($tool->assigment->school->subsystem->department)}}
                                @endif
                            @endif
                        </span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span lang=ES style='font-size:12.0pt'>
                                @if($tool->assigment)
                                @if($tool->assigment->school)
                                    {{$tool->assigment->school->subsystem->name}}
                                @endif
                            @endif

                            </span></b></p>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><u><span lang=ES style='font-size:
  12.0pt'>

                                @if($tool->assigment)
                                    @if($tool->assigment->school)
                                        {{$tool->assigment->school->translated.' '.$tool->assigment->school->number}}
                                    @endif
                                @endif

                            </span></u></b></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><span style='font-size:16.0pt;
font-family:"Arial",sans-serif'>{{mb_strtoupper($tool->desc).': '.mb_strtoupper($tool->title)}}</span></b></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="100%"
           style='width:100.0%;border-collapse:collapse;border:none'>
        <tr>
            <td width="22%" valign=top style='width:22.0%;border:solid black 1.0pt;
  background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Elabora:</span></b></p>
            </td>
            <td width="78%" colspan=5 valign=top style='width:78.0%;border:solid black 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span>{{$tool->user->fullname}}</b>
                </p>
            </td>
        </tr>
        <tr>
            <td width="22%" valign=top style='width:22.0%;border:solid black 1.0pt;
  border-top:none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Carrera(s): </span></b></p>
            </td>
            <td width="78%" colspan=5 valign=top style='width:78.0%;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span>
                        @if($tool->assigment)
                            {{$tool->assigment->careers}}
                        @endif
                    </b></p>
            </td>
        </tr>
        <tr>
            <td width="22%" valign=top style='width:22.0%;border:solid black 1.0pt;
  border-top:none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Grupos:</span></b></p>
            </td>
            <td width="43%" valign=top style='width:43.0%;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span>
                        @if($tool->assigment)
                            {{$tool->assigment->groups}}
                        @endif
                    </b></p>
            </td>
            <td width="12%" colspan=2 valign=top style='width:1.0%;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Periodo escolar:</span></b></p>
            </td>
            <td width="19%" colspan=2 style='width:19.0%;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span>
                        @if($tool->assigment)
                            {{$tool->assigment->cycle}}
                        @endif
                    </b></p>
            </td>
        </tr>
        <tr>
            <td width="22%" valign=top style='width:22.0%;border:solid black 1.0pt;
  border-top:none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Asignatura:</span></b></p>
            </td>
            <td width="78%" colspan=5 valign=top style='width:78.0%;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span>
                        @if($tool->assigment)
                            {{$tool->assigment->program->course->name}}
                        @endif
                    </b></p>
            </td>
        </tr>
        <tr>
            <td width="22%" valign=top style='width:22.0%;border:solid black 1.0pt;
  border-top:none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Nombre del alumno(s):</span></b></p>
            </td>
            <td width="44%" colspan=2 valign=top style='width:44.0%;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=Listavistosa-nfasis11 style='margin:0cm;margin-bottom:.0001pt;
  text-align:justify;line-height:normal'><b><span lang=ES style='font-size:
  12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
            <td width="21%" colspan=2 valign=top style='width:10.0%;border-top:none;
  border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
  color:white'>Total Puntos Obtenidos:</span></b></p>
            </td>
            <td width="13%" valign=top style='width:13.0%;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=Listavistosa-nfasis11 style='margin:0cm;margin-bottom:.0001pt;
  text-align:justify;line-height:normal'><b><span lang=ES style='font-size:
  12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></b></p>
            </td>
        </tr>
        <tr height=0>
            <td width=155 style='border:none'></td>
            <td width=304 style='border:none'></td>
            <td width=7 style='border:none'></td>
            <td width=106 style='border:none'></td>
            <td width=42 style='border:none'></td>
            <td width=92 style='border:none'></td>
        </tr>
    </table>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><b><i><span lang=ES>Instrucciones</span></i></b><span
                lang=ES>: Anote en cada casilla los puntos obtenidos por el alumno en cada
criterio por evaluar.</span></p>

    <div align=center>

        <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="100%"
               style='width:100.0%;border-collapse:collapse;border:none'>
            <tr>
                <td width="16%" style='width:16.0%;border:solid windowtext 1.0pt;background:
  #404040;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>Indicador</span></b></p>
                </td>
                <td width="17%" style='width:17.1%;border:solid windowtext 1.0pt;border-left:
  none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>{{$tool->header4}}</span></b></p>
                </td>
                <td width="22%" style='width:22.52%;border:solid windowtext 1.0pt;border-left:
  none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>{{$tool->header3}}</span></b></p>
                </td>

                @if($tool->kind != 'list')
                    <td width="17%" style='width:17.7%;border:solid windowtext 1.0pt;border-left:
  none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>{{$tool->header2}}</span></b></p>
                    </td>
                    <td width="17%" style='width:17.7%;border:solid windowtext 1.0pt;border-left:
  none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>{{$tool->header1}}</span></b></p>
                    </td>

                @endif
                <td width="8%" style='width:8.96%;border:solid windowtext 1.0pt;border-left:
  none;background:#404040;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-size:10.0pt;
  font-family:"Arial",sans-serif;color:white'>Observaciones</span></b></p>
                </td>
            </tr>

            @foreach($tool->indicators as $indicator)
                <tr style='height:35.9pt'>
                    <td width="16%" valign=top style='width:16.0%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                        <p class=Default style='text-align:justify'><span
                                    style='font-size:8.0pt'>{{$indicator->title}}</span></p>
                    </td>
                    <td width="17%" valign=top style='width:17.1%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                        <p class=Default style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Calibri",sans-serif;color:windowtext'>{{$indicator->detail4}}</span></p>
                    </td>
                    <td width="22%" valign=top style='width:22.52%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                        <p class=Default style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Calibri",sans-serif;color:windowtext'>{{$indicator->detail3}}</span></p>
                    </td>
                    @if($tool->kind != 'list')
                        <td width="17%" valign=top style='width:17.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                            <p class=Default style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Calibri",sans-serif;color:windowtext'>{{$indicator->detail2}}</span></p>
                        </td>
                        <td width="17%" valign=top style='width:17.7%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                            <p class=Default style='text-align:justify'><span lang=ES style='font-size:
  9.0pt;font-family:"Calibri",sans-serif;color:windowtext'>{{$indicator->detail1}}</span></p>
                        </td>

                    @endif
                    <td width="8%" valign=top style='width:8.96%;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:35.9pt'>
                        <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=ES style='font-size:9.0pt'>&nbsp;</span></p>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

    <p class=MsoNormal><u><span lang=ES><span style='text-decoration:none'>&nbsp;</span></span></u></p>

    <p class=MsoNormal><span lang=ES>
            @if($tool->notes)
                Notas: {{$tool->notes}}
                @endif
        </span></p>

    <p class=MsoNormal><span lang=ES>&nbsp;</span></p>

    <p class=MsoNormal><span lang=ES>&nbsp;</span></p>

    <p class=MsoNormal><span lang=ES>&nbsp;</span></p>

</div>

</body>

</html>
