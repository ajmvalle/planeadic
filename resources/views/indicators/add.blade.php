@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Instrumentos de evaluación</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nuevo indicador para <strong>{{$tool->title}}</strong></h4>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'indicators.store','enctype'=>'multipart/form-data']) !!}

                        {{Form::hidden('tool_id',$tool->id)}}


                        <div id="collapseGen" class="panel-collapse ">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 {{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label>Título</label>
                                        <input type="text" name="title" class="form-control border-input"
                                               value="{{old('title')}}"
                                               placeholder="Describa aquí que aspecto del producto (o competencia) se evalúa">
                                    </div>

                                </div>

                                @if($tool->kind =='rubric')
                                    <div class="row">

                                        <div class="col-md-3 {{ $errors->has('detail4') ? ' has-error' : '' }}">
                                            <label>{{$tool->header4}}</label>
                                            <textarea rows="3" name="detail4" class="form-control border-input"
                                                      placeholder="Describa aquí que aspecto del producto (o competencia) se evalúa">{{old('detail4')}}</textarea>
                                        </div>

                                        <div class="col-md-3 {{ $errors->has('detail3') ? ' has-error' : '' }}">
                                            <label>{{$tool->header3}}</label>
                                            <textarea rows="3" name="detail3" class="form-control border-input"
                                                      placeholder="Describa aquí que aspecto del producto (o competencia) se evalúa">{{old('detail3')}}</textarea>
                                        </div>

                                        <div class="col-md-3 {{ $errors->has('detail2') ? ' has-error' : '' }}">
                                            <label>{{$tool->header2}}</label>
                                            <textarea rows="3" name="detail2" class="form-control border-input"
                                                      placeholder="Describa aquí que aspecto del producto (o competencia) se evalúa">{{old('detail2')}}</textarea>
                                        </div>

                                        <div class="col-md-3 {{ $errors->has('detail1') ? ' has-error' : '' }}">
                                            <label>{{$tool->header1}}</label>
                                            <textarea rows="3" name="detail1" class="form-control border-input"
                                                      placeholder="Describa aquí que aspecto del producto (o competencia) se evalúa">{{old('detail1')}}</textarea>
                                        </div>

                                    </div>
                                @endif


                                <div class="row">
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                                        <button type="button"
                                                onclick="location.href='{{ route('tools.edit',$tool->id) }}'"
                                                class="btn btn-success btn-fill btn-wd">Cancelar
                                        </button>
                                    </div>

                                </div>

                            </div>

                        </div>
                        {!! Form::close() !!}

                    </div>


                </div>
            </div>
        </div>


    </div>


@endsection
