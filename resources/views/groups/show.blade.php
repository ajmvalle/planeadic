@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{$group->url}}">{{$group->title}}</a>

@endsection

@section('content')

    <div class="col-md-8">
        <div class="card card-plain">
            <div class="card-content">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                            <li class="active"><a href="#assigments" data-toggle="tab">Portafolios</a>
                            </li>
                            <li><a href="#members"
                                   data-toggle="tab">Miembros</a></li>
                        </ul>
                    </div>
                </div>
                <div id="my-tab-content" class="tab-content ">
                    <div class="tab-pane active" id="assigments">
                        <div class="card">
                            @include('groups.assigments')
                        </div>
                    </div>
                    <div class="tab-pane" id="members">
                        <div class="card">
                            @include('groups.members')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-plain">
            <div class="card-content">
                @include('groups.chat')
            </div>
        </div>
    </div>

@endsection
