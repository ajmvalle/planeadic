@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-header">

    @can('create',$group)
        <div class="col-md-12 ">
            <button onclick="location.href='{{ route('groups.addassigment',['group_id'=>$group->id]) }}'" type="button"
                    title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                        class="fa fa-plus"></i>Agregar Portafolio de Grupo
            </button>
        </div>
    @endcan
</div>

<div class="card-content table-responsive ">
    <table class="table table-striped">
        <thead>
        <th>Nombre</th>
        <th>Programa</th>
        <th>Avance</th>
        <th>Opciones</th>
        </thead>
        <tbody>
        @foreach($assigments as $assigment)
            <tr>
                <td>{{$assigment->name}}</td>
                <td>{{empty($assigment->program_id) ? "" : $assigment->program->name}}</td>
                <td>{{$assigment->advance}}%</td>
                <td>
                    <div class="row">
                        <div class="col-md-3">
                            <a onclick="location.href='{{ $assigment->url }}'"
                               title="Planificar" type="button"
                               class="btn btn-sm btn-primary btn-icon"><i
                                        class="fa fa-calendar"></i></a>
                        </div>
                        <div class="col-md-3">
                            <form method="POST"
                                  action="{{route('assigments.copy',['id'=>$assigment->id])}}">
                                {!! Form::token() !!} {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <button onclick="return confirm('¿Desea importar una copia de este portafolio?')"
                                        title="Importar"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa ti-import"></i>
                                </button>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <a onclick="location.href='{{ route('groups.editassigments',['id'=>$assigment->id]) }}'"
                               title="Editar" type="button"
                               class="btn btn-sm btn-warning btn-icon"><i
                                        class="fa fa-edit"></i></a>
                        </div>
                        <div class="col-md-3">
                            @include('buttons.delete',['route'=>'assigments.delete','id'=>$assigment->id])
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$assigments->render()}}
</div>

