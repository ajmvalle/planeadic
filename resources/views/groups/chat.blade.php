<div class="row">
    <div class="col-md-12">
        <div class="card">

            {!! Form::open(['method'=> 'POST','route'=>'groups.chat']) !!}

            {{Form::hidden('group_id',$group->id)}}
            <div class="card-content">
                <div class="form-group col-md-12 {{ $errors->has('comment') ? ' has-error' : '' }}">
                    <label class="control-label">Agrega notas relevantes al grupo aquí:</label>
                    <textarea name="comment" class="form-control border-input"
                              placeholder="Ingrese un comentario para los miembros del grupo"
                              rows="3">{{old('comment')}}</textarea>
                </div>
                <div class="form-group col-md-12">
                    <div class="text-center media-middle">
                        <button type="submit" class="btn btn-info btn-fill">Agregar</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>  <!-- end card -->

</div>
<div class="row">

    <div class="card card-timeline card-plain">
        <div class="card-content">
            <ul class="timeline">

                @foreach($comments as $i => $comment)

                    <li class="{{$i%2==0?"timeline-inverted":""}}">

                        <div class="timeline-badge">
                            <img src="{{$comment->user->imagen}}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;">
                        </div>

                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <span class="label label-danger">{{$comment->user->name}}</span>
                            </div>
                            <div class="timeline-body">
                                <p>{{$comment->comment}}</p>
                            </div>
                            <h6>
                                <i class="ti-time"></i>
                                {{$comment->created_at}}
                            </h6>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

</div>