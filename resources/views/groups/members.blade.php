<div class="card-content table-responsive ">
    <table class="table table-striped">
        <thead>
        <th>Edita</th>
        <th>Nombre</th>
        <th>Escuela</th>
        <th>Opciones</th>
        </thead>
        <tbody>
        @foreach($members as $member)
            <tr>


                <td>
                    <input type="checkbox" class="switch" disabled {{$member->pivot->edit?"checked":""}}>
                </td>
                <td>
                    <div class="img-container">
                        <img src="{{$member->imagen}}"
                             style="width:35px; height:35px; float:left; border-radius:50%; margin-right:25px;">

                    </div>
                    {{$member->fullname}}
                </td>


                @if($member->school!=null)
                    <td>{{$member->school->shortname}}</td>
                @endif

                <td>
                    @can('update',$group)
                        @if($member->id==auth()->user()->id)
                            <a>Administrador</a>

                        @else
                            <form method="POST"
                                  action="{{route('groups.exchange')}}">
                                {{ csrf_field() }}
                                {!! Form::hidden('group_id',$group->id) !!}
                                {!! Form::hidden('member_id',$member->id) !!}
                                {!! Form::hidden('edit',$member->pivot->edit) !!}

                                <button onclick="return confirm('Cambiará Permiso del usuario')"
                                        title="Cambiar Permiso" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-exchange"></i>
                                </button>
                            </form>
                        @endif
                    @endcan
                </td>


            </tr>
        @endforeach
        </tbody>
    </table>
    {{$members->render()}}
</div>
