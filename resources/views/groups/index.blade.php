@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('groups.index')}}">Grupos colegiados de trabajo</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6">
                            <h4 class="card-title">Trabajo Colaborativo</h4>
                            <p class="category">¡Colabora y comparte en la comunidad planeadic!</p>
                        </div>

                        <div class="col-md-3 ">
                            <button onclick="location.href='{{ route('groups.search') }}'" type="button"
                                    title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                        class="fa fa-search"></i>Buscar Grupos
                            </button>
                        </div>

                        <div class="col-md-3 ">
                            <button onclick="location.href='{{ route('groups.add') }}'" type="button"
                                    title="Agregar" class="btn btn-sm btn-primary btn-icon"><i
                                        class="fa fa-plus"></i>Crear Grupo
                            </button>
                        </div>

                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>Título</th>
                            <th>Creador</th>
                            <th>Miembros</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td>{{$group->title}}</td>
                                    <td>
                                        <div class="img-container">
                                            <img src="{{$group->user->imagen}}"
                                                 style="width:30px; height:30px; float:left; border-radius:50%; margin-right:25px;">
                                            {{$group->user->fullname}}
                                        </div>
                                    </td>
                                    <td>{{count($group->users)}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a onclick="location.href='{{ $group->url }}'"
                                                   title="Visitar" type="button"
                                                   class="btn btn-sm btn-primary btn-icon"><i
                                                            class="fa fa-external-link"></i></a>
                                            </div>
                                            @can('update',$group)
                                                <div class="col-md-3">
                                                    <a onclick="location.href='{{ route('groups.edit',$group->id) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                            @endcan
                                            @can('delete',$group)
                                                <div class="col-md-3">
                                                    @include('buttons.delete',['route'=>'groups.delete','id'=>$group->id])
                                                </div>
                                                @else
                                                <div class="col-md-3">
                                                    <form method="POST"
                                                          action="{{route('groups.quit',['id'=>$group->id])}}">
                                                        {!! Form::token() !!} {{ csrf_field() }}
                                                        <button onclick="return confirm('¿Desea salir del grupo?')"
                                                                title="Salir" type="submit"
                                                                class="btn btn-sm btn-danger btn-icon"><i
                                                                    class="fa fa-chain-broken"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$groups->render()}}
                    </div>
                </div>
            </div>

        </div>
    </div>




@endsection
