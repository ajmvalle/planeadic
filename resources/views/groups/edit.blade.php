@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('groups.edit',$group->id)}}">Modificación de título</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Grupos colaborativos de trabajo colegiado</h4>
                        <p class="category">Editar un grupo, invita a tus compaleros a colaborar contigo</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'groups.update']) !!}
                            {!! Form::hidden('id',$group->id) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input" value="{{ $group->title }}"
                                           placeholder="Nombre del Grupo, sugerimos agregar escuela o estado. Ejemplo: Grupo colegiado Juan de la Luz Enríquez">
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                            <button type="button" onclick="location.href='{{ route('groups.index') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
