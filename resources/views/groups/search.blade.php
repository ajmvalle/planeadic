@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('groups.index')}}">Grupos colegiados de trabajo</a>

@endsection

@section('content')


    <nav class="navbar navbar-default">
        <div class="container-fluid">


            <div class="collapse navbar-collapse">
                {!! Form::open(['method'=> 'POST','route'=>'groups.locate','role'=>'search','class'=>'navbar-form navbar-left navbar-search-form']) !!}
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" value="{{$text}}" name="text" class="form-control" placeholder="Busque aquí...">
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Grupos disponibles</h4>

                        </div>
                        <div class="card-content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Grupo</th>
                                <th>Creador</th>
                                <th>Fecha</th>
                                <th>Miembros</th>
                                <th>Agregar</th>
                                </thead>
                                <tbody>
                                @foreach($groups as $group)
                                    <tr>
                                        <td>{{$group->title}}</td>
                                        <td>
                                            <img src="{{$group->user->imagen}}" style="width:35px; height:35px; float:left; border-radius:50%; margin-right:25px;">
                                            {{$group->user->fullname}}
                                        </td>
                                        <td>{{$group->created_at}}</td>
                                        <td>{{count($group->users)}}</td>
                                        <td>
                                            <div class="col-md-3">
                                                <form method="POST"
                                                      action="{{route('groups.subscribe')}}">
                                                    {!! Form::token() !!} {{ csrf_field() }}{!! Form::hidden('id',$group->id) !!}
                                                    <button onclick="return confirm('¿Desea añadirse a este grupo?')"
                                                            href="{{route('groups.subscribe')}}"
                                                            title="Ingresar" type="submit"
                                                            class="btn btn-sm btn-primary btn-icon"><i
                                                                class="fa fa-chain"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$groups->render()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
