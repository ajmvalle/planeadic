@extends('layouts.app')

@section('title')
    @if(!$activity->plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$activity->plan->assigment->url}}">
            {{$activity->plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$activity->plan->assigment->group->url}}">
            {{$activity->plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$activity->plan->assigment->url}}">
            {{$activity->plan->assigment->name}}
        </a>
    @endif

    <a class="navbar-brand">/</a>

    <a class="navbar-brand" href="{{route('plans.activities',$activity->plan)}}">
        Actividades
    </a>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nueva Actividad</h4>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {!! Form::open(['method'=> 'POST','route'=>'activities.update','enctype'=>'multipart/form-data']) !!}
                        {{Form::hidden('id',$activity->id)}}
                        {{Form::hidden('plan_id',$activity->plan_id)}}
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
                                    <label>Número</label>
                                    <input type="text" name="order" class="form-control border-input"
                                           placeholder="Orden de la Actividad en secuencia"
                                           value="{{$activity->order}}">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Fase de la actividad</label>
                                    <select name="section" class="form-control border-input">
                                        <option value="">Seleccione</option>
                                        <option value="start" {{$activity->section == 'start'? ("selected='selected'"): ("") }} >
                                            Apertura
                                        </option>
                                        <option value="dev" {{$activity->section == 'dev'? ("selected='selected'"): ("") }}>
                                            Desarrollo
                                        </option>
                                        <option value="close" {{$activity->section == 'close'? ("selected='selected'"): ("") }}>
                                            Cierre
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <div class="checkbox">
                                    <input id="checkbox1" name="bextra"
                                           type="checkbox" {{$activity->extra? "checked": ""}}>
                                    <label for="checkbox1">
                                        Reforzamiento
                                    </label>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::Label('attribute_id', 'Atributo:') !!}
                                {!! Form::select('attribute_id', $attributes, $activity->attribute_id, ['class' => 'form-control border-input']) !!}
                            </div>
                        </div>

                        <method
                                :pteacher="{{json_encode($activity->teacher)}}"
                                :pstudent="{{json_encode($activity->student)}}"
                                :pproduct="{{json_encode($activity->product)}}"
                                :presources="{{json_encode($activity->resources)}}"
                                :peva_tool="{{json_encode($activity->eva_tool)}}"
                                :peva_percent="{{json_encode($activity->eva_percent)}}"
                                :peva_kind="{{json_encode($activity->eva_kind)}}"
                                :ptime="{{json_encode($activity->time)}}"
                                :ptime_student="{{json_encode($activity->time_student)}}"
                                :puser="{{json_encode(auth()->user()->id)}}"></method>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('plans.activities',$activity->plan) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>

                        {{Form::close()}}


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
