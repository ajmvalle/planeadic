@extends('layouts.app')

@section('title')
    @if(!$plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$plan->assigment->group->url}}">
            {{$plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>
    @endif

    <a class="navbar-brand">/</a>

    <a class="navbar-brand" href="{{route('plans.activities',$plan)}}">
        Actividades
    </a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nueva Actividad</h4>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        {!! Form::open(['method'=> 'POST','route'=>'activities.store','enctype'=>'multipart/form-data']) !!}
                        {{Form::hidden('plan_id',$plan->id)}}
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
                                    <label>Número</label>
                                    <input type="text" name="order" class="form-control border-input"
                                           placeholder="Orden de la Actividad en secuencia"
                                           value="{{count($plan->activities)+1}}">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Fase de la actividad</label>
                                    <select name="section" class="form-control border-input">
                                        <option value="" {{$section==''?"selected":""}}>Seleccione</option>
                                        <option value="start" {{$section=='start'?"selected":""}}>Apertura
                                        </option>
                                        <option value="dev" {{$section=='dev'?"selected":""}}>Desarrollo</option>
                                        <option value="close" {{$section=='close'?"selected":""}}>Cierre</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <br>
                                <div class="checkbox">
                                    <input id="checkbox1" name="bextra"
                                           type="checkbox" {{$plan->block->program->nme?'':'disabled'}} >
                                    <label for="checkbox1">
                                        Reforzamiento
                                    </label>
                                </div>

                            </div>

                        </div>

                        @if(!$plan->block->program->nme)
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::Label('attribute_id', 'Atributo:') !!}
                                    {!! Form::select('attribute_id', $attributes, old('attribute_id'), ['class' => 'form-control border-input ']) !!}
                                </div>
                            </div>
                        @endif

                        <method
                                :pteacher="{{json_encode(old('teacher'))}}"
                                :pstudent="{{json_encode(old('student'))}}"
                                :pproduct="{{json_encode(old('product'))}}"
                                :presources="{{json_encode($plan->material)}}"
                                :peva_tool="{{json_encode(old('eva_tool'))}}"
                                :peva_percent="{{json_encode(old('eva_percent'))}}"
                                :peva_kind="{{json_encode(old('eva_kind'))}}"
                                :ptime="{{json_encode(old('time'))}}"
                                :ptime_student="{{json_encode(old('time_student'))}}"
                                :puser="{{json_encode(auth()->user()->id)}}"></method>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button"
                                    onclick="location.href='{{  route('plans.activities',$plan) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>

                        {{Form::close()}}


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
