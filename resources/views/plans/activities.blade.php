@extends('layouts.app')

@section('title')
    @if(!$plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$plan->assigment->group->url}}">
            {{$plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>
    @endif

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="card-title">Avance del portafolio </h4>
                            </div>

                            <div class="col-md-8">
                                <p class="category"><strong>No. Planes: {{count($plan->assigment->plans)}} Horas
                                        Planeadas {{$plan->assigment->hours}}
                                        de {{$plan->assigment->program->hours}} ({{$plan->assigment->advance}}
                                        %)</strong></p>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="{{$plan->assigment->advance}}" aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width: {{$plan->assigment->advance}}%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card">
                    <div class="panel panel-border panel-default">
                        <a data-toggle="collapse" href="#collapseInfo">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Objetivo
                                    <i class="ti-angle-down"></i>
                                </h4>
                            </div>
                        </a>
                        <div id="collapseInfo" class="panel-collapse">
                            <div class="panel-body">

                                @if($plan->assigment->program->nme)
                                    <p><strong>Contenido Central: </strong>{{$plan->block->title}}</p>
                                    <p><strong>Aprendizajes Esperados: </strong>{{$plan->block->expecteds}}</p>
                                @else
                                    <p><strong>Competencia Profesional: </strong>{{$plan->block->competence}}</p>
                                @endif

                            </div>
                        </div>
                    </div>

                </div>
                <div class="card">

                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-4">
                                <h5 class="title"><strong>{{$plan->title}} (Horas:{{$plan->hours}}) </strong>
                                </h5>

                            </div>
                            <div class="col-md-4">
                                <h5 class="title"><strong>Evaluación: {{$plan->totalpercent}}% </strong>
                                </h5>

                            </div>
                            <div class="col-md-4">
                                <button onclick="location.href='{{ route('assigments.dashboard',$plan->assigment->id) }}'"
                                        type="button"
                                        title="Regresar a Portafolio" class="btn btn-sm btn-primary btn-icon"><i
                                            class="fa fa-calendar"></i>
                                </button>

                                @if(!$plan->assigment->isGrupal)
                                    <button onclick="location.href='{{ route('plans.print',$plan) }}'"
                                            type="button"
                                            title="Imprimir" class="btn btn-sm btn-warning btn-icon"><i
                                                class="fa fa-print"></i>
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-content">

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseStart">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Apertura (Actividades: {{$plan->startCount}}, Horas:{{$plan->startTime}} ,
                                        Evaluación:{{$plan->startEva}}% )

                                        <button onclick="location.href='{{ route('activities.add',['plan'=>$plan,'section'=>'start']) }}'"
                                                type="button"
                                                title="Agregar Actividad de Apertura" class="btn btn-sm btn-success btn-icon"><i
                                                    class="fa fa-plus"></i>Agregar
                                        </button>
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseStart" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Momento para que el estudiante relacione sus experiencias con los contenidos
                                        centrales y específicos a desarrollar(o bien competencia profesional), se
                                        interese en ellos, genere expectativas
                                        y experimente el deseo de aprender</p>
                                    <div class="row">

                                        @if(count($plan->startActivities)>0 ||count($plan->extraStartActivities)>0)

                                            <div class="card-content table-responsive table-full-width">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <th>Producto</th>
                                                    <th>Tiempo</th>
                                                    <th>Instrumento</th>
                                                    <th>Porcentaje</th>
                                                    <th>Opciones</th>
                                                    </thead>
                                                    <tbody>
                                                    @each('plans.details',$plan->startActivities,'act')
                                                    @each('plans.details',$plan->extraStartActivities,'act')
                                                    </tbody>
                                                </table>
                                            </div>

                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseDev">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Desarrollo (Actividades: {{$plan->devCount}}, Horas:{{$plan->devTime}} ,
                                        Evaluación:{{$plan->devEva}}% )
                                        <button onclick="location.href='{{ route('activities.add',['plan'=>$plan,'section'=>'dev']) }}'"
                                                type="button"
                                                title="Agregar Actividad" class="btn btn-sm btn-success btn-icon"><i
                                                    class="fa fa-plus"></i>Agregar
                                        </button>
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseDev" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Momento para que el estudiante acceda a contenidos científicos, tecnológicos o
                                        humanísticos, y los contraste con los saberes que tenía y
                                        recuperó e identificó en la apertura.
                                        Mediante esa contrastación, modifica a los saberes, los enriquece, sustituye o
                                        bien incorpora
                                        nuevos conocimientos. El desarrollo propicia que el estudiante sistematice y
                                        argumente sus saberes además que los ejercite o experimente y que transfiera su
                                        aprendizaje a situaciones distintas bajo condiciones de actuación
                                        predeterminadas. Recomendable seguir la Taxonomía de Marzano y Kendall.
                                    </p>
                                    <div class="row">
                                        @if(count($plan->devActivities)>0 ||count($plan->extraDevActivities)>0)
                                            <div class="card-content table-responsive table-full-width">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <th>Producto</th>
                                                    <th>Tiempo</th>
                                                    <th>Instrumento</th>
                                                    <th>Porcentaje</th>
                                                    <th>Opciones</th>
                                                    </thead>
                                                    <tbody>
                                                    @each('plans.details',$plan->devActivities,'act')
                                                    @each('plans.details',$plan->extraDevActivities,'act')
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseClose">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Cierre (Actividades: {{$plan->closeCount}}, Horas:{{$plan->closeTime}} ,
                                        Evaluación:{{$plan->closeEva}}% )
                                        <button onclick="location.href='{{ route('activities.add',['plan'=>$plan,'section'=>'close']) }}'"
                                                type="button"
                                                title="Agregar Actividad" class="btn btn-sm btn-success btn-icon"><i
                                                    class="fa fa-plus"></i>Agregar
                                        </button>
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseClose" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Se busca que el estudiante identifique los contenidos que aprendió en el
                                        desarrollo, identificar los factores que promovieron u obstaculizaron el avance
                                        haciendo una síntesis o reflexión de sus aprendizajes.
                                    </p>
                                    <div class="row">
                                        <div class="card-content table-responsive table-full-width">
                                            @if(count($plan->closeActivities)>0 || count($plan->extraCloseActivities)>0)
                                                <table class="table table-striped">
                                                    <thead>
                                                    <th>Producto</th>
                                                    <th>Tiempo</th>
                                                    <th>Instrumento</th>
                                                    <th>Porcentaje</th>
                                                    <th>Opciones</th>
                                                    </thead>
                                                    <tbody>
                                                    @each('plans.details',$plan->closeActivities,'act')
                                                    @each('plans.details',$plan->extraCloseActivities,'act')
                                                    </tbody>
                                                </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
