<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="name" class="form-control border-input"
                   value="{{$assigment->name}}" disabled
                   placeholder="Nombre para identificar la asignación de su materia">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Ciclo o Periodo de Aplicación</label>
            <input type="text" name="cycle" class="form-control border-input"
                   value="{{$assigment->cycle}}" disabled
                   placeholder="Ciclo o periodo de aplicación">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Facilitador</label>
            <input type="text" class="form-control border-input"
                   value="{{$assigment->userfullname}}" disabled
                   placeholder="Nombre para identificar la asignación de su materia">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Escuela</label>
            <input type="text" name="cycle" class="form-control border-input"
                   value="{{$assigment->school->name.' '.$assigment->school->number.' ('.$assigment->school->officialkey.')'}}" disabled
                   placeholder="Ciclo o periodo de aplicación">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Asignatura</label>
            <input type="text" class="form-control border-input"
                   value="{{$assigment->program->course->name.'-'.$assigment->program->name}}" disabled
                   placeholder="Nombre para identificar la asignación de su materia">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label>Campo Disciplinar</label>
            <input type="text" class="form-control border-input"
                   value="{{$assigment->program->area}}" disabled
                   placeholder="Nombre para identificar la asignación de su materia">
        </div>
    </div>
</div>
