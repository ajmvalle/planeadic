@extends('layouts.app')

@section('title')
    @if(!$plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$plan->assigment->group->url}}">
            {{$plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>
    @endif

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'plans.update']) !!}
                        {{Form::hidden('id',$plan->id)}}
                        {{Form::hidden('assigment_id',$plan->assigment->id)}}

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseGen">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Información General
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseGen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>Número</label>
                                                <input type="text" name="order" class="form-control border-input"
                                                       placeholder="Enumeración del Plan"
                                                       value="{{$plan->order}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input type="text" name="title" class="form-control border-input"
                                                       placeholder="Ejemplo: Parcial I Bloque 2"
                                                       value="{{$plan->title}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Parcial</label>
                                                <select name="partial" class="form-control border-input">
                                                    <option value="" {{$plan->partial==''?"selected":""}}>
                                                        Seleccione
                                                    </option>
                                                    <option {{$plan->partial=='1'? 'selected': ''}} value="1">
                                                        Parcial I
                                                    </option>
                                                    <option {{$plan->partial=='2'? 'selected': ''}} value="2">
                                                        Parcial II
                                                    </option>
                                                    <option {{$plan->partial=='3'? 'selected': ''}} value="3">
                                                        Parcial III
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bloque</label>
                                                {!! Form::select('block_id', $blocks, $plan->block->id, ['class' => 'form-control border-input']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Plantel</label>
                                                <input type="text" name="acronym" disabled
                                                       class="form-control border-input"
                                                       placeholder="Abreviatura"
                                                       value="{{$plan->assigment->school->name.' '.$plan->assigment->school->number}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Elaboración</label>
                                                <input type="date" name="elaboration"
                                                       value="{{ \Carbon\Carbon::createFromDate($plan->elaboration->year,$plan->elaboration->month,$plan->elaboration->day)->format('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Elaboración">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Aplica Desde</label>
                                                <input type="date" name="apply_from"
                                                       value="{{ \Carbon\Carbon::createFromDate($plan->apply_from->year,$plan->apply_from->month,$plan->apply_from->day)->format('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Desde">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Aplica Hasta</label>
                                                <input type="date" name="apply_to"
                                                       value="{{ \Carbon\Carbon::createFromDate($plan->apply_to->year,$plan->apply_to->month,$plan->apply_to->day)->format('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Hasta">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Profesor</label>
                                                <input type="text" name="teacher" class="form-control border-input"
                                                       placeholder="Nombre completo de la escuela" disabled
                                                       value="{{$plan->assigment->userfullname}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Periodo</label>
                                                <input type="text" name="cycle" class="form-control border-input"
                                                       placeholder="Nombre completo de la escuela" disabled
                                                       value="{{$plan->assigment->cycle}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Horas</label>
                                                <input type="text" name="hours" class="form-control border-input"
                                                       placeholder="Horas" disabled
                                                       value="{{$plan->hours}}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Eje</label>
                                                <textarea type="text" name="axis"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias "
                                                          disabled
                                                          rows="2">{{ $plan->block->centralcontent!=null?$plan->block->centralcontent->axis:"N/A" }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Componente</label>
                                                <textarea type="text" name="component"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          disabled
                                                          rows="2">{{$plan->block->centralcontent!=null?$plan->block->centralcontent->component:"N/A"}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contenido Central</label>
                                                <textarea type="text" name="centralcontent"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias "
                                                          disabled
                                                          rows="2">{{$plan->block->centralcontent!=null?$plan->block->centralcontent->title:"N/A"}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contenidos Específicos</label>
                                                <textarea type="text" name="specifics"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          rows="3">{{$plan->specifics}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Propósito de la planeación didáctica por Campo
                                                    Disciplinar</label>
                                                <textarea type="text" name="purpose_area"
                                                          class="form-control border-input"
                                                          rows="3">{{$plan->purpose_area}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Propósito de la planeación didáctica por asignatura</label>
                                                <textarea type="text" name="purpose"
                                                          class="form-control border-input"
                                                          rows="3">{{$plan->purpose}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Aprendizajes Esperados</label>
                                                <textarea type="text" name="expecteds"
                                                          class="form-control border-input"
                                                          rows="3">{{$plan->expecteds}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Proceso de Aprendizaje</label>
                                                <textarea type="text" name="process"
                                                          class="form-control border-input"
                                                          rows="3">{{$plan->process}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Productos Esperados</label>
                                                <textarea type="text" name="products"
                                                          class="form-control border-input"
                                                          rows="3">{{$plan->products}}</textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Habilidades Socioemocionales</label>
                                                <select name="hse" class="form-control border-input">
                                                    <option value="" {{!$plan->hse ? 'selected="selected"': "" }}>
                                                        Seleccione
                                                    </option>
                                                    <option value="1" {{$plan->hse==1 ? 'selected="selected"': "" }}>
                                                        Conoce T - Autoconocimiento
                                                    </option>
                                                    <option value="2" {{$plan->hse==2 ? 'selected="selected"': "" }}>
                                                        Conoce T - Autoregulación
                                                    </option>
                                                    <option value="3" {{$plan->hse==3 ? 'selected="selected"': "" }}>
                                                        Relaciona T - Conciencia Social
                                                    </option>
                                                    <option value="4" {{$plan->hse==4 ? 'selected="selected"': "" }}>
                                                        Relaciona T - Colaboración
                                                    </option>
                                                    <option value="5" {{$plan->hse==5 ? 'selected="selected"': "" }}>
                                                        Elige T - Toma responsable de Decisiones
                                                    </option>
                                                    <option value="6" {{$plan->hse==6 ? 'selected="selected"': "" }}>
                                                        Elige T - Perseverancia
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <ambit :pprofile_ambits="{{json_encode($plan->profile_ambits)}}"></ambit>


                                </div>
                            </div>
                        </div>


                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseTran">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Transversalidad
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseTran" class="panel-collapse collapse">
                                <div class="panel-body">

                                    @if(!$plan->assigment->program->nme)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('integrator') ? ' has-error' : '' }}">
                                                    <label>Tema Integrador</label>
                                                    <input type="text" name="integrator"
                                                           class="form-control border-input"
                                                           placeholder="Tema que servirá de eje para la transversalidad"
                                                           value="{{$plan->integrator}}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('trans_integrator') ? ' has-error' : '' }}">
                                                    <label>Transversalidad Tema Integrador</label>
                                                    <input type="text" name="trans_integrator"
                                                           class="form-control border-input"
                                                           placeholder="Asignaturas y/o módulos que se relacionan al tema integrador"
                                                           value="{{$plan->trans_integrator}}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('trans_course') ? ' has-error' : '' }}">
                                                    <label>Transversalidad de Asignatura</label>
                                                    <input type="text" name="trans_course"
                                                           class="form-control border-input"
                                                           placeholder="Asignaturas y/o módulos que se relacionan a la asignatura"
                                                           value="{{$plan->trans_course}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox1" name="isspace"
                                                           type="checkbox" {{$plan->space? "checked":""}} >
                                                    <label for="checkbox1">
                                                        Espacio
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox2" name="isenergy"
                                                           type="checkbox" {{$plan->energy? "checked":""}} >
                                                    <label for="checkbox2">
                                                        Energía
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox3" name="isdiversity"
                                                           type="checkbox" {{$plan->diversity? "checked":""}} >
                                                    <label for="checkbox3">
                                                        Diversidad
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox4" name="isthetime"
                                                           type="checkbox" {{$plan->thetime? "checked":""}} >
                                                    <label for="checkbox4">
                                                        Tiempo
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox5" name="ismatter"
                                                           type="checkbox" {{$plan->space? "checked":""}} >
                                                    <label for="checkbox5">
                                                        Materia
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('transversality') ? ' has-error' : '' }}">
                                                <label>Transversalidad con otras asignaturas</label>
                                                <textarea type="text" name="transversality"
                                                          class="form-control border-input"
                                                          placeholder="Ejemplo: Geometría Analítica, pendiente o cambio en una función, etc.">{{$plan->transversality}}</textarea>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>


                        @if(!$plan->assigment->program->nme)

                            <div class="panel panel-border panel-default">
                                <a data-toggle="collapse" href="#collapseProf">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            Información Adicional
                                            <i class="ti-angle-down"></i>
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapseProf" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('factual') ? ' has-error' : '' }}">
                                                    <label>Contenidos Fácticos</label>
                                                    <textarea name="factual" class="form-control border-input"
                                                              placeholder="Contenidos fácticos de la asignatura o submódulo">{{$plan->factual}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('fundamental') ? ' has-error' : '' }}">
                                                    <label>Concepto Fundamental</label>
                                                    <textarea name="fundamental" class="form-control border-input"
                                                              placeholder="Concepto fundamental de la asignatura/submódulo">{{$plan->fundamental}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('subsidiary') ? ' has-error' : '' }}">
                                                    <label>Conceptos Subsidiarios</label>
                                                    <textarea name="subsidiary" class="form-control border-input"
                                                              placeholder="Contenidos subsidiario de la asignatura o submódulo">{{$plan->subsidiary}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('procedimental') ? ' has-error' : '' }}">
                                                    <label>Contenidos Procedimentales</label>
                                                    <textarea name="procedimental" class="form-control border-input"
                                                              placeholder="Contenidos procedimentales de la asignatura o submódulo">{{$plan->procedimental}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('attitudinal') ? ' has-error' : '' }}">
                                                    <label>Contenidos Actitudinales</label>
                                                    <textarea name="attitudinal" class="form-control border-input"
                                                              placeholder="Contenidos actitudinales de la asignatura/submódulo">{{$plan->attitudinal}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('profesionalcontent') ? ' has-error' : '' }}">
                                                    <label>Contenidos en competencias profesionales</label>
                                                    <textarea name="profesionalcontent"
                                                              class="form-control border-input"
                                                              placeholder="Contenidos en competencias profesionales">{{$plan->profesionalcontent}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        @endif

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseSrc">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Referencias
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseSrc" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea type="text" name="references"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias bibliográficas"
                                                          rows="5">{{$plan->references}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea type="text" name="extrareferences"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí referencias adicionales"
                                                          rows="5">{{$plan->extrareferences}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea type="text" name="collaborations"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí Colaboraciones de sus compañeros docentes"
                                                          rows="5">{{$plan->collaborations}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseResources">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Recursos(Detalle)
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseResources" class="panel-collapse collapse">
                                <div class="panel-body">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Equipo</label>
                                                <textarea type="text" name="equipment"
                                                          class="form-control border-input"
                                                          placeholder="Describa equipo computacional,  mecánico, eléctrico, etc.">{{$plan->equipment}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!$plan->assigment->program->nme)

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Equipo de Bioseguridad</label>
                                                    <textarea type="text" name="biosecurity"
                                                              class="form-control border-input"
                                                              placeholder="Describa si va a utilizar equipo de bioseguridad y sus características">{{$plan->biosecurity}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Reactivos</label>
                                                    <textarea type="text" name="reactives"
                                                              class="form-control border-input"
                                                              placeholder="Describa reactivos">{{$plan->reactives}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Material</label>
                                                <textarea type="text" name="material"
                                                          class="form-control border-input"
                                                          placeholder="Describa material para desarrollar la clase">{{$plan->material}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                            <button type="button"
                                    onclick="location.href='{{ route('assigments.dashboard',$plan->assigment->id) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                            <button type="button"
                                    onclick="location.href='{{ route('plans.print',$plan->id) }}'"
                                    class="btn btn-warning btn-fill btn-wd">Imprimir
                            </button>

                            <button type="button"
                                    onclick="location.href='{{ route('plans.activities',$plan) }}'"
                                    class="btn btn-primary btn-fill btn-wd">Actividades
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Atributos de Competencias Genéricas</h4>
                    </div>
                    <div class="card-content">
                        {!! Form::open(['method'=> 'POST','route'=>'plans.addattribute']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">

                                {{Form::hidden('id',$plan->id)}}

                                {!! Form::select('attribute_id', $attributes, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}
                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Número</th>
                                <th>Atributo</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($plan->attributes as $attribute)
                                    <tr>
                                        <td>{{$attribute->competence->code}}</td>
                                        <td>{{$attribute->code}}</td>
                                        <td>{{$attribute->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'plans.removeattribute']) !!}
                                            {{Form::hidden('attribute_id',$attribute->id)}}
                                            {{Form::hidden('id',$plan->id)}}
                                            <button onclick="return confirm('Confirme eliminación del atributo')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>  <!-- end card -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias Disciplinares</h4>
                    </div>
                    <div class="card-content">

                        {!! Form::open(['method'=> 'POST','route'=>'plans.addcompetence']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">


                                {{Form::hidden('id',$plan->id)}}

                                {!! Form::select('competence_id', $dcompetences, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}

                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Competencia</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($plan->discompetences as $competence)
                                    <tr>
                                        <td>{{$competence->code}}</td>
                                        <td>{{$competence->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'plans.removecompetence']) !!}
                                            {{Form::hidden('competence_id',$competence->id)}}
                                            {{Form::hidden('id',$plan->id)}}
                                            <button onclick="return confirm('Confirme eliminación de la competencia')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>  <!-- end card -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Competencias Profesionales</h4>
                    </div>
                    <div class="card-content">

                        {!! Form::open(['method'=> 'POST','route'=>'plans.addcompetence']) !!}
                        <div class="col-md-12">
                            <div class="col-md-9">


                                {{Form::hidden('id',$plan->id)}}

                                {!! Form::select('competence_id', $pcompetences, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                            <div class="col-md-3">
                                <button title="Agregar" type="submit"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>

                            </div>

                        </div>

                        {!! Form::close() !!}

                        <div class="table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Clave</th>
                                <th>Competencia</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($plan->profcompetences as $competence)
                                    <tr>
                                        <td>{{$competence->code}}</td>
                                        <td>{{$competence->title}}</td>
                                        <td>
                                            {!! Form::open(['method'=> 'POST','route'=>'plans.removecompetence']) !!}
                                            {{Form::hidden('competence_id',$competence->id)}}
                                            {{Form::hidden('id',$plan->id)}}
                                            <button onclick="return confirm('Confirme eliminación de la competencia')"
                                                    title="Eliminar" type="submit"
                                                    class="btn btn-sm btn-danger btn-icon"><i
                                                        class="fa fa-eraser"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>  <!-- end card -->
        </div>

    </div>

    </div>


@endsection
