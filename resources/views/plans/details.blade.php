<tr>
    <td>{{$act->product}}</td>
    <td>{{$act->time}}</td>
    <td>{{$act->tool}}</td>
    <td>{{$act->eva_percent}}</td>
    <td>
        <div class="row">
            <div class="col-md-4">
                <a onclick="location.href='{{ route('activities.edit',['id'=>$act->id]) }}'"
                   title="Editar" type="button"
                   class="btn btn-sm btn-warning btn-icon"><i
                            class="fa fa-edit"></i></a>
            </div>
            <div class="col-md-4">
                @include('buttons.delete',['route'=>'activities.delete','id'=>$act->id])
            </div>
        </div>
    </td>
</tr>

