@extends('layouts.app')

@section('title')
    @if(!$plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$plan->assigment->group->url}}">
            {{$plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>
    @endif

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Enviar Planeación <strong>{{$plan->block->program->name}}</strong>
                        </h4>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'plans.confirmsend']) !!}
                        {{Form::hidden('id',$plan->id)}}
                        {{Form::hidden('assigment_id',$plan->assigment->id)}}

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseGen">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Información General
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseGen" class="panel-collapse ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>Número</label>
                                                <input type="text" name="order" class="form-control border-input"
                                                       placeholder="Enumeración" disabled
                                                       value="{{$plan->order}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Título</label>
                                                <input type="text" name="title" class="form-control border-input"
                                                       disabled
                                                       placeholder="Identifica tu Planeación, por ejemplo Parcial I Bloque 2"
                                                       value="{{$plan->title}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Parcial</label>
                                                <select disabled name="partial" class="form-control border-input">
                                                    <option value="" {{$plan->partial==''?"selected":""}}>
                                                        Seleccione
                                                    </option>
                                                    <option {{$plan->partial=='1'? 'selected': ''}} value="1">
                                                        Parcial I
                                                    </option>
                                                    <option {{$plan->partial=='2'? 'selected': ''}} value="2">
                                                        Parcial II
                                                    </option>
                                                    <option {{$plan->partial=='3'? 'selected': ''}} value="3">
                                                        Parcial III
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bloque</label>
                                                <input type="text" name="title" class="form-control border-input"
                                                       placeholder="Bloque" disabled
                                                       value="{{$plan->block->description}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Programa</label>
                                                <input type="text" name="schoolinst" class="form-control border-input"
                                                       placeholder="Nombre completo de la escuela" disabled
                                                       value="{{$plan->assigment->program->name}} ({{$plan->assigment->program->course->name}})">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Actividades</label>
                                                <input type="text" name="acronym" disabled
                                                       class="form-control border-input"
                                                       placeholder="Abreviatura"
                                                       value="{{count($plan->activities)}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Horas</label>
                                                <input type="text" name="hours" class="form-control border-input"
                                                       placeholder="Horas" disabled
                                                       value="{{$plan->hours}} de {{$plan->assigment->program->hours}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <div class="form-group">
                                                <label>Portafolio</label>
                                                {!! Form::select('assigment_id', $assigments, 0, ['class' => 'form-control border-input']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-info btn-fill btn-wd">Enviar Copia
                                            </button>
                                            <button type="button"
                                                    onclick="location.href='{{ route('assigments.dashboard',$plan->assigment->id) }}'"
                                                    class="btn btn-success btn-fill btn-wd">Cancelar
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>


    </div>

    </div>


@endsection
