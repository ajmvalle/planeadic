@extends('layouts.app')

@section('title')
    @if(!$assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$assigment->url}}">
            {{$assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$assigment->group->url}}">
            {{$assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$assigment->url}}">
            {{$assigment->name}}
        </a>
    @endif

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">

                            </div>

                            <div class="col-md-6">
                                <p class="category"><strong>Horas Planeadas {{$assigment->hours}}
                                        de {{$assigment->program->hours}} ({{$assigment->advance}}%)</strong></p>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="{{$assigment->advance}}" aria-valuemin="0" aria-valuemax="100"
                                         style="width: {{$assigment->advance}}%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                    <li><a href="#home" data-toggle="tab"
                                           aria-expanded="false">Identificación</a></li>
                                    <li class="{{$assigment->no_plans()==0?"active":""}}"><a href="#program"
                                                                                             data-toggle="tab"
                                                                                             aria-expanded="false">Programa</a>
                                    </li>
                                    <li class="{{$assigment->no_plans()>0?"active":""}}"><a href="#plans"
                                                                                            data-toggle="tab"
                                                                                            aria-expanded="false">Planeaciones({{$assigment->no_plans()}}
                                            )</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane " id="home">
                                <div class="card-content">
                                    @include('plans.identification', ['assigment' => $assigment])
                                </div>
                            </div>
                            <div class="{{$assigment->no_plans()==0? "tab-pane active":"tab-pane"}}" id="program">
                                @include('plans.program', ['assigment' => $assigment])
                            </div>
                            <div class="{{$assigment->no_plans()>0? "tab-pane active":"tab-pane"}}" id="plans">

                                @include('plans.index', ['assigment' => $assigment])

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection