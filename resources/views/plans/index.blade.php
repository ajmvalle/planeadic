<div class="card">

    <div class="card-content table-responsive table-full-width">
        <table class="table table-striped">
            <thead>
            <th>Título</th>
            <th>Horas</th>
            <th>Parcial</th>
            <th>Bloque</th>
            <th>Fecha</th>
            <th>Opciones</th>
            </thead>
            <tbody>
            @foreach($assigment->plans as $plan)
                <tr>
                    <td>{{$plan->title}}</td>
                    <td>{{$plan->hours}}</td>
                    <td>{{$plan->partial}}</td>
                    <td>{{$plan->block->description}}</td>
                    <td>{{$plan->elaboration->format('d/m/Y')}}</td>
                    <td>
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{route('plans.edit',$plan)}}"
                                   title="Editar" type="button"
                                   class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-pencil"></i></a>
                            </div>

                            <div class="col-md-2">
                                <a href="{{route('plans.activities',$plan)}}"
                                   title="Actividades" type="button"
                                   class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-list"></i></a>
                            </div>

                            @if(!$plan->assigment->isGrupal)
                                <div class="col-md-2">
                                    <a target="_blank"
                                       href="{{route('plans.print',$plan)}}"
                                       title="Imprimir" type="button"
                                       class="btn btn-sm btn-warning btn-icon"><i
                                                class="fa fa-print"></i></a>
                                </div>

                                <div class="col-md-2">
                                    <a href="{{route('evidences.index',$plan)}}"
                                       title="Evidencias" type="button"
                                       class="btn btn-sm btn-success btn-icon"><i
                                                class="fa fa-camera"></i></a>
                                </div>
                            @endif

                            <div class="col-md-2">
                                <a href="{{route('plans.send',$plan)}}"
                                   title="Enviar Copia" type="button"
                                   class="btn btn-sm btn-primary btn-icon"><i
                                            class="fa fa-paper-plane"></i></a>
                            </div>

                            <div class="col-md-2">
                                @include('buttons.delete',['route'=>'plans.delete','id'=>$plan->id])
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


</div>