<div class="card">
    @if($assigment->program->nme)
        <div class="card-content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                <th>Descripción</th>
                <th>Eje</th>
                <th>Contenido</th>
                <th>Opciones</th>
                </thead>
                <tbody>
                @foreach($assigment->program->blocks as $block)
                    <tr>
                        <td>{{$block->description}}</td>
                        <td>{{$block->axis}}</td>
                        <td>{{$block->title}}</td>
                        <td>
                            <div class="row">

                                <div class="col-md-6">
                                    <a onclick="location.href='{{ route('plans.add',['block'=>$block,'assigment'=>$assigment]) }}'"
                                       title="Crear Plan" type="button"
                                       class="btn btn-sm btn-success btn-icon"><i
                                                class="fa fa-plus"></i>Crear Plan</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="card-content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                <th>Descripción</th>
                <th>Competencia</th>
                <th>Situación</th>
                <th>Opciones</th>
                </thead>
                <tbody>
                @foreach($assigment->program->blocks as $block)
                    <tr>
                        <td>{{$block->description}}</td>
                        <td>{{$block->competence}}</td>
                        <td>{{$block->situations}}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-6">
                                    <a  href="{{ $block->url }}"
                                        target="_blank"
                                       title="Ver Detalle" type="button"
                                       class="btn btn-sm btn-success btn-icon"><i
                                                class="fa fa-eye"></i></a>
                                </div>

                                <div class="col-md-6">
                                    <a onclick="location.href='{{ route('plans.add',['block'=>$block,'assigment'=>$assigment]) }}'"
                                       title="Crear Plan" type="button"
                                       class="btn btn-sm btn-success btn-icon"><i
                                                class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>