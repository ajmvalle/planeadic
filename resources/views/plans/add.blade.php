@extends('layouts.app')

@section('title')

    @if(!$assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$assigment->url}}">
            {{$assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$assigment->group->url}}">
            {{$assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$assigment->url}}">
            {{$assigment->name}}
        </a>
    @endif

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'plans.store','enctype'=>'multipart/form-data']) !!}

                        {{Form::hidden('assigment_id',$assigment->id)}}
                        {{Form::hidden('block_id',$block->id)}}

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseGen">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Información General
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseGen" class="panel-collapse ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
                                                <label>Número</label>
                                                <input type="text" name="order" class="form-control border-input"
                                                       placeholder="Orden"
                                                       value="{{count($assigment->plans)+1}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                                <label>Título</label>
                                                <input type="text" name="title" class="form-control border-input"
                                                       placeholder="Ejemplo: Parcial I Bloque 2"
                                                       value="Planeación {{count($assigment->plans)+1}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Parcial</label>
                                                <select name="partial" class="form-control border-input">
                                                    <option value="" {{old('partial')==''?"selected":""}}>Seleccione
                                                    </option>
                                                    <option {{old('partial')=='1'? 'selected': ''}} value="1">Parcial
                                                        I
                                                    </option>
                                                    <option {{old('partial')=='2'? 'selected': ''}} value="2">Parcial
                                                        II
                                                    </option>
                                                    <option {{old('partial')=='3'? 'selected': ''}} value="3">Parcial
                                                        III
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Bloque</label>
                                                <input type="text" name="title" disabled
                                                       class="form-control border-input"
                                                       value="{{$block->description}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Plantel</label>
                                                <input type="text" name="acronym" disabled
                                                       class="form-control border-input"
                                                       placeholder="Abreviatura"
                                                       value="{{$assigment->school->name.' '.$assigment->school->number}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('elaboration') ? ' has-error' : '' }}">
                                                <label>Elaboración</label>
                                                <input type="date" name="elaboration" value="{{date('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Elaboración">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('elaboration') ? ' has-error' : '' }}">
                                                <label>Aplica Desde</label>
                                                <input type="date" name="apply_from" value="{{date('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Elaboración">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('elaboration') ? ' has-error' : '' }}">
                                                <label>Aplica Hasta</label>
                                                <input type="date" name="apply_to" value="{{date('Y-m-d')}}"
                                                       class="form-control border-input"
                                                       placeholder="Elaboración">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Profesor</label>
                                                <input type="text" name="teacher" class="form-control border-input"
                                                       placeholder="Nombre completo de la escuela" disabled
                                                       value="{{$assigment->userfullname}}">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Periodo</label>
                                                <input type="text" name="cycle" class="form-control border-input"
                                                       placeholder="Nombre completo de la escuela" disabled
                                                       value="{{$assigment->cycle}}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Horas</label>
                                                <input type="text" name="hours" class="form-control border-input"
                                                       placeholder="Horas" disabled
                                                       value="">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Eje</label>
                                                <textarea type="text" name="axis"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          disabled
                                                          rows="2">{{$block->axis==""?"N/A":$block->axis}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Componente</label>
                                                <textarea type="text" name="component"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          disabled
                                                          rows="2">{{$block->component==""?"N/A":$block->component}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contenido Central</label>
                                                <textarea type="text" name="centralcontent"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          disabled
                                                          rows="2">{{$block->title==""?"N/A":$block->title}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contenidos Específicos</label>
                                                <textarea type="text" name="specifics"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias"
                                                          rows="3">{{$block->specifics}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Aprendizajes Esperados</label>
                                                <textarea type="text" name="expecteds"
                                                          class="form-control border-input"
                                                          rows="3">{{$block->expecteds}}</textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Proceso de Aprendizaje</label>
                                                <textarea type="text" name="process"
                                                          class="form-control border-input"
                                                          rows="3">{{$block->process}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Productos Esperados</label>
                                                <textarea type="text" name="products"
                                                          class="form-control border-input"
                                                          rows="3">{{$block->products}}</textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Propósito de la planeación didáctica por
                                                    Campo Disciplinar</label>
                                                <textarea type="text" name="purpose_area"
                                                          class="form-control border-input"
                                                          rows="3">{{$block->program->purpose_area}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Propósito de la planeación didáctica por
                                                    asignatura</label>
                                                <textarea type="text" name="purpose"
                                                          class="form-control border-input"
                                                          rows="3">{{$block->program->purpose}}</textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Habilidades Socioemocionales</label>
                                                <select name="hse" class="form-control border-input">
                                                    <option value="" selected="selected">Seleccione</option>
                                                    <option value="1">Conoce T - Autoconocimiento</option>
                                                    <option value="2">Conoce T - Autoregulación</option>
                                                    <option value="3">Relaciona T - Conciencia Social</option>
                                                    <option value="4">Relaciona T - Colaboración</option>
                                                    <option value="5">Elige T - Toma responsable de Decisiones
                                                    </option>
                                                    <option value="6">Elige T - Perseverancia</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <ambit :pprofile_ambits="{{json_encode(old('profile_ambits'))}}"></ambit>


                                </div>
                            </div>
                        </div>


                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseTran">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Transversalidad
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseTran" class="panel-collapse collapse">
                                <div class="panel-body">

                                    @if(!$assigment->program->nme)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('integrator') ? ' has-error' : '' }}">
                                                    <label>Tema Integrador</label>
                                                    <input type="text" name="integrator"
                                                           class="form-control border-input"
                                                           placeholder="Tema que servirá de eje para la transversalidad"
                                                           value="{{old('integrator')}}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('trans_integrator') ? ' has-error' : '' }}">
                                                    <label>Transversalidad Tema Integrador</label>
                                                    <input type="text" name="trans_integrator"
                                                           class="form-control border-input"
                                                           placeholder="Asignaturas y/o módulos que se relacionan al tema integrador"
                                                           value="{{old('trans_integrator')}}">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('trans_course') ? ' has-error' : '' }}">
                                                    <label>Transversalidad de Asignatura</label>
                                                    <input type="text" name="trans_course"
                                                           class="form-control border-input"
                                                           placeholder="Asignaturas y/o módulos que se relacionan a la asignatura"
                                                           value="{{old('trans_course')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox1" name="isspace" type="checkbox">
                                                    <label for="checkbox1">
                                                        Espacio
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox2" name="isenergy" type="checkbox">
                                                    <label for="checkbox2">
                                                        Energía
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox3" name="isdiversity" type="checkbox">
                                                    <label for="checkbox3">
                                                        Diversidad
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox4" name="isthetime" type="checkbox">
                                                    <label for="checkbox4">
                                                        Tiempo
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <input id="checkbox5" name="ismatter" type="checkbox">
                                                    <label for="checkbox5">
                                                        Materia
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('integrator') ? ' has-error' : '' }}">
                                                <label>Transversalidad con otras asignaturas</label>
                                                <textarea type="text" name="transversality"
                                                          class="form-control border-input"
                                                          placeholder="Ejemplo: Geometría Analítica, pendiente o cambio en una función, etc.">{{old('transversality')}}</textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(!$assigment->program->nme)
                            <div class="panel panel-border panel-default">
                                <a data-toggle="collapse" href="#collapseProf">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            Información Adicional
                                            <i class="ti-angle-down"></i>
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapseProf" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('factual') ? ' has-error' : '' }}">
                                                    <label>Contenidos Fácticos</label>
                                                    <textarea name="factual" class="form-control border-input"
                                                              placeholder="Contenidos fácticos de la asignatura o submódulo">{{old('factual')}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('fundamental') ? ' has-error' : '' }}">
                                                    <label>Concepto Fundamental</label>
                                                    <textarea name="fundamental" class="form-control border-input"
                                                              placeholder="Concepto fundamental de la asignatura/submódulo">{{old('fundamental')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('subsidiary') ? ' has-error' : '' }}">
                                                    <label>Conceptos Subsidiarios</label>
                                                    <textarea name="subsidiary" class="form-control border-input"
                                                              placeholder="Contenidos subsidiario de la asignatura o submódulo">{{old('subsidiary')}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group {{ $errors->has('procedimental') ? ' has-error' : '' }}">
                                                    <label>Contenidos Procedimentales</label>
                                                    <textarea name="procedimental" class="form-control border-input"
                                                              placeholder="Contenidos procedimentales de la asignatura o submódulo">{{old('procedimental')}}</textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('attitudinal') ? ' has-error' : '' }}">
                                                    <label>Contenidos Actitudinales</label>
                                                    <textarea name="attitudinal" class="form-control border-input"
                                                              placeholder="Contenidos actitudinales de la asignatura/submódulo">{{old('attitudinal')}}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('profesionalcontent') ? ' has-error' : '' }}">
                                                    <label>Contenidos en competencias profesionales</label>
                                                    <textarea name="profesionalcontent"
                                                              class="form-control border-input"
                                                              placeholder="Contenidos en competencias profesionales">{{old('profesionalcontent')}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseComp">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Competencias
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseComp" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="card">
                                        <hr>
                                        <div class="col-md-12">
                                            <h4 class="card-title">Competencias Genéricas</h4>
                                            <p class="category">Atributos de las Competencias asociadas al bloque</p>
                                        </div>

                                        <div class="card-content">
                                            <div class="card-content table-responsive table-full-width">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <th>Clave</th>
                                                    <th>Número</th>
                                                    <th>Atributo</th>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($block->attributes as $attribute)
                                                        <tr>
                                                            <td>{{$attribute->competence->code}}</td>
                                                            <td>{{$attribute->code}}</td>
                                                            <td>{{$attribute->title}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <hr>
                                        <div class="col-md-12">
                                            <h4 class="card-title">Competencias Disciplinares</h4>
                                            <p class="category">Competencias asociadas al bloque</p>
                                        </div>

                                        <div class="card-content table-responsive table-full-width">
                                            <table class="table table-striped">
                                                <thead>
                                                <th>Clave</th>
                                                <th>Competencia</th>
                                                </thead>
                                                <tbody>
                                                @foreach($block->discompetences as $competence)
                                                    <tr>
                                                        <td>{{$competence->code}}</td>
                                                        <td>{{$competence->title}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="card">
                                        <hr>
                                        <div class="col-md-12">
                                            <h4 class="card-title">Competencias Profesionales</h4>
                                            <p class="category">Competencias asociadas al bloque</p>
                                        </div>

                                        <div class="card-content table-responsive table-full-width">
                                            <table class="table table-striped">
                                                <thead>
                                                <th>Clave</th>
                                                <th>Competencia</th>
                                                </thead>
                                                <tbody>
                                                @foreach($block->profcompetences as $competence)
                                                    <tr>
                                                        <td>{{$competence->code}}</td>
                                                        <td>{{$competence->title}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseSrc">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Referencias
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseSrc" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Referencias Bibliográficas</label>
                                                <textarea type="text" name="references"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí las referencias Bibliográficas"
                                                          rows="5"
                                                          value="{{old('references')}}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Otras referencias</label>
                                                <textarea type="text" name="extrareferences"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí otras referencias como páginas de Internet"
                                                          rows="5"
                                                          value="{{old('extrareferences')}}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Colaboradores</label>
                                                <textarea type="text" name="collaborations"
                                                          class="form-control border-input"
                                                          placeholder="Indique aquí a compañeros colaboradores" rows="5"
                                                          value="{{old('collaborations')}}"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-border panel-default">
                            <a data-toggle="collapse" href="#collapseResources">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Recursos(Detalle)
                                        <i class="ti-angle-down"></i>
                                    </h4>
                                </div>
                            </a>
                            <div id="collapseResources" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Equipo</label>
                                                <textarea type="text" name="equipment"
                                                          class="form-control border-input"
                                                          placeholder="Describa equipo computacional,  mecánico, eléctrico, etc.">{{old('equipment')}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!$assigment->program->nme)

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Equipo de Bioseguridad</label>
                                                    <textarea type="text" name="biosecurity"
                                                              class="form-control border-input"
                                                              placeholder="Describa si va a utilizar equipo de bioseguridad y sus características">{{old('biosecurity')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Reactivos</label>
                                                    <textarea type="text" name="reactives"
                                                              class="form-control border-input"
                                                              placeholder="Describa reactivos">{{old('reactives')}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Material</label>
                                                <textarea type="text" name="material"
                                                          class="form-control border-input"
                                                          placeholder="Describa material para desarrollar la clase">{{old('material')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button" onclick="location.href='{{ $assigment->url }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
