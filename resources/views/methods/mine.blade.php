@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('methods.mine')}}">Mis Estrategias </a>


@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-4">

                            <h4 class="card-title">Estrategias</h4>
                            <p class="category">De Enseñanza-Aprendizaje</p>
                        </div>

                        <div class="col-md-8 ">
                            <button onclick="location.href='{{ route('methods.addmine') }}'" type="button"
                                 title="Agregar" class="btn btn-sm btn-success btn-icon"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>Nombre</th>
                            <th>Resumen</th>

                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($methods as $method)
                                <tr>
                                    <td>{{$method->title}}</td>
                                    <td>{{$method->summary}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a onclick="location.href='{{ route('methods.editmine',['id'=>$method->id]) }}'"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-warning btn-icon"><i
                                                            class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-md-4">
                                                @include('buttons.delete',['route'=>'methods.deletemine','id'=>$method->id])
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$methods->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
