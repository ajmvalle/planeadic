@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('methods.index')}}">Estrategias Generales</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Estrategias</h4>
                        <p class="category">Aquí las estrategias que puedes agregar a tu planeación</p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                </div>

                @if(count($methods)>0)

                    <div class="card col-md-8 col-md-offset-2">
                        <div class="card-content ">

                            <div class="panel card grid-bx panel-info">
                                <div class="panel-body">
                                    <div id="myCarousel" class="carousel slide"
                                         data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            @foreach($methods as $key => $method)
                                                <div class="item {{ $key == 0 ? ' active' : '' }}">
                                                    <div class="row">

                                                        <div class="col-sm-6 d-md-table-cell"><img
                                                                    src="{{asset('/uploads/methods/'.$method->image)}}"
                                                                    height="250" width="250"
                                                                    alt="{{$method->summary}}"></div>
                                                        <div class="col-sm-6 d-md-table-cell">
                                                            <strong>{{$method->title}}:</strong>
                                                            <br>
                                                            <p>{{$method->summary}}</p>
                                                            <br>
                                                            <small>{{$method->reference}}</small>
                                                        </div>

                                                    </div>

                                                </div>

                                            @endforeach
                                        </div>

                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="ti-arrow-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="pull-right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="ti-arrow-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endif


            </div>


        </div>
    </div>




@endsection
