@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('methods.mine')}}">Mis Estrategias </a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Estrategias</h4>
                        <p class="category">Dar de alta una estrategia de Enseñanza-Aprendizaje</p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'methods.storemine','enctype'=>'multipart/form-data']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Título</label>
                                    <input type="text" name="title" class="form-control border-input"
                                           placeholder="Titule la estrategiao técnica">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Resumen</label>
                                    <input type="text" name="summary" class="form-control border-input"
                                           placeholder="Describa brevemente el objetivo de esta estrategia">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Docente</label>
                                    <textarea name="dev[teacher]" class="form-control border-input" rows="4"
                                              placeholder="Actividades del docente">{{old('dev[teacher]')}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Alumno</label>
                                    <textarea name="dev[student]" class="form-control border-input" rows="4"
                                              placeholder="Actividades del alumno">{{old('dev[student]')}}</textarea>
                                </div>
                            </div>

                        </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        <label>Tiempo</label>
                                        <input type="text" name="dev[time]" class="form-control border-input" value="0"
                                               placeholder="Tiempo estimado en Horas">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        <label>Producto</label>
                                        <input type="text" name="dev[product]" class="form-control border-input"
                                               placeholder="Productos esperados">
                                    </div>
                                </div>

                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Referencias Pedagógicas</label>
                                    <textarea name="reference" class="form-control border-input" size="3"
                                              placeholder="Fundamentos pedagógicos aplicados">
                                        </textarea>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>
                            <button type="button" onclick="location.href='{{ route('methods.mine') }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
