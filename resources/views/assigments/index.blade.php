@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('assigments.index')}}">
        Mis Portafolios
    </a>

@endsection

@section('content')


    <div class="card-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li> {{$errors->first()}} </li>
                                </ul>
                            </div>
                        @endif
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4 class="card-title">Portafolios</h4>
                                <p class="category">Ingrese o modifique su carga y comience el proceso de planeación</p>
                            </div>

                            <div class="col-md-6 ">
                                <button onclick="location.href='{{ route('assigments.add') }}'" type="button"
                                        title="Agregar" class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>

                        </div>

                        <div class="card-content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <th>Nombre</th>
                                <th>Ciclo</th>
                                <th>Programa</th>
                                <th>Planeaciones</th>
                                <th>Opciones</th>
                                </thead>
                                <tbody>
                                @foreach($assigments as $assigment)
                                    <tr>
                                        <td>{{$assigment->name}}</td>
                                        <td>{{$assigment->cycle}}</td>
                                        <td>{{empty($assigment->program_id) ? "" : $assigment->program->name}}</td>
                                        <td>{{count($assigment->plans)}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <a onclick="location.href='{{ $assigment->url }}'"
                                                       title="Planificar" type="button"
                                                       class="btn btn-sm btn-primary btn-icon"><i
                                                                class="fa fa-calendar"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    @include('buttons.copyassigment',['route'=>'assigments.copy','id'=>$assigment->id])
                                                </div>
                                                <div class="col-md-3">
                                                    <a onclick="location.href='{{ route('assigments.edit',['id'=>$assigment->id]) }}'"
                                                       title="Editar" type="button"
                                                       class="btn btn-sm btn-warning btn-icon"><i
                                                                class="fa fa-edit"></i></a>
                                                </div>
                                                <div class="col-md-3">
                                                    @include('buttons.delete',['route'=>'assigments.delete','id'=>$assigment->id])
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$assigments->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
