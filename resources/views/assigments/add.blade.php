@extends('layouts.app')

@section('title')

    @isset($group)

        <a class="navbar-brand" href="{{route('groups.show', [$group->id, "grupo_colegiad"])}}">
           {{$group->title}}
        </a>
        @else

        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>
    @endisset


@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Portafolios</h4>
                        <p class="category">Dar de alta Portafolio
                            @isset($group)
                                <strong>
                                    {{$group->title}}
                                </strong>
                            @endisset
                        </p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'assigments.store']) !!}
                        @isset($group)
                            {!! Form::hidden('group_id',$group->id) !!}
                        @endisset
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control border-input "
                                           value="{{ old('name') }}"
                                           placeholder="Nombre para identificar la asignación de su materia">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('cycle') ? ' has-error' : '' }}">
                                    <label>Ciclo o Periodo de Aplicación</label>
                                    <input type="text" name="cycle" class="form-control border-input"
                                           value="{{ old('cycle') }}"
                                           placeholder="Ciclo o periodo de aplicación">
                                </div>
                            </div>

                            <div class="col-md-4">
                                {!! Form::Label('format_id', 'Formato:') !!}
                                {!! Form::select('format_id', $formats, 0, ['class' => 'form-control border-input']) !!}
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('school_id') ? ' has-error' : '' }}">
                                    {!! Form::Label('item', 'Escuela:') !!}
                                    {!! Form::select('school_id', $schools, $user->school_id, ['class' => 'form-control border-input']) !!}
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('careers') ? ' has-error' : '' }}">
                                    <label>Carrera(s)</label>
                                    <input type="text" name="careers" class="form-control border-input"
                                           value="{{old('careers')}}"
                                           placeholder="Carreras o especialidadades de los grupos">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('groups') ? ' has-error' : '' }}">
                                    <label>Grupo(s)</label>
                                    <input type="text" name="groups" class="form-control border-input"
                                           value="{{old('groups')}}"
                                           placeholder="Grupos">
                                </div>
                            </div>
                        </div>

                        <program :specialty="{{json_encode(new \App\Specialty())}}"
                                 :program="{{json_encode(new \App\Program())}}"
                                 :course="{{json_encode(new \App\Course())}}"></program>

                        <div class="row">
                            <div class="col-md-12">
                                <label>Contexto</label>
                                <textarea name="context" class="form-control border-input" rows="3"
                                          placeholder="Contexto Escolar">{{auth()->user()->context}}</textarea>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Agregar</button>

                            <button type="button"
                                    onclick="location.href='{{ is_null($group)?route('assigments.index'):route('groups.show', [$group->id, "grupo_colegiad"]) }}'"
                                    class="btn btn-success btn-fill btn-wd">Cancelar
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
