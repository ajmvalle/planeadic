@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Mantenimiento del sistema</a>

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Portafolios</h4>
                        <p class="category">Modificar datos generales de Portafolio
                            @isset($assigment->group)
                                <strong>
                                     de
                                    {{$assigment->group->title}}
                                </strong>
                            @endisset
                        </p>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            {!! Form::open(['method'=> 'POST','route'=>'assigments.update']) !!}
                            {!! Form::hidden('id',$assigment->id) !!}
                            {!! Form::hidden('group_id',$assigment->group_id) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="name" class="form-control border-input"
                                               value="{{$assigment->name}}"
                                               placeholder="Nombre para identificar la asignación de su materia">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ciclo o Periodo de Aplicación</label>
                                        <input type="text" name="cycle" class="form-control border-input"
                                               value="{{$assigment->cycle}}"
                                               placeholder="Ciclo o periodo de aplicación">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    {!! Form::Label('format_id', 'Formato:') !!}
                                    {!! Form::select('format_id', $formats, $assigment->format_id, ['class' => 'form-control border-input']) !!}
                                </div>

                            </div>



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('school_id') ? ' has-error' : '' }}">
                                        {!! Form::Label('item', 'Escuela:') !!}
                                        {!! Form::select('school_id', $schools, $assigment->school_id, ['class' => 'form-control border-input']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Carrera(s)</label>
                                        <input type="text" name="careers" class="form-control border-input"
                                               value="{{$assigment->careers}}"
                                               placeholder="Carreras o especialidadades de los grupos">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Grupos(s)</label>
                                        <input type="text" name="groups" class="form-control border-input"
                                               value="{{$assigment->groups}}"
                                               placeholder="Grupos">
                                    </div>
                                </div>
                            </div>


                            <program :specialty="{{is_null($assigment->program->course->specialty)? json_encode(new \App\Specialty()) : json_encode($assigment->program->course->specialty)}}"
                                     :program="{{json_encode($assigment->program)}}"
                                     :course="{{json_encode($assigment->program->course)}}"></program>


                            <div class="row">
                                <div class="col-md-12">
                                    <label>Contexto</label>
                                    <textarea  name="context" class="form-control border-input" rows="3"
                                               placeholder="Contexto Escolar">{{$assigment->context}}</textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">Editar</button>
                                <button type="button" onclick="location.href='{{ is_null($assigment->group_id)?route('assigments.index'):route('groups.show', [$assigment->group_id, "grupo_colegiad"]) }}'"
                                        class="btn btn-success btn-fill btn-wd">Cancelar
                                </button>
                            </div>
                            <div class="clearfix"></div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
