@extends('layouts.primary')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="texto01" align="center">AVISO DE PRIVACIDAD</h2>
                    <p class="texto01" data-original-title="" title=""><strong data-original-title=""
                                                                               title="">Planeadic.com </strong>,
                        con ubicación en el Municipio de Medellín de Bravo, Veracruz México, es el
                        responsable del uso y protección de sus datos personales, y al respecto le informamos lo
                        siguiente:</p>
                    <h4 class="titulo">¿Para qué fines utilizaremos sus datos personales?</h4>
                    <p class="texto01" data-original-title="" title="">Los datos personales que recabamos de usted, los
                        utilizaremos
                        para las siguientes finalidades que son necesarias para el servicio que solicita: </p>
                    <ul>
                        <li class="texto01"><strong data-original-title="" title=""><em>Llenar la información de las
                                    planeaciones
                                    didácticas que el usuario elabora en el sitio denominado PLANEADIC</em></strong>
                        </li>
                    </ul>
                    <p class="texto01" data-original-title="" title="">De manera adicional, utilizaremos su información
                        personal
                        para
                        las siguientes finalidades que <strong data-original-title="" title="">no son
                            necesarias</strong> para
                        el
                        servicio
                        solicitado, pero que nos permiten y facilitan brindarle una mejor atención: </p>
                    <ul>
                        <li class="texto01"><strong data-original-title="" title=""><em>Conocer las necesidades de
                                    formación
                                    docente</em></strong></li>
                    </ul>
                    <p class="texto01" data-original-title="" title="">En caso de que no desee que sus datos personales
                        sean
                        tratados
                        para estos fines adicionales, desde este momento usted nos puede comunicar lo anterior, con un
                        mensaje a
                        través
                        del formulario que se encuentra en <strong
                                data-original-title=""
                                title=""><a href="http://www.planeadic.com">
                                http://www.planeadic.com</a></strong></p>
                    <p class="texto01" data-original-title="" title="">La negativa para el uso de sus datos personales
                        para
                        estas
                        finalidades no podrá ser un motivo para que le neguemos los servicios y productos que solicita o
                        contrata con
                        nosotros.</p>
                    <h4 class="titulo">¿Qué datos personales utilizaremos para estos fines?</h4>
                    <p class="texto01" data-original-title="" title="">Para llevar a cabo las finalidades descritas en
                        el
                        presente aviso
                        de privacidad, utilizaremos los siguientes datos personales:</p>
                    <ul>
                        <li>Nombre Completo</li>
                        <li>Dirección de Correo Electrónico</li>
                        <li>Plantel e Institución donde presta servicios</li>
                        <li>Registro Federal de Contribuyentes</li>
                        <li>Teléfono</li>
                        <li>Dirección</li>
                    </ul>
                    <p class="texto01" data-original-title="" title="">Además de los datos personales mencionados
                        anteriormente,
                        para
                        las finalidades informadas en el presente aviso de privacidad NO utilizamos datos sensibles, que
                        requieren de
                        especial protección. </p>
                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">¿Con
                            quién
                            compartimos su
                            información personal y para qué fines?</strong><br>
                        Le informamos que sus datos personales NO son compartidos dentro y fuera del país.</p>


                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">¿Cómo
                            puede
                            acceder,
                            rectificar o cancelar sus datos personales, u oponerse a su uso?</strong><br>
                        Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y
                        las
                        condiciones
                        del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su
                        información
                        personal en
                        caso
                        de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de
                        nuestros
                        registros o
                        bases de datos cuando considere que la misma no está siendo utilizada conforme a los principios,
                        deberes
                        y
                        obligaciones previstas en la normativa (Cancelación); así como oponerse al uso de sus datos
                        personales
                        para
                        fines específicos
                        (Oposición). Estos derechos se conocen como derechos ARCO.</p>
                    <p class="texto01" data-original-title="" title="">Para el ejercicio de cualquiera de los derechos
                        ARCO,
                        usted
                        deberá presentar la solicitud respectiva en <strong data-original-title="" title=""><em><a
                                        href="http://www.planeadic.com">
                                    http://www.planeadic.com</a> </em></strong></p>
                    <p class="texto01" data-original-title="" title="">Para conocer el procedimiento y requisitos para
                        el
                        ejercicio de
                        los derechos ARCO, usted podrá un mensaje en el formulario indicado, a través de ese medio, se
                        atenderá
                        cualquier duda que pudiera tener respecto al
                        tratamiento de su información. </p>
                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">¿Cómo
                            puede
                            revocar su
                            consentimiento para el uso de sus datos personales?</strong><br>
                        Usted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de
                        sus
                        datos
                        personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos
                        atender su
                        solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación
                        legal
                        requiramos
                        seguir tratando
                        sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de
                        su
                        consentimiento
                        implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su
                        relación con
                        nosotros. </p>
                    <p class="texto01" data-original-title="" title="">Para revocar su consentimiento deberá presentar
                        su
                        solicitud en
                        <strong data-original-title="" title=""><em><a
                                        href="http://www.planeadic.com">
                                    http://www.planeadic.com</a> </em></strong></p>

                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">¿Cómo
                            puede
                            limitar el
                            uso o divulgación de su información personal? </strong><br>El administrador de Planeadic no compartirá su
                        información para
                        fines mercadológicos con otras entidades, en dado caso será modificado el presente aviso de
                        privacidad y
                        será
                        notificado a través de correo electrónico cualquier modificación.
                    </p>

                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">El uso de
                            tecnologías de
                            rastreo en nuestro portal de Internet</strong><br>
                        Le informamos que en nuestra página de Internet utilizamos <em>cookies</em>, <em>web
                            beacons</em> y
                        otras
                        tecnologías a través de las cuales es posible monitorear su comportamiento como usuario de
                        Internet, así
                        como
                        brindarle un mejor servicio y experiencia de usuario al navegar en nuestra página. </p>
                    <p class="texto01" data-original-title="" title="">Le informamos que por el momento PLANEADIC no
                        utiliza
                        información
                        personal para este efecto</p>
                    <p class="texto01" data-original-title="" title=""><strong data-original-title="" title="">¿Cómo
                            puede
                            conocer los
                            cambios a este aviso de privacidad?</strong><br>
                        El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas
                        de
                        nuevos
                        requerimientos legales; de nuestras propias necesidades por los productos o servicios que
                        ofrecemos; de
                        nuestras
                        prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas. </p>
                    <p class="texto01" data-original-title="" title="">Nos comprometemos a mantenerlo informado sobre
                        los
                        cambios que
                        pueda sufrir el presente aviso de privacidad, a través de correo electrónico</p>
                    <p class="texto01" data-original-title="" title="">El procedimiento a través del cual se llevarán a
                        cabo las
                        notificaciones sobre cambios o actualizaciones al presente aviso de privacidad es el siguiente:
                        <strong
                                data-original-title="" title="">Recibirá un correo electrónico con la liga a este mismo
                            sitio
                            informando
                            de la existencia de cambio en el aviso de privacidad</strong></p>
                    <p data-original-title="" title="" align="right">&nbsp;</p>
                    <p data-original-title="" title="" align="right"><span
                                class="texto01">Última   actualización <strong
                                    data-original-title="" title="">[<em>15/Mayo/2018</em>]</strong></span><strong
                                data-original-title=""
                                title="">.</strong>
                    </p></td>
                    </tr>
                    </tbody></table>
                    <p class="texto01" data-original-title="" title="">&nbsp;</p>

                </div>

            </div>
        </div>
    </div>
@endsection
