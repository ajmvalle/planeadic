@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Panel de Supervisión</a>

@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card ">
                    @each('supervise.teacher',$teachers,'teacher')
                </div>
            </div>
        </div>
    </div>
@endsection