<div class="panel panel-border panel-default">
    <a data-toggle="collapse" href="#collapse{{$teacher->id}}">
        <div class="panel-heading">
            <h4 class="panel-title">
                {{$teacher->fullname}}
                <i class="ti-angle-down"></i>
            </h4>
        </div>
    </a>
    <div id="collapse{{$teacher->id}}" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="row">

                @if(count($teacher->assigments)>0)

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>
                            <th>Portafolio</th>
                            <th>Ciclo</th>
                            <th>Programa</th>
                            <th>Avance</th>
                            </thead>
                            <tbody>
                            @foreach($teacher->assigments as $indexKey => $assigment)
                                <tr>
                                    <td>{{$assigment->name}}</td>
                                    <td>{{$assigment->cycle}}</td>
                                    <td>{{empty($assigment->program_id) ? "" : $assigment->program->name}}</td>
                                    <td>
                                    <div class="col-md-12">
                                        <p class="category"><strong>Horas Planeadas {{$assigment->hours}}
                                                de {{$assigment->program->hours}} ({{$assigment->advance}}%)</strong></p>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar"
                                                 aria-valuenow="{{$assigment->advance}}" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$assigment->advance}}%;">
                                            </div>
                                        </div>
                                    </div>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                @endif

            </div>
        </div>
    </div>

</div>