@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('profile.show')}}">Mi perfil</a>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{asset('img/background2.jpg')}}" alt="..."/>
                    </div>
                    <div class="card-content">
                        <div class="author">
                            <a href="{{$user->imagen}}"> <img class="avatar border-white" src="{{$user->imagen}}"
                                                              alt="..."/></a>
                            <h4 class="card-title">{{auth()->user()->name}} </h4>
                            <h5 class="title">{{auth()->user()->email}} </h5>
                        </div>
                        <form enctype="multipart/form-data" action="{{route('profile.avatar')}}" method="POST">
                            <div class="h-100 d-inline-blocks">
                                <label class="fileContainer center-block text-center">
                                    Toca <strong>aquí</strong> para cambiar Imagen
                                    <input name="avatar" type="file"/>
                                </label>
                            </div>
                            <input class="text-center" type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="text-center">

                                <input type="submit" value="Confirmar"
                                       class="btn btn-primary btn-sm btn-wd">
                                </input>

                                <a type="button"

                                   onclick="return confirm('¿Borrar imagen de perfil?')"
                                   href="{{route('profile.delavatar')}}"

                                   class="btn btn-danger btn-sm btn-wd">Eliminar
                                </a>
                            </div>

                        </form>

                    </div>
                    <hr>
                    <div class="text-center">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <h5>{{auth()->user()->TotalEcas}}<br/>
                                    <small>Planeaciones</small>
                                </h5>
                            </div>
                            <div class="col-md-5">
                                <h5>{{auth()->user()->created_at->format('d/m/y')}}<br/>
                                    <small>Ingreso</small>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">

                    <div class="card-header">
                        <h4 class="card-title">Facturación</h4>
                    </div>

                    <div class="card-content">

                        {!! Form::open(['url' => route('profile.tax'), 'method' => 'post']) !!}
                        {!! Form::hidden('id',$user->id) !!}

                        <div class="form-group">
                            <label>RFC</label>
                            <input type="text" name="taxid" class="form-control border-input"
                                   value="{{$user->taxid}}"
                                   placeholder="Ingrese aquí el RFC para facturar">
                        </div>

                        <small>Ingrese el RFC al que desea se facturen sus pagos, en el caso de estar vacío este dato la
                            factura se generará al público en general
                        </small>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Confirmar</button>
                        </div>

                        {!! Form::close() !!}
                    </div>


                </div>

            </div>
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Editar Perfil</h4>
                    </div>
                    <div class="card-content">

                        {!! Form::open(['url' => route('profile.update'), 'method' => 'post']) !!}
                        {!! Form::hidden('id',$user->id) !!}
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group ">
                                    <label>Título</label>
                                    <input type="text" class="form-control border-input"
                                           name="title" value="{{$user->title}}"
                                           placeholder="Lic., Ing., etc...">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control border-input" disabled=""
                                           name="name" value="{{$user->name}}"
                                           placeholder="Lucía, Juan Carlos, etc.">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Apellidos</label>
                                    <input type="text" class="form-control border-input" disabled=""
                                           name="lastname" value="{{$user->lastname}}"
                                           placeholder="Pérez, Hernández, etc.">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Carrera o Profesión</label>
                                    <input type="text" class="form-control border-input"
                                           name="profession"
                                           placeholder="Licenciado en Ciencias de la Educación"
                                           value="{{$user->profession}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control border-input"
                                           name="address"
                                           placeholder="Av. de las Américas, Fracc. Vista Hermosa"
                                           value="{{$user->address}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Teléfono</label>
                                    <input type="text" class="form-control border-input" placeholder="2299123456"
                                           name="phone"
                                           value="{{$user->phone}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <br>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <input type="radio" name="sex" id="radio1"
                                               value="male" {{($user->sex!="female") ?"checked":""}}>
                                        <label for="radio1">
                                            Hombre
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="radio">
                                        <input type="radio" name="sex" id="radio2"
                                               value="female" {{($user->sex=="female") ?"checked":""}} >
                                        <label for="radio2">
                                            Mujer
                                        </label>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::Label('item', 'Escuela:') !!}
                                    @if($user->principal)
                                        {!! Form::select('school_id', $schools, $user->school_id, ['class' => 'form-control border-input','disabled'=>'']) !!}
                                    @else
                                        {!! Form::select('school_id', $schools, $user->school_id, ['class' => 'form-control border-input']) !!}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Contexto Social y Escolar</label>
                                    <textarea rows="3" class="form-control border-input" name="context"
                                              placeholder="Puedes describir aquí el contexto de tu entorno social y escolar"
                                              value="Mike">{{$user->context}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Presidente de Academia</label>
                                    <input type="text" class="form-control border-input"
                                           name="academychief"
                                           placeholder="Ej. Ing. Pedro Pérez"
                                           value="{{$user->academychief}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jefe de Serv. Docentes</label>
                                    <input type="text" class="form-control border-input"
                                           name="teacherschief"
                                           placeholder="Ej. Loc.. Martín Martínez"
                                           value="{{$user->teacherschief}}">
                                </div>
                            </div>
                        </div>

                        <div class=" checkbox">

                            <input id="checkbox2" name="bauthlogo"
                                   type="checkbox" {{$user->authlogo? "checked":""}} >
                            <label for="checkbox2">
                                <strong>Soy Servidor Público,</strong> autorizo y requiero el uso de logotipos oficiales
                                en mis planeaciones.
                            </label>
                        </div>

                        <div class=" checkbox">

                            <input id="checkbox2" name="bprincipal" disabled=""
                                   type="checkbox" {{$user->principal? "checked":""}} >
                            <label for="checkbox2">
                                <strong>Soy Director de plantel,</strong> (Activar mediante correo a buzonadmin@planeadic.com).
                            </label>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">Actualizar datos</button>
                        </div>
                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>


        </div>
    </div>
    </div>

@endsection
