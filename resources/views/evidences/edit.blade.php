@extends('layouts.app')

@section('title')
    @if(!$evidence->plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$evidence->plan->assigment->url}}">
            {{$evidence->plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$evidence->plan->assigment->group->url}}">
            {{$evidence->plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$evidence->plan->assigment->url}}">
            {{$evidence->plan->assigment->name}}
        </a>
    @endif
    <a class="navbar-brand">/</a>
    <a class="navbar-brand" href="{{route('evidences.index',$evidence->plan)}}">
        Evidencias
    </a>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edición de Evidencia</h4>
                    </div>
                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Lo sentimos!</strong> Existen valores necesarios para completar su petición.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['method'=> 'POST','route'=>'evidences.update','enctype'=>'multipart/form-data']) !!}

                        {{Form::hidden('id',$evidence->id)}}
                        {{Form::hidden('plan_id',$evidence->plan_id)}}


                        <div id="collapseGen" class="panel-collapse ">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-9 {{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Descripción</label>
                                        <input type="text" name="description" class="form-control border-input"
                                               value="{{$evidence->description}}"
                                               placeholder="Pequeña descripción para la relatoría">
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Fecha</label>
                                            <input type="date" name="elaboration"
                                                   class="form-control border-input"
                                                   value="{{ \Carbon\Carbon::createFromDate($evidence->elaboration->year,$evidence->elaboration->month,$evidence->elaboration->day)->format('Y-m-d')}}"
                                                   placeholder="Fecha de Evidencia">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-3 {{ $errors->has('kind') ? ' has-error' : '' }}">
                                        <label>Tipo de Evidencia</label>
                                        <select name="kind" class="form-control border-input">
                                            <option value="" {{!$evidence->kind ? 'selected="selected"': "" }}>
                                                Seleccione
                                            </option>
                                            <option value="1" {{$evidence->kind==1 ? 'selected="selected"': "" }}>De
                                                Clase
                                            </option>
                                            <option value="2" {{$evidence->kind==2 ? 'selected="selected"': "" }}>De
                                                evaluación
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::Label('activity_id', 'Producto de Actividad:') !!}
                                        {!! Form::select('activity_id', $activities, $evidence->activity_id, ['class' => 'form-control border-input']) !!}
                                    </div>

                                </div>

                                <div class="row text-center">
                                    <img src="{{asset('/uploads/evidence/'.$evidence->image)}}">

                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <label for="file-upload" class="custom-file-upload">
                                            <i class="fa fa-cloud-upload"></i> Subir Imagen
                                        </label>
                                        <input id="file-upload" name='imagen' type="file" style="display:none;">
                                    </div>
                                </div>


                                <div class="row">

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Guardar</button>
                                        <button type="button"
                                                onclick="location.href='{{ route('evidences.index',$evidence->plan) }}'"
                                                class="btn btn-success btn-fill btn-wd">Cancelar
                                        </button>
                                    </div>


                                    <div class="clearfix"></div>


                                </div>


                            </div>

                        </div>
                        {!! Form::close() !!}

                    </div>


                </div>
            </div>
        </div>


    </div>


@endsection
