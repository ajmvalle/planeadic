@extends('layouts.app')

@section('title')
    @if(!$plan->assigment->isGrupal)
        <a class="navbar-brand" href="{{route('assigments.index')}}">
            Mis Portafolios
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>

    @else

        <a class="navbar-brand" href="{{$plan->assigment->group->url}}">
            {{$plan->assigment->group->title}}
        </a>

        <a class="navbar-brand">/</a>

        <a class="navbar-brand" href="{{$plan->assigment->url}}">
            {{$plan->assigment->name}}
        </a>
    @endif

@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content ">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Propósito de la planeación</h4>
                            </div>
                            <div class="col-md-1">
                                <a
                                        href="{{route('assigments.dashboard',$plan->assigment)}}"
                                        title="Ir a Plan" type="button"
                                        class="btn btn-sm btn-success btn-icon"><i
                                            class="fa fa-calendar"></i></a>
                            </div>

                            <div class="col-md-7">
                                <a target="_blank"
                                   href="{{route('plans.print',$plan)}}"
                                   title="Imprimir" type="button"
                                   class="btn btn-sm btn-warning btn-icon"><i
                                            class="fa fa-print"></i></a>
                            </div>
                        </div>

                        <textarea class="form-control border-input">{{$plan->purpose}}</textarea>
                    </div>

                    <div class="card-header">
                        <div class="col-md-3">
                            <h4 class="card-title">Agregar Evidencia</h4>
                        </div>

                        <div class="col-md-9 ">
                            <button onclick="location.href='{{ route('evidences.add',$plan) }}'"
                                    type="button"
                                    title="Agregar Evidencia" class="btn btn-sm btn-success btn-icon"><i
                                        class="fa fa-plus"></i>
                            </button>

                        </div>
                    </div>

                    <div class="card-content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead>

                            <th>Fecha</th>
                            <th>Descripción</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @foreach($plan->evidences as $evi)
                                <tr>
                                    <td>{{$evi->elaboration->format('d/m/Y')}}</td>
                                    <td>{{$evi->description}}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a href="{{route('evidences.edit',$evi)}}"
                                                   title="Editar" type="button"
                                                   class="btn btn-sm btn-success btn-icon"><i
                                                            class="fa fa-pencil"></i></a>
                                            </div>
                                            <div class="col-md-3">
                                                @include('buttons.delete',['route'=>'evidences.delete','id'=>$evi->id])
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>


                @if(count($plan->evidences)>0)
                    <div class="card col-md-6 col-md-offset-3">
                        <div class="card-content ">

                            <div class="panel card grid-bx panel-info">
                                <div class="panel-body">
                                    <div id="myCarousel" class="carousel slide"
                                         data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0"
                                                class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner" role="listbox">
                                            @foreach($plan->evidences as $key => $evi)
                                                <div class="item {{ $key == 0 ? ' active' : '' }}">
                                                    <img src="{{asset('/uploads/evidence/'.$evi->image)}}"
                                                         alt="{{$evi->description}}">
                                                    <div class="carousel-caption">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="ti-arrow-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="pull-right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="ti-arrow-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endif


            </div>
        </div>
    </div>




@endsection
