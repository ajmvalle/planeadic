@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">{{$block->program->course->name}} {{$block->program->hours}}
        horas</a>

@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Detalle de Bloque </h4>
                    </div>
                    <div class="card-content">

                        @if($block->program->nme)

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Eje de aprendizaje</label>
                                        <input type="text" name="axis" class="form-control border-input"
                                               value="{{$block->centralcontent->axis}}"
                                               placeholder="Eje de aprendizaje">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Componente</label>
                                        <input type="text" name="component" class="form-control border-input"
                                               value="{{$block->centralcontent->component}}"
                                               placeholder="Componente">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contenido Central</label>
                                        <input type="text" name="title" class="form-control border-input"
                                               value="{{$block->centralcontent->title}}"
                                               placeholder="Texto del Contenido Central">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contenidos Específicos</label>
                                        <textarea size="3" type="text" name="specifics" class="form-control border-input"
                                                  placeholder="Contenidos Específicos">{{$block->centralcontent->specifics}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Aprendizajes Esperados</label>
                                        <textarea size="3" type="text" name="expecteds" class="form-control border-input"
                                                  placeholder="Aprendizajes esperados">{{$block->centralcontent->expecteds}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Procesos de Aprendizaje </label>
                                        <textarea size="3" type="text" name="process" class="form-control border-input"
                                                  placeholder="Procesos de Aprendizaje">{{$block->centralcontent->process}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Productos Esperados</label>
                                        <textarea size="3" type="text" name="products" class="form-control border-input"
                                                  placeholder="Productos Esperados">{{$block->centralcontent->products}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            @else

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Competencia a desarrollar</label>
                                        <input type="text" name="competence" class="form-control border-input"
                                               value="{{$block->competence}}"
                                               placeholder="Competencia Profesional">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Situaciones</label>
                                        <textarea type="text" name="situations" class="form-control border-input"
                                                  placeholder="Situaciones (Componente profesional)">{{$block->situations}}</textarea>
                                    </div>
                                </div>
                            </div>


                        @endif



                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <hr>
            <div class="col-md-12">
                <h4 class="card-title">Competencias</h4>
                <p class="category">Competencias asociadas al bloque</p>
            </div>


            <div class="card-content">
                <div class="card-content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <th>Clave</th>
                        <th>Competencia</th>
                        </thead>
                        <tbody>
                        @foreach($block->competences as $competence)
                            <tr>
                                <td>{{$competence->code}}</td>
                                <td>{{$competence->title}}</td>
                           
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="card">
            <hr>
            <div class="col-md-12">
                <h4 class="card-title">Atributos</h4>
                <p class="category">Atributos de las Competencias asociadas al bloque</p>
            </div>


            <div class="card-content">
                <div class="card-content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <th>Clave</th>
                        <th>Atributo</th>
                        </thead>
                        <tbody>
                        @foreach($block->attributes as $attribute)
                            <tr>
                                <td>{{$attribute->competence->code}}</td>
                                <td>{{$attribute->title}}</td>
                              
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>


@endsection
