@extends('layouts.app')

@section('title')
    <a class="navbar-brand" href="{{route('home')}}">Búsqueda de Planeaciones</a>

@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-content">
                        <div id="chartHours" class="ct-chart"></div>
                        <div class="footer">
                            <hr>
                            <div class="stats">
                                <i class="ti-reload"></i> Búsqueda de Planeaciones Didácticas
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
